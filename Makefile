SRCROOT = $(CURDIR)
include Makefile.common

build:
	$(MAKE) -C src $@

po:
	$(MAKE) -C po

# The non source files that should get installed
INSTALLFILES = colour help login.banner scripthelp talkhelp wizhelp COPYING INSTALL LICENSE README
install-home:
	install -d $(DESTDIR)$(libdir)/mw
	cp -a $(INSTALLFILES) $(DESTDIR)$(libdir)/mw/

ifeq ($(GITVER),)
# These rules can only be called from inside an exported tree
install: install-home
	install -d $(DESTDIR)$(initddir)
	install mwserv.init $(DESTDIR)$(initddir)/mwserv
	$(MAKE) -C src $@
	$(MAKE) -C po $@

deb:
	cp -a debian-template debian
	cp mw.service debian/mw3.service
	dch --create --package mw3 -v $(VERSION) -c debian/changelog "Build mw3 $(VERSION) package"
	debuild -e VERSION=$(VERSION) -us -uc -b

tarball:
	tar zchvf ../$(MWVERSION).tar.gz -C .. $(MWVERSION)

rpm: tarball
	rpmbuild -ta ../$(MWVERSION).tar.gz

else
# These rules can only be called from an svn working directory
ifdef TMPDIR
# A temp dir exists so svn export and palm off the invocation to the exported makefile
export:
	rm -rf $(TMPDIR)/$(MWVERSION)
	git checkout-index -a -f --prefix=$(TMPDIR)/$(MWVERSION)/
	# Store the git hash so we know it in the exported directory
	echo $(GITVER) > $(TMPDIR)/$(MWVERSION)/mw.rev

install tarball rpm: export
	$(MAKE) -C $(TMPDIR)/$(MWVERSION) $@
	rm -rf $(TMPDIR)/$(MWVERSION)

deb: export
	$(MAKE) -C $(TMPDIR)/$(MWVERSION) $@
	mv $(TMPDIR)/mw3_$(VERSION)*.deb $(CURDIR)/
	mv $(TMPDIR)/mw3-dbg_$(VERSION)*.deb $(CURDIR)/
	rm -rf $(TMPDIR)/$(MWVERSION)
else
# A temp dir doesn't exist so create it and invoke this makefile again
install deb export tarball rpm:
	$(MAKE) TMPDIR=`mktemp -d` $@

endif
endif

clean:
	$(MAKE) -C src $@
	$(MAKE) -C duktape $@
	$(MAKE) -C po $@

version:
	@echo $(VERSION)

.PHONY: build clean rpm deb tarball export install-home install version
