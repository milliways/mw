#ifndef IPC_H
#define IPC_H

#include <unistd.h>
#include "user.h"
#include "socket.h"

#define FCC(ch4) ((((uint32_t)(ch4) & 0xFF) << 24) |     \
		  (((uint32_t)(ch4) & 0xFF00) << 8) |    \
		  (((uint32_t)(ch4) & 0xFF0000) >> 8) |  \
		  (((uint32_t)(ch4) & 0xFF000000) >> 24))

/* this needs gcc 4.6 or above to work */
#if defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6))
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmultichar"
#endif

enum ipc_types {
	IPC_NOOP	= 0,	// Just go through the motions, don't actually write anything to the FIFO
	IPC_NEWMAIL	= 1,
	IPC_STATUS	= 2,
	IPC_GROUPS	= 3,
	IPC_REALNAME	= 4,
	IPC_CONTACT	= 5,
	IPC_LASTREAD	= 6,
	IPC_SPECIAL	= 7,
	IPC_TIMEOUT	= 8,
	IPC_USERNAME	= 9,
	IPC_TEXT	= 11,
	IPC_CHATPRIVS	= 12,
	IPC_CHATMODE	= 13,
	IPC_KICK	= 14,
	IPC_PASSWD	= 15,
	IPC_CHANNEL	= 16,
	IPC_WIZ		= 17,
	IPC_GAG		= 18,
	IPC_GAGLIST	= 19,
	IPC_SCRIPTIPC	= 21,
	IPC_SCRIPTRPC	= 22,
	IPC_CHECKONOFF	= 23,
	IPC_PROTLEVEL	= 24,
	IPC_PROTPOWER	= 25,
	IPC_DOING	= 26,
	IPC_ERROR	= FCC('ERRO'),
	IPC_HELLO	= FCC('HELO'),
	IPC_UPTIME	= FCC('UPTM'),
	IPC_SAYTOROOM	= FCC('SAYR'),
	IPC_SAYTOUSER	= FCC('SAYU'),
	IPC_SAYTOALL	= FCC('WALL'),
	IPC_TALKERROR	= FCC('TERR'),
	IPC_REPLAY      = FCC('PLAY'),
	IPC_EVENT	= FCC('EVNT'),
	IPC_ACTION	= FCC('ACTN'),
	IPC_WHOLIST	= FCC('WHOL')
};

enum ipc_errors {
	IPC_ERROR_UNDEFINED,
	IPC_ERROR_NONCE
};

#if defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6))
#pragma GCC diagnostic pop
#endif


int ipc_connect(const char *host, const char *port, struct user *user);
int ipc_connected(void);
void ipc_check(void);
void ipc_close(void);
int ipc_getfd(void);
unsigned int ipc_send_to_username(const char * dest, enum ipc_types msgtype, const char * data);
unsigned int ipc_send_to_all(enum ipc_types msgtype, const char * data);
void ipcmsg_transmit(ipc_message_t * msg);
int msg_queue(ipc_message_t *msg, ipc_connection_t *conn);
const char * ipc_nametype(enum ipc_types msgtype);

const char **plural_suffix;
int plural_suffix_size(void);

#endif
