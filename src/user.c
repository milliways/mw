#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>

#include "user.h"
#include "files.h"
#include "perms.h"

#define USERFILE STATEDIR"/users.bb"

int userdb_open(int flags)
{
	int ufd = open(USERFILE, flags|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP);
	if (ufd < 0)
		perror(USERFILE);
	return ufd;
}

void userdb_write(struct user *user)
{
	int outfile = userdb_open(O_WRONLY|O_APPEND|O_CREAT);
	if (outfile < 0)
		exit(-1);
	Lock_File(outfile);
	write(outfile, &user->record, sizeof(user->record));
	user->posn = (lseek(outfile, 0, SEEK_CUR) - sizeof(user->record));
	Unlock_File(outfile);
	close(outfile);
}

int update_user_fd(int fd, struct user *user)
{
	struct person *rec = &user->record;

	if (pwrite(fd, rec, sizeof(*rec), user->posn) != sizeof(*rec)) {
		perror("Failed to update user");
		return 1;
	}
	return 0;
}

void update_user(struct user *user)
{
	int outfile;

	outfile = userdb_open(O_WRONLY|O_CREAT);
	if (outfile < 0)
		return;
	update_user_fd(outfile, user);
	close(outfile);
}

static int fetch_user_fd(int fd, struct person *rec, off_t offset)
{
	ssize_t bytes = pread(fd, rec, sizeof(*rec), offset);
	if (bytes == 0)
		return 1; /* EOF */

	if (bytes == sizeof(*rec))
		return 0; /* Done */

	/* Partial read or error */
	perror("pread");
	return -1;
}

int fetch_user(struct user *user, int32_t userposn)
{
        int outfile = userdb_open(O_RDWR|O_CREAT);
	if (outfile < 0)
		return -1;

	int ret = fetch_user_fd(outfile, &user->record, userposn);
        close(outfile);
	if (ret == 0)
		user->posn = userposn;
	return ret;
}

int fetch_first_user(int fd, struct user *user)
{
	int ret;

	if (user == NULL)
		return -1;

	ret = fetch_user_fd(fd, &user->record, 0);
	if (ret == 0)
		user->posn = 0;
	return ret;
}

int fetch_next_user(int fd, struct user *user)
{
	int ret;

	if (user == NULL)
		return -1;

	ret = fetch_user_fd(fd, &user->record, user->posn + sizeof(user->record));
	if (ret == 0)
		user->posn += sizeof(user->record);
	return ret;
}

static int user_find_name(const char *name, struct user *user, int *found)
{
	struct person *record = &user->record;
	ssize_t bytes;
	int fd = open(USERFILE, O_RDONLY);

	if (fd < 0)
		return 1;

	*found = 0;
	user->posn = 0;
	while((bytes = read(fd, record, sizeof(*record))) == sizeof(*record)) {
		if (bytes != sizeof(*record)) {
			perror(USERFILE);
			close(fd);
			return 1;
		}
		if (!strcasecmp(record->name, name) && !u_del(user)) {
			*found = 1;
			break;
		}
		user->posn += sizeof(*record);
	}
	close(fd);
	return 0;
}

int user_exists(const char *name, struct user *usr)
{
        int found;

	return !user_find_name(name, usr, &found) && found;
}
