#ifndef FILES_H
#define FILES_H

#include <sys/file.h>
#include <errno.h>
#include <stdio.h>

static inline void Lock_File(int f)
{
	errno=0;
	do{
		if (flock(f,LOCK_EX))
			perror("Locking");
	}while (errno==EINTR);
}

static inline void Unlock_File(int f)
{
	if (flock(f,LOCK_UN))
	{
		perror("Unlocking");
	}
}

#endif
