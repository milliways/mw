#include <stdlib.h>
#include <string.h>
#include "sqlite.h"
#include "rooms.h"
#include "util.h"

int isanum(char *a, int *result, int onlydecimal);

void RoomInit(struct room *room)
{
	memset(room, 0, sizeof(struct room));
	room->num = -1;
}

void RoomDestroy(struct room *room)
{
	SAFE_FREE(room->name);
	SAFE_FREE(room->desc);
	SAFE_FREE(room->prompt);
}

static int str2num(char *in)
{
	int out;

	if (in == NULL)
	{
		return 0;
	}
	else
	{
		out = atoi(in);
		free(in);
		return(out);
	}
}

int LoadRoom(struct room *room, int num)
{
	if (room==NULL) return(0);
	if (num < 0 || num > 65535) return(0);

	room->num=num;
	room->name = db_room_get(num, "name");
	room->desc = db_room_get(num, "topic");
	room->prompt = db_room_get(num, "prompt");
	room->hidden = str2num(db_room_get(num, "hidden"));
	room->sproof = str2num(db_room_get(num, "soundproof"));
	room->locked = str2num(db_room_get(num, "locked"));

	return 1;
}
