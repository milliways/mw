#ifndef ROOMS_H
#define ROOMS_H

#include <stdint.h>

struct room
{
	int32_t num;     /* room number */
	char *name;      /* room name */
	char *desc;      /* description */
	char *prompt;    /* alternate prompt */
	int32_t hidden;  /* cant find room using 'room' command */
	int32_t sproof;  /* soundproof? */
	int32_t locked;  /* locked? */
};

void RoomInit(struct room *room);
void RoomDestroy(struct room *room);
int LoadRoom(struct room *room, int num);

#endif /* ROOMS_H */
