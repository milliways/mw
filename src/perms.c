/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <stdbool.h>

#include "bb.h"
#include "perms.h"

static const char user_perm_chars[8] = {
	[MWUSR_REGD]    = 'r',
	[MWUSR_MOD]     = 'm',
	[MWUSR_SUPER]   = 's',
	[MWUSR_BANNED]  = 'b',
	[MWUSR_MESG]    = 'n',
	[MWUSR_INFORM]  = 'i',
	[MWUSR_BEEPS]   = 'p',
	[MWUSR_DELETED] = 'D'
};

static const char folder_stat_chars[7] = {
	[MWFOLDR_ACTIVE]    = 'a',
	[MWFOLDR_RUNREG]    = 'r',
	[MWFOLDR_WUNREG]    = 'w',
	[MWFOLDR_RREG]      = 'R',
	[MWFOLDR_WREG]      = 'W',
	[MWFOLDR_PRIVATE]   = 'p',
	[MWFOLDR_MODERATED] = 'm'
};

int allowed_r(struct folder *fol, struct user *user)
/* permission to read from folder */
{
	struct person *usr = &user->record;

	if (u_god(user)) return(true); /* superuser */
	if ((fol->groups)&(usr->groups)) /* you are in a common group */
	{
	if (u_reg(user) && f_r_reg(fol->g_status)) return(true); /* registered */
	if (!u_reg(user) && f_r_unreg(fol->g_status)) return(true); /* unregistered */
	}else  /* not in a common group */
	{
	if (u_reg(user) && f_r_reg(fol->status)) return(true); /* registered */
	if (!u_reg(user) && f_r_unreg(fol->status)) return(true); /* unregistered */
	}
	return(false);
}

int is_private(struct folder *fol, struct user *usr)
{
	if ((fol->groups) & (usr->record.groups))
		return(f_private(fol->g_status));
	else
		return(f_private(fol->status));
}

int is_moderated(struct folder *fol, struct user *usr)
{
	if (u_god(usr)) return false;
	if ((fol->groups) & (usr->record.groups))
		return(f_moderated(fol->g_status));
	else
		return(f_moderated(fol->status));
}

int allowed_w(struct folder *fol, struct user *user) /* permission to write to folder */
{
	struct person *usr = &user->record;

	if (u_god(user)) return(true); /* superuser */
	if ((fol->groups)&(usr->groups)) /* you are in a common group */
	{
	if (u_reg(user) && f_w_reg(fol->g_status)) return(true); /* registered */
	if (!u_reg(user) && f_w_unreg(fol->g_status)) return(true); /* unregistered */
	}else  /* not in a common group */
	{
	if (u_reg(user) && f_w_reg(fol->status)) return(true); /* registered */
	if (!u_reg(user) && f_w_unreg(fol->status)) return(true); /* unregistered */
	}
	return(false);
}

char user_stats(const char *string, char ustat)
{
	int mode=0;
	int i;
	char p=0; /*pattern buffer */
	for (i=0;i<strlen(string);i++)
		switch (string[i])
		{
			case '=': mode=0;break;
			case '+': mode=-1;break;
			case '-': mode=1;break;
			case 'r': p=p|1;break;
			case 'm': p=p|(1<<1);break;
			case 's': p=p|(1<<2);break;
			case 'b': p=p|(1<<3);break;
			case 'n': p=p|(1<<4);break;
			case 'i': p=p|(1<<5);break;
			case 'p': p=p|(1<<6);break;
			case 'D': p=p|(1<<7);break;
		};
	if (mode==0) return(p);
	else if (mode==-1) return(ustat|p);
	else if (mode==1) return(ustat&(~p));
	else return(p);
}

char mesg_stats(char *string, char mstat)
{
	int mode=0;
	int i;
	char p=0; /*pattern buffer */
	for (i=0;i<strlen(string);i++)
		switch (string[i])
		{
			case '=': mode=0;break;
			case '+': mode=-1;break;
			case '-': mode=1;break;
			case 'm': p=p|(1<<0);break;
			case 'd': p=p|(1<<1);break;
			case 'r': p=p|(1<<2);break;
			case 'x': p=p|(1<<3);break;
		};
	if (mode==0) return(p);
	else if (mode==-1) return(mstat|p);
	else if (mode==1) return(mstat&(~p));
	else return(p);
}

char folder_stats(char *string, char st)
{
	int mode=0;
	int i;
	char p=0; /*pattern buffer */
	for (i=0;i<strlen(string);i++)
		switch (string[i])
		{
			case '=': mode=0;break;
			case '+': mode=-1;break;
			case '-': mode=1;break;
			case 'a': p=p|1;break;
			case 'r': p=p|(1<<1);break;
			case 'w': p=p|(1<<2);break;
			case 'R': p=p|(1<<3);break;
			case 'W': p=p|(1<<4);break;
			case 'p': p=p|(1<<5);break;
			case 'm': p=p|(1<<6);break;
		};
	if (mode==0) return(p);
	else if (mode==-1) return(st|p);
	else if (mode==1) return(st&~p);
	return(p);
}

char folder_groups(const char *string, char st)
{
	int mode=0;
	int i;
	char p=0; /*pattern buffer */
	for (i=0;i<strlen(string);i++)
		switch (string[i])
		{
			case '=': mode=0;break;
			case '+': mode=-1;break;
			case '-': mode=1;break;
			case '1': p=p|1;break;
			case '2': p=p|(1<<1);break;
			case '3': p=p|(1<<2);break;
			case '4': p=p|(1<<3);break;
			case '5': p=p|(1<<4);break;
			case '6': p=p|(1<<5);break;
			case '7': p=p|(1<<6);break;
			case '8': p=p|(1<<7);break;
		};
	if (mode==0) return(p);
	else if (mode==-1) return(st|p);
	else if (mode==1) return(st&~p);
	return(p);
}

static void show_stats(unsigned char st, char *tmp, size_t tmpsize, const char *map, int flag)
{
	int bit = 0;
	int pos = 0;
	while(bit < tmpsize) {
		if (st & (1<<bit))
			tmp[pos++] = map[bit];
		else if (flag)
			tmp[pos++] = '-';
		bit++;
	}
	tmp[pos] = '\0';
}

void show_user_stats(unsigned char ustat, char *tmp, int flag)
{
	show_stats(ustat, tmp, MWUSR_SIZE, user_perm_chars, flag);
}

void show_mesg_stats(unsigned char mstat, char *tmp, int flag)
{
	int i=0;

	if (mstat&(1<<0)) tmp[i++]='m'; else if (flag) tmp[i++]='-';
	if (mstat&(1<<1)) tmp[i++]='d'; else if (flag) tmp[i++]='-';
	if (mstat&(1<<2)) tmp[i++]='r'; else if (flag) tmp[i++]='-';
	if (mstat&(1<<3)) tmp[i++]='x'; else if (flag) tmp[i++]='-';
	tmp[i]=0;
}

void show_fold_stats(char st, char *tmp, int flag)
{
	show_stats(st, tmp, MWFOLDR_SIZE, folder_stat_chars, flag);
}

void show_fold_groups(char st, char *tmp, int flag)
{
	int pos = 0;
	int i;
	for (i = 0; i < 8; i++)
		if (st & (1<<i))
			tmp[pos++]='1'+i;
		else if (flag)
			tmp[pos++]='-';
	tmp[pos] = '\0';
}

int get_subscribe(struct user *user, int folder)
{
	struct person *usr = &user->record;

	if (folder<=31 && folder>=0)
	{
		if (usr->folders[0] & (1<<folder) )
			return(true);
		else
			return(false);
	}else
	if (folder>=32 && folder <=63)
	{
		if (usr->folders[1] & (1<< (folder-32)))
			return(true);
		else
			return(false);
	}else
	{
		printf("Error in get_subscribe folder %d\n",folder);
		return(false);
	}
}

void set_subscribe(struct user *user,int folder,int status)
{
	struct person *usr = &user->record;

	if (folder>=0 && folder<=31)
	{
		if (status==true)
		{
			usr->folders[0]|=(1<<folder);
		}else
		if (status==false)
		{
			usr->folders[0]&=~(1<<folder);
		}else
			printf("Error in set_subscribe to folder %d\n",folder);
	}else
	if (folder>=32 && folder<=63)
	{
		if (status==true)
		{
			usr->folders[1]|=(1<<(folder-32));
		}else
		if (status==false)
		{
			usr->folders[1]&=~(1<<(folder-32));
		}else
			printf("Error in set_subscribe to folder %d\n",folder);
	}else
		printf("Error in set_subscribe to folder %d\n",folder);
}
