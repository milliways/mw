#ifndef SPECIAL_H
#define SPECIAL_H

unsigned short set_special(const char *string, unsigned short stat);
void show_special(unsigned short stat, char *tmp, int flag);

enum {
	MWSPCL_COVENTRY    =  0,
	MWSPCL_SUPER,
	MWSPCL_WIZCHAT,
	MWSPCL_WIZCHATOFF,
	MWSPCL_TIMEOUT,
	MWSPCL_TIMESTAMP,
	MWSPCL_POSTINFO,
	MWSPCL_UNUSED_7,
	MWSPCL_COLOUROFF,
	MWSPCL_CHGINFO,
	MWSPCL_UNUSED_10,
	MWSPCL_QUIET,
	MWSPCL_TCUNAMES,
	MWSPCL_DEVEL,
	MWSPCL_FIXEDCONTACT,
	MWSPCL_NOWRAP,
	/* Dummy for limit checking */
	MWSPCL_SIZE
};

#define s_coventry(user)      ((user)->record.special & (1<<MWSPCL_COVENTRY))
#define s_superuser(user)     ((user)->record.special & (1<<MWSPCL_SUPER))
#define s_wizchat(user)       ((user)->record.special & (1<<MWSPCL_WIZCHAT))
#define s_chatoff(user)       ((user)->record.special & (1<<MWSPCL_WIZCHATOFF))
#define s_timeout(user)       ((user)->record.special & (1<<MWSPCL_TIMEOUT))
#define s_timestamp(user)     ((user)->record.special & (1<<MWSPCL_TIMESTAMP))
#define s_postinfo(user)      ((user)->record.special & (1<<MWSPCL_POSTINFO))
#define s_colouroff(user)     ((user)->record.special & (1<<MWSPCL_COLOUROFF))
#define s_changeinfo(user)    ((user)->record.special & (1<<MWSPCL_CHGINFO))
#define s_quiet(user)         ((user)->record.special & (1<<MWSPCL_QUIET))
#define s_tcunames(user)      ((user)->record.special & (1<<MWSPCL_TCUNAMES))
#define s_devel(user)         ((user)->record.special & (1<<MWSPCL_DEVEL))
#define s_fixedcontact(user)  ((user)->record.special & (1<<MWSPCL_FIXEDCONTACT))
#define s_nolinewrap(user)    ((user)->record.special & (1<<MWSPCL_NOWRAP))

#endif /* SPECIAL_H */
