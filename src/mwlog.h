#ifndef MWLOG_H
#define MWLOG_H

#include "user.h"

#ifndef LOGDIR
#define LOGDIR "/var/log/mw"
#endif
#define LOGFILE LOGDIR "/log.bb"
#define LOGLINESIZE 2048

void mwlog(struct user *user, const char *fmt, ...) __attribute__((format(printf,2,3)));

#endif /* MWLOG_H */
