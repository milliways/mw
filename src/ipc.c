#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>

#include "ipc.h"
#include "iconv.h"
#include "strings.h"
#include "bb.h"

/* client mode uses this as its connection */
ipc_connection_t * ipcsock = NULL;
char *ipc_host = NULL;
char *ipc_port = NULL;
struct user *ipc_user;

const char *_plural_suffix[] = {
	[0] = "",
	[1] = "'s",
	[2] = "'",
	[3] = "'d",
	[4] = "'ll",
};
const char **plural_suffix = _plural_suffix;
int plural_suffix_size(void)
{
	return sizeof(_plural_suffix) / sizeof(_plural_suffix[0]);
}

const char *get_nonce(void);

int ipc_connected(void)
{
	/* we have never connected */
	if (ipc_host == NULL) return -1;
	/* we are not connected now */
	if (ipcsock == NULL) return 0;
	if (ipcsock->fd == -1) return 0;
	if (ipcsock->state == IPCSTATE_ERROR) return 0;
	/* everything must be fine */
	return 1;
}

int ipc_connect(const char *host, const char *port, struct user *user)
{
	if (host == NULL) {
		fprintf(stderr, "Need a host to connect to.\n");
		return -1;
	}
	if (port == NULL) {
		fprintf(stderr, "Need a port to connect to.\n");
		return -1;
	}
	if (user == NULL) {
		fprintf(stderr, "Need a user to identify as.\n");
		return -1;
	}
	if (ipcsock == NULL)
		ipcsock = ipcconn_create();

	/* already connected, why are we here again ? */
	if (ipcsock->fd != -1) return 1;

	/* we dont have anywhere to connect to yet */
	if (host == NULL) {
		printf("Nowhere to connect to. help.\n");
		return -1;
	}

	int fd = ipcconn_connect(host, port);
	if (fd < 0) {
		fprintf(stderr, "Connection to server failed.\n");
		return -1;
	}

	if (ipc_host != host) {
		if (ipc_host != NULL) free(ipc_host);
		if (ipc_port != NULL) free(ipc_port);
		ipc_host = strdup(host);
		ipc_port = strdup(port);
	}
	ipc_user = user;

	ipcsock->fd = fd;
	ipcsock->state = IPCSTATE_CONNECTED;

	ipc_message_t * msg = ipcmsg_create(IPC_HELLO, ipc_user->posn);
	uint32_t pid = getpid();
	ipcmsg_append(&msg, &pid, sizeof(pid));
	const char *nonce = get_nonce();
	ipcmsg_append(&msg, nonce, strlen(nonce));
	ipcmsg_send(msg, ipcsock);

	/* now send any queued up messages */
	if (!list_empty(&(ipcsock->outq))) {
		struct list_head *pos, *q;
		list_for_each_safe(pos, q, &ipcsock->outq) {
			outq_msg_t * oq = list_entry(pos, outq_msg_t, list);
			list_del(&(oq->list));
			oq->msg->refcount--;
			//ipcmsg_summary("BACKLOG", oq->msg);
			ipcmsg_send(oq->msg, ipcsock);
			free(oq);
		}
	}
	return 0;
}

void ipc_check(void)
{
	/* not connected, try again */
	if (ipc_connected() <= 0)
		ipc_connect(ipc_host, ipc_port, ipc_user);
}

void ipc_close()
{
	if (ipcsock == NULL) return;
	close(ipcsock->fd);
	ipcsock->state = IPCSTATE_DELETED;
	free(ipcsock);
	ipcsock=NULL;
}

int ipc_getfd()
{
	if (ipcsock == NULL) return -1;
	return ipcsock->fd;
}

extern uint32_t who_find(const char *username);

/*
 * Send a message to a remote process addressed by username.
 * Returns the number of processes successfuly sent messages
 *
 * DEPRECATED ! - move away from this function
 * 		  server should decide who messages go to not you
 *
 *  Any program including this must provide its own who_find()
 */
unsigned int ipc_send_to_username(const char * dest, enum ipc_types msgtype, const char * data) {
	int	ret = 0;

	if (! data) data = "";

	char	buff[MAXTEXTLENGTH];
	snprintf(buff, MAXTEXTLENGTH - 1, "%s",data);
	//ensure text (everything after the first 5 bytes) going into the pipes is clean utf-8 after all the truncating etc. that might have happened to it.
	//
	//
	long uposn = SYSTEM_USER;
	if (dest) uposn = who_find(dest);
	if (uposn == -1) return 0;

	ipc_check();
	if(utf8_cleanup(buff)>=0) {
		ssize_t dgram_len = strlen(buff);
		ipc_message_t *msg = ipcmsg_create(msgtype, ipc_user->posn);
		ipcmsg_destination(msg, uposn);
		ipcmsg_append(&msg, buff, dgram_len);
		ipcmsg_send(msg, ipcsock);
	} else {
		fprintf(stderr, "Failed to utf8_cleanup the message\n");
	}
	return ret;
}

/*
 * Send a message to all remote processes
 * If filter is not NULL, it is called for each process and
 * must return 0 (don't send) or non-zero (send).
 * Returns the number of processes successfuly sent messages
 */
unsigned int ipc_send_to_all(enum ipc_types msgtype, const char * data) {
	return ipc_send_to_username(NULL, msgtype, data);
}


/*
 * most of the mw code should use this to send a message object
 * fourcc and destination will control what happens when it
 * gets to the server
 */
void ipcmsg_transmit(ipc_message_t * msg)
{
	ipc_check();
	ipcmsg_send(msg, ipcsock);
}

const char * ipc_nametype(enum ipc_types msgtype)
{
	switch (msgtype) {
		case IPC_NOOP: return "IPC_NOOP"; break;
		case IPC_NEWMAIL: return "IPC_NEWMAIL"; break;
		case IPC_STATUS: return "IPC_STATUS"; break;
		case IPC_GROUPS: return "IPC_GROUPS"; break;
		case IPC_REALNAME: return "IPC_REALNAME"; break;
		case IPC_CONTACT: return "IPC_CONTACT"; break;
		case IPC_LASTREAD: return "IPC_LASTREAD"; break;
		case IPC_SPECIAL: return "IPC_SPECIAL"; break;
		case IPC_TIMEOUT: return "IPC_TIMEOUT"; break;
		case IPC_USERNAME: return "IPC_USERNAME"; break;
		case IPC_TEXT: return "IPC_TEXT"; break;
		case IPC_CHATPRIVS: return "IPC_CHATPRIVS"; break;
		case IPC_CHATMODE: return "IPC_CHATMODE"; break;
		case IPC_KICK: return "IPC_KICK"; break;
		case IPC_PASSWD: return "IPC_PASSWD"; break;
		case IPC_CHANNEL: return "IPC_CHANNEL"; break;
		case IPC_WIZ: return "IPC_WIZ"; break;
		case IPC_GAG: return "IPC_GAG"; break;
		case IPC_GAGLIST: return "IPC_GAGLIST"; break;
		case IPC_SCRIPTIPC: return "IPC_SCRIPTIPC"; break;
		case IPC_SCRIPTRPC: return "IPC_SCRIPTRPC"; break;
		case IPC_CHECKONOFF: return "IPC_CHECKONOFF"; break;
		case IPC_PROTLEVEL: return "IPC_PROTLEVEL"; break;
		case IPC_PROTPOWER: return "IPC_PROTPOWER"; break;
		case IPC_DOING: return "IPC_DOING"; break;
		case IPC_HELLO: return "IPC_HELLO"; break;
		case IPC_UPTIME: return "IPC_UPTIME"; break;
		case IPC_SAYTOROOM: return "IPC_SAYTOROOM"; break;
		case IPC_SAYTOUSER: return "IPC_SAYTOUSER"; break;
		case IPC_SAYTOALL: return "IPC_SAYTOALL"; break;
		case IPC_TALKERROR: return "IPC_TALKERROR"; break;
		case IPC_REPLAY: return "IPC_REPLAY"; break;
		case IPC_EVENT: return "IPC_EVENT"; break;
		case IPC_ACTION: return "IPC_ACTION"; break;
		case IPC_WHOLIST: return "IPC_WHOLIST"; break;
		case IPC_ERROR: return "IPC_ERROR"; break;
	}
	return "IPC_Unknown";
}
