#include <string.h>
#include "nonce-def.h"
#include "nonce.h"

const char * get_nonce()
{
	return NONCE;
}

int match_nonce(const char *test)
{
	if (strcmp(test, NONCE)==0) return 1;
	return 0;
}
