#ifndef ICONV_H
#define ICONV_H

#include <wchar.h>

int convert_string_charset(char *input_buffer, const char *input_charset, size_t input_length,
                 char *output_buffer, const char *output_charset, size_t output_buffer_length,
                 size_t *output_bytes_used,  size_t *irreversible_count,
                 size_t *unconverted_bytes, size_t *invalid_count, const char *substitute);

int utf8_cleanup(char *buff);

/* set local charset independently of locale */
int set_local_charset(const char * set);

/* query local charset */
char *get_local_charset(void);

/* wrapper around set_locale printing failure messages */
int locale_mesgs(const char * locale);

/* set locale */
int set_locale(const char * locale);

void init_locale(void);

#define ENOOUTPUTCS -1
#define ENOINPUTCS -2
#define ENOOUTPUTBUFF -3
#define ENOINPUTBUFF -4
#define EZEROLENTGHINPUT -5
#define EOUTPUTNOTSUPPORTED -6
#define EUTFCONVERTFAILED -7
#define ECONVERTNULLFAILED -8
#define EMALLOCSUBS -9
#define ECONVERTSUBSFAILED -10
#define EINPUTNOTSUPPORTED -11
#define EOPENCONVERTFAILED -12
#define EOUTPUTBUFFMUCHTOOSHORT -13

#define WOUTPUTTOOSHORT 1
#define WINVALIDCHARS 2
#define WICONVFAIL 4

#endif /* ICONV_H */
