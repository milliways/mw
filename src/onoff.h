#ifndef MW_ONOFF_H
#define MW_ONOFF_H

enum onoff_action {
	/* Relating to mw itself */
	ONOFF_LOGON  = 0,
	ONOFF_LOGOFF = 1,
	/* Relating to the talker */
	ONOFF_JOIN   = 2,
	ONOFF_LEAVE  = 3,

	ONOFF_SIZE /* Keep at end */
};

#define LOGON_NORMAL   0
#define LOGON_AUTOCHAT 1

#define LOGOFF_NORMAL  0
#define LOGOFF_TIMEOUT 1
#define LOGOFF_EOFS    2
#define LOGOFF_MROD    3
#define LOGOFF_BANNED  4
#define LOGOFF_ERROR   5
#define LOGOFF_DELETED 6

#define JOIN_NORMAL   0
#define JOIN_SUMMONED 1
#define JOIN_FORCED   2

#define LEAVE_NORMAL 0
#define LEAVE_ZOD    1
#define LEAVE_FORCED 2
#define LEAVE_EXIT   3

#endif /* MW_ONOFF_H */
