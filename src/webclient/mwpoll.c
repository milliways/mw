#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <libpq-fe.h>

#include <talker_privs.h>
#include <strings.h>
#include <files.h>
#include "import.h"
#include "comms.h"
#include "../client/onoff.h"

struct user me;
struct user *user = &me;

#define DEFAULT_IDLE 60
int clientidle = DEFAULT_IDLE;
time_t lastcomm = 0;

char *authtext = NULL;
extern int die;

static int mydaemon(void)
{
	int pid = fork();

	if (pid == -1) {
		printf("Fork failed: %s\n", strerror(errno));
		exit(0);
	} else
	if (pid != 0) {
		return pid;
	} else {
		/* why is this needed ?
		if (seteuid(110)) {
			printf("seteuid failed. %s\n", strerror(errno));
			exit(0);
		}
		*/
		setsid();
		close(0);
		close(1);
		close(2);
	}
	return 0;
}


static void usage(const char *name)
{
	printf("Usage: %s [-u username] [-c channel] [-dsa] [-h host:port]\n", name);
	printf("-u username	Select username\n");
	printf("-c channel	Initial channel to join\n");
	printf("-d		Debug (foreground) mode\n");
	printf("-s		Session mode for sucssite\n");
	printf("-q		Quiet entry\n");
	printf("-a		Auto create user if required\n");
	printf("-h host:port	Specify alternate server to connect to.\n");
}

int main(int argc, char ** argv)
{
	int opt;
	int channel = 0;
	int debug = 0;
	int dbsession = 0;
	int quietentry = 0;
	int autocreate = 0;
	const char *username = NULL;
	char passwd[80];
	char rawpw[80];
	char default_host[] = IPCHOST_DEFAULT;
	char default_port[] = IPCPORT_DEFAULT;
	char *svrhost = default_host;
	char *svrport = default_port;

	while ((opt=getopt(argc,argv,"u:c:h:dsqa"))!=-1) {
		switch (opt) {
			case 'u':
				username = optarg;
				break;
			case 'h':
				svrhost = optarg;
				break;
			case 'c':
				channel = atoi(optarg);
				break;
			case 'd':
				debug++;
				break;
			case 's':
				dbsession++;
				break;
			case 'q':
				quietentry++;
				break;
			case 'a':
				autocreate++;
				break;
			default:
				usage(argv[0]);
				return 1;
				break;
		}
	}

	/* if the option is set, read an initial password */
	rawpw[0]=0;
	if (autocreate) {
		char *p;
		if (fgets(rawpw, sizeof rawpw, stdin) == NULL) {
			printf("Expected initial password\n");
			return 1;
		}
		if ((p=strchr(rawpw,'\r'))!=NULL) *p=0;
		if ((p=strchr(rawpw,'\n'))!=NULL) *p=0;
	} else {
	    /* read the password / session string */
		char *p;
		if (fgets(passwd, sizeof passwd, stdin) == NULL) {
			printf("Expected password\n");
			return 1;
		}
		if ((p=strchr(passwd,'\r'))!=NULL) *p=0;
		if ((p=strchr(passwd,'\n'))!=NULL) *p=0;
	}

	if (!user_exists(username, user)) {
		if (autocreate) {
			create_user(user, username, rawpw);
		} else {
			printf("User '%s' not found.\n", username);
			return 1;
		}
	}

	/* check the session / password */
	if (dbsession) {
		PGconn *dbconn;
		PGresult *dbres;
		dbconn = PQconnectdb("dbname=sucssite");
		if (PQstatus(dbconn) != CONNECTION_OK) {
			printf("DB Connect error\n");
			PQfinish(dbconn);
			return 1;
		}
		const char *vals[1];
		vals[0] = passwd;
		dbres = PQexecParams(dbconn, "SELECT username from session where hash = $1", 1, NULL, vals, NULL, NULL, 0);
		if (PQresultStatus(dbres) != PGRES_TUPLES_OK)
		{
			printf("SELECT failed: %s", PQerrorMessage(dbconn));
			PQclear(dbres);
			PQfinish(dbconn);
			return 1;
		}
		if (PQntuples(dbres) < 1) {
			printf("Session not found '%s'\n", passwd);
			PQclear(dbres);
			PQfinish(dbconn);
			return 1;
		}
		char *who = PQgetvalue(dbres, 0, 0);
		if (strcasecmp(who, username)!=0) {
			printf("Username mismatch '%s' != '%s'\n", username, who);
			PQclear(dbres);
			PQfinish(dbconn);
			return 1;
		}

		PQclear(dbres);
		PQfinish(dbconn);
	} else {
		char salt[3];
		strncpy(salt, me.record.passwd, 2);
		if (strcmp(crypt(passwd,salt), me.record.passwd)!=0) {
			printf("Incorrect password\n");
			return 1;
		}
	}

	/* mark as in talker */
	time_t loggedin = time(0);
	me.record.chatmode = 0;
	me.record.chatmode = cm_set(&me, CM_ONCHAT);
	me.record.idletime = loggedin;
	me.record.room = channel;
	update_user(&me);

	/* choose an authtext */
	{
		int i;
		authtext = malloc(9);
		srand(time(NULL));
		for (i=0;i<8;i++) authtext[i] = 'A' + (rand() % 26);
	}

	if (!debug) {
		int cpid = mydaemon();
		if (cpid != 0) {
			printf("%d\n%s\n", cpid, authtext);
			return 0;
		}
	} else {
		printf("Debugging foreground mode.\n");
		printf("PID: %d\n", getpid());
		printf("AUTH: %s\n", authtext);
		printf("http://sucs.org/~arthur/mw/index.php?mwsess=a:2:{s:3:\"pid\";i:%d;s:4:\"auth\";s:%zd:\"%s\";}\n", getpid(), strlen(authtext), authtext);
	}

	if ((svrport = strrchr(svrhost, ':')) != NULL) {
		*svrport = 0;
		svrport++;
	} else {
		svrport = default_port;
	}
	if (ipc_connect(svrhost, svrport, &me) < 0)
		exit(1);
	open_command_socket();

	if (u_ban(&me)) {
		ipc_close();
		close_cmd();
        	return 0;
	}

	loggedin = lastcomm = loggedin;

	/* spock, announce us please... */
	if (!quietentry) {
	  talk_rawbcast("\03310%s has just joined web talker room %d", me.record.name, me.record.room);
	}
	announce_logon(me.record.name, ONOFF_LOGON, quietentry);

	/* the main loop */
	while (mainloop(1000)==0) {
		time_t now = time(NULL);
		update_user(user);
		if ((now - lastcomm) > clientidle) {
			printf("Client Idle out.\n");
			die = 2;
			break;
		}
	};

	time_t now = time(0);
	me.record.idletime = now;
	me.record.lastlogout = now;
	me.record.timeused += now - loggedin;
	me.record.chatmode = cm_clear(&me, CM_ONCHAT);

	update_user(user);
	if (!quietentry) {
	  talk_rawbcast("\03311%s has just left the web talker", me.record.name);
	}
	announce_logoff(me.record.name, LOGOFF_NORMAL, NULL, NULL, quietentry);
	me.record.idletime=time(0);
	update_user(&me);
	ipc_close();
	close_cmd();
	return 0;
}
