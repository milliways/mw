#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "mwstring.h"

mwstring *mws_new(int size)
{
	mwstring *new = calloc(1, sizeof(mwstring));
	new->body = calloc(1, size);
	new->len = size;
	return new;
}

void mws_add(mwstring *s, const char *fmt, ...)
{
	char *new;
	va_list va;
	int newsize;

	va_start(va, fmt);
	vasprintf(&new, fmt, va);
	va_end(va);

	newsize = strlen(s->body) + strlen(new) + 1;
	if (newsize > s->len) {
		s->body = realloc(s->body, newsize);
		s->len = newsize;
	}
	strcat(s->body, new);
	free(new);
}

const char *mws_cstr(mwstring *s) {
	return s->body;
}

int mws_len(mwstring *s) {
	return strlen(s->body);
}

void mws_free(mwstring *s) {
	free(s->body);
	s->body = NULL;
	s->len = 0;
	free(s);
}
