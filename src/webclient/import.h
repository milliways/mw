#ifndef IMPORT_H
#define IMPORT_H

#include <user.h>

unsigned long cm_flags(unsigned long cm, unsigned long flags, int mode);
char *quotetext(const char *a);
void show_chatmodes(unsigned long cm, char *tmp, int flag);
void show_chatprivs(unsigned long cp, char *tmp, int flag);
const char *skip_first_word(const char *args);
void talk_send_to_room(const char *text, int channel, const char *type, int plural);

#endif /* IMPORT_H */
