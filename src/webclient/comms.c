#include <stdio.h>

#include <files.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <strings.h>
#include <pwd.h>
#include <inttypes.h>
#include <ctype.h>

#include <talker_privs.h>
#include <special.h>
#include <perms.h>
#include <bb.h>
#include <util.h>
#include <mwlog.h>
#include "import.h"
#include "comms.h"
#include "mwstring.h"
#include "../socket.h"

int command_sock;

int die = 0;

#define min(a,b) a<b?a:b
#define max(a,b) a>b?a:b

#define UNIX_PATH_MAX 108

/* how long to wait for a long-poll */
#define DEFAULT_TIMEOUT 20

extern time_t lastcomm;
extern char * authtext;

extern struct user * const user;

typedef struct {
	struct list_head list;
	enum ipc_types state;
	struct user user;
	char *text;
	uint64_t serial;
	time_t when;
} MESG;


static struct list_head msglist;

typedef struct {
	struct list_head list;
	int fd;
	int authd;
	int timeout;
	time_t longwait;
} CONNECTION;

struct list_head connlist;


char * json_escape(char *in);
static void handle_mesg(void);

/* unix socket to accept control commands from */
void open_command_socket()
{
	struct sockaddr_un sa;
	command_sock = socket(PF_UNIX, SOCK_SEQPACKET, 0);
	if (command_sock == -1) {
		fprintf(stderr, "Error creating socket: %s\n", strerror(errno));
		exit(1);
	}
	sa.sun_family = AF_UNIX;
	snprintf(sa.sun_path, UNIX_PATH_MAX, MSGDIR"/mwpoll.%d", getpid());
	if (bind(command_sock, (struct sockaddr *)&sa, sizeof(sa))) {
		fprintf(stderr, "Error binding %s: %s\n", sa.sun_path, strerror(errno));
		close(command_sock);
		command_sock = -1;
		exit(1);
	}
	chmod(sa.sun_path, 0777);

	if (listen(command_sock, 1)) {
		fprintf(stderr, "Error listening on socket: %s\n", strerror(errno));
		exit(1);
	}

	INIT_LIST_HEAD(&msglist);
	INIT_LIST_HEAD(&connlist);
}

static char *   whoinfo = NULL;
static time_t   whowhen = 0;
static int      whowant = 0;
#define WHOCACHE 120  /* cache who data for this many seconds */

const char * fetch_who(int immediate)
{
	time_t now = time(NULL);
	if (whoinfo == NULL || whowhen < (now - WHOCACHE)) {
		ipc_message_t * msg = ipcmsg_create(IPC_WHOLIST, user->posn);
		ipcmsg_transmit(msg);
		/* dont want stale, come back later */
		if (!immediate) {
			whowant = 1;
			return NULL;
		}
	}
	/* no answer we can give, sorry, you will HAVE to come back */
	if (whoinfo == NULL) {
		whowant=1;
		return NULL;
	}
	/* they want an immediate answer, even if its stale */
	return whoinfo;
}

/* we got a message */
static void accept_pipe_cmd(ipc_message_t *ipc, struct user *mesg_user)
{
	if (ipc->head.type == IPC_WHOLIST) {
		/* keep a copy for later */
		if (whoinfo != NULL)
			free(whoinfo);
		whoinfo = strdup((char *)(ipc + 1));
		whowhen = time(NULL);
		/* if we want it, fall through, otherwise stop now */
		if (whowant != 0)
			whowant=0;
		else
			return;
	}

	MESG *msg = malloc(sizeof(MESG));
	msg->state = ipc->head.type;
	//msg->pid = ipc->head.src;
	msg->user = *mesg_user;
	msg->text = json_escape((char *)(ipc + 1));
	msg->serial = ipc->head.serial;
	msg->when = ipc->head.when;

	list_add_tail(&msg->list, &msglist);

	printf("From=%s type=%d msg='%s'\n", mesg_user->record.name, ipc->head.type,
	       (char *)(ipc + 1));
}

static int handle_command(CONNECTION *co);
static int send_list(CONNECTION *co);

static void accept_new(void)
{
	CONNECTION *new = malloc(sizeof(CONNECTION));
	struct sockaddr sa;
	socklen_t sal = sizeof(sa);
	new->fd = accept(command_sock, &sa, &sal);
	if (new->fd == -1) {
		fprintf(stderr, "Error on accept: %s\n", strerror(errno));
		free(new);
		return;
	}
	new->authd = 0;
	new->timeout = DEFAULT_TIMEOUT;
	new->longwait = 0;
	list_add(&(new->list), &connlist);
	printf("Adding connection fd %d\n", new->fd);
}

int mainloop(int millis)
{
	fd_set readfds, exceptfds;
	static struct timeval tmout;
	int nfds;
	int ret;
	int select_error;
	struct list_head *pos, *q;
	int incoming_pipe = ipc_getfd();

        FD_ZERO(&readfds);
        FD_ZERO(&exceptfds);
        FD_SET(incoming_pipe, &readfds);
        FD_SET(incoming_pipe, &exceptfds);
        FD_SET(command_sock, &readfds);
        FD_SET(command_sock, &exceptfds);

	nfds = max(incoming_pipe, command_sock);

	list_for_each(pos, &connlist) {
		CONNECTION *co = list_entry(pos, CONNECTION, list);
		FD_SET(co->fd, &readfds);
		FD_SET(co->fd, &exceptfds);
		nfds = max(nfds, co->fd);
	}
	nfds += 1;

        if (millis >= 0)
        {
                tmout.tv_sec = millis / 1000;
                tmout.tv_usec = (millis % 1000) * 1000;
        }
        ret = select(nfds, &readfds, NULL, &exceptfds, (millis<0)?(NULL):(&tmout));
        select_error = errno;
        if (ret > 0) {
                if (FD_ISSET(incoming_pipe, &exceptfds)) {
                        fprintf(stderr, "\nError reading incoming message pipe. panic.\n");
                        return -1;
                }
                if (FD_ISSET(command_sock, &exceptfds)) {
                        fprintf(stderr, "\nError on command socket, argh.\n");
                        return -1;
                }
                if (FD_ISSET(incoming_pipe, &readfds))
			handle_mesg();
                if (FD_ISSET(command_sock, &readfds)) {
			accept_new();
		}
		list_for_each_safe(pos, q, &connlist) {
			CONNECTION *co = list_entry(pos, CONNECTION, list);
			if (FD_ISSET(co->fd, &exceptfds)) {
				printf("Dropping connection fd=%d\n", co->fd);
				list_del(pos);
				close(co->fd);
				free(co);
			}else
			if (FD_ISSET(co->fd, &readfds)) {
				if (handle_command(co)) {
					printf("Finishing connection fd=%d\n", co->fd);
					list_del(pos);
					close(co->fd);
					free(co);
				}
			}
		}
        }
	list_for_each_safe(pos, q, &connlist) {
		CONNECTION *co = list_entry(pos, CONNECTION, list);
		if (co->longwait != 0) {
			time_t now = time(NULL);
			if (!list_empty(&msglist)) {
				if (send_list(co)) {
					printf("Finishing connection after long wait fd=%d\n", co->fd);
					list_del(pos);
					close(co->fd);
					free(co);
				}
			}else
			if (now - co->longwait > co->timeout) {
				printf("Long poll timeout fd=%d\n", co->fd);
				send_list(co);
				list_del(pos);
				close(co->fd);
				free(co);
			}
		}
	}

        errno = select_error;
        if (ret<0 && select_error == EINVAL)
        {
                char buf[256];
                snprintf(buf, sizeof buf, "\n{EINVAL: command_sock=%d, incoming_pipe=%d}\n", command_sock, incoming_pipe);
		fwrite(buf, strlen(buf), 1, stderr);
        }
	if (die) return -1; else
        if (nfds < 0) return nfds; else
		return 0;
}

static int send_list(CONNECTION *co)
{
	MESG *tmp;
	struct list_head *pos, *q;
	mwstring *buff = mws_new(2048);

	int n=0;
	mws_add(buff, "[");
	list_for_each_safe(pos, q, &msglist) {
		tmp = list_entry(pos, MESG, list);

		mws_add(buff, "{");
		if (tmp->state >= 32) /* send new codes as the fourcc string */
			mws_add(buff, "\"state\":\"%4.4s\",", (char *)&(tmp->state));
		else
			mws_add(buff, "\"state\":%d,", tmp->state);
		mws_add(buff, "\"serial\":%"PRId64",", tmp->serial);
		mws_add(buff, "\"when\":%"PRId64",", tmp->when);
		mws_add(buff, "\"name\":\"%s\",\"text\":\"%s\"}", tmp->user.record.name, tmp->text);
		if (pos->next != &msglist) {
			mws_add(buff, ",");
		}

		list_del(pos);

		if (tmp->state == IPC_KICK) die=3;
		free(tmp->text);
		free(tmp);
		n++;
	}
	mws_add(buff, "]");

	co->longwait = 0;
	send(co->fd, mws_cstr(buff), mws_len(buff), 0);
	mws_free(buff);
	printf("Sending %d items.\n", n);
	return n>0?1:0;
}

char * json_escape(char *original)
{
	int i;
	unsigned char *in = (unsigned char *)original;
	mwstring *line = mws_new(strlen(original)+1);
	char * out;

	for (i=0;i<strlen(original);i++) {
		switch (*in) {
			case '\b': mws_add(line, "\\b"); break;
			case '\n': mws_add(line, "\\n"); break;
			case '\r': mws_add(line, "\\r"); break;
			case '\t': mws_add(line, "\\t"); break;
			case '"':  mws_add(line, "\\\""); break;
			case '\\': mws_add(line, "\\\\"); break;
			case '/':  mws_add(line, "\\/"); break;
			default:
				if (*in < ' ')
					mws_add(line, "\\u%04x", *in);
				else
					mws_add(line, "%c", *in);
				break;
		}
		in++;
	}

	out = (char *)mws_cstr(line);
	free(line);

	return out;
}

static int handle_command(CONNECTION *co)
{
	/* read and answer the command socket */
	char buff[8192];
	int ret;

	if ((ret = recv(co->fd, buff, sizeof buff, MSG_DONTWAIT))<=0) {

		if (errno == EAGAIN || errno == EWOULDBLOCK) {
			// not ready, try again later
			return 0;
		}

		if (ret == 0) {
			// end of the line
			return 1;
		}

		fprintf(stderr, "Error on cmd sock read: %s\n", strerror(errno));
		return 1;
	}

	buff[ret] = 0;
	printf("handle_command(%s)\n", buff);

	lastcomm = time(NULL);

	if (strcasecmp(buff, "quit")==0) {
		snprintf(buff, sizeof buff, "BYE");
		send(co->fd, buff, strlen(buff), 0);
		die = 1;
		return 1;
	}else
	if (strncasecmp(buff, "auth ", 5)==0) {
		ret=0;
		if (strcmp(&buff[5], authtext)==0)
		{
			co->authd = 1;
			snprintf(buff, sizeof buff, "{\"status\":\"OK\"}");
		}else {
			snprintf(buff, sizeof buff, "{\"status\":\"Bad Auth\"}");
			ret = 1;
		}
		send(co->fd, buff, strlen(buff), 0);
		return ret;
	}else
	if (strncasecmp(buff, "timeout ", 8)==0) {
		co->timeout = atoi(&buff[8]);
		snprintf(buff, sizeof buff, "{\"timeout\":%d}", co->timeout);
		send(co->fd, buff, strlen(buff), 0);
		return 0; // dont close
	}else
	if (strncasecmp(buff, "channel ", 8)==0) {
                char line[8192];
		int oldroom;
		int newroom;
		oldroom = user->record.room;
		newroom = atoi(&buff[8]);
		user->record.idletime = lastcomm;
		if (newroom == oldroom) {
			return 1;
		}else if (newroom<0 || newroom>65535) {
			snprintf(line, sizeof line, "\03314Not leaving to invalid room ID %d", newroom);
			ipc_send_to_username(user->record.name, IPC_TEXT, line);
			return 1;
		}else{
			user->record.room = newroom;
		    /*
			snprintf(line, sizeof line, "\03313%s has left to room %d", user->record.name, newroom);
			talk_send_to_room(line, oldroom, "raw", -1);
                        snprintf(line, sizeof line, "\03314Leaving room %d for room %d",oldroom,newroom);
                        ipc_send_to_username(user->record.name,IPC_TEXT,line);
	                snprintf(line, sizeof line, "\03312%s has just arrived in room %d", user->record.name, user->record.room);
        	        talk_send_to_room(line, newroom, "raw", -1);
        	*/
			snprintf(buff, sizeof buff, "{\"channel\":%d}", user->record.room);
			send(co->fd, buff, strlen(buff), 0);
			return 1; // dont close
		}
	}else
	if (co->authd && strncasecmp(buff, "sayto ", 6)==0) {
		char *to;
		char *text;
		const char *type = "says";
		_autofree char *status = NULL;

		if (buff[strlen(buff) - 1] == '?')
			type = "asks";

		to = buff + 6; // Skip 'sayto '
		while (isspace(*to)) to++; // Skip whitespace to username
		text = to;
		while (!isspace(*text)) text++; // Skip username
		*text = '\0'; // Hasta la vista, username
		text++;
		while (*text && isspace(*text)) text++;  // Trim space before message

		ipc_message_t * msg = ipcmsg_create(IPC_SAYTOUSER, user->posn);
		json_t * j = json_init(NULL);
		json_addstring(j, "target", to);
		json_addstring(j, "type", type);
		json_addstring(j, "text", text);
		ipcmsg_json_encode(&msg, j);
		json_decref(j);
		ipcmsg_transmit(msg);

		mwlog(user, "SAYTO %s %s", to, text);

		asprintf(&status, "{\"status\":\"sayto sent to %s (%s)\"}", to, text);
		send(co->fd, status, strlen(status), 0);
		user->record.idletime = lastcomm;
		return 1;
	}else
        if (co->authd && strncasecmp(buff, "emote ", 6)==0) {
		talk_send_to_room(&buff[6], user->record.room, "emote", 0);
		mwlog(user, "EMOTE %s", &buff[6]);
                snprintf(buff, sizeof buff, "{\"status\":\"Emote Sent to channel %d\"}", user->record.room);
                send(co->fd, buff, strlen(buff), 0);
		user->record.idletime = lastcomm;
                return 1;
        }else
        if (co->authd && strncasecmp(buff, "replay ", 7)==0) {
		_autofree char *line = NULL;
		const char *style = NULL;
		const char *value = NULL;
		char *tmpu;
		style = skip_first_word(buff);
		value = skip_first_word(style);
		if ((tmpu = strchr(style, ' '))!=NULL) *tmpu=0;
		asprintf(&line, "{\"%s\":%ld}", style, atol(value));
                ipc_send_to_username(NULL, IPC_REPLAY, line);
		user->record.idletime = lastcomm;
                return 1;
        }else
	if (co->authd && strcasecmp(buff, "who")==0) {
		const char *line = fetch_who(0);
		if (line != NULL)
			send(co->fd, line, strlen(line), 0);
		return 1;
	}else
	if (co->authd && strcasecmp(buff, "whoami")==0) {
		mwstring *line = mws_new(2048);
		char stats[64];
		char *tmp;
		mws_add(line, "{");
		mws_add(line, "\"name\":\"%s\",", user->record.name);
		tmp = json_escape(user->record.realname);
		mws_add(line, "\"realname\":\"%s\",", tmp);
		free(tmp);
		tmp = json_escape(user->record.contact);
		mws_add(line, "\"contact\":\"%s\",", tmp);
		free(tmp);
		tmp = json_escape(user->record.doing);
		mws_add(line, "\"doing\":\"%s\",", tmp);
		free(tmp);
		mws_add(line, "\"lastlogout\":%d,", user->record.lastlogout);
		show_user_stats(user->record.status, stats, 0);
		mws_add(line, "\"status\":\"%s\",", stats);
		show_special(user->record.special, stats, 0);
		mws_add(line, "\"special\":\"%s\",", stats);
		show_chatmodes(user->record.chatmode, stats, 0);
		mws_add(line, "\"chatmodes\":\"%s\",", stats);
		show_chatprivs(user->record.chatprivs, stats, 0);
		mws_add(line, "\"chatprivs\":\"%s\",", stats);

		mws_add(line, "\"room\":%d,", user->record.room);
		mws_add(line, "\"colour\":%d", user->record.colour);
		mws_add(line, "}");
		send(co->fd, mws_cstr(line), mws_len(line), 0);
		mws_free(line);
		return 1;
	}else
	if (co->authd && strncasecmp(buff, "say ", 4)==0) {
		talk_send_to_room(&buff[4], user->record.room, "say", -1);
		mwlog(user, "SAY %s", &buff[4]);
		snprintf(buff, sizeof buff, "{\"status\":\"Message Sent to channel %d\"}", user->record.room);
		send(co->fd, buff, strlen(buff), 0);
		user->record.idletime = lastcomm;
		return 1;
	}else
	if (co->authd && strcasecmp(buff, "fetch")==0) {
		if (list_empty(&msglist)) {
			co->longwait = time(NULL);
			return 0;
		} else {
			return send_list(co);
		}
	}
	return 0;
}

extern ipc_connection_t * ipcsock;

static void handle_mesg(void)
{
	long mesg_posn;
	static struct user mesg_user;
	struct person *urec = &mesg_user.record;

	ipc_message_t * msg = read_socket(ipcsock, 1);

	while (msg != NULL) {
		if ((mesg_posn=msg->head.src)==SYSTEM_USER)
			strcpy(urec->name, "System");
		else
			fetch_user(&mesg_user, mesg_posn);

		accept_pipe_cmd(msg, &mesg_user);

		ipcmsg_destroy(msg);
		msg = read_socket(ipcsock, 0);
        }
}

void close_cmd(void)
{
	char path[UNIX_PATH_MAX];
	close(command_sock);
	command_sock = -1;
	snprintf(path, UNIX_PATH_MAX, MSGDIR"/mwpoll.%d", getpid());
	unlink(path);
}

void talk_rawbcast(const char *fmt, ...)
{
	va_list va;
	char *text = NULL;

	va_start(va, fmt);
	vasprintf(&text, fmt, va);
	va_end(va);
	ipc_send_to_all(IPC_TEXT, text);
	free(text);

}

void create_user(struct user *me, const char *username, const char *password)
{
	struct person *urec = &me->record;
	struct passwd *pw;
	char salt[3];

	salt[0] = 'a' + (rand() % 26);
	salt[1] = 'a' + (rand() % 26);
	salt[2] = 0;
	memset(me, 0, sizeof(*me));
	urec->colour=0;
	urec->status |= (1<<6);
	urec->special |= (1<<12);
	urec->lastlogout = time(0);
	urec->timeout = 86400;
	urec->doing[0]=0;
	urec->folders[0] = SETALLLONG;
	urec->folders[1] = SETALLLONG;
	urec->status |= 1;
	urec->groups |= 1;
	urec->chatprivs |= CP_SCRIPT;
	snprintf(urec->name, sizeof(urec->name), "%s", username);
	snprintf(urec->passwd, sizeof(urec->passwd), "%s", crypt(password,salt));
	if ((pw=getpwnam(username))!=NULL) {
		snprintf(urec->realname, sizeof(urec->realname), "%s", pw->pw_gecos);
		snprintf(urec->contact, sizeof(urec->contact), "%s@sucs.org", pw->pw_name);
		mwlog(user, "CREATED %s <%s>", urec->realname, urec->contact);
	} else {
		mwlog(user, "CREATED Auto web user");
	}
	userdb_write(me);
}

int32_t who_find(const char *username)
{
          if (whoinfo == NULL) return -1;
	  json_error_t err;
          json_t *wlist = json_loads(whoinfo, 0, &err);
          if (wlist == NULL) return -1;

          size_t wi;
          json_t *entry;
          json_array_foreach(wlist, wi, entry) {
                  const char *name = json_getstring(entry, "name");
                  int32_t uid = json_getint(entry, "id");
                  if (strcasecmp(username, name)==0) {
                          json_decref(wlist);
                          return uid;
                  }
          }
          json_decref(wlist);
          return -1;
}

