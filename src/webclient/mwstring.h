
typedef struct {
	char *body;
	size_t len;
} mwstring;

/* mwstring.c */
mwstring *mws_new(int size);
void mws_add(mwstring *s, const char *fmt, ...) __attribute__((format(printf,2,3)));
const char *mws_cstr(mwstring *s);
int mws_len(mwstring *s);
void mws_free(mwstring *s);
