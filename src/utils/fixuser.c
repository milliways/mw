#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <bb.h>
#include <user.h>
#include <files.h>

int main(void)
{
	struct person user;
	const char *path = "users.bb";
	int count=0;
	int ff, ff2;

	if ((ff=open(path, O_RDONLY))<0) {
		fprintf(stderr, "%s: %s", path, strerror(errno));
		exit(1);
	}

	if ((ff2=open("users.bb.new", O_WRONLY|O_CREAT, 0600))<0) {
		fprintf(stderr, "users.bb.new: %s", strerror(errno));
		exit(1);
	}

	while (read(ff, &user, sizeof(user))>0) {
		printf("%d: %20s %40s\n", count, user.name, user.realname);
		write(ff2, &user, sizeof(user));
		count++;
		if (!strcasecmp(user.name, "dot")) {
			close(ff2);
			close(ff);
			exit(1);
		}
	}
	close(ff);
	close(ff2);
}
