#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <iconv.h>
#include <locale.h>

#include "iconv.h"

#ifdef TEST_ICONV
#  define DEBUG_ICONV
#endif

#ifdef DEBUG_ICONV
#  include <readline/readline.h>
#endif

static char * local_charset = NULL;

/**** convert_string_charset ****
	a generic iconv function to convert a string from any charset to another
	expects memory to be allocated for output_buffer

	returns: 0 if successful.
			 +ve if something was converted but errors occured in the conversion
			 -ve if the conversion fails.
	input_buffer: pointer to the string you want to convert.
	input_charset: the iconv encoding description string of the input string
	input_length: the length in bytes of the input string
	output_buffer: pointer to the memory to put the converted string into (must be allocated)
	output_charset: the iconv encoding description string of the output string
	output_buffer_length: the number of bytes allocated for output_buffer
	output_bytes_used: on success/toolong/invalchars the number of bytes used in output_buffer
	irreversible_count: on _success_ the number of chars that were converted in an irreversible way
	unconverted_bytes: on _toolong_ the number of bytes in input buffer that were not converted.
	invalid_count: on invalchars the number of invalid chars that were discovered in the string
	substitute: if an invalid char is discovered replace it with this ***UTF-8*** string
					(the utf8 string is converted into your desired charset)

	for sanities sake this function always attempts to put '\0' (in the converted charset)
	at the end of the output buffer even if there was not a '\0' in the input string.

	it is assumed this function is used on a system that understands UTF-8!
	if not you will get EOUTPUTNOTSUPPORTED.
*/
int convert_string_charset(char *input_buffer, const char *input_charset, size_t input_length,
                 char *output_buffer, const char *output_charset, size_t output_buffer_length,
                 size_t *output_bytes_used,  size_t *irreversible_count,
                 size_t *unconverted_bytes, size_t *invalid_count, const char *substitute)
{
	const char *input_ptr;
	char *output_ptr;
	iconv_t conversion_descriptor;
	size_t input_bytes_left, output_bytes_left, not_conv_count=0;

	char converted_null[8], null_string[1]="";
	char *converted_substitute=NULL;
	size_t converted_substitute_length=output_buffer_length, converted_null_length;
	size_t nconv;
	int local_invalid_count=0;
	int i, too_big=0, iconv_failed=0, retval=0;
	char output_charset_ignore[50];

	if(output_charset==NULL) {
		return ENOOUTPUTCS;
	}
	if(input_charset==NULL) {
		return ENOINPUTCS;
	}
	if(output_buffer==NULL) {
		return ENOINPUTBUFF;
	}
	if(input_buffer==NULL) {
		return ENOINPUTBUFF;
	}
/*	if(input_length<1) {
		return EZEROLENTGHINPUT;
	}*/ //thinking about it this isn't really an error.  if you do this you will still get a null string back in your chosen encoding.

	if( strcmp(output_charset,"LOCAL")==0 ) {
		if(local_charset!=NULL) {
			output_charset = local_charset;
		} else {
			output_charset = null_string;
		}
	}
	if( strcmp(output_charset,"LOCAL//TRANSLIT")==0 ) {
		if(local_charset!=NULL) {
			output_charset = output_charset_ignore;
			snprintf(output_charset_ignore, 50, "%s//TRANSLIT", local_charset);
			output_charset_ignore[49]='\0';

		} else {
			output_charset = null_string;
		}
	}

	if( strcmp(input_charset,"LOCAL")==0 ) {
		if(local_charset!=NULL) {
			input_charset = local_charset;
		} else {
			input_charset = null_string;
		}
	}
	conversion_descriptor = iconv_open(output_charset, "UTF-8");
	if(conversion_descriptor == (iconv_t)-1) {
		if(errno == EINVAL) {
			return EOUTPUTNOTSUPPORTED;
		} else {
			return EUTFCONVERTFAILED;
		}
	}
	/* convert '\0' to local charsets representation */
	input_bytes_left=1;
	output_bytes_left=8;
	input_ptr=null_string;
	output_ptr=converted_null;
	nconv = iconv(conversion_descriptor,
	              (char **)&input_ptr, &input_bytes_left,
	              &output_ptr, &output_bytes_left);
	if(nconv!=0) {
		iconv_close(conversion_descriptor);
		return ECONVERTNULLFAILED;
	}
	converted_null_length = 8-output_bytes_left;
	if(converted_null_length > output_buffer_length)
	{
		iconv_close(conversion_descriptor);
		return EOUTPUTBUFFMUCHTOOSHORT;
	}

	/* if the caller specified a invalid char substitute string convert that to the output format */
	if(substitute!=NULL) {
		output_bytes_left = sizeof(char) * strlen(substitute) * converted_null_length;
		converted_substitute = malloc(output_bytes_left);
		if(converted_substitute==NULL) {
			iconv_close(conversion_descriptor);
			return EMALLOCSUBS;
		}
		input_bytes_left = strlen(substitute);
		input_ptr=substitute;
		output_ptr=converted_substitute;
		nconv = iconv(conversion_descriptor,
		              (char**)&input_ptr, &input_bytes_left,
		              &output_ptr, &output_bytes_left );
		if(nconv!=0) {
			free(converted_substitute);
			iconv_close(conversion_descriptor);
			return ECONVERTSUBSFAILED;
		}
		converted_substitute_length = (size_t)(output_ptr - converted_substitute);
	}
	iconv_close(conversion_descriptor);

	/* open an iconv convertor for the main conversion */
	conversion_descriptor = iconv_open(output_charset, input_charset);
	if(conversion_descriptor == (iconv_t)-1) {
		if(errno == EINVAL) {
			free(converted_substitute);
			return EINPUTNOTSUPPORTED;
		} else {
			free(converted_substitute);
			return EOPENCONVERTFAILED;
		}
	}

	input_ptr = input_buffer;
	output_ptr = output_buffer;
	input_bytes_left = input_length;
	/* we tell iconv the buffer is shorter than it really is so we always have space to put a null on the end of it */
	output_bytes_left = output_buffer_length - converted_null_length;

	while(input_bytes_left > 0) {
		nconv = iconv(conversion_descriptor,
		              (char **)&input_ptr, &input_bytes_left,
		              &output_ptr, &output_bytes_left );
		if(nconv == -1) {
			if(errno == EILSEQ || errno == EINVAL) {
				/* invalid input sequence, we skip over its in the input buffer */
				local_invalid_count++;
				input_ptr++;
				input_bytes_left--;
				if(converted_substitute!=NULL) {
					/* if the caller specified a substitute string we add it here */
					if(output_bytes_left >= converted_substitute_length) {
						for(i=0; i<converted_substitute_length ; i++) {
							output_ptr[0]=converted_substitute[i];
							output_ptr++;
							output_bytes_left--;
						}
					} else {
						too_big=1;
					}
				}

			} else if (errno == E2BIG) {
				/* this happens if the output buffer if not big enough, we truncate to this point but warn the caller */
				too_big=1;
				not_conv_count=input_bytes_left;
				input_bytes_left=0;
			}
			else {
				/* some other error occured, we truncate to this point but warn the user */
				iconv_failed=1;
				not_conv_count=input_bytes_left;
				input_bytes_left=0;

			}
			errno=0;
		}
	}
	/* put a null on the end of the converted string */
	for(i=0; i<converted_null_length; i++) {
		output_ptr[0]=converted_null[i];
		output_ptr++;
	}
	if(local_invalid_count>0) {
		retval += WINVALIDCHARS;
	}

	if(invalid_count!=NULL) {
		*invalid_count = local_invalid_count;
	}

	if(too_big) {
		retval += WOUTPUTTOOSHORT;
	}

	if(unconverted_bytes!=NULL) {
		*unconverted_bytes=not_conv_count;
	}

	if(iconv_failed) {
		retval += WICONVFAIL;
	}

	if(output_bytes_used!=NULL) {
		*output_bytes_used = output_buffer_length - output_bytes_left;
	}

	if(irreversible_count!=NULL) {
		if(retval==0) {
			*irreversible_count=nconv;
		} else {
			*irreversible_count=0;
		}
	}

	free(converted_substitute);
	iconv_close(conversion_descriptor);

	return retval;
}

int utf8_cleanup(char *buff)
{
	// useful after various dirty things (snprintf/gags etc.) might have been employed.
	char *buffcpy;
	int conversion_result=-1;

	buffcpy=malloc(strlen(buff)+1);
	if(buffcpy!=NULL)
	{
		strcpy(buffcpy, buff);
		conversion_result=convert_string_charset(buffcpy, "UTF-8", strlen(buffcpy), buff, "UTF-8", strlen(buff)+1, NULL, NULL, NULL, NULL, NULL);
		free(buffcpy);
	}
	return conversion_result;
}

/* set local charset independently of locale */
int set_local_charset(const char * set)
{
    iconv_t conv;

    /* check whether our environment supports this charset */
    conv = iconv_open("UTF-8", set);
    if (conv == (iconv_t)-1) {
	return 0;
    } else {
	iconv_close(conv);
	free(local_charset);
	local_charset = strdup(set);
	return 1;
    }
}

/* query local charset */
char * get_local_charset(void)
{
    return local_charset;
}

/* change locale */
int set_locale(const char *locale)
{
    const char *set = strchr(locale, '.');
    char * oldset = NULL;
    const char * lcc = locale;

    if (set==NULL || strcmp(".", set)==0) { /* no charset? */

	/* C Locale is just ASCII */
	if (strcmp(locale, "C")==0) {
	    set = "ASCII";
	} else {
	    // locale without charset, default to utf-8
	    char * newlc = alloca(strlen(locale)+6+1);
	    strcpy(newlc, locale);
	    if (set == NULL) {
		    strcat(newlc, ".");
	    }
	    strcat(newlc, "UTF-8");
	    lcc = newlc;

	    set = "UTF-8";
	}
    } else
	set++;

    /* dunno what strdup(NULL) does so be conservative */
    oldset = get_local_charset();
    if (oldset) oldset = strdup(oldset);
    if (!set_local_charset(set)) {
	free(oldset);
	return 2;
    }

    if (setlocale(LC_ALL, lcc) == NULL) {
	if (oldset) set_local_charset(oldset);
	printf("Set locale '%s' failed, resetting charset=%s\n", lcc, oldset);
	return 3;
    }
    return 0;
}

/* wrapper around set_locale printing failure messages */
int locale_mesgs(const char * locale)
{
    int reason = set_locale(locale);
    switch (reason) {
	case 1: fprintf(stderr, "No character set specified!\n");
		break;
	case 2: printf("Character set %s not supported, sorry.\n",
			strchr(locale, '.')+1);
		break;
	case 3: fprintf(stderr, "Setting locale failed.\n");
		break;
    }

    return reason;
}

void init_locale(void)
{
    char * locale = NULL;
    char * test;
    if (local_charset) return; /* already init'd */

    if ((test=getenv("LC_CTYPE"))!=NULL) locale = test;
    if ((test=getenv("LC_ALL"))!=NULL) locale = test;
    if ((test=getenv("LANG"))!=NULL) locale = test;

    if (locale == NULL)
	/* fall back to ASCII */
	set_local_charset("ASCII");
    else
	if (locale_mesgs(locale)) {
	    fprintf(stderr, "Please change your locale environment (e.g. LANG) to something which this system supports.\n");
	    exit(1);
	} else {
	    /* check this environment supports UTF-8 */
	    iconv_t conv = iconv_open("ASCII", "UTF-8");
	    if (conv == (iconv_t)-1) {
		fprintf(stderr, "This environment doesn't seem to support UTF-8. Milliways requires UTF-8 support to work.\n");
		exit(1);
	    } else
		iconv_close(conv);
	}
}
