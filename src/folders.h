#ifndef FOLDERS_H
#define FOLDERS_H

#include <stdint.h>

#define FOLNAMESIZE 	10	/* length of folder names */
#define TOPICSIZE 	30	/* length of the topic of the folder */

struct folder
{
	unsigned char status; /* status for people not in the same group */
	char name[FOLNAMESIZE+1];
	char topic[TOPICSIZE+1];
	int32_t first; /* Ref no of first message in the folder */
	int32_t last; /* Ref no of last message in the folder */
	char g_status; /* status for people in the same group */
	char groups; /* which groups g_status applies to */
	char spare[10];
};

/* folder.status
bit 	use
0	active
1	read by unregistered
2	wrte by unregistered
3	read by registered
4	wrte by registered
5	private folder
6	moderated folder
7	-
*/
#define MWFOLDR_ACTIVE    0
#define MWFOLDR_RUNREG    1
#define MWFOLDR_WUNREG    2
#define MWFOLDR_RREG      3
#define MWFOLDR_WREG      4
#define MWFOLDR_PRIVATE   5
#define MWFOLDR_MODERATED 6
/* Dummy for limit checking */
#define MWFOLDR_SIZE      7

#define f_active(stat)    (stat & (1 << MWFOLDR_ACTIVE))
#define f_r_unreg(stat)   (stat & (1 << MWFOLDR_RUNREG))
#define f_w_unreg(stat)   (stat & (1 << MWFOLDR_WUNREG))
#define f_r_reg(stat)     (stat & (1 << MWFOLDR_RREG))
#define f_w_reg(stat)     (stat & (1 << MWFOLDR_WREG))
#define f_private(stat)   (stat & (1 << MWFOLDR_PRIVATE))
#define f_moderated(stat) (stat & (1 << MWFOLDR_MODERATED))

#define FOL_IDX_DIR   STATEDIR"/index"
#define FOL_TXT_DIR   STATEDIR"/text"
/* Subdirectories of TXT_DIR and IDX_DIR */
/* Use these for the *_type arguments to the functions below */
#define FOL_LIVE      NULL
#define FOL_MOD       "mod"
#define FOL_NEW       "new"
#define FOL_TMP       "tmp"

extern const char *folder_types[4];

int openfolderfile(int mode);
int nofolders(void);
int create_folder_file(void);
int foldernumber(const char *name);
int get_folder_entry(int file, struct folder *tmp);
int get_folder_number(struct folder *fol, int num);
int folders_init(void);
int open_folder_index(struct folder *folder, const char *fol_type, int flags, int mode);
int open_folder_text(struct folder *folder, const char *fol_type, int flags, int mode);
int rename_folder(struct folder *folder, const char *newname);
int remove_folder(struct folder *folder, const char *fol_type);
int move_folder(struct folder *folder, const char *from_type, const char *to_type);

#endif /* FOLDERS_H */
