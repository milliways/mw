#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "sqlite.h"
#include <sqlite3.h>

static sqlite3* db_open(const char *dbname)
{
	sqlite3 *db;
	int error = sqlite3_open(dbname, &db);

	if (error != SQLITE_OK) {
		printf("Error opening db %s: %s\n", dbname, sqlite3_errmsg(db));
		return NULL;
	}
	sqlite3_busy_timeout(db, 500);
	return db;
}

static void db_close(sqlite3 *db)
{
	sqlite3_close(db);
}

static int db_callback(void *ptr, int ncols, char **row, char **cols)
{
	struct db_result *query = ptr;
	struct db_data *ndata;
	int i;
	unsigned int j;

	if (query == NULL) return 1;

	if (query->colNames == NULL) {
		/* this is our first time */
		query->colNames = calloc(ncols+1, sizeof(char *));
		query->data = NULL;
		query->rows = 0;
		query->cols = ncols;

		/* copy the column names */
		for (i=0;i<ncols;i++) {
			query->colNames[i] = strdup(cols[i]);
			for (j=0;j<strlen(query->colNames[i]);j++)
				if (query->colNames[i][j] == '.') query->colNames[i][j]='_';
		}
		query->colNames[ncols] = NULL;

		/* fill in more */
	}
	query->rows++;

	/* Copy the data in and attach it */
	ndata = malloc(sizeof(*ndata));
	ndata->field = calloc(ncols+1, sizeof(char *));
	for (i=0;i<ncols;i++) {
		if(row[i] != NULL) {
			ndata->field[i] = strdup(row[i]);
		} else {
			ndata->field[i] = NULL;
		}
	}
	ndata->row = query->rows - 1;
	ndata->next = query->data;
	query->data = ndata;

	return 0;
}

struct db_result *db_query(const char *dbname, const char *query, int quiet)
{
	struct db_result *new;
	int ret;
	char *error = NULL;
	sqlite3 *db;
	int retries = 5;

	if (dbname == NULL) return 0;

	db = db_open(dbname);
	if (!db) {
		return NULL;
	}

	new = malloc(sizeof(*new));
	if (new == NULL)
		return NULL;
	new->rows = -1;
	new->cols = -1;
	new->colNames = NULL;
	new->data = NULL;

	do {
		ret = sqlite3_exec(db, query, db_callback, new, &error);
		if (ret == SQLITE_BUSY) {
			usleep(1000000/retries);
			retries--;
		}
	} while (ret == SQLITE_BUSY && retries > 0);

	if (ret != SQLITE_OK && ret != SQLITE_BUSY) {
		if (!quiet) {
			switch (ret) {
				case SQLITE_BUSY: printf("BUSY "); break;
				case SQLITE_DONE: printf("DONE "); break;
				case SQLITE_ERROR: printf("ERROR "); break;
				case SQLITE_MISUSE: printf("MISUSE "); break;
				case SQLITE_ROW: printf("ROW "); break;
			}

			printf("Error %s (%d) on query %s\n", error, ret, query);
		}
		sqlite3_free(error);
		db_close(db);
		free(new);
		return NULL;
	}

	db_close(db);
	return new;
}

/* how many rows in that result */
static int db_numrows(struct db_result *result)
{
	if (result == NULL) return 0;
	return result->rows;
}

char * db_item(struct db_result *result, int row, const char *field)
{
	int i;
	int col=-1;
	struct db_data *ndata;

	if (result == NULL) return NULL;
	if (row < 0 || row >= result->rows) return NULL;

	col = -1;
	for(i=0;i<result->cols;i++) {
		if (strcasecmp(field, result->colNames[i])==0)
			col = i;
	}
	if (col == -1) return NULL;

	/* find the row */
	ndata = result->data;
	while (ndata != NULL && ndata->row != row) ndata=ndata->next;
	if (ndata == NULL)  return NULL;

	return ndata->field[col];
}

void db_free(struct db_result *result)
{
	struct db_data *ndata, *nnext;
	int i;

	if (result == NULL) {
		return;
	}

	/* throw away the field names */
	for (i=0;i<result->cols;i++) {
		free(result->colNames[i]);
	}
	free(result->colNames);
	/* throw away the returned rows */
	ndata = result->data;
	while (ndata != NULL) {
		for (i=0;i<result->cols;i++) {
			if(ndata->field[i]) {
				free(ndata->field[i]);
			}
		}
		free(ndata->field);
		nnext = ndata->next;
		free(ndata);
		ndata = nnext;
	}
	/* make sure it cant be used after free */
	result->data = NULL;
	result->cols = 0;
	result->rows = 0;
	result->colNames = 0;
	free(result);
}

char * db_room_get(int room, const char *opt)
{
	struct db_result *qres;
	char *value;
	char *out = NULL;
	char *query = sqlite3_mprintf("SELECT * from "USERDB_ROOMS" WHERE room=%d AND opt=%Q", room, opt);
	qres = db_query(USERSQL, query, 1);
	sqlite3_free(query);

	if (qres == NULL) return NULL;
	if (db_numrows(qres) < 1) goto out_dbfree;

	if ((value = db_item(qres, 0, "arg"))==NULL)
		goto out_dbfree;

	out = strdup(value);
out_dbfree:
	db_free(qres);
	return out;
}

void db_room_set(int room, const char *opt, const char *arg)
{
	struct db_result *qres;
	char *query = sqlite3_mprintf("SELECT * from "USERDB_ROOMS" WHERE room=%d AND opt=%Q", room, opt);
	qres = db_query(USERSQL, query, 1);
	sqlite3_free(query);

	/* set a new value */
	struct db_result *res;
	if (arg==NULL || *arg==0) {
		char *q= sqlite3_mprintf("DELETE FROM "USERDB_ROOMS" WHERE room=%d AND opt=%Q", room, opt);
		db_free(db_query(USERSQL, q, 1));
		sqlite3_free(q);
	}else
	if (qres != NULL && qres->rows > 0) {
		char *q= sqlite3_mprintf("UPDATE "USERDB_ROOMS" SET arg=%Q WHERE room=%d AND opt=%Q", arg, room, opt);
		db_free(db_query(USERSQL, q, 1));
		sqlite3_free(q);
	} else {
		char *q= sqlite3_mprintf("INSERT INTO "USERDB_ROOMS" (room, opt, arg) VALUES (%d,%Q,%Q)", room, opt, arg);
		res = db_query(USERSQL, q, 1);
		if (res == NULL) {
			/* it failed, try to create the table */
			const char *create = "CREATE TABLE "USERDB_ROOMS" (room integer, opt text, arg text)";
			res = db_query(USERSQL, create, 0);
			if (res != NULL) {
				/* it worked, resubmit the insert */
				db_free(res);
				res = db_query(USERSQL, q, 0);
			}
		}
		db_free(res);
		sqlite3_free(q);
	}
	db_free(qres);
}
