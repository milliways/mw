#ifndef BB_H
#define BB_H

/* bb.h
 * Miscellaneous structures/constants.
 */


#ifndef HOMEPATH
#define HOMEPATH	"/usr/local/lib/mw"
#endif

#ifndef STATEDIR
#define STATEDIR	"/var/lib/mw"
#endif

#ifndef MSGDIR
#define MSGDIR		"/run/mw"
#endif

#ifndef SYSCONFDIR
#define SYSCONFDIR "/etc"
#endif

#define LOGIN_BANNER    HOMEPATH"/login.banner"
#define LOCKFILE	"/tmp/bb.locked"
#define EDITOR		"/usr/bin/vim"
#define SECUREEDITOR	"/bin/rnano"

#define HELPDIR         HOMEPATH"/help"
#define WIZHELP         HOMEPATH"/wizhelp"
#define SCRIPTHELP      HOMEPATH"/scripthelp"
#define TALKHELP        HOMEPATH"/talkhelp"

#define TEXT_END	".t"
#define INDEX_END	".i"
#define MOD_END		".m"

#define CMD_BOARD_STR	"!"
#define CMD_TALK_STR	"."
#define CMD_SCRIPT_STR	","

#define MAXTEXTLENGTH	2048	/* text buffer size */
#define MAXPIPELENGTH	4096	/* maximum length of a pipe message */

#define RUNAWAY		1200
#define MAXRUNAWAY	5000

#define RIGHTS_BBS	0
#define RIGHTS_TALK	1

#define PACKAGE	"mw"
#define LOCALEDIR HOMEPATH"/locale"

#endif /* BB_H */
