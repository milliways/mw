#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <regex.h>
#include <pthread.h>
#include <sqlite3.h>
#include <openssl/md5.h>
#include <curl/curl.h>

#include <sqlite.h>
#include "str_util.h"
#include "js.h"
#include "log.h"
#include "uri.h"
#include "user.h"

extern struct user * const user;
static void *file_url(void * data);

/*
 * Check whats said for URLs and log them
 */

struct uripatt {
	const char *regex;
	int   	regflags; /* compile flags */
	regex_t	*patt; /* compiled version */
	enum { END=0, URL, FLAG, IGNORE, NOLOG } type;
	uint32_t flags;
};


struct uripatt urilist[] = {
	{"^http://$", 		REG_ICASE, NULL, IGNORE, 0},
	{"^https://$", 		REG_ICASE, NULL, IGNORE, 0},
	{"^sucs.org$",  	REG_ICASE, NULL, IGNORE, 0}, /* Ignore the SUCS homepage, "sucs.org" rarely refers to the website and we can presume people know it */
	{"^http://", 		REG_ICASE, NULL, URL,    0},
	{"^https://", 		REG_ICASE, NULL, URL,    0},
	{"^www\\.", 		REG_ICASE, NULL, URL,    0},
	{"^[[:alnum:]_.-]+\\.(com|org|net|gov|(co|org|net|gov)\\.uk)$", 	REG_ICASE|REG_EXTENDED, NULL, URL,    0},
	{"^[[:alnum:]_.-]+\\.(com|org|net|gov|(co|org|net|gov)\\.uk)/", 	REG_ICASE|REG_EXTENDED, NULL, URL,    0},
	{"[+-]nsfw", 		REG_ICASE, NULL, FLAG,   URLFLAG_NSFW},
	{"(nsfw)", 		REG_ICASE, NULL, FLAG,   URLFLAG_NSFW},
	{"[+-]nsfl", 		REG_ICASE, NULL, FLAG,   URLFLAG_NSFW},
	{"^[+-]anon$", 		REG_ICASE, NULL, FLAG,   URLFLAG_ANON},
	{"^(anon)$", 		REG_ICASE, NULL, FLAG,   URLFLAG_ANON},
	{"^[+-]coward$", 	REG_ICASE, NULL, FLAG,   URLFLAG_ANON},
	{"^(coward)$", 		REG_ICASE, NULL, FLAG,   URLFLAG_ANON},
	{"^[+-]sucs$", 		REG_ICASE, NULL, FLAG,   URLFLAG_SUCS},
	{"^(sucs)$", 		REG_ICASE, NULL, FLAG,   URLFLAG_SUCS},
	{"^[+-]member",		REG_ICASE, NULL, FLAG,   URLFLAG_SUCS},
	{"^-log", 		REG_ICASE, NULL, NOLOG, 0},
	{"^(-log)$", 		REG_ICASE, NULL, NOLOG, 0},
	{"^(nolog)$", 		REG_ICASE, NULL, NOLOG, 0},
	{ NULL, 0, NULL, END, 0 }
};

struct urihit {
	char * url;
	uint32_t flags;
};

void catchuri(const char *what)
{
	char *text = strip_colours(what);
	char *token;
	/* kludge, find at most 20 URLs */
	char *foundurl[20];
	int nfoundurl=0;
	uint32_t flags = 0;

	/* split the line into words on whitespace */
	for (token = strtok(text, " \t"); token != NULL; token=strtok(NULL, " \t")) {
		struct uripatt *u = urilist;
		/* compare the words again the regex list */
		while (u!=NULL && u->regex!=NULL && u->type != END) {
			/* compile the pattern, first time only */
			if (u->patt == NULL) {
				int err;
				u->patt = malloc(sizeof(regex_t));
				if ((err=regcomp(u->patt, u->regex, u->regflags))) {
					char errmsg[128];
					regerror(err, u->patt, errmsg,128);
					printf("%s:%d %s\n", __FILE__, __LINE__, errmsg);
					continue;
				}
			}
			/* compare the pattern and store a hit */
			if (regexec(u->patt, token, 0, NULL, 0) == 0) {
				if (u->type == IGNORE) {
					/* skip this word */
					break;
				}
				if (u->type == URL) {
					/* found a url */
					/*printf("Found url: '%s'\n", token); */
					if (nfoundurl < 20) {
						if(nfoundurl == 0 || foundurl[nfoundurl - 1] != token) {
							foundurl[nfoundurl++] = token;
						}
					}
				}
				if (u->type == FLAG) {
					/* printf("Found flag: '%s' -> %x\n", token, u->flags); */
					flags |= u->flags;
				}
				if (u->type == NOLOG) {
					/* ignore whole line */
					/* printf("IGNORE LINE '%s'\n", token); */
					free(text);
					return;
				}
			}
			u++;
		}
	}

	/*
	 * we found urls, so launch background threads to
	 * categorise and file the details
	 * cant do it in foreground, would stall the ui
	 */
	if (nfoundurl>0) {
		int i;
		pthread_attr_t ptattr;
		pthread_attr_init(&ptattr);
		pthread_attr_setdetachstate(&ptattr, PTHREAD_CREATE_DETACHED);
		for (i=0;i<nfoundurl;i++) {
			struct urihit *uri;
			pthread_t pt;
			uri = malloc(sizeof(struct urihit));
			uri->url = strdup(foundurl[i]);
			uri->flags = flags;
			pthread_create(&pt, &ptattr, file_url, uri);
		}
		pthread_attr_destroy(&ptattr);
	}
	free(text);
}

static char * cleanup_url(const char *in)
{
	char *out;
	const char *p;
	if (in == NULL) return NULL;
	out = malloc(strlen(in)+10);
	out[0]=0;

	if ((p=strstr(in, "://")) == NULL) {
		strcpy(out, "http://");
		p = in;
	} else {
		p += 3;
	}
	strcat(out, in);
	if (strchr(p, '/')==NULL) {
		strcat(out, "/");
	}
	return out;
}

static char *md5(const char *in)
{
	int i;
	char *out;
	unsigned char msg[16];
	MD5((unsigned char *)in,strlen(in),msg);
	out = malloc(33);
	out[0]=0;
	for(i=0;i<16;i++)
		snprintf(&out[strlen(out)],3,"%02x", msg[i]);
	return out;
}

static char * extract_title(const char *body)
{
	char *p,*q;
	char *out;
	int len;
	if ((p=strcasestr(body, "<title>"))==NULL) return NULL;
	p += 7;
	if ((q=strcasestr(p, "</title>"))==NULL) return NULL;
	len = q - p;
	out = malloc(len+1);
	memcpy(out, p, len);
	out[len]=0;
	return out;
}

/* buffer up the headers and parse them on the way
 * if you find a bad content type then abort the connection
 */
static size_t headlimit(void *ptr, size_t size, size_t nmemb, void *stream)
{
	int addsize = size*nmemb;
	struct block_t *b = stream;

	if (stream == NULL) return 0;
	int done = block_append(b, ptr, addsize);

	/* given too much header, choke on it */
	if (done < addsize) return done;

	/* look for a whole Content-type */
	char *p = strcasestr(b->p_buffer, "Content-Type: ");
	if (p) {
		char *end = strchr(p+14, '\r');
		if (end == NULL) end = strchr(p+14, '\n');
		/* we have the entire content-type line, parse it */
		if (end != NULL) {
			if (strncasecmp(p+14, "text/html",9)!=0) {
				/* not html, skip the rest & body*/
				return 0;
			}
		}
	}
	return done;
}

/* Receive data from curl into a malloced memory chunk */
static size_t urldata(void *ptr, size_t size, size_t nmemb, void *stream)
{
	int addsize = size*nmemb;
	struct block_t *b = stream;

	if (stream == NULL) return 0;
	return block_append(b, ptr, addsize);
}

static void *file_url(void * data)
{
	struct urihit *uri = data;
	char path[1024];
	struct db_result *res;
	char *url;
	CURL *c;
	char *md = NULL;
	char *title=NULL;
	char flags[1024];
	struct block_t * deli=block_new(1024);
	struct block_t * body=block_new(1024);
	struct block_t * head=block_new(4096);

	c = curl_easy_init();

	/* set max download sizes */
	block_limit(deli, 8192);
	block_limit(body, 8192);
	block_limit(head, 4096);

	/* lets go fishing on del.icio.us */
	url = cleanup_url(uri->url);
	md = md5(url);
	snprintf(path,sizeof(path), "http://feeds.delicious.com/v2/json/urlinfo/%s", md);
	free(md);
	curl_easy_setopt(c, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(c, CURLOPT_URL, path);
	curl_easy_setopt(c, CURLOPT_NOPROGRESS, 1);
	curl_easy_setopt(c, CURLOPT_AUTOREFERER, 1);
	curl_easy_setopt(c, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(c, CURLOPT_MAXREDIRS, 5);
	curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, urldata);
	curl_easy_setopt(c, CURLOPT_WRITEDATA, deli);
	curl_easy_perform(c);

	if (deli->i_used < 2) {
		block_free(deli);
		deli=NULL;
	}

	/* this means, they havent heard of it */
	if (deli != NULL && deli->i_used > 2 && strncmp(deli->p_buffer,"[]",2)==0) {
		block_free(deli);
		deli=NULL;
	}

	/* grab the pages title */
	curl_easy_setopt(c, CURLOPT_URL, url);
	curl_easy_setopt(c, CURLOPT_WRITEDATA, body);
	curl_easy_setopt(c, CURLOPT_HEADERFUNCTION, headlimit);
	curl_easy_setopt(c, CURLOPT_WRITEHEADER, head);
	curl_easy_perform(c);

	if (body->i_used > 0) {
		title = extract_title(body->p_buffer);
		if (title != NULL && *title == 0) {
			free(title);
			title=NULL;
		}
	}
	block_free(body);
	block_free(head);

	/* fishing mission complete, store the results */

	uri_make_flags_str(uri->flags, flags, sizeof(flags));

	char *query = sqlite3_mprintf(
			"INSERT INTO mwuri (user, url, added, flags, title, tags) "
			"VALUES (%Q,%Q,datetime('now'),%Q,%Q,%Q)",
			user->record.name, url, flags, title, deli==NULL?NULL:deli->p_buffer);
	res = db_query(MWURI_DB, query, 1);
	if (res == NULL) {
		res = db_query(MWURI_DB, "CREATE TABLE mwuri "
				"(id INTEGER PRIMARY KEY AUTOINCREMENT, "
				"user TEXT, url TEXT, added TEXT, flags TEXT, "
				"title TEXT, tags TEXT)", 0);
		if (res != NULL) {
			db_free(res);
			res = db_query(MWURI_DB, query, 0);
		}
	}
	db_free(res);
	sqlite3_free(query);
	free(url);
	if (title) free(title);
	if (deli) block_free(deli);
	curl_easy_cleanup(c);
	free(uri->url);
	free(uri); /* aka data */
	return NULL;
}

/* store the doing/status string in the db */
void catchdoing(const char *what)
{
	struct db_result *res;

	char *query = sqlite3_mprintf("INSERT INTO mwdoing (user, added, doing) "
			"VALUES (%Q,datetime('now'),%Q)", user->record.name, what);
	res = db_query(MWURI_DB, query, 1);
	if (res == NULL) {
		res = db_query(MWURI_DB, "CREATE TABLE mwdoing (id INTEGER PRIMARY KEY AUTOINCREMENT, user text, added text, doing text )", 0);
		if (res != NULL) {
			db_free(res);
			res = db_query(MWURI_DB, query, 0);
		}
	}
	db_free(res);
	sqlite3_free(query);
}


/* block handling functions */
struct block_t * block_new(int size)
{
	struct block_t *new = calloc(1, sizeof(struct block_t));
	new->p_buffer = calloc(1, size);
	new->i_size = size;
	new->i_used = 0;
	new->p_cursor = new->p_buffer;
	new->i_limit = 0;

	return new;
}

void block_limit(struct block_t *p, int limit)
{
	if (p == NULL) return;
	p->i_limit = limit;
}

void block_free(struct block_t *p)
{
	if (p==NULL) return;
	if (p->p_buffer) free(p->p_buffer);
	p->p_buffer = NULL;
	p->i_size = 0;
	free(p);
}

void block_resize(struct block_t *old, int size)
{
	int offset;

	if (old == NULL) return;
	offset = old->p_cursor - old->p_buffer;
	if (old->i_limit > 0 && size > old->i_limit) size = old->i_limit;
	if (size > old->i_size) {
		old->p_buffer = realloc(old->p_buffer, size);
		old->i_size = size;
		old->p_cursor = old->p_buffer + offset;
	}
}

int block_append(struct block_t *p, void *data, int size)
{
	if (p == NULL) return 0;
	/* buffers are always kept nul terminated */
	int newsize = p->i_used + size + 1;
	/* it needs to be bigger grow it */
	if (newsize > p->i_size) {
		block_resize(p, p->i_used + size + 1);
		/* it didnt grow enough, limit the copy */
		if (p->i_size < newsize)
			size = p->i_size - p->i_used - 1;
	}
	memcpy(&p->p_buffer[p->i_used], data, size);
	p->i_used += size;
	/* terminate it for safe string searches */
	p->p_buffer[p->i_used] = 0;
	return size;
}
