#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <sqlite.h>
#include "sqlite.h"

static sqlite3* db_open(const char *dbname)
{
	sqlite3 *db;
	int error = sqlite3_open(dbname, &db);

	if (error != SQLITE_OK) {
		printf("Error opening db %s: %s\n", dbname, sqlite3_errmsg(db));
		return NULL;
	}
	sqlite3_busy_timeout(db, 500);
	return db;
}

static void db_close(sqlite3 *db)
{
	sqlite3_close(db);
}

static int db_callback(void *ptr, int ncols, char **row, char **cols)
{
	struct db_result *query = ptr;
	struct db_data *ndata;
	int i;
	unsigned int j;

	if (query == NULL) return 1;

	if (query->colNames == NULL) {
		/* this is our first time */
		query->colNames = calloc(ncols+1, sizeof(char *));
		query->data = NULL;
		query->rows = 0;
		query->cols = ncols;

		/* copy the column names */
		for (i=0;i<ncols;i++) {
			query->colNames[i] = strdup(cols[i]);
			for (j=0;j<strlen(query->colNames[i]);j++)
				if (query->colNames[i][j] == '.') query->colNames[i][j]='_';
		}
		query->colNames[ncols] = NULL;

		/* fill in more */
	}
	query->rows++;

	/* Copy the data in and attach it */
	ndata = malloc(sizeof(*ndata));
	ndata->field = calloc(ncols+1, sizeof(char *));
	for (i=0;i<ncols;i++) {
		if(row[i] != NULL) {
			ndata->field[i] = strdup(row[i]);
		} else {
			ndata->field[i] = NULL;
		}
	}
	ndata->row = query->rows - 1;
	ndata->next = query->data;
	query->data = ndata;

	return 0;
}

struct js_db_result* js_db_query(char *dbname, const char *query)
{
	struct js_db_result *new;

	sqlite3 *db;
	new = malloc(sizeof(struct js_db_result));

	if(new == NULL) {
		return NULL;
	}

	new->error_text=NULL;
	new->query_error=0;
	new->db_error=0;


	if (dbname == NULL) {
		new->error_text=strdup("Database name was NULL");
		new->db_error=-1;
		return new;
	}


	db = db_open(dbname);
	if (!db) {
		new->error_text=strdup("Failed to open database");
		new->db_error=-2;
		return new;
	}

	new->query_result = malloc(sizeof(struct db_result));
	if(new->query_result == NULL) {
		new->error_text=strdup("malloc error for query result");
		new->db_error=-3;
		db_close(db);
		return new;
	}

	new->query_result->rows = -1;
	new->query_result->cols = -1;
	new->query_result->colNames = NULL;
	new->query_result->data = NULL;

	new->query_error = sqlite3_exec(db, query, db_callback, new->query_result, &new->error_text);

	db_close(db);
	return new;
}

/* how many rows in that result */
static int db_numrows(struct db_result *result)
{
	if (result == NULL) return 0;
	return result->rows;
}

void js_db_free(struct js_db_result *result)
{
	if(result->query_result!=NULL) {
		db_free(result->query_result);
	}
	if(result->error_text!=NULL) {
		sqlite3_free(result->error_text);
	}
	free(result);
}

char * userdb_get(const char *table, const char *user, const char *opt)
{
	struct db_result *old;
	char *value;
	char *out = NULL;

	char *query = sqlite3_mprintf("SELECT * from %Q WHERE user=%Q AND opt=%Q", table, user, opt);
	old  = db_query(USERSQL, query, 1);
	sqlite3_free(query);

	if (old == NULL) return NULL;
	if (db_numrows(old) < 1) goto out_dbfree;

	if ((value = db_item(old, 0, "arg"))==NULL)
		goto out_dbfree;

	out = strdup(value);
out_dbfree:
	db_free(old);
	return out;
}

int userdb_key_exists(const char *table, const char *user, const char *key)
{
	struct db_result *result;
	char *query;
	int nrows;

	query = sqlite3_mprintf("SELECT * from %Q WHERE user=%Q AND opt=%Q", table, user, key);
	result  = db_query(USERSQL, query, 1);
	sqlite3_free(query);

	if (result == NULL)
		return 0;
	nrows = db_numrows(result);
	db_free(result);
	if (nrows < 1)
		return 0;
	return 1;
}
void userdb_set(const char *table, const char *user, const char *opt, const char *arg)
{
	struct db_result *old;

	char *query = sqlite3_mprintf("SELECT * from %Q WHERE user=%Q AND opt=%Q", table, user, opt);
	old  = db_query(USERSQL, query, 1);
	sqlite3_free(query);

	/* set a new value */
	struct db_result *res;
	if (arg==NULL || *arg==0) {
		char *q= sqlite3_mprintf("DELETE FROM %q WHERE user=%Q AND opt=%Q", table, user, opt);
		db_free(db_query(USERSQL, q, 1));
		sqlite3_free(q);
	}else
	if (old != NULL && old->rows > 0) {
		char *q= sqlite3_mprintf("UPDATE %q SET arg=%Q WHERE user=%Q AND opt=%Q", table, arg, user, opt);
		db_free(db_query(USERSQL, q, 1));
		sqlite3_free(q);
	} else {
		char *q= sqlite3_mprintf("INSERT INTO %q (user, opt, arg) VALUES (%Q,%Q,%Q)", table, user, opt, arg);
		res = db_query(USERSQL, q, 1);
		if (res == NULL) {
			/* it failed, try to create the table */
			char *create = sqlite3_mprintf("CREATE TABLE %q (user text, opt text, arg text)", table);
			res = db_query(USERSQL, create, 0);
			sqlite3_free(create);
			if (res != NULL) {
				/* it worked, resubmit the insert */
				db_free(res);
				res = db_query(USERSQL, q, 0);
			}
		}
		db_free(res);
		sqlite3_free(q);
	}
	db_free(old);
}
