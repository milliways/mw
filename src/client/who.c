/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <stdbool.h>

#include "talker_privs.h"
#include "special.h"
#include "strings.h"
#include "str_util.h"
#include "ipc.h"
#include "perms.h"
#include "talker.h"
#include "userio.h"
#include "who.h"
#include "main.h"
#include <str_util.h>
#include <util.h>

extern int busy;
extern struct user * const user;

char *itime(unsigned long t)
{
	static char out[20];

	if (t>=3600*48) sprintf(out,"%2ldd%2ldh",t/86400, (t%86400)/3600);
	else
	if (t>=3600) sprintf(out,"%2ldh%2ldm",t/3600,(t%3600)/60);
	else
	if (t>=60) sprintf(out,"%2ldm%2lds",t/60,t%60);
	else
	sprintf(out,"%5lds",t);
	return(out);
}

static json_t * whoinfo = NULL;
static time_t   whowhen = 0;
static int      whotype = -1;
#define WHOCACHE 120  /* cache who data for this many seconds */
#define NO_CALLBACK -1

/* check if we need to refresh the wholist
 * and request it if we do 
 * report if we did
 */
static int refresh_wholist(int type)
{
	time_t now = time(0);
	if (whoinfo == NULL || whowhen < (now - WHOCACHE)) {
		whotype = type;
		ipc_message_t * msg = ipcmsg_create(IPC_WHOLIST, user->posn);
		ipcmsg_transmit(msg);
		return 1;
	}
	return 0;
}

/* If we don't get the who list within the timeout, fail */
#define WHOINFO_TIMEOUT 10000
#define WHOINFO_CHECK_INTERVAL (WHOINFO_TIMEOUT / 250)
int require_whoinfo(void)
{
	int n = 0;

	while (whoinfo == NULL && n < WHOINFO_TIMEOUT) {
		idle(-1, WHOINFO_CHECK_INTERVAL);
		n += WHOINFO_CHECK_INTERVAL;
	}
	if (whoinfo == NULL)
		return 1;
	return 0;
}

void display_wholist(int mode)
{
	time_t now = time(NULL);

	/* we dont have a recent cache of who list,
	 * request one, and get called back when it arrives */
	if (refresh_wholist(mode)) {
		return;
	}

	busy++;
	if (mode == 1)
		format_message("\nUser status...");
	else
		format_message("\n %-*s %-*s  Idle  What...",NAMESIZE,"Name",20,"RealName");

	_autofree char *divider = NULL;
	for (int i = 0; i < screen_w(); i++) string_add(&divider, "-");
	format_message("%s", divider);

	size_t wi;
	json_t *entry;
	json_array_foreach(whoinfo, wi, entry) {
		const char *name = json_getstring(entry, "name");
		const char *realname = json_getstring(entry, "realname");
		time_t dowhen = json_getint(entry, "dowhen");
		const char * doing = json_getstring(entry, "doing");
		time_t idletime = json_getint(entry, "idletime");
		int channel = json_getint(entry, "channel");
		json_t *perms = json_object_get(entry, "perms");

		if (mode == 1) {
			if (dowhen && doing!=NULL && doing[0] != 0)
				format_message("%s %s\033-- (%s ago)",name, doing ,itime(time(0)-dowhen));
		} else {
			char chat=' ';
			//const char *status = json_getstring(perms, "status");
			const char *chatmode = json_getstring(perms, "chatmode");
			const char *chatprivs = json_getstring(perms, "chatprivs");
			const char *prot = json_getstring(perms, "protection");

			if (json_getint(perms, "wizchat") > 0) chat='*';

			format_message("%c%-*s %-30.30s %6s Room %d (%s,%s,%s)\033--",chat,NAMESIZE,name,realname,itime(now-idletime),channel, chatmode, prot, chatprivs);
		}
	}
	format_message("%s", divider);
	busy--;
}

void update_wholist(ipc_message_t *msg)
{
	/* there is an old one, clear it */
	if (whoinfo != NULL) {
		json_decref(whoinfo);
	}

	/* store the new one */
	whoinfo = json_init(msg);
	whowhen = time(NULL);

	/* we were waiting for one, show it */
	if (whotype >= 0) {
		display_wholist(whotype);
		whotype = -1;
	}
}

char *part_who(const char *text, int status)
{
	static int len = 0;
	static int index = 0; /* looking for the Nth answer */

	/* we dont have a recent cache of who list,
	 * ask for an update, then do the best you can */
	refresh_wholist(NO_CALLBACK);

	/* we dont even have stale info */
	if (whoinfo == NULL) return(NULL);

	/* start a new search */
	if (status == 0) {
		len = strlen(text);
		index = 0;
	}

	size_t wi;
	json_t *entry;
	int count = 0;
	json_array_foreach(whoinfo, wi, entry) {
		const char *name = json_getstring(entry, "name");
		if (name == NULL) continue;
		if (strncasecmp(name, text, len)==0) {
			/* found the one we want */
			if (count == index) {
				index++;
				return strdup(name);
			}
			/* found one we dont want */
			count++;
		}
	}
	/* run out of answers */
	return NULL;
}

char *part_who_talk(const char *text, int status)
{
	static int len = 0;
	static int index = 0; /* looking for the Nth answer */

	/* we dont have a recent cache of who list,
	 * ask for an update, then do the best you can */
	refresh_wholist(NO_CALLBACK);

	/* we dont even have stale info */
	if (whoinfo == NULL) return(NULL);

	/* start a new search */
	if (status == 0) {
		len = strlen(text);
		index = 0;
	}

	size_t wi;
	json_t *entry;
	int count = 0;
	json_array_foreach(whoinfo, wi, entry) {
		const char *name = json_getstring(entry, "name");
		json_t * perms = json_object_get(entry, "perms");
		const char *chatmode = NULL;
		if (perms != NULL) chatmode=json_getstring(perms, "chatmode");

		if (name == NULL) continue;
		if (chatmode == NULL) continue;

		/* isnt on chat */
		if (strchr(chatmode, 'c')==NULL) continue;

		if (strncasecmp(name, text, len)==0) {
			/* found the one we want */
			if (count == index) {
				index++;
				return strdup(name);
			}
			/* found one we dont want */
			count++;
		}
	}
	/* run out of answers */
	return NULL;
}

/* grab a handle to the current wholist
 * you need to json_decref it when finished
 */
json_t * grab_wholist(void)
{
	/* we dont have a recent cache of who list,
	 * ask for an update, then do the best you can */
	refresh_wholist(NO_CALLBACK);

	if (whoinfo != NULL) json_incref(whoinfo);
	return whoinfo;
}

int32_t who_find(const char *username)
{
	json_t *wlist = grab_wholist();
	if (wlist == NULL) return -1;

	size_t wi;
	json_t *entry;
	json_array_foreach(wlist, wi, entry) {
		const char *name = json_getstring(entry, "name");
		int32_t uid = json_getint(entry, "id");
		if (strcasecmp(username, name)==0) {
			json_decref(wlist);
			return uid;
		}
	}
	json_decref(wlist);
	return -1;
}
