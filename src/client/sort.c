/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/

#include <stdio.h>
#include "sort.h"

static int IsBigger(struct listing *a, struct listing *b)
{
	if (a->size < b->size) return(1); else return(0);
}

static struct listing *Insert(struct listing *a, struct listing *b)
{
	struct listing *c=a;
	struct listing *p=NULL;

	while(c&&IsBigger(b,c))
	{
		p=c;
		c=c->next;
	}

	if(c==a)
	{
		b->next=c;
		return(b);
	}
	p->next=b;
	b->next=c;
	return(a);
}

struct listing *Sort(struct listing *head)
{
	struct listing *new=NULL;
	struct listing *this=head;
	struct listing *next;
	while(this)
	{
		next=this->next;
		new=Insert(new,this);
		this=next;
	}
	return(new);
}
