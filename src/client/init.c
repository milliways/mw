/*
 * Init file handling
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pwd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <errno.h>
#include <curl/curl.h>

#include <sqlite.h>
#include "sqlite.h"
#include "intl.h"
#include "alias.h"
#include "js.h"
#include "init.h"
#include "user.h"
#include "script.h"
#include "frl.h"

extern struct user * const user;

/* drop and restore user level privs */
static int private_myuid = -1;
static int private_mygid = -1;

int perms_drop(void)
{
	private_myuid=geteuid();
	private_mygid=getegid();
	if (seteuid(getuid()) == -1) return -1;
	if (setegid(getgid()) == -1) return -1;
	return private_myuid;
}

void perms_restore(void)
{
	if (private_myuid != -1) {
		seteuid(private_myuid);
		private_myuid = -1;
	}
	if (private_mygid != -1) {
		setegid(private_mygid);
		private_mygid = -1;
	}
}

static FILE *OpenInitFile(const char *base, const char *filename)
{
	FILE *file;

	if (strncasecmp(filename, "http://", 7)==0
	||  strncasecmp(filename, "https://", 8)==0) {
		CURL *cl;
		char cerr[CURL_ERROR_SIZE];
		/* use libcurl to grab the file */
		perms_drop();
		file = tmpfile();
		if (file == NULL) {
			fprintf(stderr, "Error opening temporary file\n");
			perms_restore();
			return NULL;
		}
		cl = curl_easy_init();
/*		curl_easy_setopt(cl, CURLOPT_NOPROGRESS, 0); */
		curl_easy_setopt(cl, CURLOPT_WRITEDATA, file);
		curl_easy_setopt(cl, CURLOPT_URL, filename);
		curl_easy_setopt(cl, CURLOPT_ERRORBUFFER, cerr);
		curl_easy_setopt(cl, CURLOPT_USERAGENT, "Milliways III v" VERSION);
		if (curl_easy_perform(cl)) {
			fprintf(stderr, "Error loading %s: %s\n", filename, cerr);
			fclose(file);
			perms_restore();
			return NULL;
		}
		curl_easy_cleanup(cl);
		perms_restore();
		fseek(file, 0, SEEK_SET);
	} else {
		char path[1024];
		struct stat stats;

		if (filename[0] == '/' ||
		    !strncmp(filename, "../", 3) ||
		    strstr(filename, "/../"))
		{
			printf(_("Cannot load \"%s\": Illegal path\n"), filename);
			return NULL;
		}
		snprintf(path, 1023, "%s/%s", base, filename);
		perms_drop();
		if (stat(path, &stats))
		{
			/* be quiet about its not there, handle higher up */
			perms_restore();
			return NULL;
		}
		if (!S_ISREG(stats.st_mode))
		{
			printf(_("Error reading %s: Not a regular file\n"), path);
			perms_restore();
			return NULL;
		}

		if ((file=fopen(path,"r"))==NULL)
		{
			if (strcmp(".mwrc", filename))	printf(_("Error reading %s: %s\n"), path, strerror(errno));
			perms_restore();
			return NULL;
		}
		perms_restore();
	}
	return file;
}

static int ReadInitFile(const char *base, const char *filename);

static int parse_mwrcfile(FILE *file, const char *base, const char *filename)
{
	char *buff, *backup, *header;
	char *a, *b, *c;
	int lineno = 0;
	
	while (!feof(file))
	{
		if ((buff = frl_line_nspace(file, "#")) == NULL) break;
		backup = buff;
		lineno+=num_lines_read();
		if (strchr("\n\r#", buff[0])!=NULL) { free(backup); continue; }

		header = strdup(backup);
		if ((a=strtok(buff," \t"))==NULL) { free(backup); free(header); continue; }

		if (*a=='#') { free(backup); free(header); continue; }

		if (!strcasecmp(a, "include"))
		{
			if ((b=strtok(NULL," \t\n\r"))==NULL)
			{
				printf(_("Malformed include in %s at line %d\n"),filename,lineno);
				free(backup);
				free(header);
				continue;
			}
			if ((c=strrchr(filename,'/'))!=NULL) {
				char rpath[1024];
				*c=0;
				snprintf(rpath,sizeof(rpath),"%s/%s", filename, b);
				*c='/';
				ReadInitFile(base, rpath);
			}else
				ReadInitFile(base, b);
		} else {
			/* mwscript has gone but we don't want to break old .mwrc's */
			const char *noopfuncs[] = {
				"alias",
				"bind",
				"rpc",
				"destroy",
				"event",
				"ipc",
				"checkonoff",
				"shutdown",
				"force",
				"function",
				"lfunction",
				"initfunc",
				"linitfunc",
				"boardfunc",
				"lboardfunc",
				NULL
			};
			int noop = 0;
			for (int i = 0; noopfuncs[i] != NULL; i++) {
				if (!strcasecmp(a, noopfuncs[i])) {
					noop = 1;
					break;
				}
			}
			if (!noop)
				fprintf(stderr, _("Loading file %s unrecognised command '%s' on line %d\n"),
				       filename, a, lineno);
		}
		free(backup);
		free(header);
	}
	fclose(file);
	return 0;
}

static int ReadInitFile(const char *base, const char *filename)
{
	FILE *file = NULL;
	const char *a;

	/* look for file in path we were given first */
	file = OpenInitFile(base, filename);

	/* it wsnt there, do we have one in the default collection */
	if (!file) 	
		file = OpenInitFile(HOMEPATH"/scripts", filename);

	/* not found anywhere, give up */
	if (!file)
		return 1;


	if ((a=strrchr(filename, '.'))!=NULL && strncasecmp(a, ".js", 3)==0) {
		load_jsfile(file, filename);
		fclose(file);
		return 0; // changed because if an error occured after this point the file exists, the js code has reported the error to the user and returning 1 will report to them that it doesn't exist
	} else {
		if (parse_mwrcfile(file, base, filename)) {
			fprintf(stderr, "Warning: file included in .mwrc not supported: %s\n", filename);
			return 1;
		}
	}
	return 0;
}

int LoadInitFile(const char *name)
{
	char *mwrc = NULL;
	struct passwd *pw;
	int dofree=0;
	int ret = 1;

	if (name == NULL) {
		mwrc = userdb_get(USERDB_PRIVATE, user->record.name, "mwrc");
		if (mwrc != NULL && mwrc[0]!=0) {
			name = mwrc;
			dofree=1;
		}
		if (mwrc != NULL && mwrc[0]==0)
			free(mwrc);
	}
	if (name == NULL) name = ".mwrc";

	if ((pw=getpwuid(getuid())) == NULL) {
		fprintf(stderr, _("Failed to get user data\n"));
		goto out;
	}

	if (!ReadInitFile(pw->pw_dir, name)) {
		ret = 0;
	}
out:
	if (dofree) free(mwrc);
	return ret;
}


