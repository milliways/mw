#ifndef ECHO_H
#define ECHO_H

void echo_off(void);
void echo_on(void);
void force_line_mode(void);
void save_terminal(void);
void fix_terminal(void);

#endif /* ECHO_H */
