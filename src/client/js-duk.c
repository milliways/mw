#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <pwd.h>
#include <errno.h>
#include <jansson.h>
#include <curl/curl.h>
#include <readline/readline.h>
#include <duktape.h>

#include <sqlite.h>
#include "js.h"
#include "main.h"
#include "script.h"
#include "talker.h"
#include "chattable.h"
#include "alias.h"
#include "user.h"
#include "alarm.h"
#include "who.h"
#include "util.h"
#include "iconv.h"
#include "sqlite.h"
#include "init.h"
#include "onoff.h"

extern struct user * const user;
struct alarm *timeout_event = NULL;
int interrupt = 1;
int js_flood;
int js_flood_limit = JS_FLOOD_LIMIT_DEFAULT;

duk_context *ctx;

struct binding {
	int type;
	int name_required;
	alias **list;
	const char *name;
};

const struct binding bindings[] = {
	{ K_BIND, 1, &bind_list, "Bind" },
	{ K_BIND_ALIAS, 1, &alias_list, "Alias" },
	{ K_BIND_EVENT, 0, &event_list, "Event bind" },
	{ K_BIND_ONOFF, 0, &onoff_list, "Check on/off bind" },
	{ K_BIND_SHUTDOWN, 0, &shutdown_list, "Shutdown bind" },
	{ K_BIND_INPUT, 0, &eventin_list, "Input event bind" },
	{ 0, 0, NULL, NULL }
};

/* Used by duktape to check for a timeout */
duk_bool_t check_exec_timeout(void *udata)
{
	return interrupt;
}

/* called if a script runs too long */
static void timeout(void *ptr)
{
	interrupt = 2;
}

/* clears the timeout event when a js finishes (or the js uses input) */
static void clear_timeout(void)
{
	if (timeout_event != NULL) {
		timeout_event->how = NULL;
		timeout_event = NULL;
	}
}

/* starts a 3 second timer that will interupt js if it is exceeded */
static void start_timeout(void)
{
	clear_timeout();
	alarm_enable();
	timeout_event = alarm_after(3, 0, NULL, &timeout);
}

/* Duktape uses a CESU-8 encoding, which allows UTF-16 surrogate pairs
   (themselves encoded in UTF-8), in order to be kinda-sorta compatible with
   ecmascript's UTF-16 requirements. This function just copies the cesu8 string,
   converting any surrogate pairs it finds to UTF-8. */
static char *cesu8_to_utf8(const char *cesu8)
{
	char *utf8 = calloc(1, strlen(cesu8) + 1);
	const unsigned char *cc = (void *)cesu8;
	char *cu = utf8;
	uint32_t hs = 0;

	while (*cc != '\0') {
		uint32_t c = 0;
		uint32_t u;

		if (cc[0] <= 0x7F) {
			*cu++ = *cc++;
			continue;
		} else if (cc[0] <= 0xDF) {
			*cu++ = *cc++;
			*cu++ = *cc++;
			continue;
		} else if (cc[0] <= 0xEF) {
			/* Surrogates are encoded in 3 chars so convert
			   back to a single UTF-16 value */
			c = ((uint32_t)cc[0] & 0xF) << 12 |
			    ((uint32_t)cc[1] & 0x3F) << 6 |
			    ((uint32_t)cc[2] & 0x3F);
		} else {
			*cu++ = *cc++;
			*cu++ = *cc++;
			*cu++ = *cc++;
			*cu++ = *cc++;
			continue;
		}
		if (hs == 0 && c >= 0xD800 && c <= 0xDBFF)
			hs = c;
		else if (hs != 0 && c >= 0xDC00 && c <= 0xDFFF) {
			/* Have high and low surrogates - convert to code point then
			   back to UTF-8 */
			u = 0x10000 + ((((uint32_t)hs & 0x3FF) << 10) | (c & 0x3FF));
			*cu++ = 0xF0 |  u >> 18;
			*cu++ = 0x80 | (u >> 12 & 0x3F);
			*cu++ = 0x80 | (u >> 6 & 0x3F);
			*cu++ = 0x80 | (u & 0x3F);
			hs = 0;
		} else {
			*cu++ = cc[0];
			*cu++ = cc[1];
			*cu++ = cc[2];
			hs = 0;
		}
		cc += 3;
	}
	*cu = '\0';
	return utf8;
}

static int js_push_username(int32_t id)
{
	struct user u;
	int ret;

	ret = fetch_user(&u, id);
	if (ret == 0)
		duk_push_string(ctx, u.record.name);
	else
		duk_push_undefined(ctx);
	return ret;
}

static int js_push_channel(uint32_t dst)
{
	struct room room = {0};
	duk_idx_t idx;

	if (!LoadRoom(&room, dst))
		return 1;

	idx = duk_push_bare_object(ctx); /* The channel object */

	duk_push_number(ctx, dst);
	duk_put_prop_string(ctx, idx, "id");
	if (room.name != NULL) {
		duk_push_string(ctx, room.name);
		duk_put_prop_string(ctx, idx, "name");
	}
	if (room.desc != NULL) {
		duk_push_string(ctx, room.desc);
		duk_put_prop_string(ctx, idx, "topic");
	}
	duk_push_boolean(ctx, room.sproof);
	duk_put_prop_string(ctx, idx, "soundproof");
	return 0;
}

static int js_push_ipcmsg_event(ipc_message_t *msg)
{
	const char *str;
	duk_idx_t idx;
	json_t *json;
	json_t *val;

	/* Only support json-formatted messages */
	if (msg == NULL || msg->head.type <= 26)
		return 1;

	json = ipcmsg_json_decode(msg);
	if (json == NULL) {
		fprintf(stderr, "mwjs error: failed to unmarshall message\n");
		return -1;
	}
	idx = duk_push_bare_object(ctx); /* msg object */

	/* The message object will have common members which are set from the
	 * msg header and the rest (json encoded) will be dependent on the
	 * message type.
	 */
	duk_push_number(ctx, msg->head.when);
	duk_put_prop_string(ctx, idx, "unixtime");
	duk_push_number(ctx, msg->head.serial);
	duk_put_prop_string(ctx, idx, "serial");
	duk_push_string(ctx, ipc_nametype(msg->head.type));
	duk_put_prop_string(ctx, idx, "ipc_type");
	js_push_username(msg->head.src);
	duk_put_prop_string(ctx, idx, "from_name");
	switch (msg->head.type) {
	case IPC_SAYTOROOM:
		if (js_push_channel(msg->head.dst)) {
			duk_pop(ctx);
			return -1;
		}
		duk_put_prop_string(ctx, idx, "channel");
		break;
	case IPC_SAYTOUSER:
		duk_push_string(ctx, json_getstring(json, "target"));
		duk_put_prop_string(ctx, idx, "to_name");
		break;
	default:
		break;
	}
	duk_push_string(ctx, json_getstring(json, "type"));
	duk_put_prop_string(ctx, idx, "type");
	duk_push_string(ctx, json_getstring(json, "text"));
	duk_put_prop_string(ctx, idx, "text");
	str = json_getstring(json, "exclude");
	if (str != NULL) {
		duk_push_string(ctx, str);
		duk_put_prop_string(ctx, idx, "excluded_name");
	}
	val = json_object_get(json, "plural");
	if (val != NULL && json_is_integer(val)) {
		unsigned p = json_integer_value(val);

		if (p >= plural_suffix_size())
			p = 0;
		duk_push_string(ctx, plural_suffix[p]);
		duk_put_prop_string(ctx, idx, "suffix");
	}
	json_decref(json);
	return 0;
}

static int js_push_logon_event(json_t *j, duk_idx_t i, int type)
{
	duk_push_boolean(ctx, type == LOGON_AUTOCHAT);
	duk_put_prop_string(ctx, i, "autochat");
	return 0;
}

static int js_push_logoff_event(json_t *j, duk_idx_t i, int type)
{
	const char *reason;
	const char *agent;
	const char *subtypes[] = {
		[LOGOFF_NORMAL] = "normal",
		[LOGOFF_TIMEOUT] = "timeout",
		[LOGOFF_EOFS] = "eof",
		[LOGOFF_MROD] = "mrod",
		[LOGOFF_BANNED] = "banned",
		[LOGOFF_ERROR] = "error",
		[LOGOFF_DELETED] = "deleted"
	};
	if (type > LOGOFF_DELETED || type < 0) {
		fprintf(stderr, "mwjs error: bad logoff event type: %d\n", type);
		duk_pop(ctx);
		return 1;
	}
	duk_push_string(ctx, subtypes[type]);
	duk_put_prop_string(ctx, i, "method");
	reason = json_getstring(j, "reason");
	if (reason != NULL) {
		duk_push_string(ctx, reason);
		duk_put_prop_string(ctx, i, "reason");
	}
	agent = json_getstring(j, "agent");
	if (agent != NULL) {
		duk_push_string(ctx, agent);
		duk_put_prop_string(ctx, i, "agent");
	}
	return 0;
}

static int js_push_join_event(json_t *j, duk_idx_t i, int type)
{
	const char *agent = json_getstring(j, "agent");
	const char *subtypes[] = {
		[JOIN_NORMAL] = "normal",
		[JOIN_SUMMONED] = "summoned",
		[JOIN_FORCED] = "forced",
	};
	if (type > JOIN_FORCED || type < 0) {
		fprintf(stderr, "mwjs error: bad join event type: %d\n", type);
		duk_pop(ctx);
		return 1;
	}
	duk_push_string(ctx, subtypes[type]);
	duk_put_prop_string(ctx, i, "method");
	if (agent != NULL) {
		duk_push_string(ctx, agent);
		duk_put_prop_string(ctx, i, "agent");
	}
	duk_push_number(ctx, json_getint(j, "channel"));
	duk_put_prop_string(ctx, i, "channel");
	return 0;
}

static int js_push_leave_event(json_t *j, duk_idx_t i, int type)
{
	const char *agent = json_getstring(j, "agent");
	const char *msg = json_getstring(j, "message");
	const char *subtypes[] = {
		[LEAVE_NORMAL] = "normal",
		[LEAVE_ZOD] = "zod",
		[LEAVE_FORCED] = "forced",
		[LEAVE_EXIT] = "logoff",
	};
	if (type > LEAVE_EXIT || type < 0) {
		fprintf(stderr, "mwjs error: bad leave event type: %d\n", type);
		duk_pop(ctx);
		return 1;
	}
	duk_push_string(ctx, subtypes[type]);
	duk_put_prop_string(ctx, i, "method");
	if (agent != NULL) {
		duk_push_string(ctx, agent);
		duk_put_prop_string(ctx, i, "agent");
	}
	if (msg != NULL) {
		duk_push_string(ctx, msg);
		duk_put_prop_string(ctx, i, "message");
	}
	duk_push_number(ctx, json_getint(j, "channel"));
	duk_put_prop_string(ctx, i, "channel");
	return 0;
}

static int (*js_push_onoff_func[ONOFF_SIZE])(json_t*, duk_idx_t, int) = {
	js_push_logon_event,
	js_push_logoff_event,
	js_push_join_event,
	js_push_leave_event
};

/* Pushes the event object and attaches the common bits to it before calling
 * the onoff action type-specific function to fill in the rest */
static int js_push_onoff_event(json_t *j, int action, int64_t unixtime)
{
	duk_idx_t idx = duk_push_bare_object(ctx);
	int type = json_getint(j, "type");
	int ret;

	duk_push_number(ctx, unixtime);
	duk_put_prop_string(ctx, idx, "unixtime");
	duk_push_boolean(ctx, json_is_true(json_object_get(j, "quiet")));
	duk_put_prop_string(ctx, idx, "quiet");
	duk_push_string(ctx, json_getstring(j, "user"));
	duk_put_prop_string(ctx, idx, "user");

	ret = js_push_onoff_func[action](j, idx, type);
	json_decref(j);
	return ret;
}

static duk_ret_t js_print(duk_context *cx)
{
	int argc = duk_get_top(cx);

	if (argc < 1)
		return 0;
	for (int i = 0; i < argc; i++) {
		const char *cesu8 = duk_to_string(cx, i - argc);
		char *utf8 = cesu8_to_utf8(cesu8);
		display_message(utf8, 0, 1);
		free(utf8);
	}
	return 0;
}

static duk_ret_t js_mwexec(duk_context *cx)
{
	char *utf8;

	if (duk_is_undefined(cx, -1)) {
		fprintf(stderr, "mwjs error: exec() requires an argument\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	if (!duk_is_string(cx, -1)) {
		fprintf(stderr, "mwjs error: exec() requires a string\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	utf8 = cesu8_to_utf8(duk_get_string(cx, -1));
	duk_pop(cx);
	DoCommand(utf8, chattable);
	free(utf8);
	return 0;
}

static duk_ret_t js_say(duk_context *cx)
{
	char *utf8;

	if (duk_is_undefined(cx, -1)) {
		fprintf(stderr, "mwjs error: say() requires an argument\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	js_flood++;
	if (js_flood > js_flood_limit) {
		fprintf(stderr, "mwjs error: Script exceeded flood limit\n");
		return DUK_RET_ERROR;
	}
	if (!duk_check_type_mask(cx, -1, DUK_TYPE_MASK_STRING | DUK_TYPE_MASK_NUMBER)) {
		fprintf(stderr, "mwjs error: say() takes a string or a number\n");
		return DUK_RET_ERROR;
	}
	utf8 = cesu8_to_utf8(duk_to_string(cx, -1));
	chat_say(utf8);
	duk_pop(cx);
	free(utf8);
	return 0;
}

static duk_ret_t js_wholist(duk_context *cx)
{
	duk_idx_t arr_idx;
	json_t * wlist;
	json_t *entry;
	time_t now;
	size_t wi;
	int i = 0;

	wlist = grab_wholist();
	if (wlist == NULL) {
		fprintf(stderr, "Could not grab who list, try again\n");
		return 0;
	}
	arr_idx = duk_push_array(cx);
	now = time(0);
	json_array_foreach(wlist, wi, entry) {
		json_t * perms = json_object_get(entry, "perms");
		duk_idx_t obj_idx;
		const char *prot;
		int dowhen;

		obj_idx = duk_push_bare_object(cx);
		/* user name */
		duk_push_string(cx, json_getstring(entry, "name"));
		duk_put_prop_string(cx, obj_idx, "username");
		/* room number */
		duk_push_int(cx, json_getint(entry, "channel"));
		duk_put_prop_string(cx, obj_idx, "room");
		/* idle time */
		duk_push_int(cx, now - json_getint(entry, "idletime"));
		duk_put_prop_string(cx, obj_idx, "idle");
		/* chat modes */
		duk_push_string(cx, json_getstring(perms, "chatmode"));
		duk_put_prop_string(cx, obj_idx, "chatmodes");
		/* protection level */
		prot = json_getstring(perms, "protection");
		duk_push_int(cx, prot[0] - '0');
		duk_put_prop_string(cx, obj_idx, "protection_level");
		duk_push_int(cx, prot[2] - '0');
		duk_put_prop_string(cx, obj_idx, "protection_power");
		/* chat perms */
		duk_push_string(cx, json_getstring(perms, "chatprivs"));
		duk_put_prop_string(cx, obj_idx, "chatprivs");
		/* status string */
		duk_push_string(cx, json_getstring(entry, "doing"));
		duk_put_prop_string(cx, obj_idx, "doing");
		dowhen = json_getint(entry, "dowhen");
		duk_push_int(cx, dowhen ? now - dowhen : 0);
		duk_put_prop_string(cx, obj_idx, "since");
		/* stick line into array */
		duk_put_prop_index(ctx, arr_idx, i++);
	}
	return 1; /* Array is returned at top of stack */
}

struct urlget {
	duk_context *cx;
	duk_idx_t nchunks;
};
/* Consume data chunk acquired by curl */
static size_t omnomnom(void *ptr, size_t size, size_t n, void *data)
{
	struct urlget *ug = data;
	if (data == NULL)
		return 0;
	duk_push_lstring(ug->cx, ptr, size * n);
	ug->nchunks++;
	return size * n;
}

static duk_ret_t js_urlget(duk_context *cx)
{
	struct urlget ug = { .cx = cx, .nchunks = 0 };
	char cerr[CURL_ERROR_SIZE];
	const char *url;
	CURL *cl;

	if (duk_is_undefined(cx, -1)) {
		fprintf(stderr, "mwjs error: urlget() expects an argument\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	if (!duk_is_string(cx, -1)) {
		fprintf(stderr, "mwjs error: urlget() requires a string\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	url = duk_get_string(cx, -1);
	cl = curl_easy_init();
	curl_easy_setopt(cl, CURLOPT_WRITEFUNCTION, omnomnom);
	curl_easy_setopt(cl, CURLOPT_WRITEDATA, &ug);
	curl_easy_setopt(cl, CURLOPT_URL, url);
	curl_easy_setopt(cl, CURLOPT_ERRORBUFFER, cerr);
	curl_easy_setopt(cl, CURLOPT_USERAGENT, "Milliways III v" VERSION);
	if (curl_easy_perform(cl)) {
		fprintf(stderr, "mwjs error: urlget(): '%s': '%s'\n", url, cerr);
		return DUK_RET_ERROR;
	}
	curl_easy_cleanup(cl);
	/* Join the chunks together */
	duk_concat(cx, ug.nchunks);
	return 1; /* Result is at the top of the stack */
}

static duk_ret_t js_beep(duk_context *cx)
{
	int i, beeps;

	beeps = duk_get_int_default(cx, -1, 1);
	if (beeps < 1 || beeps > 5) {
		beeps = 0;
		fprintf(stderr, "beep() will only do between 1 and 5 beeps\n");
	}
	for (i = 0; i < beeps; i++)
		write(STDOUT_FILENO, "\7", 1);
	return 0;
}

/* Caller must free the returned memory */
static char *prompt_to_local(duk_context *cx)
{
	_autofree char *instr = NULL;
	const char *dukstr;
	duk_size_t dukbufsize;
	size_t promptbufsize;
	char *prompt;
	size_t len;
	int err;

	len = duk_get_length(cx, -1);
	dukstr = duk_get_lstring(cx, -1, &dukbufsize);
	promptbufsize = sizeof(char) * ((len * 3) + 1); // Still ugly
	prompt = malloc(promptbufsize); // Freed by caller
	duk_pop(cx);

	if (dukstr == NULL)
		return strdup("? ");

	/* To retain const correctness we have to dup the js string
	   which might seem slow but prompts will usually be short
	   so it's not worth worrying about */
	instr = strdup(dukstr);
	err = convert_string_charset(instr, "UTF-8", dukbufsize,
	                             prompt, "LOCAL//TRANSLIT", promptbufsize,
	                             NULL, NULL, NULL, NULL, NULL);
	if (err < 0) {
		fprintf(stderr, "mwsjs error: input() failed to convert "
				"prompt string: %d\n", err);
	}
	if (prompt == NULL)
		return strdup("? ");
	return prompt;
}

static duk_ret_t js_input(duk_context *cx)
{
	_autofree char *prompt = NULL;
	_autofree char *outstr = NULL;
	_autofree char *line = NULL;
	size_t len;
	int err;

	if (!duk_is_undefined(cx, -1) && !duk_is_string(cx, -1)) {
		fprintf(stderr, "mwjs error: argument to input() must be a string\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	prompt = prompt_to_local(cx);

	busy++;
	clear_timeout();
	line = readline(prompt);
	start_timeout();
	busy--;

	if (line == NULL) {
		duk_push_string(cx, "");
		return 1;
	}
	len = strlen(line) * 3 + 3; // Also ugly
	outstr = malloc(len);
	err = convert_string_charset(
	                line, "LOCAL", strlen(line),
                        outstr, "UTF-8//TRANSLIT", len,
                        NULL, NULL, NULL, NULL, NULL);
	if (err < 0) {
		fprintf(stderr, "mwjs error: input(): garbled string\n");
		return DUK_RET_ERROR;
	}
	duk_push_string(cx, outstr);
	return 1;
}

static duk_ret_t js_termsize(duk_context *cx)
{
	int idx;

	idx = duk_push_bare_object(cx);
	duk_push_int(cx, screen_w());
	duk_put_prop_string(cx, idx, "width");
	duk_push_int(cx, screen_h());
	duk_put_prop_string(cx, idx, "height");
	return 1; /* Result is at top of stack */
}

/* me = mw.whoami(); */
static duk_ret_t js_whoami(duk_context *cx)
{
	duk_push_string(cx, user->record.name);
	return 1;
}

static duk_ret_t js_bind(duk_context *cx)
{
	const char *bind_name = NULL;
	const char *func_name = NULL;
	const struct binding *bind;
	int bind_type = -1;

	if (duk_is_undefined(cx, -2) ||
	    duk_is_undefined(cx, -3)) {
		fprintf(stderr, "mwjs error: bind() expects 2 or 3 arguments\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	if (duk_is_string(cx, -3)) {
		bind_name = duk_get_string(cx, -3);
		bind_type = K_BIND;
		if (!duk_is_undefined(cx, -1)) {
			fprintf(stderr, "mwjs error: bind(str, str) expects 2 arguments\n");
			return DUK_RET_SYNTAX_ERROR;
		}
		duk_pop(cx);
	} else if (duk_is_number(cx, -3)) {
		bind_type = duk_get_int_default(cx, -3, -1);
		if (bind_type == K_BIND ||
		    bind_type == K_BIND_ALIAS) {
			bind_name = duk_get_string(cx, -2);
		} else {
			duk_pop(cx);
		}
	} else {
		fprintf(stderr, "mwjs error: first argument to bind() must be a "
		                "string or bind id\n");
		return DUK_RET_SYNTAX_ERROR;
	}

	func_name = duk_get_string(cx, -1);
	if (func_name == NULL) {
		fprintf(stderr, "mwjs error: bind(): invalid function name\n");
		return DUK_RET_SYNTAX_ERROR;
	}

	for (bind = &bindings[0]; bind->list != NULL && bind->type != bind_type; bind++);

	if (bind->list == NULL) {
		fprintf(stderr, "Bind type %d not recognised\n", bind_type);
		return DUK_RET_ERROR;
	}
	if (bind->name_required) {
		if (bind_name == NULL || bind_name[0] == '\0') {
			fprintf(stderr, "Bind name is empty\n");
			return DUK_RET_ERROR;
		}
	} else {
		bind_name = func_name;
		func_name = "";
	}
	if (AddLink(bind->list, bind_name, func_name)) {
		fprintf(stderr, "Warning: %s %s already exists.\n",
		        bind->name, bind_name);
	}
	return 0;
}

static duk_ret_t js_unbind(duk_context *cx)
{
	const char *bind_name = "";
	const struct binding *bind;
	int bind_type = -1;

	if (duk_is_undefined(cx, -2)) {
		fprintf(stderr, "mwjs error: unbind() expects 1 or 2 arguments\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	if (duk_is_string(cx, -2)) {
		bind_name = duk_get_string(cx, -2);
		bind_type = K_BIND;
		if (!duk_is_undefined(cx, -1)) {
			fprintf(stderr, "mwjs error: unbind(str) expects 1 argument\n");
			return DUK_RET_SYNTAX_ERROR;
		}
		duk_pop(cx);
	} else if (duk_is_number(cx, -2)) {
		bind_type = duk_get_int_default(cx, -2, -1);
		bind_name = duk_get_string(cx, -1);
	} else {
		fprintf(stderr, "mwjs error: first argument to unbind() must be a "
		                "string or bind id\n");
		return DUK_RET_SYNTAX_ERROR;
	}

	for (bind = &bindings[0]; bind->list != NULL && bind->type != bind_type; bind++);

	if (bind->list == NULL) {
		fprintf(stderr, "Bind type %d not recognised\n", bind_type);
		return DUK_RET_ERROR;
	}
	if (bind->name_required) {
		if (bind_name == NULL || bind_name[0] == '\0') {
			fprintf(stderr, "Bind name is empty\n");
			return DUK_RET_ERROR;
		}
	}
	if (!DestroyLink(bind->list, bind_name)) {
		fprintf(stderr, "Warning: %s %s does not exist\n",
		        bind->name, bind_name);
	}
	return 0;
}

static void js_push_result_entry(duk_context *cx, struct db_data *entry, int ncols)
{
	duk_idx_t idx;
	int i;

	if (entry == NULL || ncols < 1) {
		duk_push_null(cx);
		return;
	}
	idx = duk_push_array(cx);
	for (i = 0; i < ncols; i++) {
		duk_push_string(cx, entry->field[i]);
		duk_put_prop_index(cx, idx, i);
	}
}

static void js_push_result_array(duk_context *cx, struct db_result *dbres)
{
	struct db_data *entry;
	duk_idx_t idx;
	int i;

	if (dbres == NULL) {
		duk_push_null(cx);
		return;
	}
	idx = duk_push_array(cx);
	for (i = 0, entry = dbres->data; entry != NULL; entry = entry->next, i++) {
		js_push_result_entry(cx, entry, dbres->cols);
		duk_put_prop_index(cx, idx, i);
	}
}

static void js_push_column_names(duk_context *cx, struct db_result *dbres)
{
	duk_idx_t idx;
	int i;

	if (dbres == NULL) {
		duk_push_null(cx);
		return;
	}
	idx = duk_push_array(cx);
	for (i = 0; i < dbres->cols; i++) {
		duk_push_string(cx, dbres->colNames[i]);
		duk_put_prop_index(cx, idx, i);
	}
}

static void js_push_dbresult(duk_context *cx, struct js_db_result *dbres)
{
	duk_idx_t idx = duk_push_bare_object(cx);
	duk_push_int(cx, dbres->db_error);
	duk_put_prop_string(cx, idx, "db_error");
	duk_push_int(cx, dbres->query_error);
	duk_put_prop_string(cx, idx, "query_error");
	if (dbres->error_text != NULL)
		duk_push_string(cx, dbres->error_text);
	else
		duk_push_string(cx, "No Error");
	duk_put_prop_string(cx, idx, "error_text");
	js_push_result_array(cx, dbres->query_result);
	duk_put_prop_string(cx, idx, "data");
	js_push_column_names(cx, dbres->query_result);
	duk_put_prop_string(cx, idx, "column_names");
}

static duk_ret_t js_dbquery(duk_context *cx)
{
	_autofree char *path = NULL;
	struct js_db_result *dbres;
	const char *dbname;
	const char *query;
	struct passwd *pw;

	if ((pw = getpwuid(getuid())) == NULL) {
		fprintf(stderr, "mwjs error: dbquery(): Error getting user information\n");
		return DUK_RET_ERROR;
	}
	if (getmylogin() == NULL) {
		fprintf(stderr, "mwjs error: dbquery(): Permission denied\n");
		return DUK_RET_ERROR;
	}
	if (duk_is_undefined(cx, -1) ||
	    duk_is_undefined(cx, -2)) {
		fprintf(stderr, "mwjs error: dbquery() requires 2 arguments\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	if (!(duk_is_string(cx, -1) && duk_is_string(cx, -2))) {
		fprintf(stderr, "mwjs error: dbquery() requires 2 strings\n");
		return DUK_RET_ERROR;
	}
	dbname = duk_get_string(cx, -2);
	query = duk_get_string(cx, -1);
	if (!dbname || dbname[0] == '/' || !strncmp(dbname, "../", 3) ||
	    strstr(dbname, "/../")) {
		fprintf(stderr, "mwjs error: dbquery(): illegal path '%s'\n", dbname);
		return DUK_RET_ERROR;
	}
	asprintf(&path, "%s/%s", pw->pw_dir, dbname);
	perms_drop();
	dbres = js_db_query(path, query);
	perms_restore();
	if (!dbres) {
		fprintf(stderr, "mwjs error: dbquery(): query failed\n");
		return DUK_RET_ERROR;
        }
	js_push_dbresult(cx, dbres);
	js_db_free(dbres);
	return 1;
}

int js_isrunning(void)
{
	return (interrupt == 0 && timeout_event != NULL);
}

static int mwjs_call_fn(int argc)
{
	duk_int_t ret;

	interrupt = 0;
	start_timeout();
	ret = duk_pcall(ctx, argc);
	clear_timeout();

	if (ret != DUK_EXEC_SUCCESS) {
		if (duk_is_error(ctx, -1)) {
			duk_get_prop_string(ctx, -1, "stack");
			fprintf(stderr, "mwjs error: %s\n", duk_safe_to_string(ctx, -1));
			duk_pop(ctx);
		} else {
			fprintf(stderr, "mwjs error: %s\n", duk_safe_to_string(ctx, -1));
		}
		script_output = 0;
	} else if (duk_is_boolean(ctx, -1) && !duk_get_boolean(ctx, -1)) {
		script_output = 0;
	}
	duk_pop(ctx);
	return (ret == DUK_EXEC_SUCCESS) ? 0 : -1;
}

int js_exec(char *name, int argc, const char **argv)
{
	int i;

	if (!duk_get_global_string(ctx, name) || !duk_is_function(ctx, -1))
		return 0;
	duk_require_stack(ctx, argc);
	for (i = 0; i < argc; i++)
		duk_push_string(ctx, argv[i]);

	(void)mwjs_call_fn(argc);
	return 0;
}

static int js_push_event(struct mwevent *ev)
{
	duk_idx_t idx = duk_push_bare_object(ctx);
	int ret = -1;

	/* Although incoming IPC messages are the only kind of event we
	 * currently handle, the message is set as a member of an event object
	 * and the event handler is passed the event object. This allows us to
	 * extend this mechanism in future to handle other kinds of client
	 * events.
	 */
	switch (ev->ev_type) {
	case MWEV_TYPE_MSG:
		duk_require_stack(ctx, 2);
		duk_push_string(ctx, "message_received");
		duk_put_prop_string(ctx, idx, "type");
		ret = js_push_ipcmsg_event(ev->ev_data.msg);
		if (ret == 0)
			duk_put_prop_string(ctx, idx, "data");
		break;
	case MWEV_TYPE_ONOFF:
		duk_require_stack(ctx, 2);
		/* ONOFF covers 4 different types of events so expose them as
		   separate event types to reduce js boilerplate */
		json_t *j = ipcmsg_json_decode(ev->ev_data.onoff);
		if (j == NULL) {
			fprintf(stderr, "mwjs error: failed to unmarshall message\n");
			return -1;
		}
		int action = json_getint(j, "action");
		if (action >= ONOFF_SIZE || action < 0)
			break;
		duk_push_string(ctx, onoff_action_name[action]);
		duk_put_prop_string(ctx, idx, "type");
		ret = js_push_onoff_event(j, action, ev->ev_data.onoff->head.when);
		if (ret == 0)
			duk_put_prop_string(ctx, idx, "data");
		break;
	case MWEV_TYPE_INPUT:
		duk_require_stack(ctx, 2);
		duk_push_string(ctx, "input");
		duk_put_prop_string(ctx, idx, "type");
		duk_push_string(ctx, ev->ev_data.input);
		duk_put_prop_string(ctx, idx, "data");
		ret = 0;
		break;
	case MWEV_TYPE_EXIT:
		duk_require_stack(ctx, 2);
		duk_push_string(ctx, "exit");
		duk_put_prop_string(ctx, idx, "type");
		duk_push_string(ctx, ev->ev_data.mrodder);
		duk_put_prop_string(ctx, idx, "data");
		ret = 0;
		break;
	case MWEV_TYPE_NONE:
		/* Fall through */
	default:
		fprintf(stderr, "mwjs warning: Unhandled event type: %d\n", ev->ev_type);
		duk_pop(ctx);
		break;
	}
	if (ret != 0)
		duk_pop(ctx);
	return ret;
}

int js_handle_event(struct mwevent *ev)
{
	unsigned len;
	unsigned i;

	duk_require_stack(ctx, 5);
	if (!duk_get_global_string(ctx, "mw")) {
		fprintf(stderr, "mwjs error: failed to lookup mw namespace\n");
		return -1;
	}
	if (!duk_get_prop_string(ctx, -1, "onevent")) {
		fprintf(stderr, "mwjs error: failed to lookup 'mw.onevent'\n");
		return -1;
	}
	if (!duk_is_array(ctx, -1)) {
		fprintf(stderr, "mwjs error: 'mw.onevent' is not an array\n");
		return -1;
	}
	len = duk_get_length(ctx, -1);
	if (len == 0)
		return 0;

	for (i = 0; i < len; i++) {
		duk_get_prop_index(ctx, -1, i);
		if (!duk_is_function(ctx, -1)) {
			duk_int_t t = duk_get_type(ctx, -1);
			fprintf(stderr, "mwjs warning: mw.onevent[%u] is of type %d, "
			                "not a function\n", i, t);
			duk_pop(ctx);
			continue;
		}
		if (js_push_event(ev) != 0) {
			duk_pop(ctx); /* Pop the function */
			continue;
		}
		mwjs_call_fn(1); /* Pops the return value */
	}
	return 0;
}

/* This limit is fairly arbitrary but a limit is needed and 256K
   seems a reasonable file and (temporary) buffer size */
#define MWJS_FILE_MAX_SIZE (256*1024)

int load_jsfile(FILE *f, const char *filename)
{
	char buf[MWJS_FILE_MAX_SIZE + 1];
	int fd = fileno(f);
	struct stat st;
	size_t ret;
	int err;

	if (fd == -1) {
		fprintf(stderr, "Failed to open '%s': %s\n", filename, strerror(errno));
		return 1;
	}
	if (fstat(fd, &st) != 0) {
		fprintf(stderr, "Failed to stat '%s': %s\n", filename, strerror(errno));
		return 1;
	}
	errno = EFBIG;
	if (st.st_size > MWJS_FILE_MAX_SIZE) {
		fprintf(stderr, "Failed to load '%s': %s\n", filename, strerror(errno));
		return 1;
	}
	ret = fread(buf, sizeof(char), st.st_size, f);
	if (ret != st.st_size) {
		fprintf(stderr, "Failed to read '%s': %llu bytes read\n",
		        filename, (unsigned long long)ret);
		return 1;
	}
	buf[st.st_size] = '\0';

	interrupt = 0;
	start_timeout();
	duk_push_string(ctx, filename);
	err = duk_pcompile_string_filename(ctx, 0, buf);
	if (err == 0)
		err = duk_pcall(ctx, 0);
	clear_timeout();

	if (err != 0)
		fprintf(stderr, "Failed to execute '%s': %s\n", filename,
		        duk_safe_to_string(ctx, -1));
	duk_pop(ctx);
	return 0;
}

int is_js(char *name)
{
	int ret = 0;

	duk_require_stack(ctx, 1);
	if (duk_get_global_string(ctx, name) && duk_is_function(ctx, -1))
		ret = 1;

	duk_pop(ctx);
	return ret;
}

void js_stop_execution(void)
{
	interrupt = 1;
}

int stop_js(void)
{
	duk_destroy_heap(ctx);
	return 0;
}

/* x = mw.store.foo; */
static duk_ret_t js_store_get(duk_context *cx)
{
	const char *key = duk_get_string(cx, 1);
	char *val = userdb_get(USERDB_PUBLIC, user->record.name, key);
	if (val == NULL)
		duk_push_undefined(cx);
	else
		duk_push_string(cx, val);
	return 1;
}

/* mw.store.foo = "bar"; */
static duk_ret_t js_store_set(duk_context *cx)
{
	const char *key = duk_get_string(cx, 1);
	const char *val = duk_get_string(cx, 2);
	userdb_set(USERDB_PUBLIC, user->record.name, key, val);
	return 0;
}

/* delete mw.store.foo; */
static duk_ret_t js_store_delete(duk_context *cx)
{
	const char *key = duk_get_string(cx, 1);

	userdb_set(USERDB_PUBLIC, user->record.name, key, NULL);
	return 0;

}

/* "foo" in mw.store */
static duk_ret_t js_store_has(duk_context *cx)
{
	const char *key = duk_get_string(cx, 1);
	int exists;

	exists = userdb_key_exists(USERDB_PUBLIC, user->record.name, key);
	duk_push_boolean(cx, exists);
	return 1;
}

static const duk_function_list_entry store_handlers[] = {
	{ "get", js_store_get, 3 },
	{ "set", js_store_set, 4 },
	{ "has", js_store_has, 2 },
	{ "deleteProperty", js_store_delete, 2 },
	{ NULL, NULL, 0 }
};

/* The owning object must be at the top of the stack when calling this */
static void define_store_object(const char *name)
{
	duk_push_string(ctx, name); /* To be used by duk_def_prop() below */

	duk_push_object(ctx); /* Store object */
	duk_push_object(ctx); /* Handler object */
	duk_put_function_list(ctx, -1, store_handlers);
	duk_push_proxy(ctx, 0);

	/* Define the Store object in the parent object */
	duk_def_prop(ctx, -3, DUK_DEFPROP_HAVE_VALUE);
}

/* The owning object must be at the top of the stack when calling this */
static void define_command_object(void)
{
	duk_del_prop_string(ctx, -1, "command");
	duk_push_string(ctx, "command"); /* To be used by duk_def_prop() below */
	duk_push_object(ctx); /* command object */
	duk_push_object(ctx); /* Handler object */
	duk_push_proxy(ctx, 0); /* Stack is now [ "command", proxy ] */

	/* Define the command object in the parent object */
	duk_def_prop(ctx, -3, DUK_DEFPROP_HAVE_VALUE);
}

static int js_push_command_object(void)
{
	if (!duk_get_global_string(ctx, "mw")) {
		fprintf(stderr, "mwjs error: failed to lookup mw namespace\n");
		duk_pop(ctx);
		return -1;
	}
	if (!duk_get_prop_string(ctx, -1, "command")) {
		fprintf(stderr, "mwjs error: failed to lookup 'mw.command'\n");
		duk_pop_2(ctx);
		return -1;
	}
	if (!duk_is_object(ctx, -1)) {
		fprintf(stderr, "mwjs error: 'mw.command' is not an object\n");
		return -1;
	}
	return 0;
}

int js_handle_command(const char *name, int argc, const char **argv)
{
	duk_require_stack(ctx, argc + 5);
	if (js_push_command_object())
		return -1;

	if (!duk_get_prop_string(ctx, -1, name)) {
		duk_pop_3(ctx);
		return 1;
	}
	if (duk_is_undefined(ctx, -1)) {
		duk_pop_3(ctx);
		return 1;
	}
	if (!duk_is_function(ctx, -1)) {
		fprintf(stderr, "mwjs warning: mw.command.%s is not a function.\n", name);
		duk_pop_3(ctx);
		return -1;
	}
	for (int i = 0; i < argc; i++)
		duk_push_string(ctx, argv[i]);

	(void)mwjs_call_fn(argc);
	return 0;
}

const char *js_command_name_get(int x)
{
	char *name = NULL;
	int i = 0;

	duk_require_stack(ctx, 4);
	if (js_push_command_object())
		return NULL;

	duk_enum(ctx, -1, 0);
	while (name == NULL && duk_next(ctx, -1, 0)) {
		const char *str = duk_get_string(ctx, -1);
		if (i++ == x)
			name = strdup(str);
		duk_pop(ctx);
	}
	duk_pop(ctx);
	return name;
}

/* The owning object must be at the top of the stack when calling this */
static void define_func(const char *name, duk_c_function func, int nargs)
{
	duk_push_string(ctx, name);
	duk_push_c_function(ctx, func, nargs);
	duk_def_prop(ctx, -3, DUK_DEFPROP_HAVE_VALUE);
}

/* The owning object must be at the top of the stack when calling this */
static void define_number(const char *name, duk_double_t num)
{
	duk_push_string(ctx, name);
	duk_push_number(ctx, num);
	duk_def_prop(ctx, -3, DUK_DEFPROP_HAVE_VALUE);
}

/* The owning object must be at the top of the stack when calling this */
static void define_string(const char *name, const char *value)
{
	duk_push_string(ctx, name);
	duk_push_string(ctx, value);
	duk_def_prop(ctx, -3, DUK_DEFPROP_HAVE_VALUE);
}

/* The owning object must be at the top of the stack when calling this */
static void define_constants(void)
{
	define_number("K_BIND_EVENT", (duk_double_t)K_BIND_EVENT);
	define_number("K_BIND_ONOFF", (duk_double_t)K_BIND_ONOFF);
	define_number("K_BIND_FORCE", (duk_double_t)K_BIND_FORCE);
	define_number("K_BIND_SHUTDOWN", (duk_double_t)K_BIND_SHUTDOWN);
	define_number("K_BIND_ALIAS", (duk_double_t)K_BIND_ALIAS);
	define_number("K_BIND_INPUT", (duk_double_t)K_BIND_INPUT);
	define_string("whoami", user->record.name);
}

static void define_api(void)
{
	duk_push_string(ctx, "mw");
	duk_push_bare_object(ctx);

	/* mw.onevent = new Array(); */
	duk_push_string(ctx, "onevent");
	duk_push_array(ctx);
	duk_def_prop(ctx, -3, DUK_DEFPROP_HAVE_VALUE);

	/* Define values under the 'mw' namespace */
	define_func("print", js_print, DUK_VARARGS);
	define_func("exec", js_mwexec, 1);
	define_func("say", js_say, 1);
	define_func("wholist", js_wholist, 0);
	define_func("urlget", js_urlget, 1);
	define_func("beep", js_beep, 1);
	define_func("input", js_input, 1);
	define_func("termsize", js_termsize, 0);
	define_func("whoami", js_whoami, 0);
	define_store_object("store");
	define_command_object();

	/* mw object */
	duk_def_prop(ctx, -3, DUK_DEFPROP_HAVE_VALUE);
}

static void fatal_handler(void *unused, const char *msg) {
	if (msg == NULL)
		fprintf(stderr, "mwjs encountered a fatal error - aborting.\n");
	else
		fprintf(stderr, "mwjs error (fatal): %s\n", msg);
	fflush(stderr);
	abort();
}

int setup_js(void)
{
	int is_local = 1;

	if (getmylogin() == NULL)
		is_local = 0;

	ctx = duk_create_heap(NULL, NULL, NULL, NULL, fatal_handler);
	if (ctx == NULL)
		return -1;

	duk_push_global_object(ctx);
	/* New API, namespaced under 'mw' */
	define_api();

	/* Old mwscripty API */
	define_constants();
	define_func("print", js_print, DUK_VARARGS);
	define_func("exec", js_mwexec, 1);
	define_func("say", js_say, 1);
	define_func("wholist", js_wholist, 0);
	define_func("urlget", js_urlget, 1);
	define_func("beep", js_beep, 1);
	define_func("input", js_input, 1);
	define_func("termsize", js_termsize, 0);
	define_func("bind", js_bind, 3);
	define_func("unbind", js_unbind, 2);
	define_store_object("Store");
	if (is_local) // Don't allow bbs user to use dbquery
		define_func("dbquery", js_dbquery, 2);
	duk_pop(ctx);
	return 0;
}
