/*
 * Alias/Bind handling
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "talker.h"
#include "main.h"
#include "intl.h"
#include "userio.h"

#include "js.h"
#include "alias.h"
alias *alias_list = NULL;
alias *bind_list = NULL;
alias *rpc_list = NULL;
alias *event_list = NULL;
alias *onoff_list = NULL;
alias *ipc_list = NULL;
alias *shutdown_list = NULL;
alias *eventin_list = NULL;

/* completely destroys the given list */
void DestroyAllLinks(alias **list)
{
	for (alias *node = *list; node != NULL; node = *list) {
		*list = node->next;
		free(node);
	}
}

/* removes the link given by 'name' in the list */
int DestroyLink(alias **list, const char *name)
{
	alias **prevnext = list;
	alias *entry = *list;

	while (entry != NULL) {
		if (!strcasecmp(entry->from, name))
			break;
		 prevnext = &entry->next;
		 entry = entry->next;
	}
	if (entry == NULL)
		return 0; /* Not found or empty */

	*prevnext = entry->next;
	free(entry);
	return 1;
}

/* adds the 'from/to' node to the given list, or redefines if in existance */
int AddLink(alias **list, const char *from, const char *to)
{
	alias *new = *list;
	int redefine = 0;
	size_t size, fromsize;

	while (new!=NULL && strcasecmp(new->from, from))
	 	new=new->next;

	if (new!=NULL)
	{
		DestroyLink(list, from);
		redefine = 1;
	}

	fromsize = strlen(from) + 1;
	size = sizeof(*new) + (strlen(to) + 1) + fromsize;
	new = calloc(1, size);
	new->from = (char *)(new + 1);
	new->to = new->from + fromsize;
	strcpy(new->from,from);
	strcpy(new->to,to);
	new->next = *list;
	*list = new;

	return redefine;
}

/* displays the list */
void ShowLinks(alias *list, const char *prompt, const char *link, int count)
{
 	char	buff[10];
	alias *al;
 	int	max;
 	int	screen_height = screen_h();

	if (list != NULL)
	{
	 	al = list;
		max = count + 2;
	 	while (al != NULL)
 	 	{
	 	 	max++;
 			al=al->next;
 		}

		printf("\n");
		count++;
		if ((count >= (screen_height - 2)) && (count <= max))
		{
			printf(_("---more---\r"));
			get_str(buff, 5);
			printf("	  \r");
			if (*buff=='q' || *buff=='Q') return;
			count=0;
		}
		printf("%s\n", prompt);
	 	count++;
		if ((count >= (screen_height - 2)) && (count <= max))
		{
			printf(_("---more---\r"));
			get_str(buff, 5);
			printf("	  \r");
			if (*buff=='q' || *buff=='Q') return;
			count=0;
		}

		al = list;
		while (al != NULL)
		{
			printf("%s %s %s\n", al->from, link, al->to);
			count++;
			if ((count >= (screen_height - 2)) && (count <= max))
			{
				printf(_("---more---\r"));
				get_str(buff, 5);
				printf("	  \r");
				if (*buff=='q' || *buff=='Q') return;
				count=0;
			}
			al=al->next;
		}
	}
}

char *FindLinks(alias *list, const char *from)
{
	alias *al = list;

	while (al != NULL && strcasecmp(from, al->from))
		al = al->next;

	if (al == NULL)
		return NULL;

	return strdup(al->to);
}

/* bind listing autocompletion for scripts and tab-completion */
char *list_bind(const char *text, int state)
{
	static alias *bind;
	static int	len;
	char		*name;

	if (state == 0)
	{
		bind = bind_list;
		len = strlen(text);
	}

	while (bind != NULL)
	{
		name = bind->from;
		bind = bind->next;
		if ((len == 0) || !strncasecmp(name, text, len))
			return strdup(name);
	}
	return NULL;
}


/* bind listing autocompletion for readline */
char *list_bind_rl(const char *text, int state)
{
	static alias *bind;
	static int js_idx;
	static int len;
	char *completion;
	const char *name;

	if (state == 0) {
		bind = bind_list;
		js_idx = 0;
		len = strlen(text);
		len--;
	}

	text++;

	while (bind != NULL) {
		name = bind->from;
		bind = bind->next;
		if ((len == 0) || !strncasecmp(name, text, len)) {
			asprintf(&completion, ",%s", name);
			return completion;
		}
	}
	while ((name = js_command_name_get(js_idx++)) != NULL) {
		if ((len == 0) || !strncasecmp(name, text, len)) {
			asprintf(&completion, ",%s", name);
			return completion;
		}
	}
	return NULL;
}

char *NextLink(alias *al, char *prev)
{
	if (prev)
	{
		while(al && strcasecmp(al->from, prev)) al = al->next;
		if (al) al = al->next;
	}

	if (!al) return(NULL);
	else return(al->from);
}
