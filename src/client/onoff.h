#ifndef ONOFF_H
#define ONOFF_H

#include <onoff.h>

extern const char *onoff_action_name[ONOFF_SIZE];

int announce_logon(const char *usr, int type, int quiet);
int announce_logoff(const char *usr, int type, const char *agent, const char *reason, int quiet);
int announce_join(const char *usr, int channel, int type, const char *agent, int quiet);
int announce_leave(const char *usr, int channel, int type, const char *agent, const char *msg, int quiet);

#endif /* ONOFF_H */
