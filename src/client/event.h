#ifndef __EVENT_H__
#define __EVENT_H__

#include <socket.h>

typedef enum {
	MWEV_TYPE_NONE = 0,
	MWEV_TYPE_MSG,
	MWEV_TYPE_ONOFF,
	MWEV_TYPE_INPUT,
	MWEV_TYPE_EXIT,
} mwev_type_t;

struct mwevent {
	mwev_type_t ev_type;
	union {
		ipc_message_t *msg;   /* MWEV_TYPE_MSG */
		ipc_message_t *onoff; /* MWEV_TYPE_ONOFF */
		const char *input;    /* MWEV_TYPE_INPUT */
		const char *mrodder;  /* MWEV_TYPE_EXIT */
	} ev_data;
};

#endif /* __EVENT_H__ */
