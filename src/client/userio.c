#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <arpa/telnet.h>
#include <string.h>

#include "userio.h"
#include "main.h"
#include "strings.h"

extern int internet;
int eof_caught=0;

static char *mw_fgets(char *string,int size, FILE *fp)
{
	int ct=0;
	int ch;
	int tty_ready;

	if(fp!=stdin || internet==0)
	{
		while ((tty_ready = idle(fileno(fp), 500)) == 0 || (tty_ready < 0 && errno == EINTR));
		if (tty_ready < 0) return NULL;
		else return fgets(string,size,fp);
	}

	while(ct<size)
	{
		while ((tty_ready = idle(0, 500)) == 0 || (tty_ready < 0 && errno == EINTR));
		if (tty_ready < 0) return NULL;
		ch=getchar();
		if(ch==EOF)
			return(NULL);
		if(ch==IAC)
		{
			ch=getachar();
			switch(ch)
			{
				case EOF:return(NULL);
				case WILL:;
				case WONT:;
				case DO:;
				case DONT:
					getchar();
					continue;
				case EC:
					if(ct>0) ct--;
					break;
				case EL:
					ct=0;
					break;
			}
		}
		else
		{
			if(ch==8 || ch==127)
			{
				if(ct>0) ct--;
			}
			else
				string[ct++]=ch;
		}
		if(ch=='\r')
			ct--;
		if(ch=='\n')
			return(string);
	}
	return(string);
}

void get_str(char *string, int size)
{
	char *ptr;
	if(is_stacked() && pop_stack(string, size))
	{
		if ((ptr=strchr(string,'\n'))!=NULL) *ptr=0;
	}
	else
	{
		fflush(stdout);
		if (mw_fgets(string,size+1,stdin)==NULL || feof(stdin))
		{
			eof_caught++;
			if (eof_caught>=3)
			{
				exit(0);
			}
			clearerr(stdin);
			*string=0;
		}else
			eof_caught=0;

		if ((ptr=(char *)strchr(string,'\n'))!=NULL)
			*ptr=0;
		else
		{
			read2cr();
		}
		if(strchr(string,'\r')!=NULL)
			*strchr(string,'\r')=0;
	}
}

void read2cr(void)
{
	int c;
	while ((c=getachar())!=EOF && c!='\n');
}

int getachar(void)
{
	int c;
	int tty_ready;
	fflush(stdout);
	fflush(stderr);
	while ((tty_ready = idle(0, 500)) == 0 || (tty_ready < 0 && errno == EINTR));
	if (tty_ready < 0) return EOF;
	c=getchar();
	while (c==255)
	{
		getchar();
		getchar();
		c=getchar();
	}
	return(c);
}
