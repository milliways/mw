#ifndef CLIENT_WHO_H
#define CLIENT_WHO_H

#include <stdint.h>
#include <ipc.h>

void check_copies(int32_t where);
char *itime(unsigned long t);
void display_wholist(int type);
void update_wholist(ipc_message_t *msg);
int require_whoinfo(void);
char *part_who_talk(const char *text, int status);
char *part_who(const char *text, int status);
json_t * grab_wholist(void);
int32_t who_find(const char *username);


#endif /* CLIENT_WHO_H */
