#ifndef JS_H
#define JS_H

#include <stdio.h>
#include "event.h"

int js_isrunning(void);
int js_exec(char *name, int argc, const char **argvc);
int load_jsfile(FILE *f, const char *filename);
int is_js(char *name);
void js_stop_execution(void);
int stop_js(void);
int setup_js(void);
int js_handle_event(struct mwevent *ev);
int js_handle_command(const char *name, int argc, const char **argv);

enum bindtype {
	K_BIND = 0,
	K_BIND_EVENT,
	K_BIND_ONOFF,
	K_BIND_FORCE,
	K_BIND_SHUTDOWN,
	K_BIND_ALIAS,
	K_BIND_INPUT
};

const char *js_command_name_get(int x);

extern int js_flood;
#define JS_FLOOD_LIMIT_DEFAULT 15
extern int js_flood_limit;

#endif /* JS_H */
