#ifndef ALARM_H
#define ALARM_H

#include <stdlib.h>

struct alarm
{
	struct timeval when;
	void *what;
	void (*how)(void *args);
	struct alarm *next;
};

extern struct alarm *volatile first_alarm, *volatile next_alarm;

void alarm_enable(void);
void alarm_disable(void);
void alarm_check(void);
void alarm_init(void);
void alarm_cleanup(void);
void alarm_handler(int sig);
struct alarm *alarm_at(const struct timeval *when, void *what, void (*how)(void *));
struct alarm *alarm_after(long secs, long usecs, void *what, void (*how)(void *));
int alarm_select(int maxfd, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout);

#endif /* ALARM_H */
