/**************************************************************
**        Name: File Readline Utilities (frl)                **
**      Author: Chris Fry                                    **
**        Date: 19/03/2000                                   **
** Description: Utilies to read lines from a file            **
**************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "frl.h"

#define READBLOCK	255

/* last number of lines to be read at any time */
static long lines_read = 0;

/**************************************************************
** Description: Reads 1 line of any length from given file,  **
**              allocated memory for it, and returns it.     **
**          In: FILE* to read from - must be open for read   **
**      Return: NULL if end of file, and no text             **
**              <text> if there is any, includes blank line  **
**************************************************************/
char *frl_line(FILE *f)
{
	int	size=0;
	int	c = 0;
	char	*line = NULL;
	char	*temp = NULL;

	while ((c != '\n') && !feof(f))
	{
		if ((c = fgetc(f)) == -1) break;
		if (c == '\n') break;
		if (size == 0 || ((size+1) & READBLOCK) == 0)
		{
			line = realloc(line, sizeof(char) * (size + READBLOCK + 1));
		}
		line[size++] = c;
	}
	if (line != NULL)
	{
		line[size] = 0;
		temp = strdup(line);
		free(line);
		return(temp);
	}
	else if (feof(f))
		return(NULL);
	else
		return(strdup(""));
}

/**************************************************************
** Description: repeatedly calls frl from a file until first **
**              non-blank line not started by given comment  **
**          In: FILE* to read from - must be open for read   **
**              char* comment to ignore - if NULL ignored    **
**      Return: NULL if end of file, and no text             **
**              <text> if there is any                       **
**************************************************************/
char *frl_line_nspace(FILE *f, const char *comment)
{
	char	*str = NULL;
	char	*ptr = NULL;
	int	found = 0;

	lines_read = 0;

	while (!found)
	{
		/* get the next line of text */
		str = frl_line(f);
		lines_read++;

		/* set initial found to true */
		found = 1;

		/* if not EOF then continue */
		if (str != NULL)
		{
			/* go through line until first non-space character, or EOL */
			ptr = str;
			while ((ptr != NULL) && isspace(*ptr)) ptr++;

			/* EOL, all spaces, so ignore, and continue search */
			if ((ptr == NULL) || (*ptr == 0))
			{
				found = 0;
				if (str!=NULL) free(str);
			}
			/* check string for match on comment */
			else if (comment!=NULL && !strncmp(ptr, comment, strlen(comment)))
			{
				found = 0;
				free(str);
			}
		}
	}

	return(str);
}

long num_lines_read(void)
{
	return(lines_read);
}
