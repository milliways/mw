#ifndef ALIAS_H
#define ALIAS_H

typedef struct alias {
	char *from;
	char *to;
	struct alias *next;
} alias;

void DestroyAllLinks(alias **list);
int DestroyLink(alias **list, const char *name);
int AddLink(alias **list, const char *from, const char *to);
void ShowLinks(alias *list, const char *prompt, const char *link, int count);
char *FindLinks(alias *list, const char *from);
char *list_bind(const char *text, int state);
char *list_bind_rl(const char *text, int state);
char *NextLink(alias *list, char *prev);

extern alias *alias_list;
extern alias *bind_list;
extern alias *rpc_list;
extern alias *event_list;
extern alias *onoff_list;
extern alias *ipc_list;
extern alias *shutdown_list;
extern alias *eventin_list;

#endif /* ALIAS_H */
