#ifndef incl_Parse_h
#define incl_Parse_h

typedef struct __cmd
{
	const char *Command;	/* Command Name */
	unsigned int Rights;	/* Rights needed to see command */
	unsigned int Args;	/* Minimum valid arguments */
	const char *ArgError;	/* Argument error message */
	const char *Desc;	/* Command description message */
	void (*Function)(struct __cmd *, int, const char **, char *);
	unsigned int Show; /* show command in help or not */
} CommandList;

typedef struct __completion
{
	const char *Command;			/* Command Name */
	int Mode;				/* Command mode */
	int FArg;				/* Argument to tab-complete */
	int LArg;				/* Argument to t-c up to */
	char *(*CPFunction)(const char *, int);	/* Function to call */
} CompletionList;

extern int ParseLine(char *, const char *[]);
extern int BoolOpt(const char *);
extern int DoCommand(char *, CommandList *);
extern void InitParser(void);
extern int command_compare(const char *, const char *);

#define MAX_ARGC	128

extern void c_addfol(CommandList *cm, int argc, const char **argv, char *args);
extern void c_autosub(CommandList *cm, int argc, const char **argv, char *args);
extern void c_beep(CommandList *cm, int argc, const char **argv, char *args);
extern void c_board(CommandList *cm, int argc, const char **argv, char *args);
extern void c_cd(CommandList *cm, int argc, const char **argv, char *args);
extern void c_catchup(CommandList *cm, int argc, const char **argv, char *args);
extern void c_changeinfo(CommandList *cm, int argc, const char **argv, char *args);
extern void c_charset(CommandList *cm, int argc, const char **argv, char *args);
extern void c_credits(CommandList *cm, int argc, const char **argv, char *args);
extern void c_colouroff(CommandList *cm, int argc, const char **argv, char *args);
extern void c_contact(CommandList *cm, int argc, const char **argv, char *args);
extern void c_date(CommandList *cm, int argc, const char **argv, char *args);
extern void c_doing(CommandList *cm, int argc, const char **argv, char *args);
extern void c_first(CommandList *cm, int argc, const char **argv, char *args);
extern void c_folder(CommandList *cm, int argc, const char **argv, char *args);
extern void c_help(CommandList *cm, int argc, const char **argv, char *args);
extern void c_inform(CommandList *cm, int argc, const char **argv, char *args);
extern void c_last(CommandList *cm, int argc, const char **argv, char *args);
extern void c_latest(CommandList *cm, int argc, const char **argv, char *args);
extern void c_listall(CommandList *cm, int argc, const char **argv, char *args);
extern void c_listnew(CommandList *cm, int argc, const char **argv, char *args);
extern void c_listusers(CommandList *cm, int argc, const char **argv, char *args);
extern void c_locale(CommandList *cm, int argc, const char **argv, char *args);
extern void c_ls(CommandList *cm, int argc, const char **argv, char *args);
extern void c_mesg(CommandList *cm, int argc, const char **argv, char *args);
extern void c_mod(CommandList *cm, int argc, const char **argv, char *args);
extern void c_msg(CommandList *cm, int argc, const char **argv, char *args);
extern void c_new(CommandList *cm, int argc, const char **argv, char *args);
extern void c_newusers(CommandList *cm, int argc, const char **argv, char *args);
extern void c_passwd(CommandList *cm, int argc, const char **argv, char *args);
extern void c_next(CommandList *cm, int argc, const char **argv, char *args);
extern void c_pwd(CommandList *cm, int argc, const char **argv, char *args);
extern void c_prev(CommandList *cm, int argc, const char **argv, char *args);
extern void c_read(CommandList *cm, int argc, const char **argv, char *args);
extern void c_reply(CommandList *cm, int argc, const char **argv, char *args);
extern void c_resub(CommandList *cm, int argc, const char **argv, char *args);
extern void c_search(CommandList *cm, int argc, const char **argv, char *args);
extern void c_since(CommandList *cm, int argc, const char **argv, char *args);
extern void c_status(CommandList *cm, int argc, const char **argv, char *args);
extern void c_su(CommandList *cm, int argc, const char **argv, char *args);
extern void c_tidyup(CommandList *cm, int argc, const char **argv, char *args);
extern void c_timeout(CommandList *cm, int argc, const char **argv, char *args);
extern void c_tell(CommandList *cm, int argc, const char **argv, char *args);
extern void c_topten(CommandList *cm, int argc, const char **argv, char *args);
extern void c_unsub(CommandList *cm, int argc, const char **argv, char *args);
extern void c_user(CommandList *cm, int argc, const char **argv, char *args);
extern void c_who(CommandList *cm, int argc, const char **argv, char *args);
extern void c_wall(CommandList *cm, int argc, const char **argv, char *args);
extern void c_wiz(CommandList *cm, int argc, const char **argv, char *args);
extern void c_emote(CommandList *cm, int argc, const char **argv, char *args);
extern void c_wizchat(CommandList *cm, int argc, const char **argv, char *args);
extern void c_write(CommandList *cm, int argc, const char **argv, char *args);
extern void c_quit(CommandList *cm, int argc, const char **argv, char *args);
extern void c_save(CommandList *cm, int argc, const char **argv, char *args);
extern void c_timestamp(CommandList *cm, int argc, const char **argv, char *args);
extern void c_postinfo(CommandList *cm, int argc, const char **argv, char *args);
extern void c_chatmode(CommandList *cm, int argc, const char **argv, char *args);
extern void c_version(CommandList *cm, int argc, const char **argv, char *args);
extern void c_alias(CommandList *cm, int argc, const char **argv, char *args);
extern void c_unalias(CommandList *cm, int argc, const char **argv, char *args);

void help_list(CommandList *c, unsigned int hidestuff, const char * const prefix);
const char *skip_first_word(const char *args);

#endif
