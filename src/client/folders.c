
/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <mwlog.h>
#include "ipc.h"
#include "files.h"
#include "perms.h"
#include "intl.h"
#include "user.h"
#include "userio.h"
#include "folders.h"
#include "mesg.h"

extern struct user * const user;

void add_folder(void)
{
	struct folder new;
	int file;
	int yes;
	int folnum = -1;
	char tmp[11];

	if (nofolders() && create_folder_file())
		exit(1);
	file=openfolderfile(O_RDWR);
	do{
		yes=get_folder_entry(file, &new);
		folnum++;
	}while (yes && ((new.status)&1));
	if (!yes)
	{
		printf(_("Sorry, no space for a new folder.\n"));
		return;
	}
	if (lseek(file, -(sizeof(new)), SEEK_CUR) < 0)
	{
		perror("add_folder (seek)");
		exit(-1);
	}
	printf(_("Create new folder :\n"));
	printf(_("Folder name (%d chars): "),FOLNAMESIZE);
	get_str(new.name, FOLNAMESIZE);
	if (new.name[0] == '\0') return;
	printf(_("Folder topic (%d chars): "),TOPICSIZE);
	get_str(new.topic,TOPICSIZE);
	printf(_("Folder status (not in group) (arwRWpm): "));
	get_str(tmp,10);
	new.status = folder_stats(tmp,0)|1;
	printf(_("Folder status (in group) (arwRWpm): "));
	get_str(tmp,10);
	new.g_status = folder_stats(tmp,0)|1;
	printf(_("Folder groups (12345678): "));
	get_str(tmp,10);
	new.groups = folder_groups(tmp,0);
	new.first = 0;
	new.last = 0;
	printf(_("Creating folder %s - %s\n"), new.name, new.topic);

	auto_subscribe(folnum,true);
	Lock_File(file); /* folder file */
	if (write(file, &new, sizeof(new)) <  0)
	{
		perror("add_folder");
		exit(0);
	}
	Unlock_File(file);
	close(file);
	mwlog(user, "FOLDER(CREATED) %s", new.name);
}

void auto_subscribe(int folnum, int state)
{
	int err;
	int ufile;
	struct user usr;
	char buff[10];

	ufile = userdb_open(O_RDWR);
	if (ufile < 0)
		return;
	Lock_File(ufile); /*user file*/

	for_each_user(&usr, ufile, err) {
		set_subscribe(&usr, folnum, state);
		update_user_fd(ufile, &usr);
	}
	Unlock_File(ufile);
	close(ufile);
	snprintf(buff,9,"%d:%d", folnum, 0);
	ipc_send_to_all(IPC_LASTREAD, buff);
}
