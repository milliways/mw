#include <ipc.h>
#include "onoff.h"

extern struct user * const user;

const char *onoff_action_name[ONOFF_SIZE] = {
	[ONOFF_LOGON] = "mw_logon",
	[ONOFF_LOGOFF] = "mw_logoff",
	[ONOFF_JOIN] = "talker_join",
	[ONOFF_LEAVE] = "talker_leave"
};

int announce_logon(const char *usr, int type, int quiet)
{
	ipc_message_t *msg;
	json_t *j;

	j = json_init(NULL);
	json_addstring(j, "user", usr);
	json_addint(j, "action", ONOFF_LOGON);
	json_addint(j, "type", type);
	json_object_set(j, "quiet", quiet ? json_true() : json_false());

	msg = ipcmsg_create(IPC_CHECKONOFF, user->posn);
	ipcmsg_destination(msg, SYSTEM_USER);
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
	return 0;
}

int announce_logoff(const char *usr, int type, const char *agent, const char *reason, int quiet)
{
	ipc_message_t *msg;
	json_t *j;

	j = json_init(NULL);
	json_addstring(j, "user", usr);
	json_addint(j, "action", ONOFF_LOGOFF);
	json_addint(j, "type", type);
	json_object_set(j, "quiet", quiet ? json_true() : json_false());
	if (agent != NULL)
		json_addstring(j, "agent", agent);
	if (reason != NULL)
		json_addstring(j, "reason", reason);

	msg = ipcmsg_create(IPC_CHECKONOFF, user->posn);
	ipcmsg_destination(msg, SYSTEM_USER);
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
	return 0;
}

int announce_join(const char *usr, int channel, int type, const char *agent, int quiet)
{
	ipc_message_t *msg;
	json_t *j;

	j = json_init(NULL);
	json_addstring(j, "user", usr);
	json_addint(j, "action", ONOFF_JOIN);
	json_addint(j, "channel", channel);
	json_addint(j, "type", type);
	json_object_set(j, "quiet", quiet ? json_true() : json_false());
	if (agent != NULL)
		json_addstring(j, "agent", agent);

	msg = ipcmsg_create(IPC_CHECKONOFF, user->posn);
	ipcmsg_destination(msg, SYSTEM_USER);
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
	return 0;
}

int announce_leave(const char *usr, int channel, int type, const char *agent, const char *lmsg, int quiet)
{
	ipc_message_t *msg;
	json_t *j;

	j = json_init(NULL);
	json_addstring(j, "user", usr);
	json_addint(j, "action", ONOFF_LEAVE);
	json_addint(j, "channel", channel);
	json_addint(j, "type", type);
	json_object_set(j, "quiet", quiet ? json_true() : json_false());
	if (agent != NULL)
		json_addstring(j, "agent", agent);
	if (lmsg != NULL)
		json_addstring(j, "message", lmsg);

	msg = ipcmsg_create(IPC_CHECKONOFF, user->posn);
	ipcmsg_destination(msg, SYSTEM_USER);
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
	return 0;
}
