#ifndef HASH_H
#define HASH_H

#define FS_UNMARKED 0
#define FS_MARKED 1
#define FS_DETACHED 4

typedef enum {
    RT_VOID,		/* No content                       */
    RT_ALIAS,		/* Alias for another record         */
    RT_HASH,		/* Pointer to another hash          */
    RT_STRING,		/* String value                     */
    RT_INT,		/* Integer value                    */

    RT_MASK = 0xff

} rectype_t;

struct hashrec {
    int field;
    char *key;
    rectype_t type;
    union {
	void *pdata;
	int idata;
    } data;
    struct hashrec *bnext;
    struct hashrec *fnext;
};

union fieldrec {
    struct {
	int inorder; /* Is the 'fnext' list ordered by key? */
	struct hashrec *firstrec;
    } data;
    union fieldrec *gnext; /* Next in free list */
};

typedef struct {
    struct hashrec *first;
} bucket_t;

typedef struct {
    int field;
    char *key;
    int bucket;
    struct hashrec **rec;
} hash_op_t;

extern union fieldrec *fields;
extern int code;

extern int hash_alloc(void);
void hash_free(int field);
void hash_iterate(struct hashrec ***iter, int field);
void hash_next(struct hashrec ***iter);
void hash_op_init(hash_op_t *op, int field);
int hash_search(hash_op_t *op, const char *key);
void hash_op_forgetkey(hash_op_t *op);
void hash_set_p(hash_op_t *op, rectype_t type, const char *value);
void hash_set_i(hash_op_t *op, rectype_t type, int value);
void hash_delete(hash_op_t *op);
void hash_purge(int field);
void hashcode(int field, const char *key);

#endif /* HASH_H */
