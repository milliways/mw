#ifndef NEW_H
#define NEW_H

#include "user.h"

void list_new_items(struct user *user, int flag);
void show_new(struct user *user);
void latest(struct user *user);

#endif /* NEW_H */
