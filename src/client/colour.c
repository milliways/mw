/**********************************************************************************
  NOTE: The global colour set is freed everytime if reloaded, but it doesnt need
        to get freed normally, so reports as a memory leak. To fix this, have created
        simple destroy routine called at end of close_down, although the OS should
        free all the memory anyway.
**********************************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include "frl.h"
#include "colour.h"
#include "user.h"
#define COLOUR_LIMIT 40

static  char *colour_chart[COLOUR_LIMIT];
static char *colour_set=NULL;

/* the current user template */
extern struct user * const user;

/* safe copy string */
static void copy_in(char *dest, int len, const char *input)
{
	snprintf(dest, len, "%s", input);
}

static int hex_decode(const char *input, int len)
{
	int i=0;
	int total=0;

	while (i<len && *input) {
		total <<= 4;
		if (*input >= '0' && *input <= '9')
			total |= *input - '0';
		else
		if (*input >= 'a' && *input <= 'f')
			total |= (*input - 'a') + 10;
		else
		if (*input >= 'A' && *input <= 'F')
			total |= (*input - 'A') + 10;
		input++;
		i++;
	}
	return total;
}

/* return colour code sequence */
int colour(const char *input, char *result, int reslen)
{
	int consume = 0;
	char output[1024];
	int outlen = sizeof(output);

	if (outlen) output[0] = '\0';
	/* original colour mode */
	if (*input == 033) {
		char text[3];
		int i = 0;
		consume = 1;
		/* build the string, skipping unicode sequences */
		while (i < 2 && input[consume] != 0) {
			if ((input[consume] & 192) == 192) {
				consume++;
				text[i++] = '-';
				while ((input[consume] & 192) == 128)
					consume++;
			} else {
				text[i++] = input[consume++];
			}
		}
		text[2] = '\0';

		/* system colour chart */
		if (strchr("0123456789",text[0]))
		{
			i=atoi(text);

			if (i>=0 && i < COLOUR_LIMIT) {
				if (colour_chart[i] != NULL)
					copy_in(result, reslen, colour_chart[i]);
				else
				if (colour_chart[0] != NULL)
					copy_in(result, reslen, colour_chart[0]);
			}
			return consume;
		}

		i=0;
		output[i++] = 033;
		output[i++] = '[';

		/* reset string ? */
		if (strchr("-n", text[0]) &&
		    strchr("-n", text[1]) )
		{
			output[i++] = '0';
			output[i++] = 'm';
			output[i] = '\0';
			copy_in(result, reslen, output);
			return(consume);
		}

		/* high intensity mode? */
		if (isupper(text[0]))
		{
			output[i++] = '1';
			output[i++] = ';';
		} else {
			output[i++] = '0';
			output[i++] = ';';
		}

		if (strchr("kKrRgGyYbBmMcCwW", text[0])!=NULL)
		{
			if (strchr("n-", text[0])==NULL) output[i++] = '3';
			if (strchr("kK",text[0])) output[i++] = '0'; else
			if (strchr("rR",text[0])) output[i++] = '1'; else
			if (strchr("gG",text[0])) output[i++] = '2'; else
			if (strchr("yY",text[0])) output[i++] = '3'; else
			if (strchr("bB",text[0])) output[i++] = '4'; else
			if (strchr("mM",text[0])) output[i++] = '5'; else
			if (strchr("cC",text[0])) output[i++] = '6'; else
			if (strchr("wW",text[0])) output[i++] = '7';

			/* seperator */
			if (strchr("n-",text[0])==NULL && strchr("n-",text[1])==NULL)
				output[i++] = ';';
		}

		if (strchr("kKrRgGyYbBmMcCwW", text[1])!=NULL)
		{
			/* now background colour */
			if (strchr("n-",text[1])==NULL) output[i++] = '4';
			if (strchr("kK",text[1])) output[i++] = '0'; else
			if (strchr("rR",text[1])) output[i++] = '1'; else
			if (strchr("gG",text[1])) output[i++] = '2'; else
			if (strchr("yY",text[1])) output[i++] = '3'; else
			if (strchr("bB",text[1])) output[i++] = '4'; else
			if (strchr("mM",text[1])) output[i++] = '5'; else
			if (strchr("cC",text[1])) output[i++] = '6'; else
			if (strchr("wW",text[1])) output[i++] = '7';
		}

		output[i++] = 'm';
		output[i] = '\0';
		copy_in(result, reslen, output);
	}
	if (*input == '&') {
		bool fg_done = false;
		/* first let us test this is a valid colour string */
		const char *p = input + 1;
		/* skip past all valid chars */
		while (*p && strchr("0123456789aAbBcCdDeEfF:", *p)) p++;
		/* not the ending we were expecting, ignore it then */
		if (*p != '&') return 0;
		/* empty pattern is not a pattern */
		if (p == input + 1) return 0;

		int outused = 0;

		do {
			char hex[7];
			int i = 0;
			consume++;
			while (i < 7 && input[consume] && strchr("0123456789aAbBcCdDeEfF", input[consume]))
				hex[i++] = input[consume++];
			hex[i] = '\0';

			if (i == 1) {
				int col = hex_decode(hex, 1);
				if (col < 10)
					snprintf(&output[outused], outlen-outused, "\033[%dm", col);
			} else
			if (i == 2) {
				int col = hex_decode(hex, 2);
				snprintf(&output[outused], outlen-outused, "\033[%d;5;%dm", fg_done?48:38, col);
				fg_done = true;
			} else {
				return 0;
			}
			outused = strlen(output);
		} while(input[consume] == ':');

		if (input[consume] != '&') {
			return 0;
		}
		consume++;
	}
	copy_in(result, reslen, output);
	return(consume);
}

void colour_free(void)
{
	int i;

	if (colour_set!=NULL) free(colour_set);
	for (i=0;i<COLOUR_LIMIT;i++)
		if (colour_chart[i]!=NULL) free(colour_chart[i]);
}

void colour_load(char *file, int quiet)
{
	FILE *ff;
	char *buff;
	int i;
	int count=0;

	if ((ff=fopen(file,"r"))==NULL)
	{
		printf("Error: Cannot open %s:%s\n",file, strerror(errno));
		return;
	}

	for (i=0;i<COLOUR_LIMIT;i++)
	{
		if (colour_chart[i]!=NULL) free(colour_chart[i]);
		colour_chart[i]=NULL;
	}
	/* throw away first line */
	if (colour_set!=NULL) free(colour_set);
	colour_set = frl_line_nspace(ff, "#");
	if (!quiet) printf("Loading colour scheme: %s\n", colour_set);

	while (!feof(ff) &&  ((buff = frl_line_nspace(ff, "#"))!=NULL))
	{
		char *c;

		if ((c=strtok(buff," \t"))==NULL) { free(buff); continue; }
		i=atoi(c);
		if (i<0 || i>=COLOUR_LIMIT) { free(buff); continue; }

		if ((c=strtok(NULL, "\n"))==NULL) { free(buff); continue; }

		while (*c==' ' || *c=='\t') c++;

		if (colour_chart[i]!=NULL)
			if (!quiet) printf("Warning: redefining colour %d\n",i);

		colour_chart[i]=strdup(c);
		count++;
		free(buff);
	}
	if (!quiet) printf("Loaded %d colour definitions.\n",count);
}

void init_colour(void)
{
	int i;
	char path[1024];

	for (i=0;i<COLOUR_LIMIT;i++) colour_chart[i]=NULL;

	snprintf(path, 1023, COLOURDIR"/%d.col", user->record.colour);
	if (access(path, R_OK))
	{
		printf("Error loading colour scheme %d.\n", user->record.colour);
		printf("Changing to default scheme.\n");
		user->record.colour=0;
		snprintf(path, 1023, COLOURDIR"/%d.col", user->record.colour);
	}
	colour_load(path, 1);

	/* CFRY: need to find default terminal fore/back colour, and whether it supports the 'concealed' ascii-seqence for text */

}

char *get_colour(void)
{
	return(colour_set);
}

void destroy_colours(void)
{
	int i;

	for (i=0;i<COLOUR_LIMIT;i++)
	{
		if (colour_chart[i]!=NULL) free(colour_chart[i]);
		colour_chart[i]=NULL;
	}
	if (colour_set!=NULL) free(colour_set);
	colour_set=NULL;
}
