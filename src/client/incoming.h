#ifndef INCOMING_H
#define INCOMING_H
#include <ipc.h>

/* Incoming message flags */
#define MST_EVENT	0x01	/* Event script will be called */
#define MST_BEEP	0x02	/* Message will be accompanied by a beep */
#define MST_HIDDEN	0x04	/* Won't be displayed (set by 'output') */
#define MST_KICK	0x08	/* Other scripts will be terminated, eg zod */
#define MST_EXIT	0x10	/* Will cause MW to exit, eg mrod (not used) */
#define MST_SCREV	0x20	/* Script event - RPC, IPC etc... */

/* event stack types */
#define EST_IPC		1
#define EST_RPC		2
#define EST_CHECKONOFF	3

struct mstack
{
	const char *text;
	const char *from;
	int flags;
	int preamble; /* Extra chars added by global/timestamp/spy */
	struct mstack *next;
	ipc_message_t *msg;
	struct user *sender;
};

extern int new_mail_waiting;

void incoming_mesg(int ignore);
void handle_mesg(void);
extern int MesgIsStacked(void);
void DisplayStack(void);
void ClearStack(void);
void PopMesg(void);

#endif /* INCOMING_H */
