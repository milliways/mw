/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/
#include <stdio.h>
#include <fcntl.h>
#include <ctype.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include "files.h"
#include "alarm.h"
#include "strings.h"
#include "str_util.h"
#include "perms.h"
#include "read.h"
#include "main.h"
#include "init.h"
#include "intl.h"
#include "add.h"
#include "talker.h"
#include "bb.h"
#include "userio.h"
#include "mesg.h"

extern int eof_caught;
extern int remote;
extern int busy;
extern int internet;

static void tidy_string(char *buff)
{
	int linelength=0;
	int ptr=0;
	int screen_width=screen_w();

	while (buff[ptr]!=0)
	{
		if (buff[ptr]!='\n')
		{
			linelength++;
			if (linelength>=screen_width)
			if (buff[ptr]==' ')
			{
				buff[ptr]='\n';
				linelength=0;
				ptr++;
			}
			else
			{
				while (buff[ptr]!=' ' && buff[ptr]!='\n')
					ptr--;
				if (buff[ptr]==' ')
				{
					buff[ptr]='\n';
					linelength=0;
					ptr++;
				}else
				{
					ptr+=linelength;
					buff[ptr]='\n';
					linelength=0;
					ptr++;
				}
			}
			else
				ptr++;
		}else
		{
			linelength=0;
			ptr++;
		}
	}
}

static void filter_returns(char *buff) /* and tabs */
{
	int a;
	int blank;
	blank=0;
	a=0;
	while(buff[a]!=0)
	{
		if (buff[a]=='\t') buff[a]=' ';
		if (buff[a]=='\n')
		{
			if (blank!=1 && buff[a+1]!='\n')
				buff[a]=' ';
			if (buff[a+1]=='\n') blank=1;
			else blank=0;
		}
		a++;
	}
}

int add_msg(int folnum, struct user *user, int replyto)
{
	struct Header *new;
	struct Header inreplyto;
	long replyposn=0l;
	struct folder *fol;
	int i,data,folderfile;
	int ptr=0;
	int limit;
	char *buff;
	char answer[10];
	char c; /* temporary storage of a character */
	struct user usr; /*temp*/
	long tt,td; /* for calculating position error in index file */

	fol=(struct folder *)malloc(sizeof(*fol));
	new=(struct Header *)malloc(sizeof(*new));

	if (!get_folder_number(fol,folnum))
	{
		free(fol);
		free(new);
		return 0;
	}

	/* check write status of folder */
	if (!allowed_w(fol,user))
	{
		printf(_("Not allowed to write to this folder.\n"));
		free(fol);
		free(new);
		return(0);
	}
	/* Assign header information */
	if (!remote)
	{
		if (replyto==0)
			printf(_("Writing in folder %s.\n"),fol->name);
		else
			printf(_("Replying to message %d in folder %s\n"),replyto,fol->name);
	}

	if (!remote && is_private(fol,user))
	{
		printf(_("Note: Operators have the right to read any 'private' messages.\n"));
		printf(_("    : If you don't like this, don't write private messages.\n"));
	}
	if (replyto>0)
	{
		long posn,tmppos;
		int headfile;

		headfile = open_folder_index(fol, FOL_LIVE, O_RDONLY, 0);
		if (headfile < 0)
		{
			free(fol);
			free(new);
			return(0);
		}

		posn=(replyto-(fol->first))*sizeof(struct Header);
		tmppos=lseek(headfile, 0, SEEK_END);
		if (posn<0 || posn>tmppos-sizeof(struct Header))
		{
			printf(_("There is no message %d\n"),replyto);
			close(headfile);
			free(fol);
			free(new);
			return(-1);
		}else
			lseek(headfile, posn, SEEK_SET);
		while (get_data(headfile,&inreplyto) && inreplyto.Ref<replyto);
		if (inreplyto.Ref!=replyto)
		{
			printf(_("reply: Error, could not find old message !\n"));
			close(headfile);
			free(fol);
			free(new);
			return(0);
		}
		replyposn=lseek(headfile, 0, SEEK_CUR)-sizeof(inreplyto);
		close(headfile);
		inreplyto.status|=(1<<2);
	}

	new->replyto=replyto;
	new->status=0;
	if (remote) new->status|=(1<<3);

	new->date=time(0);
	if (!remote) printf(_("Message from %s\n"), user->record.name);
	strncpy(new->from, user->record.name, NAMESIZE);
	new->from[NAMESIZE] = '\0';

	if (replyto==0)
	{
		char t[NAMESIZE+1];
		printf(_("Send to: "));
		get_str(new->to,NAMESIZE);
		strncpy(t,new->to,NAMESIZE);
		strip_name(t);
		if (is_private(fol,user) && !user_exists(t, &usr))
		{
			printf(_("Message must be addressed to an existing user.\n"));
			free(fol);
			free(new);
			return(0);
		}
		printf(_("Subject: "));
		get_str(new->subject,SUBJECTSIZE);
	} else
	{
		char foo[SUBJECTSIZE+1];
		printf(_("\nEnter subject or press <ENTER> to accept old.\n"));
		printf(_("Subject: %s\n"),inreplyto.subject);
		printf(_("Subject: "));
		get_str(foo,SUBJECTSIZE);
		if (*foo)
		{
			printf(_("Send to: "));
			get_str(new->to,SUBJECTSIZE);
			if (is_private(fol,user) && !user_exists(new->to, &usr))
			{
				printf(_("Message must be addressed to an existing user.\n"));
				free(fol);
				free(new);
				return(0);
			}
			strncpy(new->subject,foo,SUBJECTSIZE);
		}
		else
		{
			strncpy(new->subject,inreplyto.subject,SUBJECTSIZE);
			strncpy(new->to,inreplyto.from,SUBJECTSIZE);
			printf(("Marked as To: %s\n"),new->to);
		}
	}
	/* read in main message text */

	clearerr(stdin);
	buff=(char *)malloc(1024);
	limit=1024;
	if (!remote)
	printf(_("Enter message ending with a . on a new line\n"));

	/*if (!feof(stdin)) buff[ptr++]=getachar(); */
	while (!feof(stdin) && buff[ptr-1]!=26 && buff[ptr-1]!=4)
	{
		c=getachar();

		if ((c=='.') && ((buff[ptr-1]=='\n') || (ptr==0))) break;

		if (c=='\n') reset_timeout(user->record.timeout);
		if ((isprint(c) || isspace(c)) && c!='\r' ) buff[ptr++]=c;
		if (ptr==limit){
			limit+=1024;
			buff=(char *)realloc(buff,limit);
		}
	}
/*	if(buff[ptr-1]=='.')
	{
		if(internet)
			getachar();
		ptr-=2;
	}
*/
	buff[ptr]=0;
	tidy_string(buff);
	new->size=ptr;
	if (!feof(stdin)&&!internet) read2cr();
	clearerr(stdin);
	/* read in current folder status & prepare message for saveing */

	if (!remote)
	do{
		printf(_("Post message, edit, reformat, or Abandon ?"));
		get_str(answer,9);
		if (eof_caught==1) break;
		if (stringcmp(answer,_("abandon"),1))
		{
			free(new);
			free(fol);
			free(buff);
			return(0);
		}
		if (stringcmp(answer,_("reformat"),1))
		{
			printf(_("Reformatting..."));
			filter_returns(buff);
			tidy_string(buff);
			printf(_("Done.\r\n"));
		}
		if (stringcmp(answer,_("edit"),1))
		{
			int tfile;
			int mask;
			char foo[128];
			char fullpath[18];
			long size;
			const char *myeditor;
			int child;

			if (u_reg(user))
			{
				if ((myeditor=getenv("EDITOR"))==NULL)
					myeditor=SECUREEDITOR;
			}else
				myeditor=SECUREEDITOR;

			if (perms_drop()==-1)
			{
				printf(_("Failed to set user id, aborting.\n"));
				perror("setuid");
				free(fol);
				free(new);
				free(buff);
				return(0);
			}
			if (chdir("/tmp"))
			{
				perror("chdir");
				free(fol);
				free(new);
				free(buff);
				return(0);
			}
			snprintf(fullpath,17,"mw3XXXXXX");
			mask=umask(~0600);
			tfile = mkstemp(fullpath);
			if (tfile < 0)
			{
				printf(_("Cannot create temporary file: %s\n"), strerror(errno));
				perms_restore();
				free(fol);
				free(new);
				free(buff);
				return(0);
			}
			write(tfile,buff,new->size);
			close(tfile);
			umask(mask);

			sprintf(foo,"%s %s",myeditor,fullpath);

			busy++;
			if ((child=fork())==0)
			{
				/* we are child */
				signal(SIGUSR1,SIG_IGN);
				execl(myeditor, myeditor, fullpath, NULL);
				exit(0);
			}else
			if (child==-1)
			{
				printf(_("ERROR: Could not spawn editor: %s\n"),strerror(errno));
			}else
			{
				int a;

				do {
					a=wait(NULL);
					if (a==-1 && errno!=EINTR)
					printf(_("ERROR: child wait (%d) %s\n"),errno, strerror(errno));
				}while (a==-1 && errno==EINTR);
			}
			busy--;

			if ((tfile=open(fullpath,O_RDONLY))<0)
			{
				perror("reading temp file");
				free(fol);
				free(new);
				free(buff);
				return(0);
			}
			size=lseek(tfile, 0, SEEK_END);
			lseek(tfile, 0, SEEK_SET);
			buff=(char *)realloc(buff,size);
			read(tfile,buff,size);
			new->size=size;
			close(tfile);
			unlink(fullpath);
			perms_restore();
		}
	}while (!stringcmp(answer,_("post"),1));

	folderfile=openfolderfile(O_RDWR);
	Lock_File(folderfile);

	/* open files ready for saving */
	const char *subdir = is_moderated(fol, user) ? FOL_MOD : FOL_LIVE;
	i = open_folder_index(fol, subdir, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
	if (i < 0)
		exit(-1);
	data = open_folder_text(fol, subdir, O_WRONLY|O_APPEND|O_CREAT, S_IRUSR|S_IWUSR);
	if (data < 0)
		exit(-1);
	Lock_File(folderfile);
	Lock_File(i);
	Lock_File(data);

	new->datafield=lseek(data, 0l, SEEK_END);
	lseek(folderfile, folnum * sizeof(struct folder), SEEK_SET);
	read(folderfile,fol,sizeof(*fol));
	lseek(folderfile, -1 * sizeof(struct folder), SEEK_CUR);
	if (!is_moderated(fol,user))
	{
		new->Ref=fol->last+1;
		fol->last++;
		if (fol->first==0) fol->first=1;
		if (replyto>0)
		{
			lseek(i, replyposn, SEEK_SET);
			write(i,&inreplyto,sizeof(inreplyto));
		}
	}else
	{
		new->Ref=0;
	}

	tt=lseek(i, 0, SEEK_END);
	td=tt%sizeof(*new);

	if (td>0){
		printf(_("Warning: Index error, skipping back %ld bytes.\r\n"),td);
		lseek(i, -td, SEEK_CUR);
	}
	if ((write(i,new,sizeof(*new)))<sizeof(*new))
	{
		perror("index");
		exit(-1);
	}
	if ((write(data,buff,new->size))<new->size)
	{
		perror("text");
		exit(-1);
	}
	free(buff);
	if (!is_moderated(fol,user))
	{
	    	int wrote = write(folderfile, fol, sizeof(*fol));
		if (wrote < 0)
		{
			perror("folders");
			exit(-1);
		} else if (0 <= wrote && wrote < sizeof(*fol))
		{
		    	fprintf(stderr, _("Warning: incomplete folder record written; folder file may be corrupt.\n"));
		}
	}
	Unlock_File(data);
	Unlock_File(i);
	Unlock_File(folderfile);

	close(i);
	close(data);
	close(folderfile);
	if (is_private(fol,user)) inform_of_mail(new->to);

	/* tell everyone what we just did */
	postinfo(user,fol,new);
	free(new);
	free(fol);
	return(0);
}
