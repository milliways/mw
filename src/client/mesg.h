#ifndef MESG_H
#define MESG_H

#include <mesg.h>

void broadcast(int state, const char *fmt, ...) __attribute__((format(printf,2,3)));
void inform_of_mail(char *to);
void postinfo(struct user *who, struct folder *fol, struct Header *mesg);

#endif /* MESG_H */
