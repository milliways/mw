#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <termcap.h>
#include <sys/types.h>
#include <stdbool.h>

#include <sqlite.h>
#include <mwlog.h>
#include "bb.h"
#include "talker_privs.h"
#include "special.h"
#include "talker.h"
#include "str_util.h"
#include "strings.h"
#include "special.h"
#include "js.h"
#include "sqlite.h"
#include "perms.h"
#include "main.h"
#include "init.h"
#include "echo.h"
#include "rooms.h"
#include "ipc.h"
#include "log.h"
#include "uri.h"
#include "util.h"
#include "intl.h"
#include "userio.h"
#include "user.h"
#include "who.h"
#include "mesg.h"
#include "incoming.h"
#include "alias.h"
#include "onoff.h"
#include "script.h"

extern int busy; /* if true dont display messages  i.e. during new/write */
extern unsigned long rights;

extern struct user * const user;
extern int quietmode;
static int runautoexec = 1;

extern CommandList table[];
extern CommandList chattable[];

extern int g_boTermCap;

static void talk_send_to_room(const char * text, int channel, const char * type, int plural) {
	ipc_message_t * msg = ipcmsg_create(IPC_SAYTOROOM, user->posn);
	ipcmsg_destination(msg, channel);
	json_t * j = json_init(NULL);
	json_addstring(j, "text", text);
	json_addstring(j, "type", type);
	if (plural > -1) json_addint(j, "plural", plural);
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void talk_send_shout(char * text){
	mwlog(user, "SHOUT %s", text);
	ipc_message_t * msg = ipcmsg_create(IPC_SAYTOALL, user->posn);
	json_t * j = json_init(NULL);
	json_addstring(j, "text", text);
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

static void talk_send_say(const char * text, int channel)
{
	mwlog(user, "SAY %s", text);
	catchuri(text);
	talk_send_to_room(text, channel, "say", -1);
}

void talk_send_raw(char * text, int channel){
	mwlog(user, "RAW %s", text);
	catchuri(text);
	talk_send_to_room(text, channel, "raw", -1);
}

void talk_send_rawbcast(char * text){
	mwlog(user, "RAWBCAST %s", text);
	ipc_send_to_all(IPC_TEXT, text);
}

void talk_send_emote(char * text, int channel, int plural){
	catchuri(text);
	mwlog(user, "EMOTE %s", text);
	talk_send_to_room(text, channel, "emote", plural);
}

void talk_sayto(char *text, const char *to, const char *type)
{
	ipc_message_t * msg = ipcmsg_create(IPC_SAYTOUSER, user->posn);
	json_t * j = json_init(NULL);
	json_addstring(j, "target", to);
	json_addstring(j, "type", type);
	json_addstring(j, "text", text);
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);

	mwlog(user, "SAYTO %s %s",to,text);
}

int screen_h(void)
{
	char	*szLines;
	int	nLines = -1;

	/* we have terminal information */
	if (g_boTermCap == 1)
	{
		char li[] = "li";
		/* check if line count exists */
		nLines = tgetnum(li);
	}

	/* not using termcap, or lines not found */
	if (nLines == -1)
	{
		/* fall back on trying to read the number of lines from the environment */
		szLines = getenv("LINES");
		/* if the number of lines could not be found, default to 24 */
		if (szLines == NULL) nLines = 24;
		/* otherwise get the number of lines */
		else nLines = atoi(szLines);
	}

	/* return the line count */
	return (nLines);
}

int screen_w(void)
{
	char	*szCols;
	int	nCols = -1;

	/* we have terminal information */
	if (g_boTermCap == 1)
	{
		char co[] = "co";
		/* check if column count exists */
		nCols = tgetnum(co);
	}

	/* not using termcap, or columns not found */
	if (nCols == -1)
	{
		/* fall back on trying to read the number of columns from the environment */
		szCols = getenv("COLUMNS");
		/* if the number of columns could not be found, default to 80 */
		if (szCols == NULL) nCols = 80;
		/* otherwise get the number of columns */
		else nCols = atoi(szCols);
	}

	/* return the column count */
	return (nCols);
}

void t_who(CommandList *cm, int argc, const char **argv, char *args)
{
	display_wholist(0);
}

void t_what(CommandList *cm, int argc, const char **argv, char *args)
{
	//who_list(1);
	display_wholist(1);
}

void t_uptime(CommandList *cm, int argc, const char **argv, char *args)
{
	ipc_message_t * msg = ipcmsg_create(IPC_UPTIME, user->posn);
	ipcmsg_transmit(msg);
}

void t_quit(CommandList *cm, int argc, const char **argv, char *args)
{
	user->record.chatmode = cm_clear(user, CM_ONCHAT);
	update_user(user);
	announce_leave(user->record.name, user->record.room, LEAVE_NORMAL, NULL, NULL, quietmode);
	set_rights();
}

void t_script(CommandList *cm, int argc, const char **argv, char *args)
{
	if (argc<2) ListScript(NULL); else ListScript(argv[1]);
}

void t_event(CommandList *cm, int argc, const char **argv, char *args)
{
	/* no arguments */
	if (argc < 2)
	{
		char	*event_text = NULL;
		int	event_count = 0;

		/* show list of events */
		while((event_text = NextLink(event_list, event_text)) != NULL)
		{
			event_count++;
			if (event_count == 1) printf("List of current event functions:\n");
			printf("  - %s\n", event_text);
		}
		if (!event_count) printf("No text events set\n");
		return;
	}

	if (AddLink(&event_list, argv[1], ""))
		printf("Event bind '%s' already exists!\n", argv[1]);
	else
		printf("Event '%s' added!\n", argv[1]);
}

void t_unevent(CommandList *cm, int argc, const char **argv, char *args)
{
	if (!strcasecmp("*", argv[1]))
	{
		DestroyAllLinks(&event_list);
		printf("All Events Destroyed!\n");
	}
	else
	{
		if (!DestroyLink(&event_list, argv[1]))
			printf("Event '%s' was not found!\n", argv[1]);
		else
			printf("Event '%s' was destroyed...\n", argv[1]);
	}
}

static int isanum(const char *a, int *result, int onlydecimal)
{
	char *end;
	if (a==NULL || strlen(a)<1) return(0);

	*result=strtol(a, &end, 10);
	if (*end==0)
	{
		return (1);
	}
	else if (!onlydecimal)
	{
		*result=strtol(a, &end, 16);
		if (*end==0)
		{
			return (1);
		}
		else
		{
			return (0);
		}
	}
	else
	{
		return (0);
	}
}

static int isanumul(const char *a, unsigned long *result, int onlydecimal)
{
	char *end;
	if (a==NULL || strlen(a)<1) return(0);

	*result=strtoul(a, &end, 10);
	if (*end==0)
	{
		return (1);
	}
	else if (!onlydecimal)
	{
		*result=strtoul(a, &end, 16);
		if (*end==0)
		{
			return (1);
		}
		else
		{
			return (0);
		}
	}
	else
	{
		return (0);
	}
}

void t_runaway(CommandList *cm, int argc, const char **argv, char *args)
{
	extern unsigned long run_away;
	unsigned long num = 0;

	if (isanumul(argv[1], &num, 1))
	{
		if (cp_test(user, CP_ADVSCR))
		{
			if (!u_god(user))
			{
				if (num < 1)
				{
					printf("Invalid Runaway. Must be greater than 0\n");
					return;
				}
			}
			run_away = num;
			if (run_away == 0) printf("Runaway disabled\n");
			else printf("Runaway set to value: %lu\n", run_away);
		}
		else
		{
			if ((num < 1) || (num > MAXRUNAWAY))
			{
				printf("Invalid Runaway. Must be between 1 and %d\n", MAXRUNAWAY);
				return;
			}
			run_away = num;
			printf("Runaway set to value: %ld\n", run_away);
		}
	}
	else
	{
		if (cp_test(user, CP_ADVSCR))
			printf("Invalid Runaway. A number greater than 0 must be specified\n");
		else
			printf("Invalid Runaway. A number between 1 and %d must be specified\n", MAXRUNAWAY);
	}
}

void t_flood(CommandList *cm, int argc, const char **argv, char *args)
{
	int num = 0;

	if (isanum(argv[1], &num, 1))
	{
		js_flood_limit = num;
		printf("Flood limit set to value: %d\n", js_flood_limit);
	}
}

void t_load(CommandList *cm, int argc, const char **argv, char *args)
{
	if (argc<2)
	{
		printf("Specify the file name.\n");
		return;
	}

	if (LoadInitFile(argv[1])) 
		fprintf(stderr, "Error loading initfile %s\n", argv[1]);

	/* run board init functions */
	RunInitFuncs(0);
	/* run talker init functions */
	RunInitFuncs(1);
}

void t_destroy(CommandList *cm, int argc, const char **argv, char *args)
{
	if (argc<2)
	{
		printf("Specify the function to destroy.\n");
		return;
	}

	if (!strcmp(argv[1],"*")) DestroyAllFunctions(1); else DestroyFunction(argv[1]);
}

void t_mwrc(CommandList *cm, int argc, const char **argv, char *args)
{
	if (argc < 2) {
	        char * old = userdb_get(USERDB_PRIVATE, user->record.name, "mwrc");
		if (old==NULL) {
			printf(_("Current mwrc path: %s\n"), _("<unset>"));
		} else {
			printf(_("Current mwrc path: %s\n"), old);
			free(old);
		}
	} else {
		userdb_set(USERDB_PRIVATE, user->record.name, "mwrc", argv[1]);
	        char * old = userdb_get(USERDB_PRIVATE, user->record.name, "mwrc");
		printf(_("Setting mwrc path to: %s\n"), old==NULL?"<unset>":old);
		free(old);
	}
}

void t_restart(CommandList *cm, int argc, const char **argv, char *args)
{
	DestroyAllLinks(&bind_list); bind_list = NULL;
	DestroyAllLinks(&alias_list); alias_list = NULL;
	DestroyAllLinks(&rpc_list); rpc_list = NULL;
	DestroyAllLinks(&event_list); event_list = NULL;
	DestroyAllLinks(&eventin_list); eventin_list = NULL;
	DestroyAllLinks(&onoff_list); onoff_list = NULL;
	DestroyAllLinks(&ipc_list); ipc_list = NULL;
	DestroyAllLinks(&shutdown_list); shutdown_list = NULL;

	stop_js();
	setup_js();

	RoomDestroy(&user->room);

	RoomInit(&user->room);
	LoadRoom(&user->room, user->record.room);

	LoadInitFile(NULL);

	/* run board init functions */
	RunInitFuncs(0);
	/* run talker init functions */
	RunInitFuncs(1);
}

void t_shout(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0]=0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);
	talk_send_shout(text);
}

void t_whisper(CommandList *cm, int argc, const char **argv, char *args)
{
	talk_sayto(args, argv[1], "whispers");
}


void t_sayto(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0]=0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	const char *type = "says";

	if (text[strlen(text)-1] == '?') type = "asks";
	talk_sayto(text, argv[1], type);
}

void t_sayto_warn(CommandList *cm, int argc, const char **argv, char *args)
{
	display_message("*** .say as an alias for .sayto is deprecated.  Please use .sa instead.", 1, 1);
	t_sayto(cm, argc, argv, args);
}

void t_notsayto(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	snprintf(text, MAXTEXTLENGTH, "%s", args);
	mwlog(user, "NSAYTO %s %s", argv[1], text);

	/* variant of say_to_room but with an exclude clause */
	ipc_message_t * msg = ipcmsg_create(IPC_SAYTOROOM, user->posn);
	ipcmsg_destination(msg, user->record.room);
	json_t * j = json_init(NULL);
	json_addstring(j, "text", text);
	json_addstring(j, "type", "notsayto");
	json_addstring(j, "exclude", argv[1]);
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void t_ventril(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];
	int i;

	text[0]=0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	i = 0;
	if (isanum(argv[1], &i, 1))
	{
		if (i<0 || i>65535)
		{
			display_message("Invalid room number. Must be between 0 and 65535", 1, 1);
		}else
		{
			talk_send_say(text,i);
		}
	}
	else
	{
		display_message("Invalid room number. Must be a number between 0 and 65535", 1, 1);
	}
}

void t_ventril_raw(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];
	int i;

	text[0]=0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	i = 0;
	if (isanum(argv[1], &i, 1))
	{
		if (i<0 || i>65535)
		{
			display_message("Invalid room number. Must be between 0 and 65535", 1, 1);
		}else
		{
			talk_send_raw(text,i);
		}
	}
	else
	{
		display_message("Invalid room number. Must be a number between 0 and 65535", 1, 1);
	}
}

void t_emote(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0] = 0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	talk_send_emote(text, user->record.room, 0);
}

void t_emotes(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0] = 0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	talk_send_emote(text, user->record.room, 1);
}

void t_emotes2(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0] = 0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	talk_send_emote(text, user->record.room, 2);
}

void t_emoted(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0] = 0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	talk_send_emote(text, user->record.room, 3);
}

void t_emotell(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0] = 0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);

	talk_send_emote(text, user->record.room, 4);
}

void t_global(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	unsigned long global = cm_test(user, CM_GLOBAL);

	if (z==1)
	{
		if (!global)
		{
			user->record.chatmode = cm_set(user, CM_GLOBAL);
			display_message("Global receive mode Enabled.", 1, 1);
		}else
			display_message("Already in Global receive mode.", 1, 1);
	}else
	if (z==0)
	{
		if (global)
		{
			user->record.chatmode = cm_clear(user, CM_GLOBAL);
			display_message("Global receive mode Disabled.", 1, 1);
		}else
			display_message("You are not in Global receive mode.", 1, 1);
	}else
		printf("%s\n", cm->ArgError);
	update_user(user);
}

void t_sticky(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	unsigned long sticky = cm_test(user, CM_STICKY);

	if (z==1)
	{
		if (!sticky)
		{
			user->record.chatmode = cm_set(user, CM_STICKY);
			display_message("Sticky room mode Enabled.", 1, 1);
		}else
			display_message("Already in Sticky room mode.", 1, 1);
	}else
	if (z==0)
	{
		if (sticky)
		{
			user->record.chatmode = cm_clear(user, CM_STICKY);
			display_message("Sticky room mode Disabled.", 1, 1);
		}else
			display_message("You are not in Sticky room mode.", 1, 1);
	}else
		printf("%s\n",cm->ArgError);
	update_user(user);
}

void t_tcunames(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	if (z==0)
	{
		if (s_tcunames(user))
		{
			user->record.special &= ~(1 << MWSPCL_TCUNAMES);
			printf("Username Tab-Completion now disabled.\n");
		}else
			printf("Username Tab-Completion was already off.\n");
	}else
	if (z==1)
	{
		if (!s_tcunames(user))
		{
			user->record.special |= (1 << MWSPCL_TCUNAMES);
			printf("Username Tab-Completion now enabled.\n");
		}else
			printf("Username Tab-Completion already enabled.\n");
	}else
		printf("%s\n",cm->ArgError);
	update_user(user);
}

void t_debug(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	extern int script_debug;

	if (z==1)
	{
		if (!script_debug)
		{
			script_debug=1;
			display_message("Script debug mode Enabled.", 1, 1);
		}else
			display_message("Already in script debug mode.", 1, 1);
	}else
	if (z==0)
	{
		if (script_debug)
		{
			script_debug=0;
			display_message("Script debug mode Disabled.", 1, 1);
		}else
			display_message("You are not in script debug mode.", 1, 1);
	}else
		printf("%s\n",cm->ArgError);
	update_user(user);
}

void t_spy(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	unsigned long global = cm_test(user, CM_SPY);

	if (z==1)
	{
		if (!global)
		{
			user->record.chatmode = cm_set(user, CM_SPY);
			display_message("Spy mode Enabled.", 1, 1);
		}else
			display_message("Already in Spy mode.", 1, 1);
	}else
	if (z==0)
	{
		if (global)
		{
			user->record.chatmode = cm_clear(user, CM_SPY);
			display_message("Spy mode Disabled.", 1, 1);
		}
		else
			display_message("You are not in Spy mode.", 1, 1);
	}else
		printf("%s\n",cm->ArgError);
	update_user(user);
}

void t_sproof(CommandList *cm, int argc, const char **argv, char *args)
{
	if(argc == 1) {
		char * sproof = db_room_get(user->record.room, "soundproof");
		if (sproof == NULL || sproof[0]!='1')
			display_message("This room is not soundproof.", 0, 1);
		else
			display_message("This room is soundproof.", 0, 1);
		if (sproof != NULL) free(sproof);

		return;
	}

	int z=BoolOpt(argv[1]);
	char * was = db_room_get(user->record.room, "soundproof");
	if ( z == 1) {
		if (was !=NULL && was[0] == '1') {
			display_message("This room is already soundproof.", 0, 1);
		} else {
			db_room_set(user->record.room, "soundproof", "1");
			char text[MAXTEXTLENGTH];
			snprintf(text, MAXTEXTLENGTH-1,
			         "\03315%s has just soundproofed this room.",
			         user->record.name);
			talk_send_raw(text, user->record.room);
		}
	} else
	if (z == 0) {
		if (was == NULL || was[0]!='1') {
			display_message("This room was not soundproof.", 0, 1);
		} else {
			db_room_set(user->record.room, "soundproof", "0");
			char text[MAXTEXTLENGTH];
			snprintf(text, MAXTEXTLENGTH-1,
			     "\03315%s has just removed the soundproofing for this room.",
			     user->record.name);
			talk_send_raw(text, user->record.room);
		}
	}
	free(was);
	//JSM
}


void t_protect(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];
	int myapl = (user->record.chatprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	int level = -1;

	/* If the sender has an authorised protection level of 0, they can't
	 * give a 2nd argument (and shouldn't be told it's possible).
	 */
	if (argc < 2 || argc > 3 || (myapl<=0 && argc>2))
	{
	    if (myapl > 0)
	    {
		display_message("Usage: protect <user> [<level>]", 1, 1);
	    } else
	    {
		display_message("Usage: protect <user>", 1, 1);
	    }
	    return;
	}

	/* if protection priv level has been granted to you, then check if we
	 * can append the third protection level argument.
	 */
	if (myapl > 0 && argc >= 3)
	{
		/* if protection level is not a valid number, or it is outside
		 * you priv range, then you cannot grant that level of
		 * protection.
		 */
		if (sscanf(argv[2],"%d", &level) != 1 || level < 0 || level > myapl)
		{
			sprintf(text, "Invalid protection level (available: 0-%d)", myapl);
			display_message(text, 1, 1);
			return;
		}
	}

	/* Note: level may be -1, in which case only temporary protection was
	 * granted (This is now handled by the receiver).
	 */
	snprintf(text, 40, "%d", level);
	ipc_send_to_username(argv[1], IPC_PROTLEVEL, text);

	/* We have given them a protection level of any kind, so give them the
	 * standard protection priv for compatibility.  Even if they are
	 * running an old version (as long as the protect/unprotect is
	 * successful), they won't notice this, because the IPC_PROTLEVEL
	 * will have set the 'p' chatmode flag to the same value.
	 */
	if (level != 0)
	{
		/* give them the old temporary protection as well/instead */
		snprintf(text,40,"+p");
		ipc_send_to_username(argv[1], IPC_CHATMODE, text);
		snprintf(text, MAXTEXTLENGTH-1, "\03315%s has just tried to protect %s",
		         user->record.name, argv[1]);
	}
	/* protect level is zero (wtf?!?), so handle unprotection (erm.. unprotect anyone?) */
	else
	{
		snprintf(text,40,"-p");
		ipc_send_to_username(argv[1], IPC_CHATMODE, text);
		snprintf(text, MAXTEXTLENGTH-1, "\03315%s has just tried to unprotect %s",
		         user->record.name, argv[1]);
	}
	talk_send_raw(text, user->record.room);
}

void t_unprotect(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	snprintf(text,40,"-p");
	ipc_send_to_username(argv[1], IPC_CHATMODE, text);
	snprintf(text,40,"0");
	ipc_send_to_username(argv[1], IPC_PROTLEVEL, text);

	snprintf(text, MAXTEXTLENGTH-1, "\03315%s has just tried to unprotect %s",
	         user->record.name, argv[1]);
	talk_send_raw(text, user->record.room);
}

void t_summon(CommandList *cm, int argc, const char **argv, char *args)
{
	char		text[MAXTEXTLENGTH];
	int		newroom;

	if (sscanf(argv[1],"%d",&newroom)!=1)
	{
		display_message("Invalid Room ID (0-65535 only)", 1, 1);
		return;
	}

	if (newroom<0 || newroom>65535)
	{
		display_message("Invalid Room ID (0-65535 only)", 1, 1);
		return;
	}

	for (int i = 2; i < argc; i++)
	{
		int j;
		/* go through all username arguments in front of this one */
		for (j = 2; j < i; j++)
		{
			/* if any usernames match, then break out of the loop */
			if (!strcasecmp(argv[i], argv[j])) break;
		}
		/* if a username match was found, skip this argument */
		if (j != i) continue;

		snprintf(text, MAXTEXTLENGTH-1, "\03315%s has just tried to summon %s to room %d",
		         user->record.name, argv[i], newroom);
		talk_send_raw(text, user->record.room);
		snprintf(text, MAXTEXTLENGTH-1, "-%05d", newroom);
		ipc_send_to_username(argv[i], IPC_CHANNEL, text);
	}
}

void t_freeze(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	snprintf(text,40,"+f");
	ipc_send_to_username(argv[1], IPC_CHATMODE, text);
	snprintf(text, MAXTEXTLENGTH-1, "\03315%s has just tried to freeze %s", user->record.name, argv[1]);
	talk_send_raw(text, user->record.room);
}

void t_unfreeze(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	snprintf(text,40,"-f");
	ipc_send_to_username(argv[1], IPC_CHATMODE, text);
	snprintf(text,MAXTEXTLENGTH-1,"\03315%s has just tried to unfreeze %s", user->record.name, argv[1]);
	talk_send_raw(text, user->record.room);
}

void t_gag(CommandList *cm, int argc, const char **argv, char *args)
{
	ipc_message_t * msg = ipcmsg_create(IPC_ACTION, user->posn);
	json_t * j = json_init(NULL);
	json_addstring(j, "target", argv[1]);
	json_addstring(j, "type", "gag");
	json_addstring(j, "gag", argv[2]);
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void t_ungag(CommandList *cm, int argc, const char **argv, char *args)
{
	ipc_message_t * msg = ipcmsg_create(IPC_ACTION, user->posn);
	json_t * j = json_init(NULL);
	json_addstring(j, "target", argv[1]);
	json_addstring(j, "type", "ungag");
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void t_zod(CommandList *cm, int argc, const char **argv, char *args)
{
	const char *excuse = skip_first_word(args);
	ipc_message_t * msg = ipcmsg_create(IPC_ACTION, user->posn);
	json_t * j = json_init(NULL);
	json_addstring(j, "target", argv[1]);
	json_addstring(j, "type", "zod");
	if (excuse!=NULL && !allspace(excuse))
		json_addstring(j, "reason", excuse);
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void t_mrod(CommandList *cm, int argc, const char **argv, char *args)
{
	const char *excuse = skip_first_word(args);
	ipc_message_t * msg = ipcmsg_create(IPC_ACTION, user->posn);
	json_t * j = json_init(NULL);
	json_addstring(j, "target", argv[1]);
	json_addstring(j, "type", "mrod");
	if (excuse!=NULL && !allspace(excuse))
		json_addstring(j, "reason", excuse);
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void t_kick(CommandList *cm, int argc, const char **argv, char *args)
{
	const char *excuse = skip_first_word(args);
	ipc_message_t * msg = ipcmsg_create(IPC_ACTION, user->posn);
	json_t * j = json_init(NULL);
	json_addstring(j, "target", argv[1]);
	json_addstring(j, "type", "zod");
	json_addstring(j, "admin", "yes");
	if (excuse!=NULL && !allspace(excuse))
		json_addstring(j, "reason", excuse);
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void t_remove(CommandList *cm, int argc, const char **argv, char *args)
{
	const char *excuse = skip_first_word(args);
	ipc_message_t * msg = ipcmsg_create(IPC_ACTION, user->posn);
	json_t * j = json_init(NULL);
	json_addstring(j, "target", argv[1]);
	json_addstring(j, "type", "mrod");
	json_addstring(j, "admin", "yes");
	if (excuse!=NULL && !allspace(excuse))
		json_addstring(j, "reason", excuse);
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

void t_raw(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0]=0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);
	talk_send_raw(text, user->record.room);
}

void t_bind(CommandList *cm, int argc, const char **argv, char *args)
{
	if (AddLink(&bind_list, argv[1], argv[2]))
		printf("Bind '%s' already exists. Has now been redefined!\n", argv[1]);
	else
		printf("Bind '%s' added!\n", argv[1]);
}

void t_unbind(CommandList *cm, int argc, const char **argv, char *args)
{
	if (!strcasecmp("*", argv[1]))
	{
		DestroyAllLinks(&bind_list);
		printf("All Binds Destroyed!\n");
	}
	else
	{
		if (!DestroyLink(&bind_list, argv[1]))
			printf("Bind '%s' was not found!\n", argv[1]);
		else
			printf("Bind '%s' was destroyed...\n", argv[1]);
	}
}

void t_room(CommandList *cm, int argc, const char **argv, char *args)
{
	switch(ChangeRoom(args, 0))
	{
	case 0:
		/* success */
		break;
	case 1:
		display_message("You cannot find that room", 1, 1);
		break;
	case 2:
		display_message("Sorry. You're frozen and can't change rooms.", 1, 1);
		break;
	case 3:
		display_message("Sorry, you cannot go in that direction.\n", 1, 1);
		break;
	case 4:
		display_message("Invalid Room ID (0-65535 only)", 1, 1);
		break;
	}
}

void t_linewrap(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	if (z==0)
	{
		if (!s_nolinewrap(user))
		{
			user->record.special |= (1 << MWSPCL_NOWRAP);
			printf("Linewrapping now disabled.\n");
		}
		else
			printf("Linewrapping was already disabled.\n");
	}
	else if (z==1)
	{
		if (s_nolinewrap(user))
		{
			user->record.special &= ~(1 << MWSPCL_NOWRAP);
			printf("Linewrapping now enabled.\n");
		}
		else
			printf("Linewrapping already enabled.\n");
	}else
		printf("%s\n",cm->ArgError);
	update_user(user);
}

void t_topic(CommandList *cm, int argc, const char **argv, char *args)
{
	_autofree char *buff=NULL;

	if (argc == 1) {
		char *topic;

		topic = db_room_get(user->record.room, "topic");
		if (topic != NULL && *topic != 0) {
			asprintf(&buff, "Topic: %s\n", topic);
		} else {
			asprintf(&buff, "No topic is set for channel %d.\n", user->record.room);
		}
		display_message(buff, 0, 1);
		if (topic != NULL) free(topic);
		return;
	} else
	if (u_god(user) || s_wizchat(user) || cp_test(user, CP_TOPIC)) {

		char text[MAXTEXTLENGTH];
		const char *topic;

		if (argc == 2 && argv[1][0] == 0) args=NULL;

		db_room_set(user->record.room, "topic", args);
		topic = db_room_get(user->record.room, "topic");
		if (topic == NULL || *topic == 0) {
			snprintf(text, MAXTEXTLENGTH-1, "\03315%s has cleared the channel topic.",
			         user->record.name);
			mwlog(user, "TOPIC <empty>");
		} else {
			snprintf(text, MAXTEXTLENGTH-1, "\03315%s has set the topic to \"%s\".",
			         user->record.name, topic);
			mwlog(user, "TOPIC %s", topic);
		}
		talk_send_raw(text, user->record.room);
	} else {
		display_message("Sorry, only admins can change the topic.", 0, 1);
	}
}

void t_uri(CommandList *cm, int argc, const char **argv, char *args)
{
	uriActionList *al = uritable;
	int wiz=0, num;
	char c;

	if(argc == 1) {
		// default action - list last 10 uris
		uri_list_display(10, NULL);
		return;
	}

	if (u_god(user))  {
		wiz=1;
	}

	while(al->action)
	{
		if( strcasecmp(al->action, argv[1]) == 0 // we have a valid action
		&& argc-1 >= al->min_argc // it has enough args
		&& argc-1 <= al->max_argc // and doesn't have too many args
		&& ( !al->needs_wiz || (al->needs_wiz && wiz)) ) // and it either doesn't need wiz or we are wiz
		{
			al->function(argc -1, &argv[1], al->needs_wiz && wiz);
			return;
		}
		al++;
	}

	if(argc == 2) // check for the posibility we have ".uri n" (same as .uri list * n)
	{
		if(sscanf(argv[1], "%d%c", &num, &c) == 1) { // check the arg is an int
			if(num > 0) {
				uri_list_display(num, NULL);
				return;
			}
		}
	}

	printf("%s\n", cm->ArgError);
}

void t_chaton(void)
{
	struct person *urec = &user->record;

	set_talk_rights();
	/* set mode to 'on talker', and update whofile for autoexec function */
	urec->chatmode = cm_set(user, CM_ONCHAT);
	update_user(user);

	format_message("\03310* Joining talker room %d. "
	               "Use \".quit\" to leave and \".help\" for help.\n",
	               urec->room);

	/* run the common stuff for entering the talker */
	enter_talker(1);
	announce_join(urec->name, urec->room, JOIN_NORMAL, NULL, quietmode);
	enter_room(urec->room);
}

void enter_room(int room)
{
	char *topic = db_room_get(room, "topic");
	if (topic != NULL) {
		if (*topic != 0) {
			_autofree char *buff=NULL;
			asprintf(&buff, "Topic: %s\n", topic);
			display_message(buff, 0, 1);
		}
		free(topic);
	}
	char * sproof = db_room_get(room, "soundproof");
	if (sproof != NULL) {
		if (strcasecmp(sproof, "1")==0) {
			display_message("This room is soundproofed.\n", 0, 1);
		}
		free(sproof);
	}
}

/**** actual talker utilities *****/

void chat_say(const char *text)
{
	talk_send_say(text, user->record.room);
}

void set_talk_rights(void)
{
	extern int current_rights;

	rights=0;
	if (cp_test(user, CP_CANRAW))    rights|=0x0001;
	if (cp_test(user, CP_CANGAG))    rights|=0x0002;
	if (cp_test(user, CP_CANZOD))    rights|=0x0004;
	if (cp_test(user, CP_CANMROD))   rights|=0x0008;
	if (cp_test(user, CP_CANGLOBAL)) rights|=0x0010;
	if (cp_test(user, CP_PROTECT))   rights|=0x0020;
	if (cp_test(user, CP_FREEZE))    rights|=0x0040;
	if (cp_test(user, CP_SUMMON))    rights|=0x0080;
	if (cp_test(user, CP_SPY))       rights|=0x0100;
	if (cp_test(user, CP_SCRIPT))    rights|=0x0200;
	if (u_god(user))                 rights|=0x0800;
	if (cp_test(user, CP_ADVSCR))    rights|=0x1000;
	if (cp_test(user, CP_DEVEL))     rights|=0x2000;

	current_rights = RIGHTS_TALK;
}

void enter_talker(int logontype)
{
	extern int	autowho;
	extern int	talker_logontype;

	/* set the 'summoned' logon mask */
	if (logontype == 2) talker_logontype |= LOGONMASK_SUMMONED;

	if (runautoexec)
	{
		/* run all talker init functions */
		if (cp_test(user, CP_SCRIPT)) RunInitFuncs(1);

		runautoexec=0;
	}

	/* if user wishes to automatically display a wholist on talker entry */
	if (autowho)
	{
		update_user(user);
		display_wholist(0);
		/* turn off the autowho, so it only occurs on first usage */
		autowho = 0;
	}
}

void t_help(CommandList *cm, int argc, const char **argv, char *args)
{
	CommandList	*c;

	if (argc < 2)
	{
		help_list(chattable, 1, CMD_TALK_STR);
		return;
	}

	if (args!=NULL && (strchr(args,'.')!=NULL || strchr(args,'/')!=NULL))
	{
		printf("Sorry, no help available on that subject.\n");
		return;
	}

	if (args==NULL)
	{
		const char *x = TALKHELP"/general";

		if (!access(x,00))
			printfile(x);
		else
			printf("No general help available for talker commands.\n");
	}else
	{
		_autofree char *x = NULL;

		/* find the command in the list */
		c = chattable;
		while (c->Command && strcasecmp(c->Command, args)) c++;

		/* do a permissions check */
		if (c->Command)
		{
			if ((rights & c->Rights) != c->Rights)
			{
				printf("Sorry, no help available on that subject.\n");
				return;
			}
		}
		asprintf(&x, TALKHELP"/%s", args);
		if (x != NULL && !access(x,00))
			printfile(x);
		else
			printf("Sorry, no help available on that subject.\n");
	}
}

void t_replay(CommandList *cm, int argc, const char **argv, char *args)
{
	if (argc < 2) {
		printf("Insufficient arguments.\n");
		return;
	}

	ipc_message_t * msg = ipcmsg_create(IPC_REPLAY, user->posn);
	json_t * j = json_init(NULL);
	json_addint(j, argv[1], atoll(argv[2]));
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	ipcmsg_transmit(msg);
}

/* changes room, returning error code on failure
 * 0 - ok
 * 1 - hidden/locked
 * 2 - frozen
 * 3 - invalid direction/room
 * 4 - invalid number
 * 5 - OOM
 */
int ChangeRoom(char *room, int quiet)
{
	struct room tmp;
	int i;
	unsigned short oldroom = user->record.room;
	_autofree char *buff=NULL;

	if (cm_test(user, CM_FROZEN))
	{
		return(2);
	}

	i = 0;
	if (isanum(room, &i, 1))
	{
		if (i>=0 && i<=65535)
		{
			RoomInit(&tmp);
			if (LoadRoom(&tmp, i))
			{
				int found = 1;

				if (tmp.locked>0) found = 0;

				if (!found)
				{
					RoomDestroy(&tmp);
					return(1);
				}
			}
			else
			{
				user->record.room = i;
			}
			RoomDestroy(&tmp);
		}
		else
		{
 			return(4);
		}

		if (i==oldroom)
		{
			if (!quiet) printf("You are already in room %d\n", i);
		}
		else
		{
			if (asprintf(&buff, "\03312%s has just arrived in room %d", user->record.name, i) < 0)
			{
				return 5;
			}
			if (!quiet) talk_send_raw(buff, i);
			user->record.room = i;
			update_user(user);

			RoomDestroy(&user->room);
			RoomInit(&user->room);
			LoadRoom(&user->room, user->record.room);

			SAFE_FREE(buff);
			if (asprintf(&buff, "\03313%s has left to room %d", user->record.name, i) < 0)
			{
				return 5;
			}
			if (!quiet) talk_send_raw(buff, oldroom);
			SAFE_FREE(buff);
			if (asprintf(&buff, "Leaving room %d for room %d", oldroom, user->record.room) < 0)
			{
				return 5;
			}
			if (!quiet) display_message(buff, 1, 1);

			enter_room(user->record.room);
		}
	}
	else
	{
		return(4);
	}

	return(0);
}
