#include <stdlib.h>
#include <string.h>

#include "gaglist.h"

char **gaglist;
size_t gaglist_len;

int gaglist_append(const char *gagname)
{
	char **tmp;

	if (gaglist_len == GAGLIST_MAX_ENTRIES)
		return 1;
	tmp = realloc(gaglist, sizeof(*gaglist) * (gaglist_len + 1));
	if (tmp == NULL) {
		free(gaglist);
		gaglist_len = 0;
		return 1;
	}
	gaglist = tmp;
	gaglist[gaglist_len++] = strdup(gagname);
	return 0;
}

void gaglist_destroy(void)
{
	for (size_t i = 0; i < gaglist_len; i++)
		free(gaglist[i]);
	gaglist_len = 0;
	free(gaglist);
	gaglist = NULL;
}

char *gaglist_tc_filter(const char *text, int state)
{
	static size_t len = 0;
	static size_t i = 0;

	if (state == 0) {
		i = 0;
		len = strlen(text);
	}

	for (; i < gaglist_len; i++) {
		if (*gaglist[i] == '\0')
			continue;
		if (len == 0 || !strncasecmp(gaglist[i], text, len))
			return strdup(gaglist[i++]);
	}
	return NULL;
}

