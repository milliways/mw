/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "incoming.h"
#include "main.h"
#include "talker_privs.h"
#include "talker.h"
#include "special.h"
#include "rooms.h"
#include "alias.h"
#include "script.h"
#include "ipc.h"
#include "perms.h"
#include "mesg.h"
#include "echo.h"
#include "intl.h"
#include "bb.h"
#include "who.h"
#include "user.h"
#include "util.h"
#include "incoming.h"
#include "gaglist.h"
#include "js.h"
#include "event.h"
#include "onoff.h"
#include <jansson.h>
#include <str_util.h>

extern struct user * const user;
extern int quietmode;
extern ipc_connection_t *ipcsock;

int new_mail_waiting=0;
int mesg_waiting = 0;

static int MesgStacked=0;
static struct mstack *MesgStack=NULL;

static void accept_pipe_cmd(ipc_message_t *msg, struct user *mesg_user);
static void force_text(ipc_message_t *msg, const char *text, const char *from);
static void force_vtext(ipc_message_t *msg, const char *from, const char *format, ...) __attribute__((format (printf, 3, 4)));
static void force_ipc(char *text, char *from);
static void force_rpc(char *text, char *from);
static void force_wiz(ipc_message_t *msg, char *text, char *from);
static void force_chatmode(char *text, struct user *usr);
static void force_status(char *text, char *from);
static void force_channel(char *text, char *from, unsigned long theirprivs);
static void force_kick(char *text, char *from, unsigned long theirprivs);
static void force_gag(char *text, unsigned long theirprivs, const char *from);
static void zod(const char *from, const char *msg);
static void mrod(char *from, char *msg);
static void force_newmail(void);
static void force_protlevel(char *text, unsigned long theirprivs, const char *from);
static void force_protpower(char *text);

#define _MIN(a,b) (a<b)?a:b

static void InsertMesg(struct mstack *new, ipc_message_t *msg)
{
	struct mstack **mp = &MesgStack;

	while (*mp != NULL)
		mp = &(*mp)->next;

	new->next=NULL;
	new->msg = msg;
	if (msg != NULL)
		msg->refcount++;

	*mp = new;
	MesgStacked++;
}

static void StackEvent(char *text, char *from, int flags)
{
	size_t textlen = strlen(text) + 1;
	size_t fromlen = strlen(from) + 1;
	struct mstack *new;
	char *ntext;
	char *nfrom;

	new = malloc(sizeof(*new) + textlen + fromlen);
	ntext = (char *)(new + 1);
	nfrom = ((char *)(new + 1)) + textlen;
	strcpy(ntext, text);
	strcpy(nfrom, from);
	new->text = ntext;
	new->from = nfrom;
	new->flags = MST_SCREV;
	new->preamble = flags;
	InsertMesg(new, NULL);
}

void ClearStack(void) {
	struct mstack *old;

	/* Nuke the entire stack */
	while (MesgStack!=NULL) {
		old=MesgStack;
		MesgStack=old->next;
		if (old->msg)
			ipcmsg_destroy(old->msg);
		free(old);
	}
	MesgStacked=0;
}

static void display_onoff(int action, const char *name, int room, int64_t when)
{
	char ts[7] = {0}; /* "hh:mm " */

	if (s_timestamp(user)) {
		struct tm *tm;
		time_t t = when;

		tm = localtime(&t);
		strftime(ts, 7, "%H:%M ", tm);
		ts[6] = '\0';
	}
	switch (action) {
	case ONOFF_LEAVE:
		format_message("%s\03311* %s has just left the talker.", ts, name);
		break;
	case ONOFF_JOIN:
		format_message("%s\03310* %s has just joined talker room %d.", ts, name, room);
		break;
	case ONOFF_LOGOFF:
		format_message("%s\03302* %s has just left the board.", ts, name);
		break;
	case ONOFF_LOGON:
		format_message("%s\03301* %s has just entered the board.", ts, name);
		break;
	}
}

void DisplayStack(void)
{
	struct mstack	*new;
	struct mstack	*old;
	char		*event_name;

	new=MesgStack;
	while (new!=NULL)
	{
		if (new->flags & MST_EVENT)
		{
			if (cp_test(user, CP_SCRIPT))
			{
				struct mwevent ev = {
						.ev_type = MWEV_TYPE_MSG,
						.ev_data.msg = new->msg,
				};
				script_output = 1;
				js_handle_event(&ev);
				event_name = NULL;
				while ((event_name = NextLink(event_list, event_name)) != NULL)
				{
					ExecEvent(event_name, new->text, "text", new->from, new->preamble);
				}
				if (script_output) display_message(new->text, new->flags & MST_BEEP, 1);
				script_output = 1;
			}
			else display_message(new->text, new->flags & MST_BEEP, 1);
		}
		else if (new->flags & MST_SCREV)
		{
			switch(new->preamble)
			{
			case EST_RPC:
			{
				const char *msg;
				char *funcname;
				char callfunc[MAXTEXTLENGTH];

				msg = skip_first_word(new->text);
				snprintf(callfunc, (MAXTEXTLENGTH-1<strlen(new->text))?MAXTEXTLENGTH-1:strlen(new->text) - strlen(msg), "%s", new->text);
				script_output=1;

				if ((funcname = FindLinks(rpc_list, callfunc)) != NULL)
				{
					ExecEvent(funcname, msg, "RPC", new->from, 0);
					free(funcname);
				}
				break;
			}
			case EST_IPC:
			{
				char	*ipc_name;

				/* go through list of ipc functions */
				ipc_name = NULL;
				script_output=1;
				while ((ipc_name = NextLink(ipc_list, ipc_name)) != NULL)
				{
					ExecEvent(ipc_name, new->text, "IPC", new->from, 0);
				}
				break;
			}
			case EST_CHECKONOFF:
			{
				struct {
					char ccode[13];
					char type[13];
					char quiet[3];
					char name[NAMESIZE + 1];
					char version[3];
					char reason[MAXTEXTLENGTH];
				} a;
				char *aargs[6] = {
					a.ccode,
					a.type,
					a.quiet,
					a.name,
					a.version,
					a.reason
				};
				int action_to_ccode[] = {
					[ONOFF_LOGON] = 3,
					[ONOFF_LOGOFF] = 2,
					[ONOFF_JOIN] = 1,
					[ONOFF_LEAVE] = 0,
				};
				char *onoff_name;
				const char *uname, *reason;
				int ccode, isquiet, action, type, room = 0;
				struct mwevent ev = {
						.ev_type = MWEV_TYPE_ONOFF,
						.ev_data.onoff = new->msg,
				};
				script_output = 1;
				if (cp_test(user, CP_SCRIPT))
					js_handle_event(&ev);

				/* go through list and find checkonoff function */
				onoff_name = NULL;
				onoff_name = NextLink(onoff_list, onoff_name);

				json_t *j = ipcmsg_json_decode(new->msg);
				uname = json_getstring(j, "user");
				action = json_getint(j, "action");
				type = json_getint(j, "type");
				isquiet = json_is_true(json_object_get(j, "quiet"));
				if (action == ONOFF_LOGOFF)
					reason = json_getstring(j, "reason");
				else
					reason = json_getstring(j, "message");
				if (action == ONOFF_JOIN)
					room = json_getint(j, "channel");

				if (action >= ONOFF_SIZE)
					break;

				ccode = action_to_ccode[action];

				/* limit the method information */
				switch (action) {
				case ONOFF_LEAVE:
					if (!cp_test(user, CP_CANZOD) && (type == LEAVE_ZOD))
					{
						type = 0;
						uname = new->from;
					}
					if (!u_god(user) && (type == LEAVE_FORCED))
					{
						type = 0;
						uname = new->from;
					}
					break;
				case ONOFF_JOIN:
					if (!cp_test(user, CP_SUMMON) && (type == JOIN_SUMMONED))
					{
						type = 0;
						uname = new->from;
					}
					if (!u_god(user) && (type == JOIN_FORCED))
					{
						type = 0;
						uname = new->from;
					}
					break;
				case ONOFF_LOGOFF:
					if (!cp_test(user, CP_CANMROD) && (type == LOGOFF_MROD))
					{
						type = 0;
						uname = new->from;
					}
					if (!u_god(user) && (
					    (type == LOGOFF_BANNED) ||
					    (type == LOGOFF_ERROR) ||
					    (type == LOGOFF_DELETED)))
					{
						type = 0;
						uname = new->from;
					}
					break;
				case ONOFF_LOGON:
					break;
				}
				/* set up the argument strings */
				memset(&a, 0, sizeof(a));
				snprintf(a.ccode, 12, "%d", ccode);
				snprintf(a.type, 12, "%d", type);
				snprintf(a.quiet, 2, "%d", isquiet ? 1 : 0);
				snprintf(a.name, NAMESIZE, "%s", uname);
				a.version[0] = '3';
				if (reason != NULL)
					snprintf(a.reason, MAXTEXTLENGTH - 1, "%s", reason);
				if (cp_test(user, CP_SCRIPT) && onoff_name != NULL)
					ExecEvent2(onoff_name, "CheckOnOff", new->from, 0, 6, aargs);
				if (script_output && !isquiet)
					display_onoff(action, uname, room, new->msg->head.when);
				break;
			}
			default:
				break;
			}
		}
		else display_message(new->text, new->flags & MST_BEEP, 1);

		old=new->next;
		free(new);
		MesgStacked--;
		new=old;
	}
	MesgStack=NULL;
}

int MesgIsStacked(void)
{
	return (MesgStacked > 0);
}

void handle_mesg()
{
	static struct user mesg_user;

	mesg_waiting = 0;

	ipc_message_t * msg = read_socket(ipcsock, 1);

	while (msg != NULL) {
		if (msg->head.src == SYSTEM_USER) {
			strcpy(mesg_user.record.name,"System");
		} else if (fetch_user(&mesg_user, msg->head.src) != 0) {
			return;
		}
		accept_pipe_cmd(msg, &mesg_user);

		msg = read_socket(ipcsock, 0);
	}
	if (msg == NULL && !ipc_connected()) {
		if (ipcsock->fd != -1) {
			close(ipcsock->fd);
			ipcsock->fd = -1;
		}
		fprintf(stderr, "Server disconnected.\n");
	}
	if (cm_test(user, CM_ONCHAT))
		set_talk_rights();
	else
		set_rights();
}

static void force_lastread(const char * newbuff) {
	int fol;
	int pos;

	sscanf(newbuff,"%d:%d",&fol,&pos);
	user->record.lastread[fol] = pos;
}

static void display_uptime(ipc_message_t *msg)
{
	json_t * j = json_init(msg);
	const char *version = json_getstring(j, "version");
	const int uptime = json_getint(j, "uptime");

	printf("Server version: %s\n", version);
	printf("Server uptime: %s\n", itime(uptime));
	json_decref(j);
}

/** Server sent us an error, usually pretty fatal */
static void handle_ipc_error(ipc_message_t *msg)
{
	json_t * j = json_init(msg);
	const char * type = json_getstring(j, "error");

	if (strcasecmp(type, "NONCE")==0) {
		printf("Incompatible server version. Quitting.\n");
	} else {
		printf("Undefined server error '%s'\n", type);
	}
	json_decref(j);
	close_down(LOGOFF_NORMAL, NULL, NULL);
}

static void display_error(ipc_message_t *msg)
{
	json_t * j = json_init(msg);
	const char * type = json_getstring(j, "type");
	const char * text = json_getstring(j, "text");

	char buff[MAXTEXTLENGTH];
	char * tb = buff;
	int len = MAXTEXTLENGTH;

	if (msg->head.type == IPC_TALKERROR) {
		snprintf(tb, len, "Talker error: %s", text);
	} else {
		snprintf(tb, len, "Unknown %s error: %s", type, text);
	}

	force_text(msg, buff, "System");
	json_decref(j);
}


/* handle formatted message */
static void display_content(ipc_message_t *msg)
{
	int32_t msg_posn = -1;
	struct user msg_user;
	const char *whom = NULL;

	if (msg->head.src == SYSTEM_USER) {
		whom = "System";
	} else {
		msg_posn = msg->head.src;
		if (fetch_user(&msg_user, msg_posn) != 0)
			return;
		whom = msg_user.record.name;
	}

	json_t * j = json_init(msg);

	if (msg->head.type == IPC_SAYTOROOM) {
		const char * type = json_getstring(j, "type");
		const char * text = json_getstring(j, "text");

		if (type == NULL) type="unknown";
	       	if (text == NULL) {
			printf("Invalid SAYTOROOM message from %s.\n", whom);
			json_decref(j);
			return;
		}

		char buff[MAXTEXTLENGTH];
		char * tb = buff;
		int len = MAXTEXTLENGTH;

		/* we have global on, prepend the channel number */
		if (cm_test(user, CM_GLOBAL)) {
			snprintf(tb, len, "%d:", msg->head.dst);
			int s = strlen(tb);
			len -= s;
			tb += s;
		}

		if (strcmp(type, "say")==0) {
			snprintf(tb, len, "%s: %s", whom, text);
		} else
		if (strcmp(type, "raw")==0) {
			snprintf(tb, len, "%s", text);
		} else
		if (strcmp(type, "emote")==0) {
			unsigned p = json_getint(j, "plural");

			if (p >= plural_suffix_size())
				p = 0;
			snprintf(tb, len, "%s%s %s", whom, plural_suffix[p], text);
		} else
		if (strcmp(type, "notsayto")==0) {
			const char *exclude = json_getstring(j, "exclude");
			if (exclude == NULL) exclude="Unknown";
			if (text[strlen(text)-1] == '?')
				snprintf(tb, len, "%s asks (-%s): %s", whom, exclude, text);
			else
				snprintf(tb, len, "%s says (-%s): %s", whom, exclude, text);
		} else {
			/* same as say for now */
			snprintf(tb, len, "%s: %s", whom, text);
		}
		force_text(msg, buff, whom);
	} else
	if (msg->head.type == IPC_SAYTOUSER) {
		const char * type = json_getstring(j, "type");
		const char * text = json_getstring(j, "text");

		if (type == NULL || text == NULL) {
			printf("Invalid SAYTOUSER message from %s.\n", whom);
			json_decref(j);
			return;
		}

		char buff[MAXTEXTLENGTH];
		char * tb = buff;
		int len = MAXTEXTLENGTH;

		/* we have global on, prepend the channel number */
		if (cm_test(user, CM_GLOBAL)) {
			if (msg_posn == -1)
				snprintf(tb, len, "?:");
			else
				snprintf(tb, len, "%d:", msg_user.record.room);
			int s = strlen(tb);
			len -= s;
			tb += s;
		}

		if (strcmp(type, "whispers")==0) {
			snprintf(tb, len, "%s whispers: %s", whom, text);
		} else
		if (strcmp(type, "asks")==0) {
			snprintf(tb, len, "%s asks: %s", whom, text);
		} else  {
			snprintf(tb, len, "%s says: %s", whom, text);
		}
		force_text(msg, buff, whom);
	} else
	if (msg->head.type == IPC_SAYTOALL) {
		const char * text = json_getstring(j, "text");

		if (text == NULL) {
			printf("Invalid SAYTOALL message from %s.\n", whom);
			json_decref(j);
			return;
		}

		char buff[MAXTEXTLENGTH];
		char * tb = buff;
		int len = MAXTEXTLENGTH;

		/* we have global on, prepend the channel number */
		if (cm_test(user, CM_GLOBAL)) {
			snprintf(tb, len, "*:");
			int s = strlen(tb);
			len -= s;
			tb += s;
		}

		snprintf(tb, len, "%s shouts: %s", whom, text);
		force_text(msg, buff, whom);
	} else
	if (msg->head.type == IPC_EVENT) {
		const char * type = json_getstring(j, "type");
		const char * text = json_getstring(j, "text");
		const char * verbose = json_getstring(j, "verbose");
		const char * reason = json_getstring(j, "reason");

		if (verbose) force_text(msg, verbose, whom);

		/* user action messages have colour 15 */
		force_vtext(msg, whom, "\033%d%s", 15, text);

		if (reason) {
			if (strcasecmp(type, "mrod")==0 || strcasecmp(type, "zod")==0) {
				force_vtext(msg, whom, "Zebedee says \"%s\".", reason);
			} else {
				force_vtext(msg, whom, "%s gives the reason \"%s\".", whom, reason);
			}
		}
	} else
	if (msg->head.type == IPC_GAGLIST) {
		json_t *jgag;
		size_t i = 0;

		if (j== NULL || !json_is_array(j)) {
			printf("Invalid GAGLIST message from %s.\n", whom);
			json_decref(j);
			return;
		}
		gaglist_destroy();
		json_array_foreach(j, i, jgag) {
			gaglist_append(json_string_value(jgag));
		}
	} else
	if (msg->head.type == IPC_CHECKONOFF) {
		struct mstack *ms;

		ms = calloc(1, sizeof(*ms));
		ms->flags = MST_SCREV;
		ms->preamble = EST_CHECKONOFF;
		ms->from = strdup(whom);
		InsertMesg(ms, msg);
	} else {
		printf("Unknown message type %4.4s", (char *)&msg->head.type);
	}

	json_decref(j);
}

static void accept_pipe_cmd(ipc_message_t *msg, struct user *mesg_user)
{
	enum ipc_types state = msg->head.type;
	char *newbuff = (char *)(msg + 1);

	/*printf("\n<message type is %d>\n", state);*/
	switch (state) {
		case IPC_NEWMAIL:
			force_newmail();
			break;
		case IPC_STATUS:
			force_status(newbuff, mesg_user->record.name);
			break;
		case IPC_GROUPS:
			user->record.groups = folder_groups(newbuff, user->record.groups);
			break;
		case IPC_REALNAME:
			strcpy(user->record.realname, newbuff);
			break;
		case IPC_PASSWD:
			strcpy(user->record.passwd, newbuff);
			break;
		case IPC_CONTACT:
			strcpy(user->record.contact, newbuff);
			break;
		case IPC_DOING:
			snprintf(user->record.doing, DOINGSIZE,"%s", newbuff);
			user->record.dowhen = time(0);
			break;
		case IPC_SPECIAL:
			user->record.special = set_special(newbuff, user->record.special);
			break;
		case IPC_CHATPRIVS:
			user->record.chatprivs = cp_setbycode(user->record.chatprivs, newbuff);
			break;
		case IPC_CHATMODE:
			force_chatmode(newbuff, mesg_user);
			break;
		case IPC_GAG:
			force_gag(newbuff, mesg_user->record.chatprivs, mesg_user->record.name);
			break;
		case IPC_GAGLIST:
			display_content(msg);
			break;
		case IPC_PROTLEVEL:
			force_protlevel(newbuff, mesg_user->record.chatprivs, mesg_user->record.name);
			break;
		case IPC_PROTPOWER:
			force_protpower(newbuff);
			break;
		case IPC_USERNAME:
			snprintf(user->record.name, NAMESIZE+1, "%s", newbuff);
			printf(_("\nYour name has been changed to '%s'\n"), user->record.name);
			break;
		case IPC_LASTREAD:
			force_lastread(newbuff);
			break;
		case IPC_TIMEOUT:
			user->record.timeout = atoi(newbuff);
			reset_timeout(user->record.timeout);
			break;
		case IPC_TEXT:
			force_text(msg, newbuff, mesg_user->record.name);
			break;
		case IPC_SCRIPTIPC:
			force_ipc(newbuff, mesg_user->record.name);
			break;
		case IPC_SCRIPTRPC:
			force_rpc(newbuff, mesg_user->record.name);
			break;
		case IPC_CHECKONOFF:
			display_content(msg);
			break;
		case IPC_WIZ:
			force_wiz(msg, newbuff, mesg_user->record.name);
			break;
		case IPC_CHANNEL:
			force_channel(newbuff, mesg_user->record.name, mesg_user->record.chatprivs);
			break;
		case IPC_KICK:
			force_kick(newbuff, mesg_user->record.name, mesg_user->record.chatprivs);
			break;
		case IPC_UPTIME:
			display_uptime(msg);
			break;
		case IPC_SAYTOROOM:
		case IPC_SAYTOUSER:
		case IPC_SAYTOALL:
		case IPC_EVENT:
			display_content(msg);
			break;
		case IPC_TALKERROR:
			display_error(msg);
			break;
		case IPC_WHOLIST:
			update_wholist(msg);
			break;
		case IPC_ERROR:
			handle_ipc_error(msg);
			break;
		default:
			devel_msg("incoming_mesg", "unknown message type %d.\007", state);
	}

	ipcmsg_destroy(msg);
	update_user(user);
}

static void force_newmail(void)
{
	new_mail_waiting++;
}


static void force_vtext(ipc_message_t *msg, const char *from, const char *format, ...)
{
	va_list va;
	_autofree char * text = NULL;
	va_start(va, format);
	vasprintf(&text, format, va);
	va_end(va);

	force_text(msg, text, from);
}

static void force_text(ipc_message_t *msg, const char *text, const char *from)
{
	char tb[MAXTEXTLENGTH];
	struct mstack *mesg;

	mesg = malloc(sizeof(*mesg));
	mesg->flags = MST_EVENT|MST_BEEP;

	tb[0]=0;
	if (s_timestamp(user))
	{
		time_t t;
		struct tm *tt;

		t= msg->head.when;
		tt=localtime(&t);
		strftime(tb, MAXTEXTLENGTH,"%H:%M ", tt);
	}
	if (cm_test(user, CM_SPY))
	{
		strncat(tb, from, MAXTEXTLENGTH - strlen(tb) - 1);
		strncat(tb, "> ", MAXTEXTLENGTH - strlen(tb) - 1);
	}
	mesg->preamble = strlen(tb);
	if (cm_test(user, CM_GLOBAL))
	{
		char *i = strchr(text, ':');
		if (i != NULL)
		{
			mesg->preamble += (i-text+1);
		}
	}
	strncat(tb, text, MAXTEXTLENGTH - strlen(tb) - 1);
	mesg->text = strdup(tb);
	mesg->from = strdup(from);

	InsertMesg(mesg, msg);
}

static void force_wiz(ipc_message_t *msg, char *newbuff, char *from)
{
	char tb[MAXTEXTLENGTH];
	struct mstack *mesg;

	mesg = malloc(sizeof(*mesg));
	mesg->flags = 0;
	mesg->preamble = 0;

	tb[0]=0;
	if (s_timestamp(user))
	{
		time_t t;
		struct tm *tt;

		t = msg->head.when;
		tt=localtime(&t);
		strftime(tb, MAXTEXTLENGTH,"%H:%M ", tt);
	}
	if (cm_test(user, CM_SPY))
	{
		strncat(tb, from, MAXTEXTLENGTH - strlen(tb) - 1);
		strncat(tb, "> ", MAXTEXTLENGTH - strlen(tb) - 1);
	}
	strncat(tb, newbuff, MAXTEXTLENGTH - strlen(tb) - 1);
	mesg->text = strdup(tb);
	mesg->from = strdup(from);

	InsertMesg(mesg, msg);
}

static void force_ipc(char *text, char *from)
{
	if (cp_test(user, CP_SCRIPT))
	{
		StackEvent(text, from, EST_IPC);
	}
}

static void force_rpc(char *text, char *from)
{
	if (cp_test(user, CP_SCRIPT))
	{
		StackEvent(text, from, EST_RPC);
	}
}

static void force_status(char *newbuff, char *from)
{
	user->record.status = user_stats(newbuff, user->record.status);
	/* we have received a ban */
	if (u_ban(user))
	{
		printf(_("\n\n--> You appear to have been banned. Goodbye... <--\r\n"));
		close_down(LOGOFF_BANNED, from, NULL);
	}
	/* we have received a +D status change */
	if (u_del(user))
	{
		printf(_("\n\n--> You appear to have been DELETED. Goodbye... <--\r\n"));
		close_down(LOGOFF_DELETED, from, NULL);
	}
}

static void force_chatmode(char *newbuff, struct user *usr)
{
	unsigned long mm;
	int ourapl = (user->record.chatprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	int theirapl = (usr->record.chatprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	int oldchat;

	if (!(usr->record.chatprivs & CP_PROTECT)) theirapl = 0;
	oldchat = cm_test(user, CM_ONCHAT);

	mm=cm_setbycode(user->record.chatmode, newbuff);
	user->record.chatmode = chatmode_describe(user->record.chatmode, mm, ourapl, theirapl, NULL);

	if (!cm_test(user, CM_ONCHAT) && oldchat)
		announce_leave(user->record.name, user->record.room, LEAVE_FORCED,
		               usr->record.name, NULL, quietmode);
	else if (cm_test(user, CM_ONCHAT) && !oldchat)
		announce_join(user->record.name, user->record.room, JOIN_FORCED,
		              usr->record.name, quietmode);

	disable_rl(1);
}

static void force_gag(char *newbuff, unsigned long theirprivs, const char *from)
{
	int ourapl = (user->record.chatprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	int theirapl = (theirprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	if (!(theirprivs & CP_PROTECT)) theirapl = 0;

	unsigned long newcm = user->record.chatmode & ~CM_GAGMASK;
	if (newbuff[0]=='+')
		newcm |= atoi(&newbuff[1]) << CM_GAGSHIFT;

	user->record.chatmode=chatmode_describe(user->record.chatmode, newcm, ourapl, theirapl, from);

	disable_rl(1);
}

static void force_protlevel(char *newbuff, unsigned long theirprivs, const char* from)
{
	unsigned long cm, pbits;
	int level;
	int ourapl = (user->record.chatprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	int theirapl = (theirprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	if (!(theirprivs & CP_PROTECT)) theirapl = 0;

	level = atoi(newbuff);
	/* `pbits' contains both the temporary protect bit and permanent
	 * protection level.  if level==-1 then give temp protection only
	 */
	pbits = (level>=0? level: 0) << CM_PROTSHIFT;
	if (level != 0) pbits |= CM_PROTECTED;
	cm=user->record.chatmode;
	cm ^= pbits; cm &= ~(CM_PROTMASK|CM_PROTECTED); cm ^= pbits;

	user->record.chatmode = chatmode_describe(user->record.chatmode, cm, ourapl, theirapl, from);
}

static void force_protpower(char *newbuff)
{
	unsigned long cp, pbits;

	pbits = atoi(newbuff) << CP_PROTSHIFT;
	cp=user->record.chatprivs;

	cp ^= pbits; cp &= ~CP_PROTMASK; cp ^= pbits;

	user->record.chatprivs = cp;
}

static void force_channel(char *newbuff, char *from, unsigned long theirprivs)
{
	int newroom=atoi(&newbuff[1]);
	char mode = newbuff[0];
	char text[MAXTEXTLENGTH];
	int theirapl = (theirprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	int prot = (user->record.chatmode & CM_PROTMASK) >> CM_PROTSHIFT;
	if (!(theirprivs & CP_PROTECT)) theirapl = 0;
	if (prot == 0 && cm_test(user, CM_PROTECTED))
		prot = 1;

	if (!cm_test(user, CM_ONCHAT))
	{
		if ((mode != 's') && (theirapl<prot))
		{
			snprintf(text,MAXTEXTLENGTH-1,"\03315%s just tried to summon you onto the talker into room %d.",from,newroom);
			display_message(text, 1, 1);
		}else
		{
			snprintf(text,MAXTEXTLENGTH-1,"\03315You have been summoned into talker room %d by %s.",newroom,from);
			display_message(text, 1, 1);
			set_talk_rights();
			user->record.room = newroom;

			snprintf(text,MAXTEXTLENGTH-1,"\03310%s has just been summoned into talker room %d by %s",user->record.name,newroom,from);
			talk_send_rawbcast(text);
			user->record.chatmode = cm_set(user, CM_ONCHAT);

			RoomDestroy(&user->room);
			RoomInit(&user->room);
			LoadRoom(&user->room, newroom);

			/* force update of user information _before_ scripts are run */
			update_user(user);

			/* run common talker entrance code with logon type set to 'summon' */
			enter_talker(2);
			announce_join(user->record.name, user->record.room, JOIN_SUMMONED, from, quietmode);
			enter_room(newroom);
		}
	}else
	{
		if ((mode != 's') && (theirapl<prot))
		{
			/*
			snprintf(text,MAXTEXTLENGTH-1,"%s just tried to summon you to room %d.",from,newroom);
			display_message(text, 1, 1);
			*/
		}else
		{
			int o;
			snprintf(text,MAXTEXTLENGTH-1,"\03312%s has just arrived in room %d",user->record.name,newroom);
			talk_send_raw(text,newroom);
			o = user->record.room;
			user->record.room = (unsigned short)newroom;
			snprintf(text,MAXTEXTLENGTH-1,"\03313%s has left to room %d", user->record.name, newroom);
			talk_send_raw(text,o);
			snprintf(text,MAXTEXTLENGTH-1,"\03315You have been summoned to room %d by %s.",newroom,from);
			display_message(text, 1, 1);

			RoomDestroy(&user->room);
			RoomInit(&user->room);
			LoadRoom(&user->room, newroom);
			enter_room(newroom);
		}
	}
	disable_rl(1);

}

static void force_kick(char *newbuff, char *from, unsigned long theirprivs)
{
	char	*msg = NULL;
	char	type;

	type = *newbuff;
	if (newbuff[1] == 'r') msg = &newbuff[2];

	if (type=='z')
	{
		zod(from, msg);
	}else
	if (type=='m')
	{
		mrod(from, msg);
	} else
	if (type=='k')
	{
		zod(from, msg);
	}else
	if (type=='r')
	{
		mrod(from, msg);
	}
}

static void zod(const char *from, const char *msg)
{
	char text[MAXTEXTLENGTH];

	/* remove from talker */
	user->record.chatmode = cm_clear(user, CM_ONCHAT);

	update_user(user);
	announce_leave(user->record.name, user->record.room, LEAVE_ZOD, from, msg, quietmode);

	/* put standard message to screen */
	snprintf(text, MAXTEXTLENGTH-1, _("\nBoing, Zebedee arrived.  \"%s\033--\", said Zebedee\n"), (msg!=NULL)?msg:_("Time for bed"));
	display_message(text, 1, 1);
	printf(_("%s just sent the Zebedee of Death to you.\n"),from);
	fflush(stdout);

	/* set 'off talker' permissions for commands */
	set_rights();
	disable_rl(1);
}

static void mrod(char *from, char *msg)
{
	char mrod_message[MAXTEXTLENGTH];
	/* make sure we have left talker */
	/*user->chatmode=cm_flags(user->chatmode, CM_ONCHAT, CM_MODE_CLEAR);*/
	update_user(user);

	printf("\n");
	printf(_("Boing, Zebedee's arrived.  \"Look up!\", says Zebedee\n"));
	printf(_("You look up; a large object is falling towards you very fast,\n"));
	printf(_("very very fast.  It looks like a Magic Roundabout!\n"));
	printf(_("\"I wouldn't stand there if I was you\", says Zebedee\n"));
	printf(_("Boing, Zebedee's left you standing all alone\n"));
	printf(_("WWWHHHEEEEEEEKKKKEEEERRRRRUUUUUNNNNNCCCCCHHHHHH\7\7\7\7\7\n"));
	printf(_("%s has just dropped the Magic Roundabout of Death on you.\n"),from);
	if (msg!=NULL)
	{
		snprintf(mrod_message, MAXTEXTLENGTH-1, _("\"%s\033--\" says Zebedee\n"), msg);
		display_message(mrod_message, 1, 1);
	}
	fflush(stdout);
	fix_terminal();
	close_down(LOGOFF_MROD, from, msg);
}
