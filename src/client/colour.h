#ifndef COLOUR_H
#define COLOUR_H

#define COLOURDIR	HOMEPATH"/colour"

void init_colour(void);
void destroy_colours(void);
void colour_load(char *file, int quiet);
void colour_free(void);
int colour(const char *input, char *output, int outlen);
char *get_colour(void);

#endif /* COLOUR_H */
