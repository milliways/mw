#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>
#include <readline/readline.h>

#include <sqlite.h>
#include <util.h>
#include <mwlog.h>
#include "uri.h"
#include "sqlite.h"
#include "talker.h"
#include "main.h"
#include "perms.h"
#include "bb.h"
#include "userio.h"
#include "user.h"
#include "mesg.h"

extern struct user * const user;

// uri action table
// action, minargc, maxargc, tcmode, needs su, function
// tcmode: 0: no tabcomplete 1: always tc 2: tc only after first char matched (su edit functions)
uriActionList uritable[]={
	{ "list",         1, 3, 1, 0, uri_list },
	{ "-log",         2, 2, 0, 0, uri_delete },
	{ "delete",       2, 2, 1, 0, uri_delete },
	{ "del",          2, 2, 0, 0, uri_delete },
	{ "!delete",      2, 2, 2, 1, uri_delete },
	{ "!del",         2, 2, 0, 1, uri_delete },
	{ "nsfw",         2, 2, 1, 0, uri_nsfw },
	{ "!nsfw",        2, 2, 2, 1, uri_nsfw },
	{ "membersonly",  2, 2, 1, 0, uri_members_only },
	{ "!membersonly", 2, 2, 2, 1, uri_members_only },
	{ "sucs",         2, 2, 0, 0, uri_members_only },
	{ "anonymous",    2, 2, 1, 0, uri_anon },
	{ "!anonymous",   2, 2, 2, 1, uri_anon },
	{ "anon",         2, 2, 0, 0, uri_anon },
	{ "displaymode",  1, 2, 1, 0, uri_display_mode },
	{ NULL,           0, 0, 1, 1, NULL }
};

// uri flags
// integer flag, db string, description used (un)setting flag, description used in list
uriFlagList uriflagtable[] = {
	{ URLFLAG_NSFW, "nsfw", "NSFW", "\033R-NSFW\033--" },
	{ URLFLAG_ANON, "anon", "anonymous on web", "[Anon on web]" },
	{ URLFLAG_SUCS, "sucs", "members-only", "(Members Only)" },
	{ 0, NULL, NULL, NULL }
};

// turns the db flag string into a bitwise flag field
static int uri_parse_flags(const char *flagstr)
{
	int flags = 0;
	uriFlagList *fl = uriflagtable;
	if(!flagstr) {
		return 0;
	}
	while(fl->flag)
	{
		if(strcasestr(flagstr, fl->flagstr)) {
			flags |= fl->flag;
		}
		fl++;
	}
	return flags;
}

// creates a string of database flags for the bitwise flags
void uri_make_flags_str(int flags, char *flagstr, int len)
{
	int n=0;
	uriFlagList *fl = uriflagtable;

	flagstr[0] = '\0';
	while(fl->flag)
	{
		if(flags & fl->flag)
		{
			n += snprintf(&flagstr[n], len-n, "%s%s", n==0?"":" ", fl->flagstr);
		}
		fl++;
	}
}

// creates a the flag descriptions show in uri list from the database flags string
static void uri_make_flags_description(char *shortflags, char *flagstr, int len)
{
	int n=0;
	uriFlagList *fl = uriflagtable;

	flagstr[0] = '\0';
	if(shortflags != NULL)
	{
		while(fl->flag)
		{
			if(strcasestr(shortflags,  fl->flagstr))
			{
				n += snprintf(&flagstr[n], len-n, " %s", fl->list_desc);
			}
			fl++;
		}
	}
}

// returns the long description for flag
// should be duplicated if you want to edit it
static const char *uri_get_flag_description(int flag)
{
	uriFlagList *fl = uriflagtable;

	while(fl->flag)
	{
		if(flag == fl->flag)
		{
			return fl->long_desc;
		}
		fl++;
	}
	return NULL;
}

// display <number> uris posted by <username> (or all users if username is NULL)
void uri_list_display(int number, const char * username)
{
	char *query;
	struct db_result *res;
	struct db_data *node;
	int width, i, len;
	char url_line[MAXTEXTLENGTH];
	char *mode;
	int disp_mode=0;
	char flagstr[1024]="";

	if(username == NULL) {
		query = sqlite3_mprintf("SELECT id,datetime(added, 'localtime'),user,url,title,flags FROM mwuri ORDER BY id DESC LIMIT %d", number);
	} else {
		query = sqlite3_mprintf("SELECT id,datetime(added, 'localtime'),user,url,title,flags FROM mwuri WHERE user=%Q ORDER BY id DESC LIMIT %d", username, number);
	}
	res = db_query(MWURI_DB, query, 1);
	fflush(stdout);
	if(res == NULL) {
		printf(".uri database lookup failed\n");
	} else {
		if(res->rows < 1)
		{
			if(username)
			{
				printf("%s has not posted any uris\n", username);
			}
			else
			{
				printf("There are not currently any uris in mwuri\n");
			}
		}
		else if(res->rows < number)
		{
			printf("Displaying all uris posted to milliways%s%s:\n", username ? " by user " : "", username ? username : "");
		}
		else
		{
			printf("Displaying the last %d uri%s posted to milliways%s%s:\n", res->rows, res->rows==1? "s":"", username ? " by user " : "", username ? username : "");
		}
		// pick up the display mode from the private db
		// we default to short if we don't get anything from the db
		mode = userdb_get(USERDB_PRIVATE, user->record.name, "mwuri_display_mode");
		if(mode)
		{
			if(strcmp(mode, "full")==0)
			{
				disp_mode = 1;
			}
		}
		node = res->data;
		width = screen_w();
		if(width > MAXTEXTLENGTH - 1) {
			width = MAXTEXTLENGTH - 1;
		}
		url_line[width]='\0';
		while(node) {
			char *id = node->field[0];
			char *added = node->field[1];
			char *usr = node->field[2];
			char *url = node->field[3];
			char *title = node->field[4];
			uri_make_flags_description(node->field[5], flagstr, sizeof(flagstr)); // flags = node->field[5]

			snprintf(url_line, width, "#\033G-%s\033-- [%s] <%s>%s\n", id, added, usr, flagstr );
			display_message(url_line, 0, 1);

			if(title != NULL) {
				len = strlen(title);
				for(i = 0; i<len; i++) {
					if(title[i]=='\n') title[i]=' ';
				}
			}
			if(disp_mode)
			{
				// in full display mode we just show the whole url + title
				snprintf(url_line, MAXTEXTLENGTH-1, " %s%s%s", url, title ? " | ":"", title ? title:"");

			}
			else
			{
				// in short display mode we try to fit url+title onto one line and shorten if it doesn't fit
				// the whole url is always shown
				if(strlen(url)+6 > width)
				{
					snprintf(url_line, width+1, " %s", url); // ' '.uri." | " is wider than the screen so just show the who url
				}
				else
				{
					if(snprintf(url_line, width+1, " %s%s%s", url, title ? " | ":"", title ? title:"") > width)
					{
						// line + title is wider than the screen so put ... at the end
						url_line[width - 1] = '.';
						url_line[width - 2] = '.';
						url_line[width - 3] = '.';
					}
				}
			}
			display_message(url_line, 0, 1);
			node = node->next;
		}
		db_free(res);
	}
	sqlite3_free(query);
}

/* parse the .uri list command and display uris if appropriate */
void uri_list(int argc, const char **argv, int wiz)
{
	char *username = NULL; // default is to display everyone's uris
	int number = 10; // default is to display 10 uris
	_autofree struct user *list_user = calloc(1, sizeof(*list_user));
	char c;

	if (list_user == NULL) {
		perror("calloc");
		return;
	}
	if(argc == 1) // just '.uri list'
	{
		username = user->record.name;
	}
	else if(strcasecmp(argv[1], "*") == 0) // wants all uris
	{
		// username is already NULL and will thus select everyone
		if(argc==3)
		{
			if(sscanf(argv[2], "%d%c", &number, &c) == 1)
			{
				if(number < 1)
				{
					printf(".uri list * - can't list <1 uris\n");
					return;
				}
			}
			else
			{
				printf(".uri list * - last parameter was not a positive integer\n");
				return;
			}

		}
		if(argc>3)
		{
			printf(".uri list * - too many parameters\n");
			return;
		}

	}
	else if (user_exists(argv[1], list_user))
	{
		username = list_user->record.name;
		if(argc==3)
		{
			if(sscanf(argv[2], "%d%c", &number, &c) == 1)
			{
				if(number < 1)
				{
					printf(".uri list %s - can't list <1 uris\n", argv[1]);
					return;
				}
			}
			else
			{
				printf(".uri list %s - last parameter was not a positive integer\n", argv[1]);
				return;
			}
		}
		if(argc>3)
		{
			printf(".uri list %s - too many parameters\n", argv[1]);
			return;
		}
	}
	else // possibly we have '.uri list n' or we might have garbage
	{
		if(argc==2)
		{
			if(sscanf(argv[1], "%d%c", &number, &c) == 1)
			{
				if(number < 1)
				{
					printf(".uri list - can't list <1 uris\n");
					return;
				}
				// we have a .uri list n
				username = user->record.name;
			}
			else
			{
				printf(".uri list - last parameter was not *, a username or a positive integer\n");
				return;
			}
		}
		if(argc>2)
		{
			printf(".uri list - invalid parameters\n");
			return;
		}

	}

	uri_list_display(number, username);
}

// checks an id supplied to uri_delete/nsfw etc. is valid or creates an id if the user specified 'last'
// if wiz is true "last" will return the the last uri in mwuri otherwise it is the current users last uri
// returns 0 if the id is not valid.
static unsigned int uri_get_id(const char *idstr, int wiz)
{
	unsigned int id;
	char c;
	char *query;
	struct db_result *res;

	if( strcasecmp( "last", idstr ) == 0)
	{
		id=0;
		if(wiz)
		{
			query = sqlite3_mprintf(
			          "SELECT id FROM mwuri "
				  "ORDER BY id DESC LIMIT 1");
		}
		else
		{
			query = sqlite3_mprintf(
			          "SELECT id FROM mwuri "
			          "WHERE user=%Q "
			          "ORDER BY id DESC LIMIT 1",
			          user->record.name);
		}
		res = db_query(MWURI_DB, query, 0);
		sqlite3_free(query);
		if(res)
		{
			if(res->rows > 0)
			{
				sscanf(res->data->field[0], "%u", &id);
			}
			db_free(res);
		}
		return id;
	}
	else if( sscanf(idstr, "%u%c", &id, &c) == 1 )
	{
		return id;
	}
	else if( sscanf(idstr, "#%u%c", &id, &c) == 1)
	{
		return id;
	}
	return 0;
}

// deletes uri argv[1] if argv[1] is the id of a valid uri or "last"
// if wiz is true any uri can be deleted otherwise only the current user's
void uri_delete(int argc, const char **argv, int wiz)
{
	unsigned int id;
	char *query;
	struct db_result *res;
	char mesg[MAXTEXTLENGTH];
	char *del_query;
	struct db_result *del_res;
	int owner;
	char answer[10];
	char *username, *uri, *added;

	if((id = uri_get_id(argv[1], wiz)) > 0 )
	{
		query = sqlite3_mprintf("SELECT user,url,added FROM mwuri WHERE id=%d", id);
		res = db_query(MWURI_DB, query, 1);
		if(res)
		{
			if(res->rows > 0)
			{
				username = res->data->field[0];
				uri = res->data->field[1];
				added = res->data->field[2];
				owner = (strcmp(user->record.name, username) == 0);

				if(owner || wiz)
				{
					snprintf(mesg, MAXTEXTLENGTH-1, "Are you sure you want to delete %s%s uri %s from mwuri?", owner? "your":username, owner? "":"'s", uri);
					display_message(mesg, 0, 1);
					get_str(answer, 5);
					if (answer[0]=='y' || answer[0]=='Y')
					{
						del_query = sqlite3_mprintf("DELETE FROM mwuri WHERE id=%d", id);
						del_res = db_query(MWURI_DB, del_query, 1);

						if(del_res)
						{
							free(del_res);
							snprintf(mesg, MAXTEXTLENGTH-1, "\03315%s has just deleted %s's uri %s [%s] from mwuri", user->record.name, username, uri, added);
							broadcast(1, "%s", mesg);
							snprintf(mesg, MAXTEXTLENGTH-1, "MWURI %s deleted uri: %s <%s> [%s]",  user->record.name, username, uri, added);
							mwlog(user, "%s", mesg);
						}
						else
						{
							printf("A db error occured trying to delete uri #%d\n", id);
						}
						sqlite3_free(del_query);
					}
				}
				else
				{
					snprintf(mesg, MAXTEXTLENGTH-1, "uri #%d %s was not posted by you. You cannot delete it", id, uri);
					display_message(mesg, 0, 1);
				}
			}
			else
			{
				printf("uri %s: couldn't find uri #%d to delete\n", argv[0], id);
			}
			db_free(res);
			sqlite3_free(query);
		}
		else
		{
			printf("uri %s: A db error occured while trying to find uri %s\n", argv[0], argv[1]);
		}
	}
	else
	{
		snprintf(mesg, MAXTEXTLENGTH-1, "uri %s: %s is not a valid uri id", argv[0], argv[1]);
		display_message(mesg, 0, 1);
	}
}

// toggles flag uri argv[1] if argv[1] is the id of a valid uri or "last"
// if wiz then any uri can be modified otherwise only the current user's
static void uri_set_flag(int argc, const char **argv, int wiz, int flag)
{
	unsigned int id;
	char mesg[MAXTEXTLENGTH];
	char *query, *update_query;
	struct db_result *res, *update_res;
	int owner;
	char answer[10];
	char *username, *uri, *added;
	int flags;
	char flagstr[1024];
	const char *desc_str = uri_get_flag_description(flag);

	if(desc_str == NULL) {
		printf("uri: whoops, someone needs to debug\n");
		return;
	}

	if((id = uri_get_id(argv[1], wiz)) > 0 )
	{
		query = sqlite3_mprintf("SELECT user,url,flags,datetime(added, 'localtime') FROM mwuri WHERE id=%d", id);
		res = db_query(MWURI_DB, query, 1);
		if(res)
		{
			if(res->rows > 0)
			{
				username = res->data->field[0];
				uri = res->data->field[1];
				flags = uri_parse_flags(res->data->field[2]);
				added = res->data->field[3];
				owner = (strcmp(user->record.name, username)==0);

				if(owner || wiz)
				{
					if(flags & flag)
					{
						snprintf(mesg, MAXTEXTLENGTH-1, "%s%s uri %s is already flaged as %s, do you want to unflag it?", owner? "your":username, owner? "":"'s", uri, desc_str);
					}
					else
					{
						snprintf(mesg, MAXTEXTLENGTH-1, "Are you sure you want to flag %s%s uri %s as %s?", owner? "your":username, owner? "":"'s", uri, desc_str);
					}
					display_message(mesg, 0, 1);
					get_str(answer, 5);
					if (answer[0]=='y' || answer[0]=='Y')
					{
						flags ^= flag; // toggle flag

						uri_make_flags_str(flags, flagstr, sizeof(flagstr));

						update_query = sqlite3_mprintf("UPDATE mwuri SET flags=%Q WHERE id=%d", flagstr, id);
						update_res = db_query(MWURI_DB, update_query, 1);

						if(update_res)
						{
							free(update_res);
							snprintf(mesg, MAXTEXTLENGTH-1, "\03304%s has just %sset the %s flag on %s's uri %s [%s] in mwuri", user->record.name, flag&flags?"":"un", desc_str, username, uri, added);
							broadcast(3, "%s", mesg);
							snprintf(mesg, MAXTEXTLENGTH-1, "MWURI %s %sset %s flag on uri: %s <%s> [%s]",  user->record.name, flag&flags?"":"un", desc_str, username, uri, added);
							mwlog(user, "%s", mesg);
						}
						else
						{
							printf("A db error occured trying to change flags on uri #%d\n", id);
						}
						sqlite3_free(update_query);
					}
				}
				else
				{
					snprintf(mesg, MAXTEXTLENGTH-1, "uri #%d %s was not posted by you. You cannot modify it", id, uri);
					display_message(mesg, 0, 1);
				}
			}
			else
			{
				printf("uri %s: couldn't find uri #%d to modify\n", argv[0], id);
			}
			db_free(res);
			sqlite3_free(query);
		}
	}
	else
	{
		snprintf(mesg, MAXTEXTLENGTH-1, "uri %s: %s is not a valid uri id", argv[0], argv[1]);
		display_message(mesg, 0, 1);
	}

}

// these 3 flag functions call the man set_flag with the right flag
void uri_nsfw(int argc, const char **argv, int wiz)
{
	uri_set_flag(argc, argv, wiz, URLFLAG_NSFW);
}

void uri_members_only(int argc, const char **argv, int wiz)
{
	uri_set_flag(argc, argv, wiz, URLFLAG_SUCS);
}

void uri_anon(int argc, const char **argv, int wiz)
{
	uri_set_flag(argc, argv, wiz, URLFLAG_ANON);
}

// sets full or short display mode for uri list
void uri_display_mode(int argc, const char **argv, int wiz)
{
	char *mode = userdb_get(USERDB_PRIVATE, user->record.name, "mwuri_display_mode");
	char default_mode[] = "short";
	if(!mode)
	{
		mode = default_mode;
	}

	if(argc == 1)
	{
		printf("Current uri list mode is: %s\n", mode ? mode:"short");
	}
	else
	{
		if(strcasecmp(argv[1],"full")==0)
		{
			userdb_set(USERDB_PRIVATE, user->record.name, "mwuri_display_mode", "full");
			if(strcasecmp(mode, "full")==0)
			{
				printf("You are already using full uri list display mode\n");
			}
			else
			{
				printf("uri list display mode set to full\n");
			}
		}
		else if(strcasecmp(argv[1],"short")==0)
		{
			userdb_set(USERDB_PRIVATE, user->record.name, "mwuri_display_mode", "short");
			if(strcasecmp(mode, "short")==0)
			{
				printf("You are already using short uri list display mode\n");
			}
			else
			{
				printf("uri list display mode set to short\n");
			}
		}
		else
		{
			printf("Usage: uri displaymode [full|short]\n");
		}
	}

	if(mode != default_mode)
	{
		free(mode);
	}
}

// sorts tab commpletion for the uri command
char *uri_action_tc(const char *text, int state)
{
 	static int i=0;
 	static int len=0;
 	static int wiz=0;
 	int do_tc=0;
 	char *c;

	if (state==0)
	{
		i=0;
		len=strlen(text);
		wiz = u_god(user);
	}

	while (uritable[i].action!=NULL)
	{
		if(len==0 && uritable[i].tc_mode==1)
		{
			// if len is 0 we tc all actions with a tc_mode 1 - tc_mode 2 are the su !edit functions for altering others uris
			do_tc = 1;
		}
		if(len > 0 && (!strncasecmp(uritable[i].action, text, len)) && uritable[i].tc_mode )
		{
			// if we have the first char(s) then all non hidden actions can be shown
			do_tc = 1;
		}
		if(!strcmp(uritable[i].action, ""))
		{
			do_tc = 0;
		}
		if(uritable[i].needs_wiz && !wiz)
		{
			// if the action needs wiz and we haven't got wiz we don't tc
			do_tc = 0;
		}
		if(do_tc)
		{
			c = strdup(uritable[i].action);
			i++;
			return(c);
		}
		i++;
	}
	return(NULL);
}

const char *uri_displaymodes[] = { "short", "full", NULL };

// tab completes arguments for the uri list and uri displaymode commands
// list: a username
// displaymode: short | full
char *uri_arg_tc(const char *text, int state)
{
	static int type=0;
	static int ptr=0;
	static int len=0;
	char *c;

	if (state==0)
	{
		type = 0;
		ptr = 0;
		len = strlen(text);

		if(strcasestr(rl_line_buffer, " list "))
		{
			type = 1;
		}
		if(strcasestr(rl_line_buffer, " displaymode "))
		{
			type = 2;
		}
	}

	if(type == 1)
	{
		// tab complete .uri list. part_user function handles it
		return(part_user(text, state));
	}
	if(type == 2)
	{
		// tab complete .uri displaymode
		while (uri_displaymodes[ptr]!=NULL)
		{
			if ((len==0 || !strncasecmp(uri_displaymodes[ptr], text, len)) && strcmp(uri_displaymodes[ptr], ""))
			{
				c = strdup(uri_displaymodes[ptr]);
				ptr++;
				return(c);
			}
			ptr++;
		}

	}
	return(NULL);
}

