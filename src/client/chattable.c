#include <stdlib.h>
#include "talker.h"

/* command string table, whilst in talker mode */
/*  "command", perms, #args, usage, descr, function, showable */

/* privs:
	   0 = normal
	   1 = raw
	   2 = gag
	   4 = zod
	   8 = mrod
	  10 = global
	  20 = protect
	  40 = freeze
	  80 = summon
	 100 = spy
	 200 = script
	 800 = SU
	1000 = Advanced Scripts
	2000 = Developer Mode
*/

CommandList chattable[]={
/* name, privs, argc, usage, help, function, visible */
{"bind"		,0x0200, 2, "Usage: bind <bind> <function>", "Add the bind to the current session", t_bind, 1},
{"debug"	,0x0200	,1, "Usage: debug <on|off>", "Toggle verbose script debugging", t_debug, 1},
{"destroy"	,0x0200	,1, "Usage: destroy <function | *>", "Destroy function (* - all functions)", t_destroy, 1},
{"doing"	,0x0000	,0, "Usage: doing <status...>", "Set status message", c_doing, 1},
{"e"		,0x0000	,1, "Usage: e <message>", "Emote message", t_emote, 1},
{"e'"		,0x0000	,1, "Usage: e' <message>", "Emote' message", t_emotes2, 0},
{"e'd"		,0x0000	,1, "Usage: e's <message>", "Emote's message", t_emoted, 0},
{"e'll"		,0x0000	,1, "Usage: e's <message>", "Emote's message", t_emotell, 0},
{"e's"		,0x0000	,1, "Usage: e's <message>", "Emote's message", t_emotes, 0},
{"em"		,0x0000	,1, "Usage: em <message>", "Emote message", t_emote, 0},
{"emote"	,0x0000	,1, "Usage: emote <message>", "Emote message", t_emote, 1},
{"emote'"	,0x0000	,1, "Usage: emote' <message>", "Emote' message", t_emotes2, 0},
{"emote'd"	,0x0000	,1, "Usage: emote's <message>", "Emote's message", t_emoted, 0},
{"emote'll"	,0x0000	,1, "Usage: emote's <message>", "Emote's message", t_emotell, 0},
{"emote's"	,0x0000	,1, "Usage: emote's <message>", "Emote's message", t_emotes, 0},
{"event"	,0x0200	,0, "Usage: event <function>", "Add text event handler function", t_event, 1},
{"floodlimit"	,0x2200	,1, "Usage: floodlimit <number>", "Set the number of lines before a script floods", t_flood, 1},
{"freeze"	,0x0040	,1, "Usage: freeze <user>", "Freeze the user to their current room", t_freeze, 1},
{"gag"		,0x0002	,1, "Usage: gag <user> [filter]", "Gag the user (with optional filter)", t_gag, 1},
{"global"	,0x0010	,1, "Usage: global <on|off>", "Global receive mode, hear all rooms", t_global, 1},
{"help"		,0x0000	,0, "Usage: help [command]", "List available commands, or help on [command]", t_help, 1},
{"linewrap"	,0x0000	,1, "Usage: linewrap <on|off>", "Line wrapping when terminal width is reached", t_linewrap, 1},
{"load"		,0x0200	,1, "Usage: load <file>", "Load script from file", t_load, 1},
{"kick"		,0x0804	,1, "Usage: kick <user> [reason]", "Eject user from talker, ignoring protection", t_kick, 1},
{"mrod"		,0x0008	,1, "Usage: mrod <user> [reason]", "Eject user from the BBS", t_mrod, 1},
{"mwrc"		,0x0200	,0, "Usage: mwrc [filename]", "Set/View your mwrc startup file", t_mwrc, 1},
{"notsayto"	,0x0003	,2, "Usage: notsayto <user> <message>", "Talk normally but exclude a single user", t_notsayto, 1},
{"protect"	,0x0020	,1, "Usage: protect <user> [level]", "Protect user from harm [to optional level]", t_protect, 1},
{"quit"		,0x0000	,0, "Usage: quit", "Leave talker", t_quit, 1},
{"raw"		,0x0001	,1, "Usage: raw <message>", "Send raw message", t_raw, 1},
{"remove"	,0x0808	,1, "Usage: remove <user> [reason]", "Eject user from BBS, ignoring protection", t_remove, 1},
{"replay"	,0x0000	,2, "Usage: replay {since|serial|count} <number>", "Replay previous messages", t_replay, 1},
{"restart"	,0x0200	,0, "Usage: restart", "Reload and restart all scripts exactly as at logon", t_restart, 1},
{"room"		,0x0000	,1, "Usage: room <room>", "Change rooms", t_room, 1},
{"runaway"	,0x0200	,1, "Usage: runaway <number>", "Set the number of lines before script runaway", t_runaway, 1},
{"sa"		,0x0000 ,2, "Usage: sayto <user> <message>", "Talk normally but only to a single user", t_sayto, 1},
{"say"		,0x0000	,2, "Usage: say <user> <message>", "Talk normally but only to a single user (deprecated)", t_sayto_warn, 0},
{"sayto"	,0x0000	,2, "Usage: sayto <user> <message>", "Talk normally but only to a single user", t_sayto, 1},
{"script"	,0x0200	,0, "Usage: script [function]", "List script functions, or source code for [function]", t_script, 1},
{"shout"	,0x0000	,1, "Usage: shout <message>", "Send message to all rooms", t_shout, 1},
{"spy"		,0x0100	,1, "Usage: spy <on|off>", "Spy mode - see who said what", t_spy, 1},
{"sproof"	,0x0800	,0, "Usage: sproof <on|off>", "Sound Proof this room", t_sproof, 1},
{"soundproof"	,0x0800	,0, "Usage: sproof <on|off>", "Sound Proof this room", t_sproof, 1},
{"sticky"	,0x0000	,1, "Usage: sticky <on|off>", "Log back on to the room you logged off from", t_sticky, 1},
{"summon"	,0x0080	,2, "Usage: summon <room> <user> [<user>...]", "Force user(s) to move into a room", t_summon, 1},
{"tcunames"	,0x0000, 1, "Usage: tcunames <on|off>", "Allow tab-completion of username on talker", t_tcunames, 1},
{"topic"	,0x0000, 0, "Usage: topic [topic]", "Set/view channel topic", t_topic, 1},
{"unbind"	,0x0200, 1, "Usage: unbind <bind | *>", "Remove the bind from the current session", t_unbind, 1},
{"unevent"	,0x0200	,1, "Usage: unevent <function | *>", "Unset [all] text event handler function(s)", t_unevent, 1},
{"unfreeze"	,0x0040	,1, "Usage: unfreeze <user>", "Allow user to change rooms again", t_unfreeze, 1},
{"ungag"	,0x0002	,1, "Usage: ungag <user>", "Remove the Gag on the user", t_ungag, 1},
{"unprotect"	,0x0020	,1, "Usage: unprotect <user>", "Remove protection from user", t_unprotect, 1},
{"uptime"	,0x0000	,0, "Usage: uptime", "Show server uptime", t_uptime, 1},
{"uri"		,0x0000	,0, "Usage: uri [list [*|username] [n] | delete <id> | nsfw <id> | membersonly <id> | anonymous <id> | displaymode <full|short>]", "View/Edit the uris in the mwuri database", t_uri, 1},
{"what"		,0x0000	,0, "Usage: what", "See peoples status", t_what, 1},
{"whisper"	,0x0000	,2, "Usage: whisper <user> <message>", "Send a message to a single user", t_whisper, 1},
{"who"		,0x0000	,0, "Usage: who", "See who's about", t_who, 1},
{"zod"		,0x0004	,1, "Usage: zod <user> [reason]", "Eject user from talker", t_zod, 1},
{">"		,0x0010	,2, "Usage: > <room> message...", "Send a message to a single room", t_ventril, 1},
{">>"		,0x0011	,2, "Usage: >> <room> raw text...", "Send a raw text to a single room", t_ventril_raw, 1},
{NULL		,0x0000	,0, "", "", NULL, 1}
};
