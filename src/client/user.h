#ifndef CLIENT_USER_H
#define CLIENT_USER_H

#include <stdint.h>
#include <user.h>

char *getmylogin(void);
void get_login(char *name, int autochat);
void list_users(int newonly);
void list_users_since(long date);
int login_ok(struct user *usr, int *autochat);
void strip_name(char *string);
void pick_salt(char *salt);
void search(const char *args, const char *ptr);

#endif
