#ifndef SCRIPT_H
#define SCRIPT_H 1

#include <stdio.h>
#include "Parse.h"

#define FF_OK 0
#define FF_NOFUNC 1
#define FF_UNBOUND 2

#define FUNCFLAG_AUTOINIT	0x00000001
#define FUNCFLAG_LOCAL		0x00000002
#define FUNCFLAG_BOARDINIT	0x00000004

#define LOGONMASK_QUIET		0x00000001
#define LOGONMASK_SUMMONED	0x00000002

/* prototype these structs */
struct code;
struct function;

typedef struct __inst {
	const char *name;
	unsigned int Rights;
	int args;
	const char *help;
	void (*Function)(struct code *, int, char **);
	int usage_count;
} Instruction;

typedef struct comp_stack {
	int compare;
	int match;
	struct comp_stack *next;
} CompStack;

typedef struct __name2func {
	const char *name;                     /* name of function    */
	char *(*function)(const char *, int); /* associated function */
} FuncNames;

/* switch for list/hash-based variable lists */

#include "hash.h"

typedef hash_op_t var_op_t;
typedef int var_list_t;

#define VAR_NEWLIST(list1) (*(list1) = hash_alloc())

#define VAR_FREELIST(vars1) hash_free(*vars1)

#define VAR_OP_INIT(op1, vars1) hash_op_init(op1, *vars1)

#define VAR_SEARCH(op1, name1) hash_search(op1, name1)

#define VAR_FOUND(vf_op) (*(vf_op)->rec != NULL)

#define VAR_STR_KEY(op1) ((char *)(**(op1)->rec).key)

#define VAR_KEY_FREE(op1) free((op1)->key)

#define VAR_TYPE_STR(op1) (*(op1)->rec && (**(op1)->rec).type == RT_STRING)
#define VAR_TYPE_ARRAY(op1) (*(op1)->rec && (**(op1)->rec).type == RT_HASH)
#define VAR_TYPE_INT(op1) (*(op1)->rec && (**(op1)->rec).type == RT_INT)

#define VAR_STR_VAL(op1) ((char *)(**(op1)->rec).data.pdata)

#define VAR_ARRAY_VAL(op1) (&(**(op1)->rec).data.idata)

#define VAR_INT_VAL(op1) ((**(op1)->rec).data.idata)

#define VAR_LIST_ITERATE(op1, vars1) \
do { \
    var_op_t *_op_11 = op1; \
    _op_11->field = *(vars1); \
    _op_11->key = 0; \
    hash_iterate(&_op_11->rec, _op_11->field); \
} while (0)

#define VAR_LIST_NEXT(op1) hash_next(&(op1)->rec)

#define VAR_STR_UPDATE(op1, val1) hash_set_p(op1, RT_STRING, val1)
#define VAR_ARRAY_UPDATE(op1, val1) hash_set_i(op1, RT_HASH, *val1)
#define VAR_INT_UPDATE(op1, val1) hash_set_i(op1, RT_INT, val1)

#define VAR_STR_CREATE(op1, val1) hash_set_p(op1, RT_STRING, val1)
#define VAR_ARRAY_CREATE(op1, val1) hash_set_i(op1, RT_HASH, *val1)
#define VAR_INT_CREATE(op1, val1) hash_set_i(op1, RT_INT, val1)

#define VAR_DESTROY(op1) hash_delete(op1)

#define VAR_FORGET(op1) hash_op_forgetkey(op1)

#define ARG_RANGE(list1, min, max) VAR_INT_FORCE(list1, "#", (max)+1); \

#define ARG_COUNT(list1)

#define VAR_STR_FORCE(list1, name1, val1) \
do { \
	var_op_t _op_6; \
	const char *_name_6 = (name1); \
	var_list_t *_list_6 = (list1); \
	const char *_val_6 = (val1); \
	VAR_OP_INIT(&_op_6, _list_6); \
	VAR_SEARCH(&_op_6, _name_6); \
	if (VAR_FOUND(&_op_6)) \
	{ \
		if (script_debug) printf("- %s: updating var $%s (%s) to '%s'\n", pc->inst->name, _name_6, VAR_STR_VAL(&_op_6), _val_6); \
		VAR_STR_UPDATE(&_op_6, _val_6); \
	} else \
	{ \
		if (script_debug) printf("- %s: creating var $%s setting to '%s'\n", pc->inst->name, _name_6, _val_6); \
		VAR_STR_CREATE(&_op_6, _val_6); \
	} \
} while (0)

#define ARG_STR_FORCE(list1, name1, val1) \
do { \
	var_op_t _op_6; \
	char _name_6[5]; \
	var_list_t *_list_6 = (list1); \
	const char *_val_6 = (val1); \
	snprintf(_name_6, 4, "%d", (name1)); \
	VAR_OP_INIT(&_op_6, _list_6); \
	VAR_SEARCH(&_op_6, _name_6); \
	if (VAR_FOUND(&_op_6)) \
		VAR_STR_UPDATE(&_op_6, _val_6); \
	else \
		VAR_STR_CREATE(&_op_6, _val_6); \
} while (0)

#define VAR_ARRAY_FORCE(list1, name1, val1) \
do { \
	var_op_t _op_6; \
	char *_name_6 = (name1); \
	var_list_t *_list_6 = (list1); \
	char *_val_6 = (val1); \
	VAR_OP_INIT(&_op_6, _list_6); \
	VAR_SEARCH(&_op_6, _name_6); \
	if (VAR_FOUND(&_op_6)) \
	{ \
		if (script_debug) printf("- %s: updating var $%s (%s) to '%s'\n", pc->inst->name, _name_6, VAR_STR_VAL(&_op_6), _val_6); \
		VAR_ARRAY_UPDATE(&_op_6, _val_6); \
	} else \
	{ \
		if (script_debug) printf("- %s: creating var $%s setting to '%s'\n", pc->inst->name, _name_6, _val_6); \
		VAR_ARRAY_CREATE(&_op_6, _val_6); \
	} \
} while (0)

#define VAR_INT_FORCE(list1, name1, val1) \
do { \
	var_op_t _op_6; \
	const char *_name_6 = (name1); \
	var_list_t *_list_6 = (list1); \
	int _val_6 = (val1); \
	VAR_OP_INIT(&_op_6, _list_6); \
	VAR_SEARCH(&_op_6, _name_6); \
	if (VAR_FOUND(&_op_6)) \
	{ \
		VAR_INT_UPDATE(&_op_6, _val_6); \
	} else \
	{ \
		VAR_INT_CREATE(&_op_6, _val_6); \
	} \
} while (0)

#define VAR_SEARCH_2(op1, llist1, glist1, name1) \
do { \
	var_op_t *_op_8 = (op1); \
	char *_name_8 = (name1); \
	VAR_OP_INIT(_op_8, (llist1)); \
	VAR_SEARCH(_op_8, _name_8); \
	if (!VAR_FOUND(_op_8)) \
	{ \
		VAR_OP_INIT(_op_8, (glist1)); \
		VAR_SEARCH(_op_8, _name_8); \
	} \
} while (0);

#define VAR_STR_PREPARE_2(op1, llist1, glist1, name1, dflt) \
do { \
	var_op_t *_op_9 = (op1); \
	VAR_SEARCH_2(_op_9, llist1, glist1, name1); \
	if (!VAR_FOUND(_op_9)) \
	{ \
		if (script_debug) printf("- %s: creating $%s.\n", pc->inst->name, name1); \
		VAR_STR_CREATE(_op_9, dflt); \
	} \
} while (0);

struct code {
	Instruction *inst;
	int argc;
	char **argv;
	struct function *parent;
	char *debug;
	struct code *next;
};

struct label {
	char *name;
	struct code *where;

	char *debug;
	struct label *next;
};

struct function {
	char *name;
	CompletionList *complist;
	int numcomp;
	int flags;
	int new;
	struct code *code;
	struct label *jump;
	struct function *next;
};

extern int script_terminate;
extern int script_debug;
extern var_list_t var_list;
extern var_list_t *local_vars;
extern struct code *script_jump; /* */
extern int busy;

extern int compare_count;
extern int compare_match;
extern char *event_body_text;
extern int script_offset;
extern volatile int script_output;
extern char *event_user;

void CallFunction(struct code *code, int argc, char **argv);
void RunInitFuncs(int talkinit);

int arg_count(var_list_t *list);
int var_isanum(var_op_t *op, int *result, int onlydecimal);

/* Readline callback functions */
char *list_script(const char *text, int state);
char *list_mwsfile(const char *text, int state);
int expand_script_dir(char **dir);

char *var_str_val(var_op_t *op);
void var_key_dup(var_op_t *op);

extern void DoScript(char *line);
int ExecEvent(char *script, const char *text, const char *event, const char *who, int pre);
int ExecEvent2(char *script, const char *event, const char *who, int pre, int numargs, char *aargs[]);

void ListScript(const char *name);
void DestroyAllFunctions(int debug);
void DestroyFunction(const char *funcname);
void DestroyVariables(int debug);

#endif /* SCRIPT_H */
