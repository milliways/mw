#include "script.h"
#include "uri.h"
#include "main.h"
#include "talker_privs.h"
#include "alias.h"
#include "who.h"
#include "gaglist.h"

/*
  modes:	0 - commands
  		1 - talker commands
  		2 - script function
  farg:		argument you want to tab-complete
  larg:		argument to tab-complete to (-1 for all)
*/

/*
  completion functions:
    part_who_talk	- users on talker
    part_gag_filter	- gag filters
    part_who		- users logged onto mw
    find_folder		- current folders
    part_comm_folder	- folder editing options
    part_user		- all users created
    part_comm_user	- user editing options
    part_comm_mesg	- message editing options
    list_bind		- script binds
    list_commands	- lists board commands
    list_chat_commands	- lists talker commands
    list_script		- lists all script functions
*/


CompletionList tctable[]={
/* command	,mode	,farg	,larg	,function */

/* talker commands */
{"bind"		,1	,2	,2	,list_script},
{"bind"		,1	,1	,2	,NULL},
{"e"		,1	,1	,-1	,part_who_talk},
{"e'"		,1	,1	,-1	,part_who_talk},
{"e'd"		,1	,1	,-1	,part_who_talk},
{"e'll"		,1	,1	,-1	,part_who_talk},
{"e's"		,1	,1	,-1	,part_who_talk},
{"emote"	,1	,1	,-1	,part_who_talk},
{"emote'"	,1	,1	,-1	,part_who_talk},
{"emote'd"	,1	,1	,-1	,part_who_talk},
{"emote'll"	,1	,1	,-1	,part_who_talk},
{"emote's"	,1	,1	,-1	,part_who_talk},
{"event"	,1	,1	,1	,list_script},
{"freeze"	,1	,1	,1	,part_who_talk},
{"gag"		,1	,2	,2	,gaglist_tc_filter},
{"gag"		,1	,1	,2	,part_who_talk},
{"kick"		,1	,1	,1	,part_who_talk},
{"load"		,1	,1	,1	,list_mwsfile},
{"mrod"		,1	,1	,1	,part_who},
{"notsayto"	,1	,1	,1	,part_who_talk},
{"protect"	,1	,1	,1	,part_who_talk},
{"raw"		,1	,1	,-1	,part_who_talk},
{"remove"	,1	,1	,1	,part_who_talk},
{"sa"		,1	,1	,1	,part_who_talk},
{"sayto"	,1	,1	,1	,part_who_talk},
{"search"	,0	,1	,1	,part_comm_search},
{"summon"	,1	,2	,-1	,part_who},
{"unbind"	,1	,1	,1	,list_bind},
{"unfreeze"	,1	,1	,1	,part_who_talk},
{"ungag"	,1	,1	,1	,part_who_talk},
{"unprotect"	,1	,1	,1	,part_who_talk},
{"uri"		,1	,2	,2	,uri_arg_tc},
{"uri"		,1	,1	,1	,uri_action_tc},
{"whisper"	,1	,1	,1	,part_who_talk},
{"zod"		,1	,1	,1	,part_who_talk},


/* board commands */
{"cd"		,0	,1	,1	,find_folder},
{"autosub"	,0	,1	,1	,find_folder},
{"tidyup"	,0	,1	,1	,find_folder},
{"topten"	,0	,1	,1	,find_folder},
{"tell"		,0	,1	,1	,part_who},
{"force"	,0	,1	,1	,part_who},
{"folder"	,0	,2	,2	,find_folder},
{"folder"	,0	,1	,2	,part_comm_folder},
{"user"		,0	,2	,2	,part_user},
{"user"		,0	,1	,2	,part_comm_user},
{"mesg"		,0	,1	,1	,part_comm_mesg},

{NULL		,0	,0	,0	,NULL}
};
