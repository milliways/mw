#ifndef TALKER_H
#define TALKER_H

#include "Parse.h"

/* talker command functions */
void t_bind(CommandList *cm, int argc, const char **argv, char *args);
void t_unbind(CommandList *cm, int argc, const char **argv, char *args);
void t_who(CommandList *cm, int argc, const char **argv, char *args);
void t_what(CommandList *cm, int argc, const char **argv, char *args);
void t_help(CommandList *cm, int argc, const char **argv, char *args);
void t_quit(CommandList *cm, int argc, const char **argv, char *args);
void t_script(CommandList *cm, int argc, const char **argv, char *args);
void t_runaway(CommandList *cm, int argc, const char **argv, char *args);
void t_flood(CommandList *cm, int argc, const char **argv, char *args);
void t_load(CommandList *cm, int argc, const char **argv, char *args);
void t_destroy(CommandList *cm, int argc, const char **argv, char *args);
void t_restart(CommandList *cm, int argc, const char **argv, char *args);
void t_debug(CommandList *cm, int argc, const char **argv, char *args);
void t_shout(CommandList *cm, int argc, const char **argv, char *args);
void t_whisper(CommandList *cm, int argc, const char **argv, char *args);
void t_sayto(CommandList *cm, int argc, const char **argv, char *args);
void t_sayto_warn(CommandList *cm, int argc, const char **argv, char *args);
void t_notsayto(CommandList *cm, int argc, const char **argv, char *args);
void t_emote(CommandList *cm, int argc, const char **argv, char *args);
void t_emotes(CommandList *cm, int argc, const char **argv, char *args);
void t_emotes2(CommandList *cm, int argc, const char **argv, char *args);
void t_emoted(CommandList *cm, int argc, const char **argv, char *args);
void t_emotell(CommandList *cm, int argc, const char **argv, char *args);
void t_event(CommandList *cm, int argc, const char **argv, char *args);
void t_unevent(CommandList *cm, int argc, const char **argv, char *args);
void t_global(CommandList *cm, int argc, const char **argv, char *args);
void t_protect(CommandList *cm, int argc, const char **argv, char *args);
void t_unprotect(CommandList *cm, int argc, const char **argv, char *args);
void t_gag(CommandList *cm, int argc, const char **argv, char *args);
void t_ungag(CommandList *cm, int argc, const char **argv, char *args);
void t_zod(CommandList *cm, int argc, const char **argv, char *args);
void t_mrod(CommandList *cm, int argc, const char **argv, char *args);
void t_kick(CommandList *cm, int argc, const char **argv, char *args);
void t_remove(CommandList *cm, int argc, const char **argv, char *args);
void t_replay(CommandList *cm, int argc, const char **argv, char *args);
void t_raw(CommandList *cm, int argc, const char **argv, char *args);
void t_room(CommandList *cm, int argc, const char **argv, char *args);
void t_ventril(CommandList *cm, int argc, const char **argv, char *args);
void t_ventril_raw(CommandList *cm, int argc, const char **argv, char *args);
void t_freeze(CommandList *cm, int argc, const char **argv, char *args);
void t_unfreeze(CommandList *cm, int argc, const char **argv, char *args);
void t_summon(CommandList *cm, int argc, const char **argv, char *args);
void t_sticky(CommandList *cm, int argc, const char **argv, char *args);
void t_tcunames(CommandList *cm, int argc, const char **argv, char *args);
void t_spy(CommandList *cm, int argc, const char **argv, char *args);
void t_sproof(CommandList *cm, int argc, const char **argv, char *args);
void t_linewrap(CommandList *cm, int argc, const char **argv, char *args);
void t_mwrc(CommandList *cm, int argc, const char **argv, char *args);
void t_uri(CommandList *cm, int argc, const char **argv, char *args);
void t_topic(CommandList *cm, int argc, const char **argv, char *args);
void t_uptime(CommandList *cm, int argc, const char **argv, char *args);
void t_chaton(void);

void chat_say(const char *text);
void talk_sayto(char *text, const char *to, const char *type);
void set_talk_rights(void);
void talk_send_emote(char * text, int channel, int plural);
void talk_send_rawbcast(char * text);
void talk_send_raw(char * text, int channel);
void talk_send_shout(char * text);
unsigned long cp_flags(unsigned long cm, unsigned long flags, int mode);

void enter_talker(int logontype);
int screen_h(void);
int screen_w(void);

int ChangeRoom(char *room, int quiet);
void enter_room(int room);

#endif /* TALKER_H */
