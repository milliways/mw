#ifndef GAGLIST_H
#define GAGLIST_H

#define GAGLIST_MAX_ENTRIES (128)

extern int gaglist_append(const char *gagname);
extern void gaglist_destroy(void);
extern char *gaglist_tc_filter(const char *text, int state);

#endif /* GAGLIST_H */
