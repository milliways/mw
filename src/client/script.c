#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <readline/readline.h>
#include <pwd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdbool.h>
#include "bb.h"
#include "talker_privs.h"
#include "strings.h"
#include "str_util.h"
#include "talker.h"
#include "init.h"
#include "alias.h"
#include "main.h"
#include "frl.h"
#include "js.h"
#include "user.h"
#include "userio.h"
#include "who.h"
#include "gaglist.h"
#include "script.h"
#include <util.h>

#define MAX_ARGC 128

extern struct user * const user;
extern unsigned long rights;
extern int busy;
extern int current_rights;

struct function *function_list=NULL;
extern const char *autoexec_arg;
var_list_t var_list = -1;
var_list_t *local_vars=NULL;
CompStack *comparison_stack=NULL;

/* very crude event driver, called on text recv */
char *event_body_text=NULL;

/* set runaway variable */
unsigned long run_away = RUNAWAY;

/* how many script threads are currently running */
int script_running=0;

/* person causing the event to occur */
char *event_user=NULL;

/* do we want to output text after an event? */
volatile int script_output = 1;

/* logon type */
int talker_logontype = 0;

/* User's home directory, for script filename expansion */
static char	       *homedir = NULL;
static size_t		homelen = 0;

/* autoexecs any new initialisation functions */
void RunInitFuncs(int talkinit)
{
	struct function *func=NULL;

    	/* execute 'init' functions */
	func=function_list;
	while(func!=NULL)
	{
		if (func->new)
		{
			if (talkinit)
			{
				if (func->flags & FUNCFLAG_AUTOINIT)
				{
					ExecEvent(func->name,autoexec_arg,"AutoExec",NULL,0);
					func->new = false;
				}
			}
			else
			{
				if (func->flags & FUNCFLAG_BOARDINIT)
				{
					ExecEvent(func->name,autoexec_arg,"AutoExec",NULL,0);
					func->new = false;
				}
			}
		}
		func=func->next;
	}
}

void DestroyFunction(const char *funcname)
{
	struct function *func, *funcold;
	struct code *fcode;
	struct label *label;

	func=function_list;
	funcold=NULL;
	while (func!=NULL && strcasecmp(funcname, func->name))
	{
		funcold=func;
		func=func->next;
	}
	if (func==NULL) {
		printf("Cant find function %s to destroy.\n", funcname);
		return;
	}
	if (funcold==NULL)
		function_list=func->next;
	else
		funcold->next=func->next;
	free(func->name);

	label=func->jump;
	while (label!=NULL) {
		struct label *oldlabel;
		oldlabel=label->next;
		free (label->name);
		free (label->debug);
		free (label);
		label=oldlabel;
	}

	fcode=func->code;
	while (fcode!=NULL) {
		int i;
		struct code *oldcode;
		free(fcode->debug);
		for (i=0;i<fcode->argc;i++)
			free(fcode->argv[i]);
		free(fcode->argv);
		oldcode=fcode->next;
		free(fcode);
		fcode=oldcode;
	}

	free(func->complist);

	free(func);
	printf("Function %s destroyed.\n",funcname);
}

void DestroyAllFunctions(int debug)
{
	struct function *func;
	struct code *fcode;
	struct label *label;

	while (function_list!=NULL)
	{
		if (debug) printf("Destroying Function %s.\n",function_list->name);

		free(function_list->name);
		label=function_list->jump;
		while (label!=NULL)
		{
			struct label *oldlabel;
			oldlabel=label->next;
			free (label->name);
			free (label->debug);
			free (label);
			label=oldlabel;
		}
		fcode=function_list->code;
		while (fcode!=NULL)
		{
			int i;
			struct code *oldcode;
			free(fcode->debug);
			for (i=0;i<fcode->argc;i++)
				free(fcode->argv[i]);
			free(fcode->argv);
			oldcode=fcode->next;
			free(fcode);
			fcode=oldcode;
		}
		free(function_list->complist);
		func = function_list;
		function_list=function_list->next;
		free(func);
	}
}

/***************
 ** Runtime...
 ***************/

/* conditionals */
int compare_count;
int compare_match;
int script_offset=0;  /* how far have we 'shift'ed the args */
struct code *script_jump;

unsigned long runaway=0;  /* count how many instructions executed to stop infinite loops */
int script_debug=0;  /* print verbose debug info */
int script_terminate;

void DoScript(char *line)
{
	const char *bits[MAX_ARGC];
	char *function_name;
	int num;

	if ((num=ParseLine(line, bits))<1) return;

	js_flood = 0;
	if (js_handle_command(bits[0], num - 1, &bits[1]) == 0)
		return;
	if ((function_name = FindLinks(bind_list, bits[0])) == NULL) {
		printf("Script bind '%s' not found.\n", bits[0]);
		return;
	}
	if (is_js(function_name))
		js_exec(function_name, num, bits);
	free(function_name);
}

int ExecEvent(char *script, const char *text, const char *event, const char *who, int pre)
{
	int retval;

	if (is_js(script)) {
		const char *argv[4];
		argv[0]=event;
		argv[1]=who;
		argv[2]=text;
		argv[3]=text+pre;
		busy++;
		retval=js_exec(script, 4, argv);
		busy--;
		return retval;
	}
	return 0;
}

int ExecEvent2(char *script, const char *event, const char *who, int pre, int numargs, char *aargs[])
{
	int		retval, i;

	if (is_js(script)) {
		const char **argv;
		argv = calloc(numargs+2,sizeof(char *));
		argv[0]=event;
		argv[1]=who;
		for (i=0;i<numargs;i++)
			argv[i+2]=aargs[i];
		retval = js_exec(script, numargs+2, argv);
		free(argv);
		return retval;
	}
	return 0;
}

void ListScript(const char *name)
{
	struct function *ptr;
	int i;

	ptr=function_list;

	if (name==NULL)
		ShowLinks(bind_list, "Available Script Binds :-", "-->", 0);
	else
	{
		while (ptr!=NULL && strcasecmp(name, ptr->name)) ptr=ptr->next;
		if (ptr==NULL)
			printf("Function %s not found.\n", name);
		else
		{
			for (struct code *c = ptr->code; c != NULL; c = c->next)
			{
				printf("%s: %s %d ", c->debug, c->inst->name, c->argc);
				for (i=0; i<c->argc; i++) printf("'%s' ", c->argv[i]);
				printf("\n");
			}
		}
	}
}

/* script function autocompletion for readline */
char *list_script(const char *text, int state)
{
	static int len;
	static struct function *fn = NULL;
	char *name;
	int hidden;

	if (state == 0)
	{
		fn = function_list;
		len = strlen(text);
	}

	while (fn != NULL)
	{
		hidden = fn->flags & FUNCFLAG_LOCAL;
		name = fn->name;
		fn = fn->next;
		if (len == 0 || !strncasecmp(name, text, len))
		{
			/* only if an interface function (default) */
			if (hidden == false)
			{
				return strdup(name);
			}
		}
	}
	return NULL;
}

/* script file autocompletion for readline */
char *list_mwsfile(const char *text, int state)
{
    char	       *relname;
    char	       *name;
    char	       *trimmed_name;
    struct stat		stats;
    int			isdir;
    struct passwd      *pw;
    char	       *part_path;

    if (!state && !homedir)
    {
	/* Can we find the user's home directory? */
	if ((pw=getpwuid(getuid()))!=NULL)
	{
	    /* Copy the home directory */
	    homedir = strdup(pw->pw_dir);
	    homelen = strlen(homedir) + 1;
	}
    }

    relname = NULL;
    /* Is the path prefix legal ? */
    if (text &&
	strncmp(text, "../", 3) != 0 &&
	strstr(text, "/../") == NULL)
    {
	/* Set euid to real uid so completion can't be used to read Arthur's
	 * home directory */
	perms_drop();
	/* Keep getting filenames until we see a legal one */
	do
	{
	    /* Use readline's file completion to do the dirty work */
	    name = rl_filename_completion_function(text, state);
	    state = 1;
	    if (name)
	    {
		/* Is the file path legal? */
		if (strcmp(name, ".") != 0 &&
		    strcmp(name, "./") != 0 &&
		    strncmp(name, "../", 3) != 0 &&
		    strstr(name, "/../") == NULL)
		{
		    part_path = strdup(name);
		    expand_script_dir(&part_path);
		    if (stat(part_path, &stats) == 0)
		    {
			/* See if this file is a directory */
			isdir = S_ISDIR(stats.st_mode);
			if (isdir || S_ISREG(stats.st_mode))
			{
			    trimmed_name = name;
			    while (strncmp(trimmed_name, "./", 2) == 0)
				trimmed_name += 2;
			    /* Copy the relative part (after our home dir) */
			    relname = strdup(trimmed_name);
			}
		    }
		    free(part_path);
		}
		/* Free the filename, bypassing the malloc wrappers */
		free(name);
	    } else
	    {
		break;
	    }
	} while (!relname);
	perms_restore();
    }

    return relname;
}

int expand_script_dir(char **dir)
{
    char	       *old;
    char	       *part_path;

    if (homedir && dir && *dir)
    {
	old = *dir;
	/* Combine the home dir & partial filename */
	if (strcmp(old, ".") == 0)
	{
	    part_path = strdup(homedir);
	}
	else
	{
	    while (strncmp(old, "./", 2) == 0)
		old += 2;
	    part_path = malloc(homelen + strlen(old) + 1);
	    sprintf(part_path, "%s/%s", homedir, old);
	}
	/* Replace the relative path with an absolute path */
	free(*dir);
	*dir = part_path;
    }

    /* Return of 0 indicates that this modification should not be shown in the
     * input line
     */
    return 0;
}
