#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <ctype.h>
#include <string.h>
#include <stdarg.h>
#include "bb.h"
#include "strings.h"

#define min(a,b) a<b?a:b
#define max(a,b) a>b?a:b

struct stacked_str *cmd_stack = NULL;
struct stacked_str **last_cmd = &cmd_stack;
static int stacked=0;

/*static char stackedline[1024];*/

void stack_str(char *string)
{
	stack_cmd(string, CMD_TEXT|CMD_TYPED);
}

void stack_cmd(char *string, int cmdtype)
{
	struct stacked_str *new;

	new = malloc(sizeof(*new));
	new->text = strdup(string);
	new->type = cmdtype;
	new->next = *last_cmd;
	*last_cmd = new;
	stacked++;
}

int is_stacked()
{
	return(stacked);
}

int pop_stack(char *string, int len)
{
	int type=0;
	pop_cmd(string, len, &type);
	return type;
}

void pop_cmd(char *string, int len, int *type)
{
	struct stacked_str *free_me;

	if (stacked > 0)
	{
		stacked--;
		strncpy(string, cmd_stack->text, len);
		*type = cmd_stack->type;
		free(cmd_stack->text);
		free_me = cmd_stack;
		cmd_stack = cmd_stack->next;
		free(free_me);
		if (cmd_stack == NULL) last_cmd = &cmd_stack;
	}
	string[len - 1] = '\0';
}
