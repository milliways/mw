#ifndef FRL_H
#define FRL_H

/**************************************************************
**        Name: File Readline Utilities (frl)                **
**      Author: Chris Fry                                    **
**        Date: 19/03/2000                                   **
** Description: Utilies to read lines from a file            **
**************************************************************/

#include <stdio.h>

typedef struct __str_list {
	char *text;
	struct __str_list *next;
} StringList;



/**************************************************************
** Description: Reads 1 line of any length from given file,  **
**              allocated memory for it, and returns it.     **
**          In: FILE* to read from - must be open for read   **
**      Return: NULL if end of file, and no text             **
**              <text> if there is any, includes blank line  **
**************************************************************/
char *frl_line(FILE *f);


/**************************************************************
** Description: repeatedly calls frl from a file until first **
**              non-blank line not started by given comment  **
**          In: FILE* to read from - must be open for read   **
**              char* comment to ignore - if NULL ignored    **
**      Return: NULL if end of file, and no text             **
**              <text> if there is any                       **
**************************************************************/
char *frl_line_nspace(FILE *f, const char *comment);


long num_lines_read(void);

#endif /* FRL_H */
