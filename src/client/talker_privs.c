#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "talker_privs.h"
#include "talker.h"
#include "bb.h"
#include "rooms.h"
#include "main.h"
#include "user.h"
#include <util.h>

extern struct user * const user;
extern int quietmode;

unsigned long chatmode_describe(unsigned long old, unsigned long new, int ourapl, int theirapl, const char *from)
{
	static char	message[81];
	unsigned long	add, sub, dif;
	/* oldp, newp are permanent protection levels.  oldep, newep are
	 * effective levels, which include temporarary protection.
	 */
	int		oldp, oldep, newp, newep, difp;

	/* get new and old protection flags */
	newp = (new & CM_PROTMASK) >> CM_PROTSHIFT;
	oldp = (old & CM_PROTMASK) >> CM_PROTSHIFT;

	dif  = old^new;
	difp = newp-oldp;
	/* Protection level changes don't count as flags added/removed. */
	add = new & dif & ~CM_PROTMASK;
	sub = old & dif & ~CM_PROTMASK;

	/* If our current protection level is greater than that of the sender
	 * then cancel any attempts at unprotection (no warning).  Temporary
	 * protection is ignored.
	 */
	if (oldp > theirapl)
	{
		/* Remove protect mask from flags, and re-calculate all
		 * variables.
		 */
		new ^= old; new &= ~CM_PROTMASK; new ^= old;
		dif = old^new;
		newp = (new & CM_PROTMASK) >> CM_PROTSHIFT;
		if (newp == 0 && (new & CM_PROTECTED)) newp = 1;
		difp = newp-oldp;
	}

	/* Calculate the effective protection level, which will be used to
	 * allow/deny other chatmode changes.  Temporary protection == level 1.
	 */
	oldep = oldp;
	if (oldp == 0 && (old & CM_PROTECTED))
		oldep = 1;
	newep = newp;
	if (newp == 0 && (new & CM_PROTECTED))
		newep = 1;

	if (newep == 0 && oldep != 0)
	{
		display_message("*** Your protection has just been removed, you are vulnerable again.", 1, 1);
	}
	else if (newep > ourapl && oldep <= ourapl)
	{
		display_message("*** You have just been protected from harm", 1, 1);
	}
	else if (newp == 0 && newep == 1 && oldep <= ourapl &&
		 (oldp != 0 || oldep != 1))
	{
		sprintf(message, "*** You have just been given temporary protection");
		display_message(message, 1, 1);
	}
	else if (newp != 0 && newep <= ourapl && oldp == 0)
	{
		sprintf(message, "*** You have just been given level %d protection", newp);
		display_message(message, 1, 1);
	}
	else if (difp > 0 && newp <= ourapl)
	{
		sprintf(message, "*** Your protection level has just been increased from %d to %d", oldp, newp);
		display_message(message, 1, 1);
	}
	else if (newp <= ourapl && oldp > ourapl)
	{
		sprintf(message, "*** Your protection level has just been decreased to %d", newp);
		display_message(message, 1, 1);
	}
	else if (difp < 0 && oldp <= ourapl)
	{
		sprintf(message, "*** Your protection level has just been decreased from %d to %d", oldp, newp);
		display_message(message, 1, 1);
	}
	else if (oldep > theirapl && newep > theirapl && dif != 0)
	{
		/* Fall back if our current protection level is greater than
		 * their authorised level.
		 */
		char text[MAXTEXTLENGTH];

		if (add != 0 && sub != 0) {
			snprintf(text, MAXTEXTLENGTH-1, "*** Someone has just tried to +%s",display_cmflags(add));
			snprintf(text+strlen(text), MAXTEXTLENGTH-strlen(text), "-%s you.\n",display_cmflags(sub));
		} else if (add != 0) {
			snprintf(text, MAXTEXTLENGTH-1, "*** Someone has just tried to +%s you.\n",display_cmflags(add));
		} else if (sub != 0) {
			snprintf(text, MAXTEXTLENGTH-1, "*** Someone has just tried to -%s you.\n",display_cmflags(sub));
		} else {
			return(old);
		}

		display_message(text, 1, 1);
		return(old);
	}

	if (add & CM_VERBOSE)
		display_message("*** You have just been verbosed!", 1, 1);
	if (sub & CM_VERBOSE)
		display_message("*** You have been de-verbosed", 1, 1);

	if (add & CM_STICKY)
		display_message("*** You now have sticky rooms enabled", 1, 1);
	if (sub & CM_STICKY)
		display_message("*** You feel less like sticky the stick insect", 1, 1);

	if (add & CM_FROZEN)
		display_message("*** You have just been frozen. You can no longer change rooms.", 1, 1);
	if (sub & CM_FROZEN)
		display_message("*** You have been defrosted. You can now change rooms again.", 1, 1);

	if (sub & CM_ONCHAT)
	{
		char text[MAXTEXTLENGTH];

		display_message("*** You have been kicked off chat", 1, 1);

		snprintf(text,MAXTEXTLENGTH-1,"\03311%s has just left talker",user->record.name);
		if (!quietmode) talk_send_rawbcast(text);

		set_rights();
	}
	if (add & CM_ONCHAT)
	{
		char text[MAXTEXTLENGTH];
		char mtext[MAXTEXTLENGTH];

		set_talk_rights();

		if (user->room.name != NULL)
			snprintf(mtext,MAXTEXTLENGTH-1,"\03310%s has just joined talker in %s",
			         user->record.name, user->room.name);
		else
			snprintf(mtext,MAXTEXTLENGTH-1,"\03310%s has just joined talker room %d",
			         user->record.name, user->record.room);
		snprintf(text,MAXTEXTLENGTH-1,"\03310%s has just joined talker room %d",
		         user->record.name, user->record.room);
		if (!quietmode) talk_send_rawbcast(text);

		printf("Now entering the talker. Use \".quit\" to leave and \".help\" for help.\n");

		/* run the common stuff for entering the talker */
		enter_talker(3);
	}

	return(new);
}
