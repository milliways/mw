#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <util.h>
#include <mwlog.h>
#include "talker_privs.h"
#include "special.h"
#include "ipc.h"
#include "perms.h"
#include "intl.h"
#include "mesg.h"
#include "bb.h"
#include "user.h"

extern struct user * const user;

void inform_of_mail(char *to)
{
	if (ipc_send_to_username(to, IPC_NEWMAIL, NULL) != 0)
		printf(_("Cannot inform %s of new mail.\n"),to);
}

void postinfo(struct user *who, struct folder *fol, struct Header *mesg)
{
        char buff[MAXTEXTLENGTH];

	if (is_private(fol,who)) {
		snprintf(buff, MAXTEXTLENGTH,"\03305INFO: New mail from %s in folder %s",
		         who->record.name, fol->name);
	} else {
		snprintf(buff, MAXTEXTLENGTH,"\03303INFO: %s has just posted in folder %s",
		         who->record.name, fol->name);
	}
	ipc_send_to_all(IPC_TEXT, buff);
}

/* broadcast state
0= everyone no choice
1= login information
2= wiz chat people only
3= changeinfo, eg !user
4= everyone on talker, no choice
0x100= raw text, eg !su, .mrod
*/

void broadcast(int state, const char *fmt, ...)
{
	char buff[MAXTEXTLENGTH];
	char text[MAXTEXTLENGTH - 14];
	va_list va;
	int israw;

	va_start(va, fmt);
	vsnprintf(text, MAXTEXTLENGTH - 15, fmt, va);
	va_end(va);

	israw = (state & 0x100) != 0;
	state &= ~0x100;

	if (state==0) mwlog(user, "WALL %s", text);
	else if (state==1 && israw) mwlog(user, "BROADCAST %s", text);
	else if (state==2) mwlog(user, "WIZ %s", text);
	else if (state==5) mwlog(user, "STATUS %s", text);

	switch (state) {
		case 1:
			snprintf(buff, MAXTEXTLENGTH, "\03304%s%s", israw?"":"SYSTEM: ", text);
			ipc_send_to_all(IPC_TEXT, buff);
			break;
		case 2:
			snprintf(buff, MAXTEXTLENGTH, "\03306%s%s", israw?"":"Wiz: ", text);
			ipc_send_to_all(IPC_WIZ, buff);
			break;
		case 3:
			snprintf(buff, MAXTEXTLENGTH, "\03303%s%s", israw?"":"CHANGE: ", text);
			ipc_send_to_all(IPC_WIZ, buff);
			break;
		case 4:
			snprintf(buff, MAXTEXTLENGTH, "\03304%s%s", israw?"":"SYSTEM: ", text);
			ipc_send_to_all(IPC_TEXT, buff);
			break;
		case 5:
			/* doing messages */
			snprintf(buff, MAXTEXTLENGTH, "\03304STATUS: \03314%s",text);
			ipc_message_t * msg = ipcmsg_create(IPC_SAYTOROOM, user->posn);
			ipcmsg_destination(msg, user->record.room);
			json_t * j = json_init(NULL);
			json_addstring(j, "text", buff);
			json_addstring(j, "type", "status");
			ipcmsg_json_encode(&msg, j);
			json_decref(j);
			ipcmsg_transmit(msg);

			break;
		default:
			snprintf(buff, MAXTEXTLENGTH, "\03304%s%s", israw?"":"SYSTEM: ", text);
			ipc_send_to_all(IPC_TEXT, buff);
	}
}
