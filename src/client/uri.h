#ifndef URI_H
#define URI_H

#define URLFLAG_NSFW	0x0001
#define URLFLAG_ANON	0x0002
#define URLFLAG_SUCS	0x0004

#define MWURI_DB STATEDIR"/mwuri.db"


typedef struct uriaction
{
	const char *action;
	int min_argc;
	int max_argc;
	int tc_mode;
	int needs_wiz;
	void (*function)(int, const char **, int);
} uriActionList;

typedef struct uriflag
{
	int flag;
	const char *flagstr;
	const char *long_desc;
	const char *list_desc;
} uriFlagList;

extern uriActionList uritable[];

void uri_make_flags_str(int flags, char *flagstr, int len);

void uri_list_display(int number, const char * username);

void uri_list(int argc, const char **argv, int wiz);
void uri_delete(int argc, const char **argv, int wiz);
void uri_nsfw(int argc, const char **argv, int wiz);
void uri_members_only(int argc, const char **argv, int wiz);
void uri_anon(int argc, const char **argv, int wiz);
void uri_display_mode(int argc, const char **argv, int wiz);

char *uri_action_tc(const char *text, int state);
char *uri_arg_tc(const char *text, int state);

#endif /* URI_H */
