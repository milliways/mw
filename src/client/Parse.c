#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "bb.h"
#include "init.h"
#include "talker.h"
#include "user.h"
#include "talker_privs.h"
#include "userio.h"
#include "alias.h"

extern unsigned long rights;
extern CommandList chattable[];
extern struct user * const user;

static void shuffle_up(char *bp)
{
	/* The strlen is right its the length of bp, which is the length
	   of bp+1 including the NUL */
	memmove(bp,bp+1,strlen(bp));
}

int ParseLine(char *line, const char *argv[])
{
	char *ptr=line;
	int n=0;
	int quoted=0;
	int slashed=0;

	while(n<MAX_ARGC-1)
	{
/*		printf("Skip spaces on [%s...]\n",ptr);*/
		while(*ptr && isspace(*ptr))
			ptr++;
		if(*ptr==0)
			break;
		argv[n++]=ptr;

/*		printf("Begin process entry %d [%s...]\n",n, ptr);*/
		while(*ptr && (!isspace(*ptr) || quoted || slashed))
		{
			if(slashed==1)
			{
				ptr++;
				slashed=0;
				continue;
			}
			if(*ptr=='\\')
			{
				slashed=1;
				shuffle_up(ptr);
				continue;
			}
			if(*ptr=='"')
			{
				quoted=1-quoted;
				shuffle_up(ptr);
				continue;
			}
			ptr++;
		}
		if(*ptr)
			*ptr++=0;
	}
	argv[n]=NULL;
	return n;
}

int BoolOpt(const char *n)
{
	if(strlen(n)==1)
	{
		if(*n=='Y' || *n=='y' || *n=='1')
			return 1;
		if(*n=='n' || *n=='N' || *n=='0')
			return 0;
	}
	if(strcasecmp(n,"on")==0)
		return 1;
	if(strcasecmp(n,"yes")==0)
		return 1;
	if(strcasecmp(n,"off")==0)
		return 0;
	if(strcasecmp(n,"no")==0)
		return 0;
	return -1;
}

const char *skip_first_word(const char *args)
{
	const char *ptr, *ptr2;

	ptr = args;
	while (ptr && *ptr && isspace(*ptr)) ptr++;
	if (ptr && *ptr)
	{
		ptr2 = strchr(ptr, ' ');
		if (ptr2 != NULL) ptr2++;
		return ptr2;
	} else return(NULL);
}

int DoCommand(char *input, CommandList *cm)
{
	CommandList *found=NULL, *exact=NULL, *backup;
	int count=0, ecount=0;
	int c;
	alias *al;
	const char *dowhat;
	char *text;
	char *args = NULL, *ptr, *p2, *tmp;
	int inlen;
	const char *argv[MAX_ARGC];
	int argc;

	/* save head of 'cm' for use in help */
	backup = cm;

	/* crunch spaces off the front */
	text = input;
	while(text && *text && isspace(*text)) text++;

	/* split off command from front to return argument string */
	ptr = text;
	while(ptr && *ptr && !isspace(*ptr)) ptr++;
	if (ptr && *ptr) args=strdup(++ptr);

	/* parse input into arguments */
	argc=ParseLine(text, argv);

	/* No input */
	if(argc==0)
	{
		if (args!=NULL) free(args);
		return 0;
	}

	dowhat=argv[0];

	/* check for aliases */
	al=alias_list;
	while (al!=NULL)
	{
		if (!strcasecmp(al->from,argv[0]))
		{
			dowhat=al->to;
			break;
		}
		al=al->next;
	}

	/* Find command */
	inlen=strlen(dowhat);

	while(cm->Command)
	{
		if ((c=command_compare(dowhat, cm->Command))==inlen && (cm->Rights==0 || ((rights&cm->Rights)==cm->Rights)))
		{
			if (c==strlen(cm->Command))  /* is it exact */
			{
				if (exact==NULL) /* first exact */
				{
					ecount=1;
					exact=cm;
				}else /* another exact (panic) */
					ecount++;
			}else if (cm->Show == 1)/* partial match */
			{
				if (found==NULL)  /* first one */
				{
					found=cm;
					count=1;
				}else
				if (ecount!=1)  /* another, and no exact match */
				{
					if (count==1) /* 2nd one */
					{
						printf("Ambiguous command. Possibilities:-\n");
						printf("%s, %s",found->Command, cm->Command);
					}else /* more */
					printf(", %s",cm->Command);
					count++;
				}
			}
		}
	 	cm++;
	}

	if (ecount>1)
	{
		printf("ERROR: There are more than one commands with that exact name.\n");
		printf("       Please report this error.\n");
		if (args!=NULL) free(args);
		return 0;
	}else
	if (count>1 && ecount==0)
	{
		printf("\nPlease be more specific\n");
		if (args!=NULL) free(args);
		return 0;
	}else
	if (ecount==1)
	{
		if(argc<exact->Args+1)
		{
			printf("%s\n",exact->ArgError);
			if (args!=NULL) free(args);
			return 0;
		}
		/* have exact function name, so we know the arg count */
		/* means we can split off (argc-1) words from front or args */
		for (c=1; c<((int)exact->Args); c++)
		{
			tmp= strdup(args);
			free(args);
			args = NULL;
			ptr = tmp;
			while (ptr && *ptr && isspace(*ptr)) ptr++;
			if (ptr && *ptr)
			{
				p2 = strchr(ptr, ' ');
				p2++;
				args = strdup(p2);
			}
			free(tmp);
		}

		exact->Function(exact,argc,argv, args);
		if (args!=NULL) free(args);
		return 1;
	}else
	if (count==1)
	{
		if(argc<found->Args+1)
		{
			printf("%s\n",found->ArgError);
			if (args!=NULL) free(args);
			return 0;
		}
		/* have exact function name, so we know the arg count */
		/* means we can split off (argc-1) words from front or args */
		for (c=1; c<((int)found->Args); c++)
		{
			tmp= strdup(args);
			free(args);
			args = NULL;
			ptr = tmp;
			while (ptr && *ptr && isspace(*ptr)) ptr++;
			if (ptr && *ptr)
			{
				p2 = strchr(ptr, ' ');
				p2++;
				args = strdup(p2);
			}
			free(tmp);
		}

		found->Function(found,argc,argv,args);
		if (args!=NULL) free(args);
		return 1;
	}
	printf("Command '%s' not found.",dowhat);
	if (backup==chattable)
		printf(" Use "CMD_TALK_STR"help for a list of commands.\n");
	else if (!cm_test(user, CM_ONCHAT))
		printf(" Use help for a list of commands.\n");
	else
		printf(" Use "CMD_BOARD_STR"help for a list of commands.\n");
	if (args!=NULL) free(args);
	return -1; /* Beats me */
}

void InitParser(void)
{
	LoadInitFile(NULL);
}

#define min(a,b)  a<b?a:b

int command_compare(const char *input, const char *comm)
{
	int a=strlen(input);
	int b=strlen(comm);
	int c=0;
	int len=min(a,b);

	if (len==0) return(0);

	while (tolower(input[c])==tolower(comm[c]) && c<len) c++;
	return(c);
}

void help_list(CommandList *c, unsigned int hidestuff, const char * const prefix)
{
	char		buff[10];
	int		count;
	int		screen_height = screen_h();

	/* now go through again and list the lines */
	count=0;
	printf("Available commands:-\n");
	while (c->Command)
	{
		if ((rights & c->Rights) == c->Rights)
		{
			if (!hidestuff)
			{
				printf("%8s%-12s - %s\n", prefix, c->Command, c->Desc);
				count++;
			}
			else if (c->Show == 1)
			{
				printf("%8s%-12s - %s\n", prefix, c->Command, c->Desc);
				count++;
			}
		}
		c++;

		if (count >= (screen_height - 2))
		{
			printf("---more---\r");
			get_str(buff, 5);
			printf("          \r");
			if (*buff=='q' || *buff=='Q') return;
			count=0;
		}
	}
	ShowLinks(alias_list, "Aliased commands :-", "is aliased to", count);
}
