#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>

#include "mwlog.h"

void mwlog(struct user *user, const char *fmt, ...)
{
	char detail[LOGLINESIZE];
	mode_t mask;
	va_list ap;
	time_t t;
	char *ts;
	FILE *fp;

	mask = umask(077);
	fp = fopen(LOGFILE, "a");
	umask(mask);
	if (fp == NULL) {
		perror(LOGFILE);
		return;
	}
	t = time(0);
	ts = asctime(gmtime(&t));
	if (ts != NULL && *ts != '\0')
		ts[strlen(ts) - 1] = '\0';
	va_start(ap, fmt);
	vsnprintf(detail, LOGLINESIZE-1, fmt, ap);
	va_end(ap);
	fprintf(fp, "%s | %*s | %s\n", ts, NAMESIZE, user->record.name, detail);
	fclose(fp);
}
