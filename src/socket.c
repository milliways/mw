#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>

#include "socket.h"
#include "util.h"
#include "ipc.h"

/* create an empty message */
ipc_message_t * ipcmsg_create(uint32_t type, uint32_t src)
{
	ipc_message_t *new = calloc(1, sizeof(ipc_message_t));
	new->head.src = src;
	new->head.type = type;
	new->head.when = time(NULL);
	return new;
}

/* add body text to a message, +1 to make C string safe */
void ipcmsg_append(ipc_message_t **_msg, const void * data, int len)
{
	ipc_message_t *msg = *_msg;
	char *newseg;

	if (msg == NULL)
		return;

	msg = realloc(msg, sizeof(*msg) + msg->bodylen + len + 1);
	newseg = ((char *)(msg + 1)) + msg->bodylen;
	memcpy(newseg, data, len);
	msg->bodylen += len;
	newseg[len] = '\0';
	*_msg = msg;
}

void ipcmsg_summary(const char *intro, ipc_message_t *msg)
{
	if (intro != NULL) printf("%s: ", intro);
	if (msg->head.type < 255)
		printf("src: %X, dst: %X, Type: %s, len=%d, when=%ld\n",
		       msg->head.src, msg->head.dst, ipc_nametype(msg->head.type),
		       msg->head.len, msg->head.when);
	else
		printf("src: %X, dst: %X, Type: '%4.4s', len=%d, when=%ld\n",
		       msg->head.src, msg->head.dst, (char *)&msg->head.type,
		       msg->head.len, msg->head.when);
}

/* set the destination (PID) of a message */
void ipcmsg_destination(ipc_message_t *msg, uint32_t dest)
{
	if (msg == NULL) return;
	msg->head.dst = dest;
}

/* free a message */
void ipcmsg_destroy(ipc_message_t * msg)
{
	if (msg == NULL) return;
	if (msg->refcount > 0) return;

	memset(msg, 0, sizeof(ipc_message_t));
	free(msg);
}

int msg_queue(ipc_message_t *msg, ipc_connection_t *conn)
{
	int wasempty = list_empty(&conn->outq);

	outq_msg_t * new = calloc(1, sizeof(outq_msg_t));
	new->msg = msg;
	new->msg->refcount++;
	list_add_tail(&(new->list), &(conn->outq));

	//ipcmsg_summary("QUEUE", msg);

	return wasempty;
}

/* immediately transmit a message to a socket */
void ipcmsg_send(ipc_message_t *msg, ipc_connection_t *conn)
{
	struct iovec iov[2];
	int iovused = 0;

	if (conn == NULL) {
		//ipcmsg_summary("NO CONN", msg);
		return;
	}
	if (msg == NULL) { msg_queue(msg, conn); return; }
	if (conn->fd == -1) { msg_queue(msg, conn); return; }

	memset(&iov, 0, sizeof(iov));
	iov[0].iov_base = &msg->head;
	iov[0].iov_len = sizeof(msg->head);
	iovused++;
	msg->head.len = msg->bodylen;
	if (msg->bodylen > 0) {
		iov[1].iov_base = (char *)(msg + 1);
		iov[1].iov_len = msg->bodylen;
		iovused++;
	}

	int bytes = 0;
	for (int i=0;i<iovused;i++) bytes += iov[i].iov_len;

	int n = writev(conn->fd, iov, iovused);
	if (n == -1) {
		ipcconn_bad(conn);
	} else
	if (n < bytes) {
		fprintf(stderr, "Short write on ipc socket.\n");
	}
	ipcmsg_destroy(msg);
}

/* mark this socket as bad/gone */
void ipcconn_bad(ipc_connection_t * conn)
{
	conn->state = IPCSTATE_ERROR;
}

/* create an empty connection socket struct */
ipc_connection_t * ipcconn_create(void)
{
	ipc_connection_t * conn = calloc(1, sizeof(ipc_connection_t));
	conn->fd = -1;
	conn->state = IPCSTATE_ERROR;
	INIT_LIST_HEAD(&(conn->outq));
	return conn;
}

/* open a tcp socket, returning the fd */
int ipcconn_connect(const char *host, const char *port)
{
	struct addrinfo hint;
	struct addrinfo *list;
	int fd;

	memset(&hint, 0, sizeof(hint));
	hint.ai_family = AF_INET;
	hint.ai_socktype = SOCK_STREAM;
	hint.ai_protocol = IPPROTO_TCP;
	hint.ai_flags = 0;

	int err = 0;
	if ((err=getaddrinfo(host, port, &hint, &list))) {
		fprintf(stderr, "Lookup failed for %s:%s %s", host, port, gai_strerror(err));
		return -1;
	}

	struct addrinfo *res = NULL;
	for (res = list; res != NULL; res = res->ai_next) {
		fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
		if (fd == -1) continue;

		int i = 0;
		setsockopt(fd, SOL_SOCKET, SO_TIMESTAMP, &i, sizeof(i));

		if (connect(fd, res->ai_addr, res->ai_addrlen) == 0) {
			break;
		}
		close(fd);
	}
	if (list) freeaddrinfo(list);
	if (res == NULL) {
		fprintf(stderr, "Failed to connect to %s:%s\n", host, port);
		return -1;
	}
	return fd;
}


/**
 * read=1
 * We got an okay to read from this socket/connection
 * make one read and see what we can do with it
 *
 * read=0
 * Try to process the existing buffer for more messages
 * return NULL if we dont have a complete one
 */
ipc_message_t * read_socket(ipc_connection_t * conn, int doread)
{
	if (doread && conn->state != IPCSTATE_ERROR && conn->fd != -1) {
		int remain = sizeof(conn->p_buffer) - conn->i_buffer;
		int ret = read(conn->fd, &conn->p_buffer[conn->i_buffer], remain);
		if (ret <= 0) {
			fprintf(stderr, "fd=%d bad read ret=%d %s\n", conn->fd, ret, strerror(errno));
			ipcconn_bad(conn);
			return NULL;
		}
		conn->i_buffer += ret;
	}

	int used = 0;

	if (conn->incoming == NULL) {
	       if (conn->i_buffer < sizeof(ipc_msghead_t)) return NULL;
	       conn->incoming = ipcmsg_create(0, 0);
	       memcpy(&conn->incoming->head, conn->p_buffer, sizeof(ipc_msghead_t));
	       used += sizeof(ipc_msghead_t);

	       if (conn->incoming->head.len > 8192){
		       fprintf(stderr, "fd=%d Stupidly long message %d bytes.\n", conn->fd, conn->incoming->head.len);
		       ipcmsg_destroy(conn->incoming);
		       conn->incoming = NULL;
		       ipcconn_bad(conn);
		       return NULL;
	       }
	}

	if (conn->incoming->bodylen < conn->incoming->head.len) {
		int wanted = conn->incoming->head.len - conn->incoming->bodylen;
		int available = conn->i_buffer - used;
		int using = _MIN(wanted, available);
		ipcmsg_append(&conn->incoming, &conn->p_buffer[used], using);
		used += using;
	}

	if (used >= conn->i_buffer) {
		conn->i_buffer = 0;
	} else {
		conn->i_buffer -= used;
		memmove(conn->p_buffer, &conn->p_buffer[used], conn->i_buffer);
	}

	if (conn->incoming->bodylen >= conn->incoming->head.len) {
		ipc_message_t * msg = conn->incoming;
		conn->incoming = NULL;
		return msg;
	}

	return NULL;
}


/**** JSON handling functions ***/
/* use http://www.digip.org/jansson/doc/2.3/  aka libjansson */

/** read json object out of a message */
json_t * ipcmsg_json_decode(ipc_message_t *msg)
{
	if (msg == NULL) return NULL;
       	if (msg->bodylen == 0) return NULL;
	json_error_t err;
	json_t * js = json_loads((char *)(msg + 1), 0, &err);

	if (js == NULL) {
		fprintf(stderr, "JSON error at byte %d: %s\n", err.position, err.text);
		return NULL;
	}

	return js;
}

/** write json object into a message */
int ipcmsg_json_encode(ipc_message_t **_msg, json_t *js)
{
	ipc_message_t *msg = *_msg;

	if (msg == NULL) return -1;
	if (js == NULL) return -1;

	char * newtext = json_dumps(js, 0);
	if (newtext == NULL) {
		fprintf(stderr, "JSON error on encode\n");
		return -1;
	}
	msg->bodylen = strlen(newtext);
	msg = realloc(msg, sizeof(*msg) + msg->bodylen + 1);
	memcpy(msg + 1, newtext, msg->bodylen + 1);
	*_msg = msg;
	return 0;
}

/* always give as a workable top level json object */
json_t * json_init(ipc_message_t * msg)
{
	json_t * js = NULL;
	if (msg != NULL) {
		js = ipcmsg_json_decode(msg);
	}
	if (js == NULL) js = json_object();
	return js;
}


/** add/replace a string value in the base object */

int json_vaddstring(json_t * js, const char * key, const char * value, ...) __attribute__((format (printf, 3, 4)));

int json_vaddstring(json_t * js, const char * key, const char * value, ...)
{
	va_list va;
	va_start(va, value);
	_autofree char *text = NULL;
	vasprintf(&text, value, va);
	va_end(va);

	return json_addstring(js, key, text);
}

int json_addstring(json_t * js, const char * key, const char * value)
{
	json_t * tmp;

	tmp = json_object_get(js, key);
	if (tmp == NULL) {
		tmp = json_string(value);
		if (tmp == NULL) return -1;
		json_object_set_new(js, key, tmp);
	} else {
		if (!json_is_string(tmp)) return -1;
		json_string_set(tmp, value);
	}
	return 0;
}

int json_addint(json_t * js, const char * key, int value)
{
	json_t * tmp;

	tmp = json_object_get(js, key);
	if (tmp == NULL) {
		tmp = json_integer(value);
		if (tmp == NULL) return -1;
		json_object_set_new(js, key, tmp);
	} else {
		if (!json_is_integer(tmp)) return -1;
		json_integer_set(tmp, value);
	}
	return 0;
}

int json_addobject(json_t * js, const char * key, json_t * obj)
{
	if (obj == NULL) return -1;
	if (!json_is_object(js)) return -1;
	if (!json_is_object(obj)) return -1;
	json_object_set(js, key, obj);
	return 0;
}

const char * json_getstring(json_t * js, const char * key)
{
	json_t * tmp;
	tmp = json_object_get(js, key);
	if (tmp == NULL) return NULL;
	if (!json_is_string(tmp)) return NULL;
	return json_string_value(tmp);
}

int json_getint(json_t * js, const char * key)
{
	json_t * tmp;
	tmp = json_object_get(js, key);
	if (tmp == NULL) return 0;
	if (!json_is_integer(tmp)) return 0;
	return json_integer_value(tmp);
}
