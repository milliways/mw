#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include "gags.h"
#include "bb.h"
#include "str_util.h"
#include "gagtable.h"
#include "user.h"
#include "talker_privs.h"
#include "gags.h"

void gag_normal(char *text);
void gag_chef(char *text);
void gag_kraut(char *text);
void gag_fudd(char *text);
void gag_jive(char *text);
void gag_mirror(char *text);
void gag_latin(char *text);
void gag_censor(char *text);
void gag_wibble(char *text);
void gag_swab(char *text);
void gag_strfry(char *text);
void gag_furby(char *text);
void gag_warez(char *text);
void gag_bunny(char *text);
void gag_duck(char *text);
void gag_morse(char *text);
void gag_french(char *text);
void gag_babel(char *text);
void gag_nosport(char *text);
void gag_ack(char *text);
static char *apply_bork(char *text, const gag_pattern_t *list, int caps);

/****
 * List ALL the gags
 ****/

GagInfo gaglist[]={
{"gag","Gag", "Ungag",
   "You have just been gagged!",
   "You have been ungagged. You can now talk normally",
   gag_normal},
{"chef","EnCheferise", "UnCheferise",
   "You have just been EnCheferised! Bork Bork Bork!",
   "You have been UnCheferised. You can now talk normally",
   gag_chef},
{"kraut","Kraut", "UnKraut",
   "You have just been Kraut'ed! Seig Heil!",
   "You have been UnKraut'ed. You can now talk normally",
   gag_kraut},
{"fudd","Fudd", "UnFudd",
   "You have just been Fudd'ed. Go hunt some Wabbits!",
   "You have been UnFudd'ed. You can now talk normally",
   gag_fudd},
{"jive","Jive", "UnJive",
   "You have just been Jive'd. Honky!",
   "You have been Un-Jive'd. You can now talk normally",
   gag_jive},
{"mirror","Mirror", "UnMirror",
   "You have just been Mirrored. Welcome to the looking glass Alice",
   "You have been Unmirrored. You can now talk normally",
   gag_mirror},
{"latin","Latinise", "DeLatinate",
   "You have just been whisked off to Rome. You now speak Pig Latin",
   "You have been DeLatinised. You can now talk normally",
   gag_latin},
{"censor","Censor", "UnCensor",
   "You have just been censored",
   "Censoring removed. You can now talk normally",
   gag_censor},
{"wibble","Wibble", "DeWibblicate",
   "You have just been Wibbled!",
   "You feel De-Wibbled. You can now talk normally",
   gag_wibble},
{"swab","Swab", "UnSwab",
   "You have just been Swabbed!",
   "You have been Un-Swabbed. You can now talk normally",
   gag_swab},
{"strfry","Strfry", "DeStrfry",
   "You have just been Strfry'd. Your text will now go manky :)",
   "You have been DeStrfry'd. You can now talk normally",
   gag_strfry},
{"furby","Furby", "UnFurby",
   "You have just been Furby'd. There are cries of 'ahh, how cute!' and 'someone shut that thing up!' :)",
   "You have been Un-Furby'd. You're not cute and annoying anymore! :(",
   gag_furby},
{"warez","Warez", "UnWarez",
   "You have just been turned into a warez kiddie! S3nd m3 w/\\R3z d00dZ!",
   "You have been Un-Warez'd. You no longer talk like a prat!",
   gag_warez},
{"duck","Duck", "De-Duck",
   "You have just switched to duck-mode",
   "You are no longer quackers!",
   gag_duck},
{"bunny","Bunny", "UnBunny",
   "You have just switched to bunny-mode",
   "You have stopped being a bunny",
   gag_bunny},
{"morse","Morse", "Un-Morse",
   "You have just been taught Morse Code. Pity you forgot the other languages you knew...",
   "You just remembered know to communicate properly! Now stop talking Morse!",
   gag_morse},
{"french","Frenchify", "DeFrenchify",
   "You now speck Onglish leek a fronchman...",
   "Your English improves again...",
   gag_french},
{"babel","Babelfish", "DeBabelise",
   "You have been Babel-Fished.  You smell yourselves suddenly badly interpreted",
   "You have been de-babelised.  You can now talk normally",
   gag_babel},
{"nosport","Anti-Sport","Re-Sport",
   "Be a good sport, sport, and dont mention sport okay?",
   "You have been granted the power to mention sport again",
   gag_nosport},
{"martian","Martian","De-Martian",
   "You have been zapped by a ray-gun and turned into a martian",
   "You are human again",
   gag_ack},
{NULL, NULL, NULL, NULL, NULL, NULL},
};

/**********************
 * Main route.
 * call the relevant gag ROUTINES
 *
 * return the new version
 **********************/

static int gag_count = -1;
char * apply_gag(struct user *speaker, const char *text)
{
	GagInfo *gi = gaglist;

	/* this never changes at runtime,
	 * count how many for fast lookups
	 */
	if (gag_count == -1) {
		gag_count = 0;
		for (gi=gaglist; gi->name != NULL; gi++)
			gag_count++;
	}

	int gag_num = (speaker->record.chatmode & CM_GAGMASK) >> CM_GAGSHIFT;

	/* not gagged, skip it */
	if (gag_num == 0) return NULL;

	/* we dont have that gag, skip it */
	if (gag_num > gag_count) return NULL;

	/* bit of a kludge, these all work on inplace strings */
	char * newtext = malloc(MAXTEXTLENGTH+1);
	strcpy(newtext, text);
	gaglist[gag_num-1].Function(newtext);
	return newtext;
}


/****************************************************************
  GAG SUPPORT ROUTINES - USEFUL THINGIES
****************************************************************/

/* given two strings of the same length, will copy colour codes from one to the other */
static char *merge_colours(char *in, char *old)
{
	char *new=NULL;
	int n, i;

	new = strdup(old);
	i=0;
	n=0;
	while(n<strlen(new))
	{
		if (new[n]==033)
			n+=3;
		else
			new[n++]=in[i++];
	}
	return(new);
}

/* replaces every word in a string with another string */
static void gag_wordrep(char *text, const char *rep)
{
	int wordcount=1;
	char *temp=text;
	char *temp2=text;

	while ((temp=strchr(temp,' ')) != NULL) {
		if (temp2+1 <= temp) {
			wordcount++;
		};
		temp++;
		temp2 = temp;
	};
	text[0] = 0;
	for (;wordcount>0;wordcount--) {
		strncat(text,rep,MAXTEXTLENGTH-strlen(rep)-1);
		text[MAXTEXTLENGTH-1] = 0;
	};
}

/****************************************************************
  ACTUAL GAG ROUTINES - PLEASE PLACE IN CORRECT ORDER!
****************************************************************/

/* normal Mmmm gag filter */
void gag_normal(char *text)
{
	int i=0, len;

	len=strlen(text);
	while (i<len)
	{
		if (text[i]==033) { i+=3; continue; }
		if (isupper(text[i]))
			text[i]='M';
		else
		if (islower(text[i]))
			text[i]='m';
		i++;
	}
}

/* encheferisation */
void gag_chef(char *text)
{
	snprintf(text, MAXTEXTLENGTH-1, "%s", apply_bork(text, chef, 0));
}

/* kraut */
void gag_kraut(char *text)
{
	snprintf(text, MAXTEXTLENGTH-1, "%s", apply_bork(text, kraut, 0));
}

/* fudd */
void gag_fudd(char *text)
{
	snprintf(text, MAXTEXTLENGTH-1, "%s", apply_bork(text, fudd, 0));
}

/* jive */
void gag_jive(char *text)
{
	snprintf(text, MAXTEXTLENGTH-1, "%s", apply_bork(text, jive, 0));
}

/* mirror image */
void gag_mirror(char *text)
{
	char *old;
	int i,len;
	char last_clr[2];

	old=strdup(text);
	len=strlen(text)-1;
	i=0;

	last_clr[0] = '-';
	last_clr[1] = '-';

	while (i<=len)
	{
		if (old[i]==033)
		{
			text[len-(i+2)]=old[i];
			text[len-(i+1)]=last_clr[0];
			text[len-(i)]=last_clr[1];
			last_clr[0] = old[i+1];
			last_clr[1] = old[i+2];
			i+=2;
		}else
			text[len-i]=old[i];
		i++;
	}
	free(old);
	old=strdup(text);
	snprintf(text, MAXTEXTLENGTH-1, "\033%c%c%s", last_clr[0], last_clr[1], old);
	free(old);
}

/* pig latin */
void gag_latin(char *text)
{
	char	*new, *c;
	char	buff[128];

	new=strdup(text);

	text[0]=0;
	c=strtok(new, " \t\n\r");
	while (c!=NULL)
	{
		if (strchr("aiouy",tolower(c[0]))!=NULL)
			snprintf(buff,127,"%s%cey ", &c[1], c[0]);
		else if (isalpha(c[0]))
			snprintf(buff,127,"%s%cay ", &c[1], c[0]);
		else
			snprintf(buff,127,"%s ", c);
		strncat(text, buff, MAXTEXTLENGTH - strlen(text) - 1);
		c=strtok(NULL, " \t\n\r");
	}
	free(new);
}

/* censor filter */
void gag_censor(char *text)
{
	snprintf(text, MAXTEXTLENGTH-1, "%s", apply_bork(text, censor, 0));
}

/* screw up the sentences with wibbles */
void gag_wibble(char *text)
{
	snprintf(text, MAXTEXTLENGTH-1, "%s", apply_bork(text, wibble, 0));
}

/* swab filter */
void gag_swab(char *text)
{
	char *new;
	char *old = strdup(text);
	char swb[MAXTEXTLENGTH];
	int i;
	int len;

	new = strip_colours(text);

	/* convert new to swb */
	i = 0;
	len = strlen(new);
	while(i<len-1)
	{
		swb[i]=new[i+1];
		swb[i+1]=new[i];
		i+=2;
	}
	if ((len % 2) == 1) swb[len-1]=new[len-1];
	swb[len]=0;

	free(new);
	new = merge_colours(swb, old);
	snprintf(text, MAXTEXTLENGTH-1, "%s", new);

	free(new);
	free(old);
}

/* strfry */
void gag_strfry(char *text)
{
#ifndef STRFRY_MISSING
	char *new;
	char *old = strdup(text);
	char str[MAXTEXTLENGTH];

	new = strip_colours(text);
	snprintf(str, MAXTEXTLENGTH-1, "%s", strfry(new));
	free(new);

	new = merge_colours(str, old);
	snprintf(text, MAXTEXTLENGTH-1, "%s", new);

	free(new);
	free(old);
#endif
}

const char *gag_type(int type)
{
	GagInfo *gi = gag_find(type);

	if (gi != NULL) return(gi->text);
	return("Unknown");
}

GagInfo * gag_find(int num)
{
	GagInfo *gi = gaglist;
	int count = 0;

	while(gi->name)
	{
		count++;
		if (count == num) return(gi);
		gi++;
	}
	return NULL;
}

const char *gag_gag_msg(int type)
{
	GagInfo *gi = gag_find(type);

	return gi ? gi->gag : "Unknown";
}

const char *gag_ungag_msg(int type)
{
	GagInfo *gi = gag_find(type);

	return gi ? gi->ungag : "Unknown";
}

int gag_code(const char *str)
{
	GagInfo *gi = gaglist;
	int count = 0;

	while(gi->name)
	{
		count++;
		if (!strcasecmp(str, gi->name)) return(count);
		gi++;
	}
	return(0);
}

/***************************************************************
 * the gag filters
 */

/* make you talk like a furby */
void gag_furby(char *text)
{
	snprintf(text, MAXTEXTLENGTH-1, "%s", apply_bork(text, furby, 0));
}

/* WaReZ KiDDie filter */
void gag_warez(char *text)
{
	int i;

	for(i=0;i<strlen(text);i++)
	{
		if (text[i] == 'a') text[i]=1;
	}

	snprintf(text, MAXTEXTLENGTH-1, "%s", apply_bork(text, warez, 0));

	for(i=0;i<strlen(text);i++)
	{
		if (text[i] == 1) text[i]='a';
		if (get_rand(1,25)<10) text[i] = toupper(text[i]);
		else if (get_rand(1,25)<10) text[i] = tolower(text[i]);
	}
}

/* turn all words into 'boing' */
void gag_bunny(char *text)
{
	gag_wordrep(text,"boing");
}

/* turn all words into 'quack' */
void gag_duck(char *text)
{
	gag_wordrep(text,"quack");
}

static const char *lookup_morse(const char c)
{
	switch(toupper(c))
	{
	case 'A': return(".-"); break;
	case 'B': return("-..."); break;
	case 'C': return("-.-."); break;
	case 'D': return("-.."); break;
	case 'E': return("."); break;
	case 'F': return("..-."); break;
	case 'G': return("--."); break;
	case 'H': return("...."); break;
	case 'I': return(".."); break;
	case 'J': return(".---"); break;
	case 'K': return("-.-"); break;
	case 'L': return(".-.."); break;
	case 'M': return("--"); break;
	case 'N': return("-."); break;
	case 'O': return("---"); break;
	case 'P': return(".--."); break;
	case 'Q': return("--.-"); break;
	case 'R': return(".-."); break;
	case 'S': return("..."); break;
	case 'T': return("-"); break;
	case 'U': return("..-"); break;
	case 'V': return("...-"); break;
	case 'W': return(".--"); break;
	case 'X': return("-..-"); break;
	case 'Y': return("-.--"); break;
	case 'Z': return("--.."); break;
	case '0': return("-----"); break;
	case '1': return(".----"); break;
	case '2': return("..---"); break;
	case '3': return("...--"); break;
	case '4': return("....-"); break;
	case '5': return("....."); break;
	case '6': return("-...."); break;
	case '7': return("--..."); break;
	case '8': return("---.."); break;
	case '9': return("----."); break;
	case ',': return("--..--"); break;
	case '.': return(".-.-.-"); break;
	case '?': return("..--.."); break;
	case '/': return("-..-."); break;
	case '-': return("-....-"); break;
	case '\'': return(".----."); break;
	case '(': return("-.--.-"); break;
	case '_': return("..--.-"); break;
	case ':': return("---..."); break;
	case ';': return("-.-.-."); break;
	case ' ': return("/"); break;
	default: break;
	}

	return("......");
}

static char *apply_morse(char *in)
{
	int i;
	char *out = NULL;

	string_add(&out, "-.-.- ");
	for (i=0; i<strlen(in); i++)
	{
		if (i>0) string_add(&out, " ");
		string_add(&out, "%s", lookup_morse(in[i]));
	}
	string_add(&out, " .-.-.");

	return(out);
}

/* speak morse code */
void gag_morse(char *text)
{
	char *out = apply_morse(text);
	snprintf(text, MAXTEXTLENGTH-1, "%s", out);
	free(out);
}

/* 'allo 'allo 'french' gag */
void gag_french(char *text)
{
	snprintf(text, MAXTEXTLENGTH-1, "%s", apply_bork(text, french, 0));
}

/* babelfish */
void gag_babel(char *text)
{
	snprintf(text, MAXTEXTLENGTH-1, "%s", apply_bork(text, babelfish, 0));
}

/* anti-sport */
void gag_nosport(char *text)
{
	snprintf(text, MAXTEXTLENGTH-1, "%s", apply_bork(text, nosport, 0));
}

#define MAX_WORDS 30

/* Mars Attacks! */
void gag_ack(char *text)
{
    if(strlen(text))
    {
        char *words[MAX_WORDS];
        memset(words, 0, MAX_WORDS * sizeof(char *));

        /* Tokenise string. */
        {
            char *p = text;
            char *tok = NULL;
            int i = 0;

            do
            {
                tok = strsep(&p, " \t");

                if(p && *p)
                {
                    words[i] = tok;
                    i++;
                }
            } while((i < MAX_WORDS) && (tok));
        }

        /* Convert to martian. */
        {
            char tmp_text[MAXTEXTLENGTH];
            int i = 0;
            tmp_text[0] = 0;
            int first = 1;

            while((i < MAX_WORDS) && words[i])
            {
                char tmp_buf[MAXTEXTLENGTH]; /*No good reason to be this big.*/

                if((*words[i]))
                {
                    int word_len;
                    int punctuation;
                    int shouting;

                    punctuation = shouting = 0;

                    word_len = strlen(words[i]);
                    word_len = (word_len / 4) + 1; /* cheap syllable count */

                    if(word_len > (MAXTEXTLENGTH / 6))
                    {
                        word_len = MAXTEXTLENGTH / 6;
                    }

                    if(strrchr(words[i], '.') ||
                       strrchr(words[i], ',') ||
                       strrchr(words[i], '!'))
                    {
                        punctuation = 1;
                        word_len++;
                    }

                    if(strrchr(words[i], '!') ||
                       (!first && isupper(*(words[i]))))
                    {
                        shouting = 1;
                    }

                    tmp_buf[0] = 0;

                    while(word_len)
                    {
                        if(shouting)
                        {
                            strcat(tmp_buf, "ACK!");
                        }
                        else
                        {
                            if(first)
                            {
                                strcat(tmp_buf, "Ack");
                                first = 0;
                            }
                            else
                            {
                                strcat(tmp_buf, "ack");
                            }
                        }
                        word_len--;
                    }
                    strcat(tmp_buf, "! ");

                    if(punctuation)
                    {
                        first = 1;
                    }

                    /* Crap range check.  Rewrite! */
                    if((strlen(tmp_text) + strlen(tmp_buf)) >= MAXTEXTLENGTH)
                    {
                        break;
                    }
                    else
                    {
                        strcat(tmp_text, tmp_buf);
                    }
                }
                i++;
            }
            strcpy(text, tmp_text);
        }
    }
}

/**
 * old style lookup table gag filters
 **/
static char *apply_bork(char *text, const gag_pattern_t *list, int caps)
{
	static char buff[MAXTEXTLENGTH];
	int tp=0, bp=0;
	int len;
	const gag_pattern_t *p;
	int rule;

	if (text==NULL) return(NULL);
	len=strlen(text);

#ifdef DEBUG
	p=list;
	rule=0;
	while (p->from!=NULL)
	{
		rule++;
		p++;
	}
	printf("This ruleset contains %d rules.\n",rule);
#endif

	while (tp<len && bp<MAXTEXTLENGTH-2)
	{
		if (text[tp]==033)
		{
			/* skip colour encoding sequences */
			/*tp+=3;*/
			buff[bp++]=text[tp++];
			buff[bp++]=text[tp++];
			buff[bp++]=text[tp++];
			continue;
		}
		p=list;
		rule=0;
		while (p->from !=NULL)
		{
			int i=0, j=0, l=strlen(p->from);
			/*if (tp+l > len) {p++; continue;} */
#ifdef DEBUG
printf("Trying rule %d 0x%p '%s' at posn %d  (%c): ",rule,p,p->from,tp,text[tp]);
#endif

			while (i<l)
			{
				if (p->from[i]=='%')
				{
					char c;

					if (i==0 && tp!=0) c=text[tp+j-1];
					else
						c=text[tp+j];

					if (tp+i==0 || !isalpha(c))
					{
#ifdef DEBUG
						printf("Failed any-char (%c)\n",c);
#endif
						break;
					}
#ifdef DEBUG
					else
						printf("%% match i=%d tp=%d j=%d c=%c ",i,tp,j,c);
#endif
				}else
				if (p->from[i]=='^')
				{
					if (tp+j!=0 && !isspace(text[tp+j-1])
						&& !ispunct(text[tp+j-1]))
					{
#ifdef DEBUG
						printf("Failed ^\n");
#endif
						break;
					}
				}else
				if (p->from[i]=='$')
				{
					 if (tp+j<len &&
						!isspace(text[tp+j]) &&
						!ispunct(text[tp+j]))
					{
#ifdef DEBUG
						printf("Failed $\n");
#endif
						break;
					}
				}else
				if (tolower(p->from[i]) != tolower(text[tp+j]))
				{
#ifdef DEBUG
					printf (" %c != %c\n",p->from[i],text[tp+j]);
#endif
					break;
				}else
					j++;
				i++;
			}
			if (i==l)
			{
/*				if (p->from[0]=='^' && tp!=0)
					buff[bp++]=' ';
*/

				/* dont let it overflow */
				if (bp+strlen(p->to) > MAXTEXTLENGTH-2)
				{
#ifdef DEBUG
					printf("Too Long\n");
#endif
					tp=len;
					break;
				}

				if (j==strlen(p->to))
				{ /* same length, copy case */
#ifdef DEBUG
	printf("Keep case. j=%d\n",j);
#endif
					for (i=0;i<strlen(p->to);i++)
					{
						if (isupper(text[tp+i]))
							buff[bp++]=toupper(p->to[i]);
						else
							buff[bp++]=tolower(p->to[i]);
					}
				}else
				{
					int cas=0;
					for (i=0;i<j;i++)
					{ if (isupper(text[tp+i])) cas++; }
					if (cas==j) /* all upper ? */
						cas=1;
					else cas=0;

#ifdef DEBUG
	printf("Ignore case. j=%d len=%d\n",j,strlen(p->to));
#endif
					for (i=0;i<strlen(p->to);i++)
						if (cas)
							buff[bp++]=toupper(p->to[i]);
						else
							buff[bp++]=p->to[i];
				}
				tp+=j;
#ifdef DEBUG
printf("Matched.\n");
#endif
				break;
			}
			p++;
			rule++;
		}
		if (p->from==NULL)
		{
			buff[bp++]=text[tp++];
#ifdef DEBUG
printf("No Match after %d rules, Just copy, New: tp=%d bp=%d\n",rule,tp,bp);
#endif
		}
	}
	buff[bp]=0;

	if (caps == 1)
	{
		/* all upper */
		int i;
		for (i=0;i<bp;i++)
		{
			buff[i]=toupper(buff[i]);
		}
	} else if (caps == -1)
	{
		/* all lower */
		int i;
		for (i=0;i<bp;i++)
		{
			buff[i]=tolower(buff[i]);
		}
	}

	return(buff);
}
