#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <netinet/in.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#include <socket.h>
#include <nonce.h>
#include <util.h>
#include <rooms.h>
#include <talker_privs.h>
#include <ipc.h>
#include <perms.h>
#include <special.h>

#include "servsock.h"
#include "replay.h"

#define REPLAY_DIR STATEDIR "/replay"

#define _STR(x) #x
#define STR(x) _STR(x)
#define STORE_SIZE 1000
#define STORE_SIZE_STR STR(STORE_SIZE)
#define STORE_SIZE_LEN (sizeof(STORE_SIZE_STR) - 1)
#define STORE_FILE_NAME_LEN (sizeof(REPLAY_DIR) + STORE_SIZE_LEN + 1)

static uint64_t serial = 0;

static ipc_message_t ** store = NULL;
static int store_next = 0;
static int store_len = 0;

static int store_wrap(int index)
{
	while (index < 0) index += STORE_SIZE;
	while (index >= STORE_SIZE) index -= STORE_SIZE;
	return index;
}

static int replay_fds[STORE_SIZE];

void replay_commit(void)
{
	/* If a user pastes a novel into the talker we don't want to be stuck
	   doing i/o for a long time afterwards, so a rate limit is needed. */
#define COMMIT_RATELIMIT (10)
	static unsigned i = 0;
	unsigned closed = 0;

	for (; i < STORE_SIZE && closed < COMMIT_RATELIMIT; i++) {
		int fd = replay_fds[i];
		if (fd >= 0) {
			replay_fds[i] = -1;
			fsync(fd);
			close(fd);
			closed++;
		}
	}
	if (i == STORE_SIZE)
		i = 0;
}

static void defer_close(unsigned n, int fd)
{
	replay_fds[n] = fd;
}

static int write_message(ipc_message_t *msg, unsigned n)
{
	char pathname[STORE_FILE_NAME_LEN];
	int fd = replay_fds[n];
	struct iovec iov[2];

	sprintf(pathname, REPLAY_DIR "/%0*u", (int)STORE_SIZE_LEN, n);
	if (fd < 0)
		fd = open(pathname, O_RDWR|O_CLOEXEC|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
	if (fd < 0) {
		perror(pathname);
		return 1;
	}
	iov[0] = (struct iovec) {
		.iov_base = msg,
		.iov_len = sizeof(*msg)
	};
	iov[1] = (struct iovec) {
		.iov_base = (char *)(msg + 1),
		.iov_len = msg->bodylen
	};
	if (pwritev(fd, iov, 2, 0) != (iov[0].iov_len + iov[1].iov_len))
		perror(pathname);
	defer_close(n, fd);
	return 0;
}

static int msg_storeable(enum ipc_types type)
{
	switch (type) {
		case IPC_NOOP:
		case IPC_NEWMAIL:
		case IPC_STATUS:
		case IPC_GROUPS:
		case IPC_REALNAME:
		case IPC_CONTACT:
		case IPC_LASTREAD:
		case IPC_SPECIAL:
		case IPC_TIMEOUT:
		case IPC_USERNAME:
		case IPC_CHATPRIVS:
		case IPC_CHATMODE:
		case IPC_KICK:
		case IPC_PASSWD:
		case IPC_CHANNEL:
		case IPC_GAG:
		case IPC_GAGLIST:
		case IPC_SCRIPTIPC:
		case IPC_SCRIPTRPC:
		case IPC_PROTLEVEL:
		case IPC_PROTPOWER:
		case IPC_DOING:
			return 0;
		case IPC_TEXT:
		case IPC_WIZ:
		case IPC_CHECKONOFF:
		case IPC_ERROR:
		case IPC_HELLO:
		case IPC_UPTIME:
		case IPC_SAYTOROOM:
		case IPC_SAYTOUSER:
		case IPC_SAYTOALL:
		case IPC_TALKERROR:
		case IPC_REPLAY:
		case IPC_EVENT:
		case IPC_ACTION:
		case IPC_WHOLIST:
			return 1;
		default:
			return 0;
	}
}

/* store the message for later replay */
void store_message(ipc_message_t *msg)
{
	/* only store info/message, not actions */
	if (!msg_storeable(msg->head.type))
		return;

	if (store_len >= STORE_SIZE) {
		/* store is full, discard oldest,
		 * it will have wrapped, so store_next is the last one */
		ipcmsg_destroy(store[store_next]);
		store_len--;
		store[store_next] = NULL;
	}

	write_message(msg, store_next);
	/* add to ref count so it wont get cleaned away yet
	 * insert it at the current location and bump pointers
	 */
	msg->refcount++;
	store[store_next] = msg;
	store_len++;
	store_next = store_wrap( store_next + 1 );
}

/* assign a unique serial number to each message
 * thus giving a definitive replay ordering
 */
void assign_serial( ipc_message_t *msg )
{
	msg->head.serial = serial++;
}

void replay(ipc_connection_t *conn, ipc_message_t *msg)
{
	/* unpack the command */
	json_t *cmd = json_init(msg);

	/* find a pointer to the start/oldest item in store */
	int oldest = store_wrap( store_next - store_len );
	int len = store_len;

	/* which type did they say */
	if (json_object_get(cmd, "serial")!=NULL) {
		/* everything after serial # */
		uint64_t want = json_getint(cmd, "serial");
		int skip = 0;
		for (int i=0;i<len;i++) {
			int idx = store_wrap(oldest+i);
			if ( store[idx] == NULL) continue;
			if ( store[idx]->head.serial >= want)
				break;
			skip++;
		}
		oldest = store_wrap( oldest + skip );
		len -= skip;
		/* if it fails, you get everything
		 * as it maybe got reset whilst you were away */
	}else
	if (json_object_get(cmd, "since")!=NULL) {
		/* everything after {unixtime} */
		int64_t want = json_getint(cmd, "since");
		while (len > 0) {
			/* list will be in date order */
			if (store[oldest]!=NULL &&
				store[oldest]->head.when >= want) break;
			len--;
			oldest = store_wrap(oldest + 1);
		}
		/* if it fails you get nothing as there is
		 * nothing newer (larger) than the date you gave */
	}else
	if (json_object_get(cmd, "count")!=NULL) {
		int want = json_getint(cmd, "count");
		if (want > store_len) want = store_len;
		oldest = store_wrap( store_next - want );
		len = want;
	} else {
		json_decref(cmd);
		send_error(conn, msg, "Invalid replay command");
		return;
	}
	json_decref(cmd);

	/* who are we doing this for */
	struct user user;
	if (fetch_user(&user, conn->user) != 0) {
		send_error(conn, msg, "Error cannot find your user record");
		return;
	}
	RoomInit(&user.room);
	LoadRoom(&user.room, user.record.room);

	/* now, go and replay those messages that are appropriate */
	for (int i=0; i<len; i++) {
		int idx = store_wrap( oldest + i );
		if (store[idx] == NULL) continue;

		store[idx]->flags |= IPCMSG_FL_REPLAY;
		/* this will be a subset of what you see in process_msg() */
		if (store[idx]->head.type == IPC_SAYTOROOM) {
			json_t * j = json_init( store[idx] );
			if (j == NULL) continue;

			const char * exclude = json_getstring(j, "exclude");

			if (exclude != NULL && strcasecmp(exclude, user.record.name)==0) {
				/* thats us ! shhh... */
				json_decref(j);
				continue;
			}
			json_decref(j);

			if (user.record.room == store[idx]->head.dst) {
				/* right room, send it */
				msg_attach(store[idx], conn);
			} else
			if (user.room.sproof < 1 && (cm_test(&user, CM_GLOBAL))) {
			/* room not soundproof, and user has global on */
				msg_attach(store[idx], conn);
			}
			/* couldnt have been for us */
			continue;
		}
		if (store[idx]->head.type == IPC_SAYTOUSER) {
			json_t * j = json_init( store[idx] );
			if (j == NULL) continue;

			const char * target = json_getstring(j, "target");

			if (target!=NULL && strcasecmp(target, user.record.name)==0) {
				/* yes, its for us */
				msg_attach(store[idx], conn);
			}
			json_decref(j);
			continue;
		}
		if (store[idx]->head.type == IPC_WIZ) {
			if (s_wizchat(&user)) {
				/* we are a wiz, we see this */
				msg_attach(store[idx], conn);
			}
			continue;
		}

		/* send them everything else */
		if (store[idx]->head.dst)
			msg_attach(store[idx], conn);
	}
	RoomDestroy(&user.room);
	return;
}

int replay_init(void)
{
	uint64_t highest_serial = 0;
	int ret;

	store = calloc(STORE_SIZE, sizeof(ipc_message_t *));
	if (store == NULL) {
		perror("Failed to allocate replay store");
		return 1;
	}
	store_next = 0;
	store_len = 0;

	ret = mkdir(REPLAY_DIR, S_IRWXU|S_IXGRP|S_IXOTH);
	if (ret == -1 && errno != EEXIST) {
		perror(REPLAY_DIR);
		return 1;
	}
	for (unsigned i = 0; i < STORE_SIZE; i++) {
		char pathname[STORE_FILE_NAME_LEN];
		ipc_message_t *msg;
		ipc_message_t mh;
		struct stat st;
		char *body;
		int fd;

		replay_fds[i] = -1;
		sprintf(pathname, REPLAY_DIR "/%0*u", (int)STORE_SIZE_LEN, i);
		fd = open(pathname, O_RDONLY|O_CLOEXEC|O_CREAT, S_IRUSR|S_IWUSR);
		if (fd < 0) {
			perror(pathname);
			return 1;
		}
		if (fstat(fd, &st) != 0) {
			perror(pathname);
			close(fd);
			return 1;
		}
		if (st.st_size == 0) {
			close(fd);
			continue;
		}
		if (pread(fd, &mh, sizeof(mh), 0) != sizeof(mh)) {
			perror("Failed to read message file");
			close(fd);
			return 1;
		}
		msg = malloc(sizeof(*msg) + mh.bodylen + 1);
		if (msg == NULL) {
			perror("Failed to allocate message read buffer");
			close(fd);
			return 1;
		}
		if (pread(fd, msg, sizeof(*msg) + mh.bodylen, 0) != sizeof(*msg) + mh.bodylen) {
			perror("Failed to read message file");
			free(msg);
			close(fd);
			return 1;
		}
		body = (char *)(msg + 1);
		body[msg->bodylen] = '\0';
		if (msg->head.serial > highest_serial) {
			highest_serial = msg->head.serial;
			store_next = store_wrap(i + 1);
		}
		msg->refcount = 1;
		store_len++;
		store[i] = msg;
		close(fd);
	}
	serial = highest_serial + 1;
	return 0;
}
