#include <stddef.h>

#include "gags.h"

/*
   in the FROM pattern:
     ^  matches beginning of a word
     $  matches end of a word (including punctuation)
     %  matches part of a word (i.e. pattern is not at start of word)
     Otherwise its normal case-insensitive substring match

   Case of replacement text matches original text where possible
   i.e. All caps etc.
*/

const gag_pattern_t chef[]={
{ "an",	"un" },
{ "au",	"oo" },
{ "a%", "e" },
{ "en$", "ee" },
{ "%ew", "oo" },
{ "e$", "e-a" },
{ "^e", "I" },
{ "%f", "ff" },
{ "%ir", "ur" },
{ "%i", "ee" },
{ "%ow", "oo" },
{ "^o", "Oo" },
{ "%o", "u" },
{ "^The", "Zee"},
{ "th$", "t"},
{ "%tion", "shun"},
{ "%u", "oo"},
{ "v", "f"},
{ "w", "v"},
{ "!", ", Bork Bork Bork!"},
{ NULL, NULL} };

const gag_pattern_t kraut[]={
{ "ing$", "ingkt"},
{ "^the$", "Ze"},
{ "^with$", "Mitt"},
{ "^is", "iss"},
{ "wr", "w-r-r"},
{ "^r", "R-r-r"},
{ "^yes", "Jawohl"},
{ "^no$", "Nein"},
{ "^No!", "Nein!"},
{ "^Not", "Nicht"},
{ "^Mr.", "Herr"},
{ "^Mrs.", "Frau"},
{ "^Miss$", "Fraulein"},
{ "^of$", "uff"},
{ "^my$", "mein"},
{ "^and$", "undt"},
{ "^one", "ein"},
{ "^is$", "Ist"},
{ "w$", "w"},
{ "^sc", "shc"},
{ "^st", "sht"},
{ "^sh", "sch"},
{ "ch", "ch"},
{ "^c", "k"},
{ "^for$", "fur"},
{ "^have$", "haf"},
{ "j", "ch"},
{ "qu", "qv"},
{ "rd", "rt"},
{ "v", "f"},
{ "^w", "v"},
{ "ward", "verrt"},
{ "wh", "v"},
{ "^th", "z"},
{ "coffee", "kafe"},
{ "thanks", "Danke"},
{ "john", "Johann"},
{ "william", "Wilhelm"},
{ "brad", "Wilhelm"},
{ "gary", "Gerhardt"},
{ "jon", "Hansel"},
{ "!", ", Seig Heil!"},
{ NULL, NULL} };

const gag_pattern_t fudd[]={
{"r","w"},
{"l","w"},
{"qu","qw"},
{"th$","f"},
{"th","d"},
{"n.","n, uh-hah-hah-hah."},
{NULL,NULL}};

const gag_pattern_t jive[]={
{ "file" , "stash" },
{ "send" , "t'row" },
{ "program" , "honky code" },
{ "atlas" , "Isaac" },
{ "unix" , "slow mo-fo" },
{ "UNIX" , "that slow mo-fo" },
{ "takes" , "snatch'd" },
{ "Mexican" , "wet-back" },
{ "Italian" , "greaser" },
{ "take" , "snatch" },
{ "don't" , "duzn't" },
{ "jive" , "JIBE" },
{ "fool" , "honkyfool" },
{ "modem" , "doodad" },
{ "%e the$" , "e da damn" },
{ "%a the$" , "a da damn" },
{ "%t the$" , "t da damn" },
{ "%d the$" , "d da damn" },
{ "^man$", "dude" },
{ "^woman$" , "mama" },
{ "girl" , "goat" },
{ "something" , "sump'n" },
{ "^lie$" , "honky jibe" },
{ "-o-", "-on rebound-" },
{ "-oo-", "-check y'out latah-"},
{ "^Sure$" , "Sho' nuff" },
{ "^get$" , "git" },
{ "will have" , "gots'ta" },
{ "will$" , "gots'ta" },
{ "got to" , "gots'ta" },
{ "I am" , "I's gots'ta be" },
{ "am not" , "aint" },
{ "is not" , "aint" },
{ "are not" , "aint" },
{ "^are your" , "is yo'" },
{ "^are you" , "you is" },
{ "^hat$", "fedora" },
{ "^shoe$", "kicker" },
{ "haven't" , "aint" },
{ "have to" , "gots'ta" },
{ "have" , "gots'" },
{ "^has$" , " gots'ta" },
{ "come over" , "mosey on down" },
{ "^come$", "mosey on down" },
{ "!", ". Right On!" },
{ "buy", "steal" },
{ "^car$", "wheels" },
{ "drive" , "roll" },
{ "^eat$" , "feed da bud" },
{ "^black$", "brother" },
{ "^negro$", "brother" },
{ "white" , "honky" },
{ "^nigger" , "gentleman" },
{ "nice" , "supa' fine" },
{ "person" , "sucka'" },
{ "^thing", "wahtahmellun" },
{ "home" , "plantation" },
{ "name" , "dojigger" },
{ "^path", "alley" },
{ "computer" , "clunker" },
{ "or" , "o'" },
{ "killed" , "wasted" },
{ "president" , "super-dude" },
{ "prime minister" , "super honcho" },
{ "injured" , "hosed" },
{ "government" , "guv'ment" },
{ "knew" , "knowed" },
{ "because" , "a'cuz" },
{ "Your" , "Yo'" },
{ "four" , "foe" },
{ "got" , "gots" },
{ "aren't" , "ain't" },
{ "young" , "yung" },
{ "^You$" , "You's" },
{ "first" , "fust" },
{ "police" , "honky pigs" },
{ "^string" , "chittlin'" },
{ "^read" , "eyeball" },
{ "^write" , "scribble" },
{ "Th" , "d" },
{ "ing$" , "in'" },
{ "^a$" , "some" },
{ "^to$" , "t'" },
{ "%tion" , "shun" },
{ "^almost$" , "mos'" },
{ "^from" , "fum" },
{ "^because$" , "cuz'" },
{ "You're" , "Youse" },
{ "alright" , "coo'" },
{ "okay" , "coo'" },
{ "%er" , "a'" },
{ "known" , "knode" },
{ "want" , "wants'" },
{ "beat" , "whup'" },
{ "exp" , "'sp" },
{ "exs" , "'s" },
{ "^exc" , "'s" },
{ "^ex" , "'es" },
{ "like" , "likes" },
{ "^did$" , "dun did" },
{ "kind of" , "kind'a" },
{ "women" , "honky chicks" },
{ "^men$" , "dudes" },
{ "^mens$" , "dudes" },
{ "^man$" , "dude" },
{ "^woman$" , "honky chick" },
{ "dead" , "wasted" },
{ "good" , "baaaad" },
{ "open$" , "jimmey" },
{ "opened$" , "jimmey'd" },
{ "^very" , "real" },
{ "per" , "puh'" },
{ "pera" , "puh'" },
{ "oar" , "o'" },
{ "^can" , "kin" },
{ "just$" , "plum" },
{ "detroit" , "Mo-town" },
{ "western electric" , "da' cave" },
{ "^believe" , "recon'" },
{ "Indianapolis" , "Nap-town" },
{ "Jack" , "Buckwheat" },
{ "Bob" , "Liva' Lips" },
{ "Phil" , "dat fine soul" },
{ "Mark" , "Amos" },
{ "Robert" , "Leroy" },
{ "Sandy" , "dat fine femahnaine ladee" },
{ "John" , "Raz'tus" },
{ "Paul" , "Fuh'rina" },
{ "Reagan" , "Kingfish" },
{ "David" , "Issac" },
{ "Ronald" , "Rolo" },
{ "Jim" , "Bo-Jangles" },
{ "Mary" , "Snow Flake" },
{ "Larry" , "Remus" },
{ "Joe" , "Massa'" },
{ "Joseph" , "Massa'" },
{ "mohammed" , "liva' lips" },
{ "pontiff" , "wiz'" },
{ "pope" , "wiz'" },
{ "pravda" , "dat commie rag" },
{ "broken" , "bugger'd" },
{ "strange" , "funky" },
{ "dance" , "boogy" },
{ "^house" , "crib" },
{ "ask" , "ax'" },
{ "^so$" , "so's" },
{ "head" , "'haid" },
{ "boss" , "main man" },
{ "wife" , "mama" },
{ "people" , "sucka's" },
{ "money" , "bre'd" },
{ ":", ", dig dis:" },
{ "amateur" , "begina'" },
{ "radio" , "transista'" },
{ "^of$" , "uh" },
{ "what" , "whut" },
{ "does" , "duz" },
{ "was" , "wuz" },
{ "^were" , "wuz" },
{ "understand it" , "dig it" },
{ "understand" , "dig it" },
{ "^my" , "mah'" },
{ "^i'", "ah'" },   /* to catch the case where there's already a ' */
{ "^i$" , "ah'" },
{ "meta" , "meta-fuckin'" },
{ "hair" , "fro" },
{ "talk" , "rap" },
{ "music" , "beat" },
{ "basket" , "hoop" },
{ "football" , "ball" },
{ "friend" , "homey" },
{ "school" , "farm" },
{ "boss" , "Man" },
{ "want to" , "wanna" },
{ "wants to" , "be hankerin' aftah" },
{ "Well" , "Sheeit" },
{ "big" , "big-ass" },
{ "bad" , "bad-ass" },
{ "small" , "little-ass" },
{ "sort of" , "radical" },
{ "^is$" , "be" },
{ "water" , "booze" },
{ "book" , "scribblin'" },
{ "magazine" , "issue of GQ" },
{ "paper" , "sheet" },
{ "up" , "down" },
{ "down" , "waaay down" },
{ "break" , "boogie" },
{ "^Hi$" , "'Sup, dude" },
{ "VAX" , "pink Cadillac" },
{NULL,NULL} };

const gag_pattern_t censor[]={
/* exact phrase matching
 * takes the form of "^<phrase>$" -> "<new phrase>"
 */
{ "^nob$", "n*b" },

/* odd words out in pattern matching
 * stops these words from being included in the pattern matches
 */
{ "scunt", "scunt" },	/* for 'scunthorpe' and misspellings of */
{ "swank", "swank" },

/* pattern match replacements (normal filter rules) */
{ "fuck", "f*ck"  },
{ "shit", "s**t" },
{ "cunt", "c**t" },
{ "bastard", "b*st*rd" },
{ "^wank", "w**k" },
{ "crap$", "cr*p" },
{ "^arse$", "a*se" },
{ "^twat", "tw*t" },
{ "^fanny", "f*n*y" },
{ "penis", "p*n*s" },
{ "vagina", "v*g*n*" },
{ "tosser$", "salad maker" },
{ "^tossa$", "light sleeper" },
{ "feck", "neck" },
{ "fart$", "fluff" },
{ "bloody", "bl**dy" },
{ "phuk", "f*ck" },
{ "dickhead", "dipwad" },
{ "tossing", "flippin'" },
{ "whore","prostitute"},
{ "whoring","prostituting"},
{ "bollocks","b*ll*cks"},
{ "bollock","b*ll*ck"},
{ "bugger","take me from behind"},
{ "buggery","anal sex"},
{ "^sod$","s*d"},
{ "^git$","g*t"},
{ "muppet","m*pp*t"},
{ "^ass$","*ss"},
{"beaver$","small furry mammal"},
{"muff","Victorian hand-warmer"},
{ "^lol$",""},
{ NULL, NULL} };

const gag_pattern_t wibble[]={
{"wibble","wibble"},
{"fish","fish"},
{"a","wib"},
{"b","ble"},
{"c","fi"},
{"d","sh"},
{"e","la"},
{"f","rd"},
{"g","ar"},
{"h","se"},
{"i","mon"},
{"j","key"},
{"k","ca"},
{"l","mel"},
{"m","wib"},
{"n","ble"},
{"o","fi"},
{"p","sh"},
{"q","la"},
{"r","rd"},
{"s","ar"},
{"t","se"},
{"u","mon"},
{"v","key"},
{"w","ca"},
{"x","mel"},
{"y","wib"},
{"z","ble"},
{NULL,NULL}};

const gag_pattern_t furby[]={
/* Phrase replacement */
{"^good morning$","dah a-loh u-tye"},
{"^morning$","dah a-loh u-tye"},
{"^good night$","dah a-loh nah-bah"},
{"^night$","dah a-loh nah-bah"},
{"^party$","dah noh-lah"},
/* Word replacement */
{"^pet$","ah-may"},
{"^light$","a-loh"},
{"^sun$","a-loh"},
{"^cloud$","a-loh may-lah"},
{"^hungry$","a-tay"},
{"^eat$","a-tay"},
{"^look$","ay-ay"},
{"^see$","ay-ay"},
{"^worried$","boh-bay"},
{"^scared$","boh-bay"},
{"^no$","boo"},
{"^can't$","boo"},
{"^big$","dah"},
{"^what$","doo"},
{"^question$","doo"},
{"^where$","doo"},
{"^fun$","doo-ay"},
{"^please$","doo-moh"},
{"^good$","e-day"},
{"^yes$","e-tah"},
{"^me$","kah"},
{"^i'm$","kah"},
{"^i$","kah"},
{"^again$","koh-koh"},
{"^more$","koh-koh"},
{"^health$","koo-doh"},
{"^sound$","lee-koo"},
{"^listen$","lee-koo"},
{"^joke$","loo-loo"},
{"^maybe$","may-bee"},
{"^love$","may-may"},
{"^hug$","may-lah"},
{"^kiss$","may-tah"},
{"^very$","mee-mee"},
{"^down$","nah-bah"},
{"^tickle$","nee-tye"},
{"^dance$","noh-lah"},
{"^happy$","noo-loo"},
{"^ask$","oh-too-mah"},
{"^ok$","o-kay"},
{"^done$","toh-dye"},
{"^like$","toh-loo"},
{"^you$","u-nye"},
{"^up$","u-tye"},
{"^yeah$","wah"},
{"!"," wah!"},
{"^sleep$","way-loh"},
{"^sing$","wee-tee"},
{"^hide$","who-bye"},
{NULL,NULL}};

/* ================================================================================ */
/* NOTE: for this filter, lower 'a's are replaced by char 1, so do the following:   */
/*       * if you want to replace a word with 'a' in it, use '\001' instead         */
/*       * if you want to replace a capital 'a' use 'A'                             */
/* ================================================================================ */

const gag_pattern_t warez[]={
/* word replacement */
{ "phone", "f0ne" },
{ "fre\001k", "phreak" },
{ "^lo\001ds of$", "l0adz/\\" },
{ "^lots of$", "l0adz/\\" },
{ "^with$", "w/" },
{ "^without$", "w/0" },
{ "^you$", "u" },
{ "^\001re$", "r" },
{ "^i've$", "I" },
{ "udes$", "00dz" },
{ "ude$", "00d" },
{ "^r\001d", "k-r/\\d" },
{ "\001wesome", "k-aw3s0me" },
{ "porn", "pr0n" },
{ "screwed", "scr00d" },
{ "elite", "'l33t" },
{ "h\001cker", "hax0r" },
{ "^do$", "d00" },
{ "people", "ppl" },
{ "^\001t the moment$", "a7m" },
{ "^confuse", "k0nf00zle" },
{ "^think", "f1nk" },
{ "women", "w1mm1n" },
{ "bec\001use", "'kuz" },
{ "^'cos$", "'kuz" },
{ "^cos$", "'kuz" },
{ "write", "ri7e" },
{ "thought", "7h0rt" },
{ "w\001s", "wuz" },
{ "w\001re", "w/\\re" },
{ "m\001nk", "m/\\nk" },
{ "house", "h0us3" },
{ "easy", "e3zy" },
{ "th\001t", "th/\\t" },
{ "l\001ter", "l8tr" },

/* exact keeps */
{ "ce", "ce" },
{ "ci", "ci" },
{ "cu", "cu" },
{ "ch", "ch" },
{ "cy", "cy" },

/* vowel/substring replacements */
{ "ight", "i7e" },
{ "e\001s", "33s" },
{ "ous", "00s" },
{ "ks$", "x" },
{ "s$", "z" },
{ "ss$", "z" },
{ "o", "0" },
{ "^el", "'l" },
{ "%e", "3" },
{ "%t", "7" },
{ "^l", "l" },
{ "l", "1" },
{ "A", "/\\" },
{ "^k", "k-k" },
{ "cks$", "x" },
{ "c\001", "ka" },
{ "co", "k0" },
{ "c", "k" },
/*{ "", "" },*/
/*{ "", "" },*/
/*{ "", "" },*/
/*{ "", "" },*/
/*{ "", "" },*/
{ NULL, NULL} };

const gag_pattern_t french[]={
/* exceptions */
{ "enough", "enoff" },
{ "me", "mon" },
{ "no", "non" },
{ "http", "http" },
{ "https", "https" },

/* generic rules */
{ "^a$", "la" },
{ "^an$", "les" },
{ "^h%", "'" },
{ "the", "ze" },
{ "this", "zis" },
{ "things", "zings"},
{ "air", "ar" },
{ "once", "vonce" },
{ "good", "gude" },
{ "morning", "moaning" },
{ "en", "on" },
{ "eak", "eck" },
{ "^x", "z" },
{ "ean", "on" },
{ "ike", "eek" },
{ "ov", "uv" },
{ "one", "un"},
{ "yes", "oui"},
{NULL,NULL}};

const gag_pattern_t babelfish[]={
/* Some phrases */
{"^babel fish$","fish of confusion"},
{"^better late than never$","late then never improve"},
{"^blue screen of death$", "blue visualization of the death"},
{"^check it out$","controlling it outside"},
{"^checking it out$","checks towards the outside"},
{"^for sure$", "for insurance"},
{"^heard of$", "intended to speak about"},
{"^it's free$", "it is before freedom"},
{"^its free$", "it is before freedom"},
{"^it is free$", "it is before freedom"},
{"^look like$", "appear"},
{"^looks like$", "appears"},
{"^not quite$", "sufficiently"},
{"^global inferno$", "the hell overall"},
{"^global_inferno$", "the hell overall"}, /* a user */
{"^make me ill$", "educate me to patients"},
{"^of death$", "of the dead women"},
{"^party hats$", "nails head"},
{"^picks up$", "gathers"},
{"^run off$", "deactivate"},
{"^runs off$", "deactivates"},
{"^bus stop$","impact of the megabus"},
{"^post office$","station"},
{"^engineering building$","construction that constructs"},
{"^New York$","Yoke Shaft"},
{"^looking for$","link look up"},
{"^three quarters of an hour$","three one hour quarters"},
{"^turn off$","sluices"},
{"^out of school$","extracurricular"},
{"^look on the bright side$","look happy one side"},
{"^up to date$","modern"},

/* Simple word substitutions */
{"^about","approximately"},
{"^accidental","random"},
{"^aerial","pilot"},
{"^against","opposition"},
{"^ah$","ampere-hour"},
{"^alias$","name of loan"},
{"^amazing$","narcotic"},
{"^amusing$","of diversion"},
{"^am$","being"},
{"^annoy$","irritate"},
{"^annoying$","repugnant"},
{"^annoyed$","obstructed"},
{"^any$","each possible"},
{"^anyone$","no matter who"},
{"^anyway$","in any event"},
{"^appear$","seem"},
{"^appears$","seems"},
{"^appropriate$","adapted"},
{"^array$","arsenal"},
{"^arrested$","stopped"},
{"^around$","lathe"},
{"^ask$","borrow"},
{"^asks$","borrows"},
{"^asked$","borrowed"},
{"^awarded$","allotted"},
{"^awkward$","clumsy"},
{"^babelfish$","fish of confusion"},
{"^bad$","defective"},
{"^back$","behind"},
{"^balance$","equilibrium"},
{"^bandit","gangster"},
{"^banger$","sausage"},
{"^bank$","battery"},
{"^bash$","strike violently"},
{"^bastard$","hybrid"},
{"^battery$","bank"},
{"^batteries$","banks"},
{"^beach$","range"},
{"^beat$","hit"},
{"^beats$","hits"},
{"^became$","would"},
{"^bend$","curving"},
{"^better$","new"},
{"^bill$","statement"},
{"^bills$","statements"},
{"^bit$","binary"},
{"^block$","tablet"},
{"^blonde$","fair"},
{"^bludgeon$","stick beat with a club"},
{"^bludgeons$","knuckles down"},
{"^board$","panel"},
{"^bond$","slavery"},
{"^booting$","the loading of the system of operation"},
{"^borrow$","ask"},
{"^borrows$","asks"},
{"^borrowed$","asked"},
{"^bottom$","inferior part"},
{"^box$","framework"},
{"^brought$","gotten at all"},
{"^bust$","statue"},
{"^campaign","expedition"},
{"^campus$","city"},
{"^case$","box"},
{"^cast$","mould"},
{"^cd$","digital disk"},
{"^cell$","stack"},
{"^channel$","port"},
{"^chap$","buster"},
{"^check$","exam"},
{"^checks$","exams"},
{"^chip$","circuit"},
{"^chromium$","bichromate of potassio"},
{"^chord$","rope"},
{"^claim$","complaint"},
{"^clear$","obvious"},
{"^clearly$","obviously"},
{"^client$","customer"},
{"^clients$","customers"},
{"^collection$","accumulation"},
{"^colour","paint"},
{"^cool$","fresh"},
{"^comes$","goes"},
{"^command$","point"},
{"^commands$","points"},
{"^contact$","communicate us"},
{"^cope$","face"},
{"^copes","faces"},
{"^corner$","wedge"},
{"^corners$","wedges"},
{"^cramp$","staple"},
{"^cramps$","staples"},
{"^crowded$","tightened"},
{"^crude$","raw"},
{"^customer$","client"},
{"^customers$","clients"},
{"^deal$","employ"},
{"^debugger$","water purifier"},
{"^delete$","unobtrusive"},
{"^deleted$","unobtrusive"},
{"^dead$","out of order"},
{"^dice$","tell"},
{"^even$","uniform"},
{"^deadline$","little ends of the stated period"},
{"^developer$","loiterer"},
{"^developers$","loiterers$"},
{"^different$","diverse"},
{"^digs$","excavations"},
{"^do$","make"},
{"^dodgy$","thorny"},
{"^door$","hatch"},
{"^download$","transfer from the central system towards the satellites"},
{"^downloading$","transferring from the central system towards the satellites"},
{"^downloads$","transfers from the central system towards the satellites"},
{"^dropped$","slackened"},
{"^edition","pad"},
{"^emission","releases"},
{"^employ","use"},
{"^employing","using"},
{"^end$","extremity"},
{"^ends$","terminates"},
{"^engineering$","machine"},
{"^enlarge$","extend"},
{"^enthusiam$","vortex"},
{"^escape$","futuristic"},
{"^escaped$","runs away"},
{"^evil$","poltergeist"},
{"^every$","continuously"},
{"^everyone$","all age"},
{"^exam$","check"},
{"^exams$","checks"},
{"^extend$","enlarge"},
{"^expedition","campagin"},
{"^face$","cope"},
{"^faces$","copes"},
{"^failed$","precipitateed"},
{"^fails$","comes to lack"},
{"^fair$","blonde"},
{"^fault","shortcoming"},
{"^faults","imperfections"},
{"^features$","devices"},
{"^fearful$","horrible"},
{"^file$","archive"},
{"^files$","archives"},
{"^first$","initially"},
{"^floor$","pavement"},
{"^flood$","plethora"},
{"^focus$","put to fire"},
{"^folder$","shirt"},
{"^freeze$","congeal"},
{"^freezes$","congeals"},
{"^fry$","roast"},
{"^fun$","recreation"},
{"^futuristic$","escape"},
{"^gag$","muzzle"},
{"^gagged$","muzzled"},
{"^gangster$","bandit"},
{"^get$","obtain"},
{"^gets$","obtains"},
{"^getting$","obtaining"},
{"^global$","total"},
{"^got$","obtained"},
{"^gun$","injector"},
{"^guns$","injectors"},
{"^gw$","gigawatt"}, /* for gateway */
{"^heard$","felt"},
{"^heap$","segment of memory"},
{"^half$","average"},
{"^held$","grasp"},
{"^hit$","beat"},
{"^hits$","beats"},
{"^horizontal$","landscape"},
{"^horrible$","fearful"},
{"^hour$","now"},
{"^hungry$","the famished one"},
{"^I$","spirit of I"},
{"^idle$","dormant"},
{"^ignore$","neglect"},
{"^imitate$","personify"},
{"^impression$","printing"},
{"^inferno$","region"},
{"^informed$","updated"},
{"^into$","indicated in"},
{"^irritate$","annoy"},
{"^just$","hardly"},
{"^justice$","troublesome army song process"},
{"^kernel$","nucleus"},
{"^kernels$","grains"},
{"^keyboard$","piano"},
{"^keyboards$","pianos"},
{"^kick$","retrocede"},
{"^kicked$","given a blow by foot"},
{"^kid$","small"},
{"^king$","three"},
{"^knife$","router"},
{"^label$","tag"},
{"^labels$","tags"},
{"^landscape$","horizontal"},
{"^latter$","posterior"},
{"^lazy$","putrefies"},
{"^lead$","cable"},
{"^lease$","rent"},
{"^leaves$","let"},
{"^let$","leaves"},
{"^library$","bookcase"},
{"^line$","string"},
{"^log$","natural logarithm"},
{"^login$","procedure of connection"},
{"^lost$","destroyed"},
{"^luck$","fortune"},
{"^machine$","engineering"},
{"^mail$","post office"},
{"^mains$","forces"},
{"^maintain$","update"},
{"^majestically$","maestro"},
{"^majestic$","maestro"},
{"^maker$","fabricator"},
{"^make$","create"},
{"^made$","generated"},
{"^map$","correspond"},
{"^maps$","correspondence"},
{"^mapped$","corresponded"},
{"^means$","average"},
{"^member$","component"},
{"^merit$","grade of transmission performance"},
{"^merits$","grades of transmission performance"},
{"^millenium$","thousands of years celebration"},
{"^minute$","tiny"},
{"^modern$","up to date"},
{"^moor$","tie"},
{"^mother$","nut"},
{"^muzzle$","delta"},
{"^name$","to call"},
{"^narrow$","shrink"},
{"^nearly$","neighbour"},
{"^necesity$","need"},
{"^need$","necesity"},
{"^new$","better"},
{"^now$","hour"},
{"^nut$","mother"},
{"^obsolete$","old-fashioned"},
{"^obvious$","clear"},
{"^obviously$","clearly"},
{"^odd$","uneven"},
{"^off$","extinguished"},
{"^officer$","public official"},
{"^on$","ignition"},
{"^opposition","against"},
{"^order$","pallet arrange"},
{"^ordering$","pallet arranging"},
{"^orders$","pallet arranges"},
{"^outlook","exterior view"},
{"^oven$","furnace"},
{"^pack$","disc battery"},
{"^packet$","parcel"},
{"^packets$","packages"},
{"^pad$","edition"},
{"^pads$","editions"},
{"^paint","colour"},
{"^pattern","model"},
{"^phenomenon","buster"},
{"^pilot","aerial"},
{"^please","I pray"},
{"^place","workstation"},
{"^placed$","disposed"},
{"^plain$","ordinary"},
{"^plot$","diagram"},
{"^point$","command"},
{"^points$","commands"},
{"^port$","gate"},
{"^post$","stamp"},
{"^posting$","fixation"},
{"^posted$","stamped"},
{"^powerful$","long-range"},
{"^pretty$","graceful"},
{"^private$","classified"},
{"^pub$","publication"},
{"^put$","position"},
{"^pwb$","printed chart"}, /* a user */
{"^quack$","charlatan"},
{"^quake$","termite"},
{"^quietly$","calm"},
{"^quite$","absolutely"},
{"^quarters$","rooms"},
{"^random$","accidental"},
{"^ratio$","reason"},
{"^ratios$","reasons"},
{"^raw$","crude"},
{"^real$","true"},
{"^region$","inferno"},
{"^right$","right"},
{"^rollercow$","cow of cylinders"}, /* rollercow is a user. actual translation was "roller cow" */
{"^root$","radish"},
{"^rope$","chord"},
{"^roundabout$","traffic circle"},
{"^router$","knife"},
{"^recover$","recoup"},
{"^recoovers$","recoups"},
{"^recoup$","recover"},
{"^recoups$","recovers"},
{"^release$","unblocking"},
{"^relaeses$","emission"},
{"^renew$","renovate"},
{"^reverse$","invest"},
{"^ready$", "it prepares"},
{"^reason$","ratio"},
{"^reasons$","ratios"},
{"^resembles$","looks like"},
{"^response$","back chat"},
{"^run!","operation!"},
{"^run$","be carried out"},
{"^running$","carrying out"},
{"^said$","aforementioned"},
{"^sanity$","validity"},
{"^screen$","shield"},
{"^script$","certificate"},
{"^seem$","look like"},
{"^seems$","resembles"},
{"^sentence$","program phrase"},
{"^server$","waiter"},
{"^several$","unites"},
{"^share$","slice"},
{"^sheep$","ewes"},
{"^shirt$","folder"},
{"^shock$","jolt"},
{"^shocks$","jolts"},
{"^shop$","acquires"},
{"^should$","must"},
{"^show$","expose"},
{"^shrink$","narrow"},
{"^silver$","money"},
{"^site$","place of"},
{"^sits$","rests"},
{"^sink$","tub"},
{"^sinks$","tubs"},
{"^slap$","stroke"},
{"^slaps$","strokes"},
{"^slip$","voucher"},
{"^small$","kid"},
{"^snail","wow"},
{"^so$","thus"},
{"^society$","company"},
{"^societies$","companies"},
{"^something$","somewhat"},
{"^sonic$","acoustic"},
{"^sorry$","afflicted"},
{"^sort$","classify"},
{"^spelling$","autograph"},
{"^spook$","phantom"},
{"^sport$","gymnast"},
{"^sports$","gymnastics"},
{"^sponsor$","sleeping partner"},
{"^sponsors$","sleeping partners"},
{"^stack$","cell"},
{"^station$","post office"},
{"^staple$","cramp"},
{"^staples$","cramps"},
{"^statement$","bill"},
{"^statements$","bills"},
{"^still$","silent"},
{"^straight$","right"},
{"^string$","line"},
{"^statue$","bust"},
{"^student$","class participant"},
{"^stuff$","substance"},
{"^style$","model"},
{"^sucs$","juices"},
{"^sufficiently$","not quite"},
{"^summon$","convene"},
{"^sure$","safe"},
{"^tablet$","block"},
{"^talker$","transmitter"},
{"^tag$","label"},
{"^tap$","cock"},
{"^tedious$","lethargic"},
{"^tell$","dice"},
{"^tempted$","tried"},
{"^they$","that set"},
{"^through$","by"},
{"^three$","king"},
{"^tie$","moor"},
{"^tied$","moored"},
{"^timeout$","extinction"},
{"^titanium$","titanic"},
{"^total","global"},
{"^tool$","instrument"},
{"^tools$","instruments"},
{"^true$","real"},
{"^tub$","sink"},
{"^tubs$","sinks"},
{"^turkey$","Peru"},
{"^turn$","twist"},
{"^turned$","twisted"},
{"^twist$","turn"},
{"^twisted$","turned"},
{"^typing$","digit"},
{"^union$","mayoralty"},
{"^up$","upwards"},
{"^update$","maintain"},
{"^updated$","informed"},
{"^user$","customer"},
{"^using","employing"},
{"^wail$","lament"},
{"^waiter","server"},
{"^Wales$","Country of Scales"},
{"^wander$","ramble"},
{"^wanders$","rambles"},
{"^watcher$","observer"},
{"^wedge$","the corner"},
{"^welshbyte$","octo of welshman"}, /* welshbyte is a user */
{"^well$","sink"},
{"^while$","moment"},
{"^wildly$","in an extravagant way"},
{"^wired$","tied with wire"},
{"^woo$","court"},
{"^woody$","timber"},
{"^worm$","ver"},
{"^vortex$","turbulence"},
{"^would$","became"},
{"^work$","run"},
{"^worked$","ran"},
{"^working$","running"},
{"^worried$","preoccupied"},
{"^wow$","snail"},
{"^wrong$","distort"},
{"^years$","several year"},

/* Exceptions to patterns */
{"^loudness$", "loudness"},

/* Patterns/prefixes */
{"^loud","strong"},
{"^pattern","model"},
{"^watch","guard"},
{NULL,NULL}};




/* no-sport-on-mw gag. */
const gag_pattern_t nosport[]={

/* general sporting terms */

{ "^angler","geometrist"},
{ "^archer$","pointed stick thrower"},
{ "^archers$","pointed stick throwers"},
{ "^athlete","spod"},
{ "ball","b*ll"},
{ "^batsman","tw*tsman"},
{ "^batsmen","tw*tsmen"},
{ "^batting","tw*tting"},
{ "^bowl","blow"},
{ "^boxer","John Prescott"},	/* After his performance in the 2001 Election
				 * Campaign */
{ "^cheer leader","pom-pom waver"},
{ "^cheer leading","pom-pom waving"},
{ "^club$","big stick"},
{ "^coach$","bus"},
{ "^conversion","algorithm"},
{ "course","smooth"}, /* to get things like racecourse, golf course */
{ "^formula$","recipe"},
{ "^goal","g**l"},
{ "^golfer","fashon victims"},
{ "^grand prix","great pricks"},
{ "^grand national","1000 imigrants"},
{ "^hiker","hic-cougher"},
{ "^jockey","j-key"},
{ "^manager$","p*ss artist"},
{ "^mountineer","mole hill climber"},
{ "^olympic$","Greek thingy"},
{ "^olympics$","Greek thingies"},
{ "^penalties$","spankings"},
{ "^penalty$","spanking"},
{ "^player$","spod"},
{ "^premiership$","over-paid f*******ing w*nkers"},
{ "^race","spodfest"},
{ "^rally$","gather round"},
{ "^ref$","w**ker"},
{ "^refs$","w**kers"}, /* You need this otherwise words like 'refuse' are
			* caught. */
{ "^referee","w**ker"},
{ "^running$","moving quickly"},
{ "^sport","sp***"},
{ "^stump","st*mp"},
{ "^tackle$","fishing apparatus"},
{ "^team$","associates"},
{ "^teams$","associates"},
{ "^umpire","w**ker"},
{ "^varsity$","a*se city"},
{ "^wicket$","wi***t"},
{ "^world cup$","global mug"},

/* sports */
{ "^angling","geometry"},
{ "^archery$","pointed stick throwing"},
{ "^athletic$","non-spod"},
{ "^athletics","running round in circles"},
{ "^badminton","good mint off"},
{ "^boxing$","professional thuggery"},
{ "^cricket$","cri****"},
{ "^cross country","angry city"},
{ "^extra time$","play time"},
{ "^fencing","walling"},
{ "^fishing","wibbleing"},
{ "^free kick","keep fit"},
{ "^football","foo*****"},
{ "^footie$","foo***"},
{ "^golf$","a good walk ruined"},
{ "^grid iron","steel table"}, /* this is a name for American Football */
{ "^hiking$","hello Queen"},
{ "^hockey","bashing people with big sticks"},
{ "^indy","Native American"}, /* as in indycar racing and the Indy 500 */
{ "^injury$","Doctor Doctor"},
{ "^injuries$","payback"},
{ "^marathon$","snickers"},
{ "^marathons$","snickers"},
{ "^motorsport$","driving round in circles"},
{ "^mountineering","mole hill climbing"},
{ "^polo","the mint with the hole"},
{ "^pool$","puddle"},
{ "^racing$","see who is the fastest"},
{ "^rugby$","a game played by men with funny shaped b*lls"},
{ "^sailing$","boring thing with boats"},
{ "^snooker$","sn**ker"},
{ "^speedway","slow-way"},
{ "^squash$","OJ"},
{ "^surf","turf"}, /* hopefully this should catch surfing, surfboard, etc */
{ "^swimming$","sinking"},
{ "^tennis","t***is"},
{ "^track and field$","concrete and grass"},
{ "^windsurf","flatulence turf"},	/* wind surfing as two words should be
                                         * caught by the ^surf entry above */


/* teams */
{ "^bluebirds$","deadbirds"},
{ "^CCFC$","Completely Cr*p Foo***** Club"},
{ "^city$","village"},
{ "^magpies","humbugs"},
{ "^rovers","dogs"},
{ "^SCFC$","Stupendously Cr*p Foo***** Club"},
{ "^swans$","Geese"},
{ "^town$","village"},
{ "^united$","disorganised"},

/* Towns, cities and places (to get sports teams and events).
 * This is also the place to put sports grounds.
 */

{ "^Aintree","Pain bush"}, /* this is where the grand national is held */
{ "^Arsenal","big gun store"},
{ "^Birmingham","Brummyport"},
{ "^Cambridge","Cam-shaftbridge"},
{ "^Cardiff","C****ff"},
{ "^Chelsea","pensioners"},
{ "^Cheltenham","Gloucester"},
{ "^Chepstow","Cheap-stow"},
{ "^Essex","EasySex"},
{ "^Glamorgan","Glamour-gun"},
{ "^Gloucestershire","Gloucestershite"},
{ "^Headingly","Footingly"},
{ "^Indianapolis","Native American City"},
{ "^Kent","C*nt"},
{ "^Lancashire","Red Rose Land"},
{ "^Leeds$","Follows"},
{ "^Liverpool$","Kidneypuddle"},
{ "^Llanelli$","Turks"},
{ "^Lords$","Satan's"},
{ "^Manchester$","W**kchester"},
{ "^Middlesex$","Transvestite"},
{ "^Newcastle$","Oldfort"},
{ "^Northamptonshire$","North Porktownshite"},
{ "^the Oval$","the decahedron"}, /* this is a cricket ground */
{ "^Oxford","Donkeyford"},
{ "^Silverstone","Bronzepebble"},
{ "^Somerset","Scrumpy Land"},
{ "^Surrey","Slurrey"},
{ "^Swansea$","Gooselake"},
{ "^Trent bridge","Tent tunnel"},
{ "^valley$","vagina"}, /* Valleys is under nationalities */
{ "^Wembley","Chaosville"},
{ "^Wimbledon","Wombleland"},
{ "^Yorkshire","White Rose Land"},

/* Countries & Nationalities.
 * note: please try not to be too offensive here!
 */
{ "^England$","Rose"},
{ "^English$","Hooligans"},
{ "^Germany$","Sauerkraut Land"},
{ "^German","Sausage"},
{ "^Ireland$","Shamrock"},
{ "^Irish$","Leprechauns"},
{ "^New Zealand$","Kiwiland"},
{ "^New Zealander","Kiwi"},
{ "^Scotland$","Thistle"},
{ "^Scot$","Angus"},
{ "^Scottish","Anguses"},
{ "^valleys","Ar*e end of nowhere"},
{ "^Wales$","Leek"},
{ "^Welsh$","Dragons"},

{ "^Aussie","XXXX drinker"},

/* things the don't fit anywhere else */
{ "bike$","trike"},
{ "bikes$","trikes"},
{ "^biker$","pussy cat"},
{ "^quake$","glorified doom"},
{ "^sack$","potato carrier"},
{ "^ashes$","embers"}, /* this is the cricket series. */

{NULL,NULL}};
