#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>
#include <strings.h>
#include <string.h>
#include <errno.h>

#include "poll.h"

struct kq_priv {
	int kqd;
};

struct kq_priv kp = {
	.kqd = -1
};

int poll_addconn(int fd, void *data)
{
	struct kevent ev;

	EV_SET(&ev, fd, EVFILT_READ, EV_ADD, 0, 0, data);
	return kevent(kp.kqd, &ev, 1, NULL, 0, NULL);
}

int poll_init(void)
{
	if (kp.kqd == -1)
		kp.kqd = kqueue();
	return 0;
}

void poll_delete(int fd)
{
	struct kevent ev;

	EV_SET(&ev, fd, EVFILT_READ, EV_DELETE, 0, 0, NULL);
	kevent(kp.kqd, &ev, 1, NULL, 0, NULL);
	EV_SET(&ev, fd, EVFILT_WRITE, EV_DELETE, 0, 0, NULL);
	kevent(kp.kqd, &ev, 1, NULL, 0, NULL);
}

int poll_with_writes(int fd, void *data)
{
	struct kevent ev;

	EV_SET(&ev, fd, EVFILT_WRITE, EV_ADD, 0, 0, data);
	return kevent(kp.kqd, &ev, 1, NULL, 0, NULL);
}

int poll_without_writes(int fd, void *data)
{
	struct kevent ev;
	int ret;

	EV_SET(&ev, fd, EVFILT_READ, EV_ADD, 0, 0, data);
	ret = kevent(kp.kqd, &ev, 1, NULL, 0, NULL);
	if (ret != 0)
		return ret;
	EV_SET(&ev, fd, EVFILT_WRITE, EV_DELETE, 0, 0, NULL);
	ret = kevent(kp.kqd, &ev, 1, NULL, 0, NULL);
	if (ret != 0 && errno != ENOENT)
		return ret;
	return 0;
}

int poll_wait(int timeout_millis, int (*callback)(poll_event_t *, int, void *), void *data)
{
#define NEVENTS (20)
	struct timespec timeout = {
		.tv_sec = timeout_millis / 1000,
		.tv_nsec = timeout_millis % 1000 * 1000000
	};
	struct kevent evs[NEVENTS];
	poll_event_t pevs[NEVENTS];
	int ret;

	ret = kevent(kp.kqd, NULL, 0, evs, NEVENTS, &timeout);
	if (ret == -1)
		return ret;
	for (int i = 0; i < ret; i++) {
		memset(&pevs[i], 0, sizeof(pevs[i]));
		pevs[i].data = evs[i].udata;
		pevs[i].is_error = (evs[i].flags & (EV_EOF | EV_ERROR));
		pevs[i].is_read = (evs[i].filter == EVFILT_READ);
		pevs[i].is_write = (evs[i].filter == EVFILT_WRITE);
	}
	ret = callback(pevs, ret, data);
	return ret;
}
