#ifndef GAGTABLE_H
#define GAGTABLE_H

#include "gags.h"

extern const gag_pattern_t chef[];
extern const gag_pattern_t kraut[];
extern const gag_pattern_t fudd[];
extern const gag_pattern_t jive[];
extern const gag_pattern_t censor[];
extern const gag_pattern_t wibble[];
extern const gag_pattern_t annoy[];
extern const gag_pattern_t tnarg[];
extern const gag_pattern_t furby[];
extern const gag_pattern_t warez[];
extern const gag_pattern_t french[];
extern const gag_pattern_t babelfish[];
extern const gag_pattern_t nosport[];
extern const gag_pattern_t unreturnable[];
#endif /* GAGTABLE_H */
