#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#include <socket.h>
#include <nonce.h>
#include <util.h>
#include <rooms.h>
#include <talker_privs.h>
#include <ipc.h>
#include <perms.h>

#include "gags.h"
#include "servsock.h"
#include "replay.h"
#include "actions.h"

/* user made a request to perform an action
 * decide if they are permitted
 * send response message
 * announce the attempt/success to the world
 */
void accept_action(ipc_connection_t *conn, ipc_message_t *msg)
{
	ipcmsg_summary("ACTION", msg);
	json_t * j = json_init(msg);

	const char *command = json_getstring(j, "type");
	const char *victim_name = json_getstring(j, "target");
	const char *reason = json_getstring(j, "reason");
	const char *admin = json_getstring(j, "admin");

	if (command == NULL || victim_name == NULL) {
		send_error(conn, msg, "Invalid server command syntax");
		return;
	}

	struct user attacker;
	struct user victim;
	int32_t toid = who_find(victim_name);

	/* user needs to be actually logged in */
	if (toid < 0) {
		send_error(conn, msg, "User '%s' not found", victim_name);
		return;
	}

	/* grab the user records of both sides */
	fetch_user(&attacker, msg->head.src);
	fetch_user(&victim, toid);

	const char *attacker_name = attacker.record.name;
	/* decide if they are allowed to */
	int success = 0;

	/* what are the protection levels */
	int prot_vi = (victim.record.chatprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	int prot_at = (attacker.record.chatprivs & CP_PROTMASK) >> CP_PROTSHIFT;

    /* ignore the old protection flags in favour for the new def/att system */
	//if (!cp_test(&attacker, CP_PROTECT)) prot_at = 0;
	//if (prot_vi == 0 && cm_test(&victim, CM_PROTECTED)) prot_vi = 1;

	/* attacker was stronger, success */
	if (prot_at >= prot_vi) success = 1;

	/* an admin can ask to ignore protection modes */
	if (admin!=NULL && strcasecmp(admin, "yes")==0 && u_god(&attacker)) {
		success = 1;
	}

	if (strcasecmp(command, "mrod")==0) {
		// tell the world
		ipc_message_t * event = ipcmsg_create(IPC_EVENT, msg->head.src);
		ipcmsg_destination(event, -1);
		json_t * ej = json_init(NULL);
		json_addstring(ej, "target", victim_name);
		json_addint(ej, "success", success);
		json_addstring(ej, "type", "mrod");

		if (success) {
			json_vaddstring(ej, "text", "%s has just dropped the Magic Roundabout of Death on %s",
			               attacker_name, victim_name);
			json_addstring(ej, "verbose", "You hear a boing in the distance, closely followed by the distant wail of a hurdy-gurdy, which is suddenly terminated by a blood curdling scream, a C major chord explosion and a few curious springing noises...");

			// kick the person
			_autofree char *buff=NULL;
			if (reason != NULL) {
				asprintf(&buff,"mr%s", reason);
				json_addstring(ej, "reason", reason);
			} else {
				asprintf(&buff,"m-");
			}
			ipc_message_t *update = ipcmsg_create(IPC_KICK, msg->head.src);
			ipcmsg_destination(update, toid);
			ipcmsg_append(&update, buff, strlen(buff));
			msg_attach_to_username(update, victim_name);
			ipcmsg_destroy(update);

		} else {
			json_vaddstring(ej, "text", "%s just tried to MROD %s and failed",
			               attacker_name, victim_name);
		}
		ipcmsg_json_encode(&event, ej);
		json_decref(ej);

		// annouce what happened to everyone else
		process_msg(conn, event);
		// process_msg will discard it when its done
		return;
	}

	if (strcasecmp(command, "zod")==0) {
		// tell the world
		ipc_message_t * event = ipcmsg_create(IPC_EVENT, msg->head.src);
		ipcmsg_destination(event, -1);
		json_t * ej = json_init(NULL);
		json_addstring(ej, "target", victim_name);
		json_addint(ej, "success", success);
		json_addstring(ej, "type", "zod");

		if (success) {
			json_vaddstring(ej, "text", "%s has just sent the Zebedee of Death to %s",
			               attacker_name, victim_name);
			json_addstring(ej, "verbose", "You hear a boing in the distance");

			// kick the person
			_autofree char *buff=NULL;
			if (reason != NULL) {
				asprintf(&buff,"zr%s", reason);
				json_addstring(ej, "reason", reason);
				printf("ZOD set reason '%s'\n", reason);
			} else {
				asprintf(&buff,"z-");
			}
			ipc_message_t *update = ipcmsg_create(IPC_KICK, msg->head.src);
			ipcmsg_destination(update, toid);
			ipcmsg_append(&update, buff, strlen(buff));
			msg_attach_to_username(update, victim_name);
			ipcmsg_destroy(update);
		} else {
			json_vaddstring(ej, "text", "%s just tried to ZoD %s and failed",
			               attacker_name, victim_name);
		}
		ipcmsg_json_encode(&event, ej);
		json_decref(ej);

		// annouce what happened to everyone else
		process_msg(conn, event);
		return;
	}

	if (strcasecmp(command, "gag")==0) {
		const char *gagtype = json_getstring(j, "gag");

		if (gagtype == NULL) {
			/* the original, and best */
			gagtype = "gag";
		}

		int gtnum = gag_code(gagtype);

		if (gtnum == 0) {
			send_error(conn, msg, "Gag type '%s' not found", gagtype);
			return;
		}

		ipc_message_t * event = ipcmsg_create(IPC_EVENT, msg->head.src);
		event->head.when = msg->head.when;
		ipcmsg_destination(event, -1);
		json_t * ej = json_init(NULL);
		json_addstring(ej, "target", victim_name);
		json_addint(ej, "success", success);
		json_addstring(ej, "type", "gag");

		if (success) {
			json_vaddstring(ej, "text", "%s has just gagged %s with %s",
			               attacker_name, victim_name, gag_type(gtnum));

			// change users mode
			_autofree char *buff=NULL;
			asprintf(&buff,"+%d", gtnum);
			ipc_message_t *update = ipcmsg_create(IPC_GAG, msg->head.src);
			update->head.when = msg->head.when;
			ipcmsg_destination(update, toid);
			ipcmsg_append(&update, buff, strlen(buff));
			msg_attach_to_username(update, victim_name);
			ipcmsg_destroy(update);

			ipc_message_t *gm = ipcmsg_create(IPC_EVENT, msg->head.src);
			gm->head.when = msg->head.when;
			json_t * gj = json_init(NULL);
			json_addstring(gj, "target", victim_name);
			json_addint(gj, "success", success);
			json_addstring(gj, "type", "gag");
			json_vaddstring(gj, "text", "%s", gag_gag_msg(gtnum));
			ipcmsg_json_encode(&gm, gj);
			msg_attach_to_username(gm, victim_name);
			ipcmsg_destroy(gm);
		} else {
			json_vaddstring(ej, "text", "%s just tried to gag %s with %s and failed",
			               attacker_name, victim_name, gag_type(gtnum));
		}
		ipcmsg_json_encode(&event, ej);
		json_decref(ej);

		// annouce what happened to everyone else
		process_msg(conn, event);
		return;
	}

	if (strcasecmp(command, "ungag")==0) {
		ipc_message_t * event = ipcmsg_create(IPC_EVENT, msg->head.src);
		event->head.when = msg->head.when;
		ipcmsg_destination(event, -1);
		json_t * ej = json_init(NULL);
		json_addstring(ej, "target", victim_name);
		json_addint(ej, "success", success);
		json_addstring(ej, "type", "ungag");

		if (success) {
			unsigned long old = victim.record.chatmode;
			unsigned long oldg = (old & CM_GAGMASK) >> CM_GAGSHIFT;

			json_vaddstring(ej, "text", "%s has just ungagged %s",
			                attacker_name, victim_name);

			// change users mode
			_autofree char *buff=NULL;
			asprintf(&buff,"-");
			ipc_message_t *update = ipcmsg_create(IPC_GAG, msg->head.src);
			update->head.when = msg->head.when;
			ipcmsg_destination(update, toid);
			ipcmsg_append(&update, buff, strlen(buff));
			msg_attach_to_username(update, victim_name);
			ipcmsg_destroy(update);

			ipc_message_t *gm = ipcmsg_create(IPC_EVENT, msg->head.src);
			gm->head.when = msg->head.when;
			json_t * gj = json_init(NULL);
			json_addstring(gj, "target", victim_name);
			json_addint(gj, "success", success);
			json_addstring(gj, "type", "ungag");
			json_vaddstring(gj, "text", "%s", gag_ungag_msg(oldg));
			ipcmsg_json_encode(&gm, gj);
			msg_attach_to_username(gm, victim_name);
			ipcmsg_destroy(gm);
		} else {
			json_vaddstring(ej, "text", "%s just tried to ungag %s and failed",
			               attacker_name, victim_name);
		}
		ipcmsg_json_encode(&event, ej);
		json_decref(ej);

		// annouce what happened to everyone else
		process_msg(conn, event);
		return;
	}
	send_error(conn, msg, "Command '%s' not understood", command);
}
