#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdbool.h>
#include <time.h>
#include <dirent.h>

#include <socket.h>
#include <nonce.h>
#include <util.h>
#include <rooms.h>
#include <talker_privs.h>
#include <ipc.h>

#include "servsock.h"
#include "replay.h"
#include "actions.h"
#include "gags.h"
#include "poll.h"
#include <folders.h>
#include <perms.h>
#include <special.h>

struct list_head connection_list;

int mainsock_die = 0;

int open_mainsock(uint16_t port)
{
	int fd = -1;
	if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		fprintf(stderr, "Error creating master socket. %s\n", strerror(errno));
		return -1;
	}

	/* we could have done v6 here as well, but the 4-in-6 stuff is
	 * now deprecated so we would have to open a second v6 only socket
	 * if we really want v6 mode */
	struct sockaddr_in in;
	in.sin_family = AF_INET;
	in.sin_port = htons(port);
	in.sin_addr.s_addr = INADDR_ANY;

	int i = 1;
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(i))) {
		fprintf(stderr, "Error enabling rebind on port. %s\n", strerror(errno));
		close(fd);
		return -1;
	}

	if (bind(fd, (struct sockaddr *)&in, sizeof(in))) {
		fprintf(stderr, "Error binding port: %s\n", strerror(errno));
		close(fd);
		return -1;
	}

	if (listen(fd, 2)) {
		fprintf(stderr, "Error listening on port: %s\n", strerror(errno));
		close(fd);
		return -1;
	}

	return fd;
}

ipc_connection_t * add_connection(int fd)
{
	/* create new structure */
	ipc_connection_t *new = ipcconn_create();
	new->fd = fd;

	/* add to list of all connections */
	list_add_tail(&(new->list), &connection_list);

	/* register interest in read events */
	if (poll_addconn(new->fd, new)) {
		fprintf(stderr, "Error adding new conn to poll: %s\n", strerror(errno));
	}

	new->state = IPCSTATE_CONNECTED;

	return new;
}

void accept_connection(int mainsock)
{
	struct sockaddr_in in;
	socklen_t inlen = sizeof(in);
	int sock;

	if ((sock=accept(mainsock, (struct sockaddr *)&in, &inlen))<0) {
		fprintf(stderr, "Error accepting new connection: %s\n", strerror(errno));
		return;
	}

	ipc_connection_t * c = add_connection(sock);

	printf("Accept fd=%d %s:%d\n", c->fd, inet_ntoa(in.sin_addr), ntohs(in.sin_port));
}

void drop_connection(ipc_connection_t * conn)
{
	printf("Drop connection fd=%d\n", conn->fd);
	poll_delete(conn->fd);
	list_del_init(&conn->list);
	if (conn->fd != -1) close(conn->fd);
	conn->fd = -1;
	conn->state = IPCSTATE_DELETED;
	if (!list_empty(&conn->outq)) {
		struct list_head *pos, *q;
		list_for_each_safe(pos, q, &conn->outq) {
			outq_msg_t * oq = list_entry(pos, outq_msg_t, list);
			list_del(&(oq->list));
			oq->msg->refcount--;
			ipcmsg_destroy(oq->msg);
			free(oq);
		}
	}
	memset(conn, 0, sizeof(ipc_connection_t));
	free(conn);

	ipc_message_t * whoinfo = msg_wholist();
	msg_attach_to_all(whoinfo);
	ipcmsg_destroy(whoinfo);
}

static int mainsock_event_cb(poll_event_t *events, int nmemb, void *data)
{
	int *mainsock = data;

	for (int i = 0; i < nmemb; i++) {
		poll_event_t *ev = &events[i];

		/* event on mainsock */
		if (ev->data == NULL) {
			if (ev->is_error) {
				return -1;
			} else
			if (ev->is_read) {
				accept_connection(*mainsock);
			} else {
				fprintf(stderr, "unexpected event on mainsock.\n");
			}
		} else {
			ipc_connection_t *c = ev->data;
			if (ev->is_error) {
				ipcconn_bad(c);
			} else
			if (ev->is_read) {
				ipc_message_t *msg = read_socket(c, 1);
				while (msg != NULL) {
					msg->head.when = time(NULL);
					process_msg(c, msg);
					msg=read_socket(c,0);
				}
			} else
			if (ev->is_write) {
				write_socket(c);
			} else {
				fprintf(stderr, "unexpected event on fd=%d.\n", c->fd);
			}
		}

		/* check if we had to drop any connections and clean them away */
		struct list_head *pos, *q;
		list_for_each_safe(pos, q, &connection_list) {
			ipc_connection_t * c = list_entry(pos, ipc_connection_t, list);
			/* connections that went bad */
			if (c->fd != -1 && c->state == IPCSTATE_ERROR) {
				drop_connection(c);
			} else if (c->state == IPCSTATE_PURGE && list_empty(&(c->outq))) {
			/* connections with a soft close */
				drop_connection(c);
			}
		}
	}
	return nmemb;
}

void watch_mainsock(int mainsock)
{
	int ret;
	struct timeval last;
	gettimeofday(&last, NULL);

	if (poll_without_writes(mainsock, NULL)) {
		fprintf(stderr, "Error adding mainsock: %s\n", strerror(errno));
		return;
	}

	do {
		while ((ret = poll_wait(1000, mainsock_event_cb, &mainsock)) >= 0) {
			/* end of events handling, do periodic stuff here */
			if (ret == 0)
				replay_commit();
		}
		/* poll got interrupted, not actually an error, go around again */
	} while (ret == -1 && errno == EINTR);

	fprintf(stderr, "poll error: %s\n", strerror(errno));
}

void write_socket(ipc_connection_t * conn)
{
	if (conn == NULL) return;
	if (conn->state == IPCSTATE_ERROR) return;

	if (list_empty(&conn->outq)) {
		/* tx queue is empty, stop write events */
		if (poll_without_writes(conn->fd, conn))
			fprintf(stderr, "Error updating poll fd=%d: %s\n",
			        conn->fd, strerror(errno));
		return;
	}
	struct list_head * pos = conn->outq.next;
	outq_msg_t * mtx = list_entry(pos, outq_msg_t, list);
	list_del(pos);

	mtx->msg->refcount--;
	ipcmsg_send(mtx->msg, conn);
	free(mtx);
}

extern time_t uptime;

void send_error(ipc_connection_t *conn, ipc_message_t *orig, const char *format, ...) __attribute__((format (printf, 3, 4)));

void send_error(ipc_connection_t *conn, ipc_message_t *orig, const char *format, ...)
{
	va_list va;
	va_start(va, format);
	_autofree char *text = NULL;
	vasprintf(&text, format, va);
	va_end(va);

	ipc_message_t * msg = ipcmsg_create(IPC_TALKERROR, 0);
	ipcmsg_destination(msg, conn->addr);

	char type[5];
	snprintf(type, 5, "%4.4s", (char *)&orig->head.type);
	json_t * j = json_init(NULL);
	json_addstring(j, "text", text);
	json_addstring(j, "type", type);
	ipcmsg_json_encode(&msg, j);
	msg_attach(msg, conn);
	ipcmsg_destroy(msg);
	json_decref(j);
}

static ipc_message_t *msg_gaglist(void)
{
	ipc_message_t * msg = ipcmsg_create(IPC_GAGLIST, SYSTEM_USER);
	json_t *arr = json_array();

	for (GagInfo *gi = gaglist; gi->name != NULL; gi++)
		json_array_append_new(arr, json_string(gi->name));

	ipcmsg_json_encode(&msg, arr);
	json_decref(arr);
	return msg;
}

void process_msg(ipc_connection_t *conn, ipc_message_t *msg)
{
	ipcmsg_summary("PROCESS", msg);
	char *body = (char *)(msg + 1);

	/* client just told us who they are */
	if (msg->head.type == IPC_HELLO) {
		if (msg->bodylen < 4) {
			printf("Invalid HELO from fd=%d. dropping.\n", conn->fd);
			ipcmsg_destroy(msg);
			drop_connection(conn);
			return;
		}
		/* user id / userposn is in the header src */
		memcpy(&conn->user, &(msg->head.src), sizeof(conn->user));
		/* first 4 bytes of body are PID / Session ID */
		memcpy(&conn->addr, body, sizeof(conn->addr));
		/* rest of body is the NONCE,
		 * check this to make sure we have a matching client */
		if (!match_nonce(&body[4])) {
			printf("Mismatched nonce from fd=%d. dropping.\n", conn->fd);
			ipcmsg_destroy(msg);

			// tell the client
			ipc_message_t * error = msg_error("NONCE");
			ipcmsg_destination(error, conn->addr);
			msg_attach(error, conn);
			ipcmsg_destroy(error);
			printf("Sending error message\n");

			// purge the tx queue and close
			conn->state = IPCSTATE_PURGE;
			return;
		}
		printf("WHO Add: pid=%d posn=%d\n", conn->addr, conn->user);
		conn->state = IPCSTATE_VALID;
		ipcmsg_destroy(msg);

		ipc_message_t * whoinfo = msg_wholist();
		msg_attach_to_all(whoinfo);
		ipcmsg_destroy(whoinfo);

		ipc_message_t *gaginfo = msg_gaglist();
		msg_attach(gaginfo, conn);
		ipcmsg_destroy(gaginfo);
		return;
	}

	/* message types after this point are only valid if your logged in */
	if (conn->state != IPCSTATE_VALID) return;

	/* ask the server how long it has been up */
	if (msg->head.type == IPC_UPTIME) {
		time_t now = time(0);

		ipcmsg_destroy(msg);

		msg = ipcmsg_create(IPC_UPTIME, 0);
		ipcmsg_destination(msg, conn->addr);

		json_t * j = json_init(NULL);
		json_addint(j, "uptime", now - uptime);
		json_addstring(j, "version", VERSION);
		ipcmsg_json_encode(&msg, j);
		msg_attach(msg, conn);
		ipcmsg_destroy(msg);
		json_decref(j);
		return;
	}

	if (msg->head.type == IPC_REPLAY) {
		replay(conn, msg);
		ipcmsg_destroy(msg);
		return;
	}

	if (msg->head.type == IPC_ACTION) {
		accept_action(conn, msg);
		ipcmsg_destroy(msg);
		return;
	}

	if (msg->head.type == IPC_WHOLIST) {
		ipc_message_t * whoinfo = msg_wholist();
		msg_attach(whoinfo, conn);
		ipcmsg_destroy(whoinfo);
		ipcmsg_destroy(msg);
		return;
	}

	/**** end of messages to the server
	 *    all below this point are intended to be retransmitted
	 */
	assign_serial(msg);

	/* load sender */
	struct user from;
	fetch_user(&from, conn->user);

	/* send message to everyone in the room */
	if (msg->head.type == IPC_SAYTOROOM) {
		msg_apply_gag(&from, msg, "text");
		json_t * j = json_init(msg);
		const char * exclude = json_getstring(j, "exclude");
		msg_attach_to_channel(msg, msg->head.dst, exclude);
		json_decref(j);
	} else if (msg->head.type == IPC_SAYTOUSER) {
		/* message is for a specific username */
		msg_apply_gag(&from, msg, "text");
		/* eventually this should be a server maintained list */
		json_t * j = json_init(msg);
		const char * target = json_getstring(j, "target");
		if (msg_attach_to_username(msg, target) < 1) {
			send_error(conn, msg, "User '%s' not found", target);
		}
		json_decref(j);
	} else if (msg->head.type == IPC_SAYTOALL) {
		/* send message to everyone (unless this room is soundproof) */
		msg_apply_gag(&from, msg, "text");
		/* load the senders room to see if its soundproof */
		struct room room;
		RoomInit(&room);
		LoadRoom(&room, from.record.room);

		printf("Shout from '%s' in %d (%s)\n", from.record.name, room.num, room.name);

		if (room.sproof > 0) {
			/* nobble the stored message to act correctly */
			/* shout in sproof room is just a say to that room */
			msg->head.type = IPC_SAYTOROOM;
			msg->head.dst = from.record.room;
			msg_attach_to_channel(msg, room.num, NULL);
			printf("Shout in soundproof room (%d) change to SAYTOROOM %d\n",
			       room.sproof, from.record.room);
		} else {
			msg_attach_to_all(msg);
		}
		RoomDestroy(&room);
	} else if (msg->head.type == IPC_EVENT) {
		printf("Event message\n");
		msg_attach_to_all(msg);
	} else if (msg->head.type == IPC_WIZ) {
		printf("Wiz message\n");
		msg_attach_to_perm(msg, PERM_WIZCHAT);
	} else if (msg->head.dst == SYSTEM_USER) {
		/* it was a broadcast message */
		printf("Broadcast message to system user %s\n", ipc_nametype(msg->head.type));
		msg_attach_to_all(msg);
	} else {
		/* otherwise redistribute this message to intended target */
		printf("Unhandled direct-to-user command %s\n", ipc_nametype(msg->head.type));
		msg_attach_to_userid(msg, msg->head.dst);
	}
	store_message(msg);
	ipcmsg_destroy(msg);
}


/* send copy to every user in given channel,
 * except username==exclude (notsayto command) */
void msg_attach_to_channel(ipc_message_t *msg, int channel, const char * exclude)
{
	int users_fd = userdb_open(O_RDONLY);
	struct user user;
	struct person *urec = &user.record;
	struct room room;

	if (users_fd < 0)
		return;

	RoomInit(&room);
	LoadRoom(&room, channel);

	struct list_head *pos;
	list_for_each(pos, &connection_list) {
		ipc_connection_t *c = list_entry(pos, ipc_connection_t, list);
		if (c->state != IPCSTATE_VALID) continue;

		if (pread(users_fd, urec, sizeof(*urec), c->user) <= 0) continue;

		/* have we been told to exclude someone in this room */
		if (exclude!=NULL && strcasecmp(exclude, urec->name)==0) continue;

		/* person not on talker, just skip them */
		if (!cm_test(&user, CM_ONCHAT)) continue;

		/* room matches, send them a copy */
		if (urec->room == msg->head.dst) {
			msg_attach(msg, c);
		} else
		/* room not soundproof, and user has global on */
		if (room.sproof < 1 && cm_test(&user, CM_GLOBAL)) {
			msg_attach(msg, c);
		}
	}
	RoomDestroy(&room);
	close(users_fd);
}

/* send copy to all sessions belonging to username */
int msg_attach_to_username(ipc_message_t *msg, const char * username)
{
	int users_fd = userdb_open(O_RDONLY);
	struct user user;
	struct person *urec = &user.record;
	int found = 0;
	struct list_head *pos;

	if (users_fd < 0)
		return 0;

	list_for_each(pos, &connection_list) {
		ipc_connection_t *c = list_entry(pos, ipc_connection_t, list);
		if (c->state != IPCSTATE_VALID) continue;

		if (pread(users_fd, urec, sizeof(*urec), c->user) <= 0) continue;

		/* is this the username we are looking for */
		if (strcasecmp(username, urec->name)==0) {
			msg_attach(msg, c);
			found++;
		}
	}

	close(users_fd);
	return found;
}

/* send copy of message to every session */
void msg_attach_to_all(ipc_message_t *msg)
{
	int users_fd = userdb_open(O_RDONLY);
	struct user user;
	struct person *urec = &user.record;
	struct list_head *pos;

	if (users_fd < 0)
		return;

	list_for_each(pos, &connection_list) {
		ipc_connection_t *c = list_entry(pos, ipc_connection_t, list);
		if (c->state != IPCSTATE_VALID) continue;

		if (pread(users_fd, urec, sizeof(*urec), c->user) <= 0) continue;

		/* person not on talker, just skip them */
		if (!cm_test(&user, CM_ONCHAT) && msg->head.type != IPC_WHOLIST)
			continue;

		msg_attach(msg, c);
		printf("Broadcast to %s in %d\n", urec->name, urec->room);
	}
	close(users_fd);
}

/* send msg to all sessions of user idetified by userid/posn */
int msg_attach_to_userid(ipc_message_t *msg, uint32_t userposn)
{
	int found = 0;
	struct list_head *pos;
	list_for_each(pos, &connection_list) {
		ipc_connection_t *c = list_entry(pos, ipc_connection_t, list);
		if (c->state == IPCSTATE_VALID) {
			if (userposn == SYSTEM_USER) {
				/* broadcast */
				msg_attach(msg, c);
				found++;
			} else
			if (userposn == c->user) {
				/* unicast */
				msg_attach(msg, c);
				found++;
			}
		}
	}
	return found;
}

void msg_attach_to_perm(ipc_message_t *msg, perm_t perm)
{
	int users_fd = userdb_open(O_RDONLY);
	struct user user;
	struct person *urec = &user.record;
	struct list_head *pos;

	if (users_fd < 0)
		return;

	list_for_each(pos, &connection_list) {
		ipc_connection_t *c = list_entry(pos, ipc_connection_t, list);
		if (c->state != IPCSTATE_VALID) continue;

		if (pread(users_fd, urec, sizeof(*urec), c->user) <= 0) continue;

		if (perm == PERM_WIZCHAT) {
			if (!s_wizchat(&user)) continue;
		} else
		if (perm == PERM_CHANGEINFO) {
			if (!s_changeinfo(&user)) continue;
		}

		/* person not on talker, just skip them */
		if (!cm_test(&user, CM_ONCHAT)) continue;

		msg_attach(msg, c);
		printf("Restricted Broadcast to %s in %d\n", urec->name, urec->room);
	}
	close(users_fd);
}

/* attach messgae to outgoing queue */
void msg_attach(ipc_message_t *msg, ipc_connection_t *conn)
{
	if (conn == NULL) return;
	/* add to end of tx queue */

	int wasempty = msg_queue(msg, conn);

	/* nothing was queueed, switch on write notifications */
	if (wasempty)
		poll_with_writes(conn->fd, conn);
}

/* test if the user is gagged and apply the filter
 * from - who sent it (to see if they are gagged)
 * msg - the message to filter
 * field - name of json field containing text to filter
 **/
void msg_apply_gag(struct user *from, ipc_message_t * msg, const char * field)
{
	_autofree char *newtext = NULL;
	json_t * j = json_init(msg);
	const char * text = json_getstring(j, field);

	if (text) newtext = apply_gag(from, text);
	if (newtext == NULL) {
		json_decref(j);
		return;
	}
	json_addstring(j, field, newtext);
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
}

static char *has_suffix(char *str, const char *suffix)
{
	size_t len = strlen(suffix);

	while((str = strstr(str, suffix)) != NULL)
		if (*(str + len) == '\0')
			return str;
		else
			str++;
	return NULL;
}

static void migrate_old_folders(void)
{
	int dfd;
	DIR *dirp;
	unsigned i;
	struct dirent *entry;
	const char *bbdirs[] = {
		FOL_IDX_DIR,
		FOL_IDX_DIR"/"FOL_NEW,
		FOL_IDX_DIR"/"FOL_MOD,
		FOL_IDX_DIR"/"FOL_TMP,
		FOL_TXT_DIR,
		FOL_TXT_DIR"/"FOL_NEW,
		FOL_TXT_DIR"/"FOL_MOD,
		FOL_TXT_DIR"/"FOL_TMP,
	};
	/* Set up required directory structure */
	for (i = 0; i < (sizeof(bbdirs) / sizeof(bbdirs[0])); i++) {
		int ret = mkdir(bbdirs[i], S_IRWXU|S_IXGRP|S_IXOTH);
		if (ret == -1 && errno != EEXIST) {
			fprintf(stderr, "Failed to create directory '%s': %s\n",
			        bbdirs[i], strerror(errno));
			return;
		}
	}
	dirp = opendir(STATEDIR);
	if (dirp == NULL) {
		perror(STATEDIR);
		return;
	}
	dfd = dirfd(dirp);
	while ((entry = readdir(dirp)) != NULL) {
		char *end, *path = NULL;

		if ((end = has_suffix(entry->d_name, ".t.t"))) {
			*end = '\0';
			asprintf(&path, FOL_TXT_DIR"/"FOL_NEW"/%s", entry->d_name);
			*end = '.';
			printf("Migrating folder '%s' to '%s'\n", entry->d_name, path);
			renameat(dfd, entry->d_name, dfd, path);
		}
		else if ((end = has_suffix(entry->d_name, ".i.t"))) {
			*end = '\0';
			asprintf(&path, FOL_IDX_DIR"/"FOL_NEW"/%s", entry->d_name);
			*end = '.';
			printf("Migrating folder '%s' to '%s'\n", entry->d_name, path);
			renameat(dfd, entry->d_name, dfd, path);
		}
		else if ((end = has_suffix(entry->d_name, ".i.m"))) {
			*end = '\0';
			asprintf(&path, FOL_IDX_DIR"/"FOL_MOD"/%s", entry->d_name);
			*end = '.';
			printf("Migrating folder '%s' to '%s'\n", entry->d_name, path);
			renameat(dfd, entry->d_name, dfd, path);
		}
		else if ((end = has_suffix(entry->d_name, ".t.m"))) {
			*end = '\0';
			asprintf(&path, FOL_TXT_DIR"/"FOL_MOD"/%s", entry->d_name);
			*end = '.';
			printf("Migrating folder '%s' to '%s'\n", entry->d_name, path);
			renameat(dfd, entry->d_name, dfd, path);
		}
		else if ((end = has_suffix(entry->d_name, ".i"))) {
			*end = '\0';
			asprintf(&path, FOL_IDX_DIR"/%s", entry->d_name);
			*end = '.';
			printf("Migrating folder '%s' to '%s'\n", entry->d_name, path);
			renameat(dfd, entry->d_name, dfd, path);
		}
		else if ((end = has_suffix(entry->d_name, ".t"))) {
			*end = '\0';
			asprintf(&path, FOL_TXT_DIR"/%s", entry->d_name);
			*end = '.';
			printf("Migrating folder '%s' to '%s'\n", entry->d_name, path);
			renameat(dfd, entry->d_name, dfd, path);
		}
		if (path != NULL)
			free(path);
	}
}

int init_server(void)
{
	INIT_LIST_HEAD(&connection_list);
	poll_init();
	return 0;
}

int init_server_after_setuid(void)
{
	migrate_old_folders();
	if (replay_init() != 0)
		return 1;
	return 0;
}

ipc_message_t * msg_wholist(void)
{
	int users_fd = userdb_open(O_RDONLY);
	struct user user;
	struct person *urec = &user.record;

	if (users_fd < 0)
		return NULL;

	ipc_message_t * msg = ipcmsg_create(IPC_WHOLIST, SYSTEM_USER);
	json_t * list = json_array();

	struct list_head *pos;
	list_for_each(pos, &connection_list) {
		ipc_connection_t *c = list_entry(pos, ipc_connection_t, list);
		if (c->state != IPCSTATE_VALID) continue;

		if (pread(users_fd, urec, sizeof(*urec), c->user) <= 0) continue;

		json_t * j = json_init(NULL);
		json_addint(j, "pid", c->addr);
		json_addint(j, "id", c->user);
		json_addstring(j, "name", urec->name);
		json_addstring(j, "realname", urec->realname);
		json_addstring(j, "doing", urec->doing);
		json_addint(j, "dowhen", urec->dowhen);
		json_addint(j, "channel", urec->room);
		json_addint(j, "idletime", urec->idletime);

		json_t * p = json_init(NULL);
		char info[80];
		show_user_stats(urec->status,info,true);
		json_addstring(p, "status", info);
		if (s_wizchat(&user) && !s_chatoff(&user))
			json_addint(p, "wizchat", 1);
		json_addstring(p, "chatmode", display_cmflags(urec->chatmode));
		json_addstring(p, "chatprivs", display_cpflags(urec->chatprivs));
		show_protection(urec->chatmode, urec->chatprivs, info, 10);
		json_addstring(p, "protection", info);
		json_addobject(j, "perms", p);
		json_decref(p);

		json_array_append_new(list, j);
	}
	ipcmsg_json_encode(&msg, list);
	json_decref(list);
	close(users_fd);
	return msg;
}

/* whats the uid/userposn for this username
 * searchs only online users
 */
int32_t who_find(const char * username)
{
	int users_fd = userdb_open(O_RDONLY);
	struct person user;
	struct list_head *pos;

	if (users_fd < 0)
		return -1;

	list_for_each(pos, &connection_list) {
		ipc_connection_t *c = list_entry(pos, ipc_connection_t, list);
		if (c->state != IPCSTATE_VALID) continue;

		lseek(users_fd, c->user, SEEK_SET);
		if (read(users_fd, &user, sizeof(user)) <= 0) continue;
		/* is this the username we are looking for */
		if (strcasecmp(username, user.name)==0) {
			close(users_fd);
			return c->user;
		}
	}

	close(users_fd);
	return -1;
}

ipc_message_t * msg_error(const char *type)
{
	ipc_message_t * msg = ipcmsg_create(IPC_ERROR, SYSTEM_USER);

	json_t * j = json_init(NULL);
	json_addstring(j, "error", type);
	ipcmsg_json_encode(&msg, j);
	json_decref(j);
	return msg;
}
