<?

require_once("config.php");

$data = unserialize(@$_REQUEST['mwsess']);
$sess = (int)$data['pid'];
$auth = trim($data['auth']);

$path = $poller_path.$sess;

header("Content-type: application/json; charset=utf-8");

$sock = socket_create(AF_UNIX, SOCK_SEQPACKET, 0);
if (@socket_connect($sock, $path) === FALSE) {
  echo "{\"status\":\"Socket open error\"}\n";
  exit;
}

$msg = "auth $auth";
if (@socket_send($sock, $msg, strlen($msg), 0)===FALSE) {
  echo "{\"status\":\"Socket error\"}\n";
  exit;
}

if (@socket_recv($sock, $response, 4096, 0)===FALSE) {
  echo "{\"status\":\"Socket error\"}\n";
  exit;
}
$res = json_decode($response, TRUE);
if ($res === NULL) { echo "{\"status\":\"Bad server response\"}\n"; exit; }

if ($res['status'] != "OK") {
  echo $response;
  exit;
}

$msg = "fetch";

if (@socket_send($sock, $msg, strlen($msg), MSG_EOF)===FALSE) {
  echo "{\"status\":\"Socket error\"}\n";
  exit;
}

if (@socket_recv($sock, $response, 262144, 0)===FALSE) {
  echo "{\"status\":\"Socket error\"}\n";
  exit;
}

if ($response!="") echo $response."\n";
else echo "[]\n";

socket_close($sock);
?>
