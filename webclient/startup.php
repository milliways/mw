<?
require_once("config.php");

$mwpoll = $poller_bin;
$username = trim($_REQUEST['username']);
$password = trim($_REQUEST['password']);

// Preliminary checks

// Have we been passed SUCSsite cookies?
// If not, test that the login details make sense.
if (@$_REQUEST['sucssite_loggedin']!="true") {
  if (empty($username) || empty($password)) {
    echo json_encode((object) ['error' => 'username and password must both be not empty']);
    exit;
  }

  if (ctype_alnum($username) === FALSE) {
    echo json_encode((object) ['error' => 'alphanumeric usernames only']);
    exit;
  }
}

// Have we been asked to create a new Milliways account?
// If so, make sure that the password supplied is suitable.
if ($action=="create") {
  $pass1 = @$_REQUEST['password1'];
  $pass2 = @$_REQUEST['password2'];
  
  if ($pass1 != $pass2) {
    echo json_encode((object) ['error' => 'Passwords don\'t match - please try again']);
    exit;	
  }

  if (empty($pass1)) {
    echo json_encode((object) ['error' => 'Password cannot be blank']);
    exit;	
  }
	
  if (ctype_alnum($pass1) === FALSE) {
    echo json_encode((object) ['error' => 'Milliways passwords can only contain alphanumeric characters']);
    exit;	
  }

  if (strlen($pass1) < 6) {
    echo json_encode((object) ['error' => 'Milliways passwords must be 6 characters or more']);
    exit;	
  }
}

$desc = array( 
  0 => array("pipe", "r"),
  1 => array("pipe", "w"),
);
$pipes = array();

// Have we got SUCSsite cookies?
// And no login attempt?
// If so, fetch the user details and try to log in without a password.
if (@$_REQUEST['sucssite_loggedin']=="true" && empty($username)) {

  // Do we appear to be in the SUCSsite environment?
  // There's only any point trying if we do.
  if (file_exists("../settings.php")) {
    // Do the bare minimum of SUCSsite init to retrieve the username for the session we've been passed
    include("../settings.php");
    // Initialise the database
    require("/usr/share/php/adodb/adodb.inc.php");
    $DB = NewADOConnection('postgres9');
    $DB->Connect('dbname='.$dbname.' user=apache');
    $DB->SetFetchMode(ADODB_FETCH_ASSOC);
	
    // Include the session library
    require($base."lib/session.php");
    $session = new Session;
    // $smarty->assign("session", $session); // This line might be useful?
    $realuser = $session->username;

    if ($action == "create") {
      $p = proc_open($mwpoll." -q -a -u ".$realuser." -s", $desc, $pipes);
    } else {
      $p = proc_open($mwpoll." -q -u ".$realuser." -s", $desc, $pipes);
    }
    $mode = "sucssite";
  }
} else {
  // If not, try logging in with a password.
  $p = proc_open($mwpoll." -q -u $username", $desc, $pipes);
  $mode = "password";
}

if (empty($p)) {
  echo "<h1>No</h1>";	
  exit;
}

if ($p === FALSE) {
  echo json_encode((object) ['error' => 'failed to exec mwpoll']);
  exit;
}

if ($mode == "sucssite" && @$_REQUEST['sucssite_loggedin']=="true" && empty($username)) {
  // If we have a sucssite session cookie, use that
  fwrite($pipes[0], trim($_REQUEST['sucssite_session'])."\n");
  if ($action="create") {
    fwrite($pipes[0], $pass1."\n");
  }
} else {
  // Try logging on using username and password
  if (fwrite($pipes[0], $password."\n") === FALSE) {
    echo "Error writing to mwpoll\n";
  }
}

$pid = fgets($pipes[1]);
if ($pid === FALSE) {
  echo "error reading pid.\n";
}

$pid = trim($pid);
if (!is_numeric($pid)) {
  if (substr($pid, -10) =="not found.") {
    // User doesn't exist - ask for a Milliways password so we can create them!
    echo json_encode((object) ['error' => $pid]);
    exit;	
  }

  echo json_encode((object) ['error' => 'Bad response: pid=$pid']);
  exit;
}

$auth = fgets($pipes[1]);

if ($auth === FALSE) {
  echo "Error reading auth string\n";
}

$sess = array (
  "pid" => $pid,
  "auth" => $auth,
  "username" => $username
);

$mwsess = serialize($sess);
echo "success:" . $mwsess;
?>
