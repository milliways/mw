<?
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');

// Force the user to use HTTPS
if (empty($_SERVER['HTTPS'])) header("Location: https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);

$_REQUEST = json_decode(file_get_contents('php://input'), true);

// Get the users MW session if it exists
$mwsess = @$_COOKIE['mwsess'];
// Get the action HTTP request variable
$action = $_REQUEST['action'];

// Check to see if login actions need to be performed
if ($action == "login" || $action == "create") {
  require("startup.php");
}

// Check if the user has a MW session
if (!isset($mwsess)) {

  // If there is no session but the user is logged in then run startup
  if ($_REQUEST['sucssite_loggedin']=="true" && $action!="logout") {
    require("startup.php");
  } else {
    echo 'Post requests only to this url';
    exit;
  }
}

$session = unserialize($mwsess);
?>
