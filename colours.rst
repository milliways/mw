Milliways Colour reference
==========================

Description
===========

This document describes the text colouration options provided by Milliways
client. The legacy system refers to ANSI-16 colouring, and the updated
one to ANSI-256 support.

Legacy
======
The existing colour scheme only permitted access to the 16 colour ANSI 
system and is a little awkward to use.

Each colour sequence is preceded by the Escape character, typically
entered by pressing control-V followed by the ESC key, this is followed
by two letters, the first setting the foreground colour, the second
setting the background colour, lowercase letters being the normal brightness
version, and uppercase being the brightly/intense coloured version.

* -n : Reset / Default Colour
* kK : Black
* rR : Red
* gG : Green
* bB : Blue
* cC : Cyan
* mM : Magenta
* yY : Yellow
* wW : white

ANSI 256
========

For the updated colour scheme sequences are now delimited by the ampersand &
character and consist of a number of hex digits seperated by colons.

Text modes are a single hex character, colours are pairs of hex characters,
     the first colour pair being the foreground colour, the optional second
     setting the background colour.

Text modes:

* 0 - Reset / normal
* 1 - Bold / increased intensity
* 2 - Faint / decreaed intensity
* 3 - Italic
* 4 - Underline
* 7 - reverse video
* 9 - crossed out

Examples:  

* &0& - reset all attributes
* &36& - set foreground colour to purple
* &11:E3& - dark blue text on yellow background

For a full colour chart, run ~alice/ansi256.sh
