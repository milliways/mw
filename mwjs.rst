==================================
Milliways JavaScript API reference
==================================

Description
===========

This document describes the JavaScript scripting API provided by the Milliways
client, ``mw``. The standard interfaces specified by the ECMAscript standards
are also available in ``mwjs`` scripts but are not documented here.

Note that everything that isn't defined under the ``mw`` namespace is
considered deprecated.

Loading Scripts
===============

``mwjs`` scripts can be loaded by adding::

    include path/to/file.js

to your ``~/.mwrc``. The path is relative to your home directory.

The `mw` Namespace
==================

The following symbols are defined in the `mw` namespace for ``mwjs`` scripts:

Functions
---------

`mw.print(args...)`
  Takes a variable number of arguments. Coerces them to strings, concatenates
  them and displays them locally.

`mw.exec(str)`
  Takes a string argument and executes it as a talker command. Preceeding '.' not needed.

`mw.say(str)`
  Takes an argument which it sends as a message in the talker.

`mw.wholist()`
  Accepts no arguments. Returns an array of objects representing the users
  currently on the talker. The objects contain the following properties:

  - username
  - room
  - idle
  - chatmodes
  - protection_level
  - protection_power
  - chatprivs
  - doing
  - since

  Note that the wholist is currently not guaranteed to be available on joining
  the talker and so the value returned from `wholist()` could be `undefined`.

`mw.urlget(url)`
  Takes a URL string, fetches it, and returns the contents of the response.

`mw.beep(n)`
  Beeps (sends the BEL character to stdout) `n` times (max 5).

`mw.input(prompt)`
  Displays the string `prompt` and accepts a line of input from the user, which
  is returned.

`mw.termsize()`
  Accepts no arguments. Returns an object representing the dimensions of the
  terminal window that ``mw`` is occupying. The object's properties are:

  - width
  - height

`mw.whoami()`
   Accepts no arguments. Returns the user's current username.

Special Objects
---------------

`mw.onevent`
   This is an array which event handler functions can be added to using
   ``mw.onevent.push(handler)``. The handler must accept an event object with a
   `data` property containing the properties relating to the particular event
   type. Event handlers can suppress the default output for events by returning
   ``false``.

`mw.command`
   This object allows user commands to be defined. To define the command
   ``,boop`` with handler function ``boopCmd()`` use ``mw.command.boop =
   boopCmd;``

`mw.store`
   This object allows easy persistent storage and lookup of basic key-value
   pairs in a database. To store value "bar" with key "foo", either use
   ``mw.store.foo = "bar";`` or ``mw.store["foo"] = "bar";``

Deprecated Interfaces
=====================

The interfaces described in this section are deprecated. They were direct
translations of the old mwscript API and do not support idiomatic javascript.
In all cases there should be an easier-to-use equivalent interface in the new
API documented above.

Constants
---------

`whoami`

  A string containing the name of the current ``Milliways`` user. This doesn't
  change even if the user's username changes. Use `mw.whoami()` instead.

Bind Types
----------

The following constants can be used with the `bind` and `unbind` functions (see below).

`K_BIND_EVENT`

  ``bind(K_BIND_EVENT, handler)`` hooks a user-defined function named in the
  string `handler` to be called when certain events occur.

`K_BIND_ONOFF`

  ``bind(K_BIND_ONOFF, )`` - To be documented.

`K_BIND_FORCE`

  ``bind(K_BIND_FORCE, )`` - To be documented.

`K_BIND_SHUTDOWN`

  ``bind(K_BIND_SHUTDOWN, )`` - To be documented.

`K_BIND_ALIAS`

  ``bind(K_BIND_ALIAS, )`` - To be documented.

`K_BIND_INPUT`

  ``bind(K_BIND_INPUT, )`` - To be documented.

Functions
---------

The following functions are defined in the global scope of ``MWJS`` scripts.

`print(str, ...)`

  Takes a variable number of arguments. Coerces them to strings, concatenates
  them and displays them (locally). Use `mw.print()` instead.

`exec(str)`

  Takes a string argument and executes it as a talker command. Use `mw.exec()`
  instead.

`say(str)`

  Takes an argument which it sends as a message in the talker. Use `mw.say()`
  instead.

`wholist()`

  Accepts no arguments. Returns an array of objects representing the users
  currently on the talker. Use `mw.wholist()` instead.

`urlget(url)`

  Takes a URL string, fetches it, and returns the contents of the response. Use
  `mw.urlget()` instead.

`beep(n)`

  Beeps (sends the BEL character to stdout) `n` times (max 5). Use `mw.beep()`
  instead.

`input(prompt)`

  Displays the string `prompt` and accepts a line of input from the user, which
  is returned. Use `mw.input()` instead.

`termsize()`

  Accepts no arguments. Returns an object representing the dimensions of the
  terminal window that ``mw`` is occupying. Use `mw.termsize()` instead.

`bind()`

  Use `mw.command` and `mw.onevent` instead.

`unbind()`

  Use `delete mw.command.<name>` instead.
