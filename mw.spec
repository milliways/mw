%{expand: %%global changeset %(cat mw.rev)}
Summary: Milliways III talker and BBS
Name: mw3
Version: 2.17.%{changeset}
Release: 1
License: GPLv2
Group: Applications/Communications
Source: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-root
Requires: readline sqlite curl
#Requires(post): systemd-units
#Requires(preun): systemd-units
#Requires(postun): systemd-units
BuildRequires: libtermcap-devel readline-devel glibc-devel sqlite-devel curl-devel openssl-devel jansson-devel postgresql-devel
URL: http://projects.sucs.org/projects/mw/

%description
An integrated talker and BBS (bulletin board system) with a peer-to-peer
architecture, intended to be used over telnet or similar.

%prep
%setup -q

%build
make libdir="%{_libdir}" bindir="%{_bindir}" RELEASE_BUILD=1

%install
make DESTDIR=$RPM_BUILD_ROOT prefix=/usr libdir="%{_libdir}" install
# We have a systemd unit file for mwserv so remove the sysv initscript
rm -rf $RPM_BUILD_ROOT/etc/init.d
#install -Dm 0644 mw.service $RPM_BUILD_ROOT/%{_unitdir}/mwserv
install -Dm 0644 mwserv.init $RPM_BUILD_ROOT/%{_initrddir}/mwserv
install -D mozjs/%{_libdir}/libmozjs-17.0.so $RPM_BUILD_ROOT/%{_libdir}/
mv $RPM_BUILD_ROOT%{_libdir}/mw/mwserv $RPM_BUILD_ROOT%{_bindir}
%find_lang mw

%clean
rm -fr $RPM_BUILD_ROOT

%pre
# Add the user "mw"
/usr/sbin/useradd -c "mw system" \
    -s /sbin/nologin -r -d %{_libdir}/mw mw 2> /dev/null || :

%post
if [ $1 -eq 1 ] ; then 
    # Initial installation (disabled by default)
    # To enable after installation, do systemctl enable mw.service
#/bin/systemctl daemon-reload >/dev/null 2>&1 || :
    /sbin/chkconfig mwserv on
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
#    /bin/systemctl --no-reload disable mw.service > /dev/null 2>&1 || :
#    /bin/systemctl stop mw.service > /dev/null 2>&1 || :
    /sbin/service mwserv stop
    /sbin/chkconfig mwserv off
fi

%postun
#/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    #/bin/systemctl try-restart mw.service >/dev/null 2>&1 || :
fi

%files -f mw.lang
%defattr (755,mw,mw)
%dir %{_libdir}/mw
%{_libdir}/mw/colour
%{_libdir}/libmozjs-17.0.so
%attr(755,mw,mw) %dir /var/log/mw
%attr(711,mw,mw) %dir /var/lib/mw
%attr(755,mw,mw) %dir /var/run/mw
%config %attr(640,mw,mw) %{_libdir}/mw/login.banner
%{_libdir}/mw/help
%{_libdir}/mw/scripthelp
%{_libdir}/mw/talkhelp
%{_libdir}/mw/wizhelp
%{_initrddir}/mwserv
#%{_unitdir}/mw.service
%attr (4711,mw,mw) %{_bindir}/mw
%attr (755,mw,mw) %{_bindir}/mwserv
%attr (4711,mw,mw) %{_libdir}/mw/mwpoll
%doc %{_libdir}/mw/COPYING
%doc %{_libdir}/mw/INSTALL
%doc %{_libdir}/mw/LICENSE
%doc %{_libdir}/mw/README

%changelog
* Wed Dec 12 2012 Thomas Lake <tswsl1989@sucs.org>
- Add build dependency for postgresql-devel

* Sat Dec 08 2012 Andrew Price <welshbyte@sucs.org>
- Bump to 2.17.* and add mwserv-related files

* Sat Apr 12 2008 Andrew Price <welshbyte@sucs.org>
- release 2.16.1033 including file layout changes

* Wed Mar 19 2008 Graham Cole <chckens@sucs.org>
- release 2.15.1011 hopefully killing off Alarm clock crashes

* Fri Jun 15 2007 Justin Mitchell <arthur@sucs.org>
- release 2.15.1 with psycodoms javascript and other bugfixes

* Sat Jul 29 2006 Peter Berry <pwb@sucs.org>
- Release 2: new iconv code and fixed some x86-64 bugs

* Sun Jun 4 2006 Peter Berry <pwb@sucs.org>
- First release of 2.14.3.
- Lots of fixes, in particular .notsayto now makes sure the user is on.

* Sat Apr 29 2006 Peter Berry <pwb@sucs.org>
- Release 2: Moved -devel deps to BuildRequires

* Thu Mar 23 2006 Peter Berry <pwb@sucs.org>
- First release of 2.14.2 preparing for x86-64 build
- Fixed: credits, jive gag
- Added: saya gag, lots of mud rooms from old "make install" in /usr/local
- Changed: gag tables moved into a new source file, portability changes, use gnu99

* Tue Nov 29 2005 Peter Berry <pwb@sucs.org>
- Release 3: Fixed bugs relating to out of range board messages

* Thu Oct 27 2005 Peter Berry <pwb@sucs.org>
- Release 2: fixed permissions on the colour directory

* Thu Oct 27 2005 Peter Berry <pwb@sucs.org>
- Initial release of version 2.14.1 with !locale and !charset commands

* Thu Sep 22 2005 Peter Berry <pwb@sucs.org>
- Fixed export of _bindir to make in build

* Wed Jan 19 2005 Peter Berry <pwb@sucs.org>
- Initial RPM release.
